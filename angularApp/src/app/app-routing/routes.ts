import { AuthGuard } from '../shared/interceptors/auth.guard';
import { CompletePaymentComponent } from '../reservations/complete-payment/complete-payment.component';
import { LoginComponent } from '../home/login/login.component';
import { AddSpaceComponent } from '../space/add-space/add-space.component';

import { Routes } from '@angular/router';
import {AddLocationComponent} from '../location/add-location/add-location.component';
import { EventComponent } from '../events/event/event.component';
import { HomePageComponent } from '../home/home-page/home-page.component';
import {LocationViewComponent} from '../location/location-view/location-view.component';
import { RegisterUserComponent } from '../home/register-user/register-user.component';
import { AddEventComponent } from '../events/add-event/add-event.component';
import {ReservationComponent} from '../reservations/reservation/reservation.component';
import {UnauthenticatedComponent} from '../core/unauthenticated/unauthenticated.component';
import {AdminGuard} from '../shared/interceptors/admin.guard';
import {VisitorGuard} from '../shared/interceptors/visitor.guard';
import {SearchComponent} from '../events/search/search.component';
import {EventImageUploadComponent} from '../events/event-image-upload/event-image-upload.component';
import {UpdateLocationComponent} from '../location/update-location/update-location.component';
import { PayFormComponent } from '../reservations/pay-form/pay-form.component';
import {LocationListComponent} from '../location/location-list/location-list.component';
import {UpdateSpaceComponent} from '../space/update-space/update-space.component';
import {TicketListComponent} from '../tickets/ticket-list/ticket-list.component';
import { CompleteSinglePaymentComponent } from '../reservations/complete-single-payment/complete-single-payment.component';
import {ChartViewComponent} from '../core/chart-view/chart-view.component';

export const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'unauthenticated', component: UnauthenticatedComponent},
  { path: 'event/:id', component: EventComponent},
  { path: 'location', component: LocationViewComponent},
  { path: 'register', component: RegisterUserComponent, canActivate: [VisitorGuard]},
  { path: 'login', component: LoginComponent, canActivate: [VisitorGuard]},
  { path: 'reservation/:eventId', component: ReservationComponent, canActivate: [AuthGuard]},
  { path: 'pay', component: PayFormComponent, canActivate: [AuthGuard]},
  { path: 'addLocation', component: AddLocationComponent, canActivate: [AdminGuard]},
  { path: 'addSpace', component: AddSpaceComponent, canActivate: [AdminGuard]},
  { path: 'addEvent', component: AddEventComponent, canActivate: [AdminGuard]},
  { path: 'search', component: SearchComponent},
  { path: 'eventImage/:id', component: EventImageUploadComponent, canActivate: [AdminGuard]},
  { path: 'locations', component: LocationListComponent, canActivate: [AdminGuard]},
  { path: 'location/:id', component: LocationViewComponent, canActivate: [AdminGuard]},
  { path: 'updateLocation/:id', component: UpdateLocationComponent, canActivate: [AdminGuard]},
  { path: 'updateSpace/:id', component: UpdateSpaceComponent, canActivate: [AdminGuard]},
  { path: 'tickets', component: TicketListComponent},
  { path: 'payment/complete', component: CompletePaymentComponent, canActivate: [AuthGuard]},
  { path: 'singlePayment/complete', component: CompleteSinglePaymentComponent, canActivate: [AuthGuard]},
  { path: 'chart/:reportType/:id/:startDate/:endDate', component: ChartViewComponent, canActivate: [AdminGuard]},
  { path: '**', redirectTo: '' },
];
