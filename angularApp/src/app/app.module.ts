import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { MaterialModule } from './material/material.module';

import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import 'hammerjs';

import { AngularOpenlayersModule } from 'ngx-openlayers';

import {AppRoutingModule} from './app-routing/app-routing.module';
import { SpaceLayoutGridComponent } from './space-layout/space-layout-grid/space-layout-grid.component';
import {MAT_DIALOG_DATA, MatDialogRef, MAT_SNACK_BAR_DATA} from '@angular/material';
import { JwtInterceptor } from './shared/interceptors/jwt.interceptor';
import {UnauthenticatedHandler} from './shared/interceptors/unauthenticated.interceptor';
import { ConfirmationDialogComponent } from './core/confirmation-dialog/confirmation-dialog.component';
import { EditEventComponent } from './events/edit-event/edit-event.component';
import { EventReportSelectionDialogComponent } from './events/event-report-selection-dialog/event-report-selection-dialog.component';
// tslint:disable-next-line:max-line-length
import { LocationReportSelectionDialogComponent } from './location/location-report-selection-dialog/location-report-selection-dialog.component';
import { DatePipe } from '@angular/common';
import { SnackbarComponent } from './core/snackbar/snackbar.component';
import {LocationModule} from './location/location.module';
import {SpaceModule} from './space/space.module';
import {SpaceLayoutModule} from './space-layout/space-layout.module';
import {TicketsModule} from './tickets/tickets.module';
import {EventsModule} from './events/events.module';
import {SharedModule} from './shared/shared.module';
import {HomeModule} from './home/home.module';
import {CoreModule} from './core/core.module';
import {ReservationsModule} from './reservations/reservations.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    NgbModule,
    AppRoutingModule,
    AngularOpenlayersModule,
    LocationModule,
    SpaceModule,
    SpaceLayoutModule,
    TicketsModule,
    EventsModule,
    SharedModule,
    HomeModule,
    CoreModule,
    ReservationsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: UnauthenticatedHandler, multi: true },
    { provide: MatDialogRef, useValue: {} },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MAT_DIALOG_DATA, useValue: [] },
    { provide: MAT_SNACK_BAR_DATA, useValue: {} },
    DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent, EventReportSelectionDialogComponent, LocationReportSelectionDialogComponent,
    EditEventComponent, SpaceLayoutGridComponent, SnackbarComponent]
})
export class AppModule { }
