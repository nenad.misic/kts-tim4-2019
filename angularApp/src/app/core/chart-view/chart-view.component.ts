import {Component, Input, OnInit} from '@angular/core';
import * as CanvasJS from '../../../assets/lib/canvasjs.min.js';
import {DataPoint} from '../../shared/model/data-point';
import {EventService} from '../../shared/services/event.service';
import {ActivatedRoute, Data} from '@angular/router';
import { DatePipe } from '@angular/common';
import { LocationService } from '../../shared/services/location.service';

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.css']
})
export class ChartViewComponent implements OnInit {

  dataPoints: Array<DataPoint> = [];
  chart: any;
  startDate: Date;
  endDate: Date;
  reportType = '';
  private months = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(private eventService: EventService, private route: ActivatedRoute,
     private datePipe: DatePipe, private locationService: LocationService) { }

  ngOnInit() {
    const id: number = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    const reportType: string = this.route.snapshot.paramMap.get('reportType');
    let chartData: Promise<any>;
    if (reportType.startsWith('event')) {
      this.reportType = 'event';
      if (reportType === 'event-earnings') {
        chartData = this.eventService.getEarnings(id);
      } else {
        chartData = this.eventService.getOccupancy(id);
      }
    } else {
      this.reportType = 'location';
      const startTicks = parseInt(this.route.snapshot.paramMap.get('startDate'), 10);
      this.startDate = new Date();
      this.startDate.setTime(startTicks);

      const endTicks = parseInt(this.route.snapshot.paramMap.get('endDate'), 10);
      this.endDate = new Date();
      this.endDate.setTime(endTicks);

      const startDateStr = this.datePipe.transform(this.startDate, 'dd.MM.yyyy');
      const endDateStr = this.datePipe.transform(this.endDate, 'dd.MM.yyyy');
      if (reportType === 'location-earnings') {
        chartData = this.locationService.getEarningsInPeriod(id, startDateStr, endDateStr);
      } else {
        chartData = this.locationService.getOccupancyInPeriod(id, startDateStr, endDateStr);
      }
    }

    chartData.then((value: any) => {
      this.chart = new CanvasJS.Chart('chartContainer', {
        animationEnabled: true,
        exportEnabled: true,
        title: {
          text: value.title
        },
        axisY: {
          title: value.unit
        },
        data: [{
          type: 'column',
          dataPoints: this.dataPoints
        }]
      });
      const arrayDataPoints = Object.keys(value.data).map(function(key) {
        return [key, value.data[key]];
      });
      const data = arrayDataPoints.map(entry => new DataPoint(entry[1], entry[0]));
      // sort the data by month/year
      if (this.reportType === 'event') {
        for (const dataPoint of data) {
          this.dataPoints.push(dataPoint);
        }
      } else {
        const dataFilled = this.fillMissingData(data);
        const dataSorted = this.sortByDate(dataFilled);
        for (const dataPoint of dataSorted) {
          this.dataPoints.push(dataPoint);
        }
      }
      this.chart.render();
    });
  }

  sortByDate(data: Array<DataPoint>): Array<DataPoint> {
    return data.sort((a: DataPoint, b: DataPoint) => {
      const labelA = a.label;
      const labelB = b.label;
      const monthYearA = labelA.split('/');
      const monthYearB = labelB.split('/');
      const monthA = monthYearA[0];
      const yearA = parseInt(monthYearA[1], 10);
      const monthB = monthYearB[0];
      const yearB = parseInt(monthYearB[1], 10);
      if (yearA !== yearB) {
        return yearA - yearB;
      }
      return this.months.indexOf(monthA) - this.months.indexOf(monthB);
    });
  }

  fillMissingData(data: Array<DataPoint>) {
    const startMonth = this.startDate.getMonth();
    const startYear = this.startDate.getFullYear();
    const endMonth = this.endDate.getMonth();
    const endYear = this.endDate.getFullYear();
    // in case start date is after end date do nothing
    if (startYear > endYear || (startYear === endYear && startMonth >= endMonth)) {
      return data;
    }
    let currentMonth = startMonth;
    let currentYear = startYear;
    console.log(data);
    while (!(currentMonth === endMonth && currentYear === endYear)) {
      currentYear = currentYear + Math.floor((currentMonth + 1) / 12);
      currentMonth = (currentMonth + 1) % 12;
      const generatedDate = this.months[currentMonth] + '/' + currentYear;
      console.log(generatedDate);
      if (data.every((dataPoint: DataPoint) => dataPoint.label !== generatedDate)) {
        data.push(new DataPoint(0, generatedDate));
      }
    }
    return data;
  }

}
