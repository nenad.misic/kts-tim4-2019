import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DummyUploadComponent} from './dummy-upload/dummy-upload.component';
import {UnauthenticatedComponent} from './unauthenticated/unauthenticated.component';
import {ChartViewComponent} from './chart-view/chart-view.component';
import {SnackbarComponent} from './snackbar/snackbar.component';
import {MapComponent} from './map/map.component';
import {MaterialModule} from '../material/material.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule
  ],
  declarations: [
    DummyUploadComponent,
    UnauthenticatedComponent,
    ChartViewComponent,
    SnackbarComponent,
    MapComponent,
  ],
  exports: [
    DummyUploadComponent,
    UnauthenticatedComponent,
    ChartViewComponent,
    SnackbarComponent,
    MapComponent,
  ]
})
export class CoreModule { }
