import { Component, OnInit } from '@angular/core';
import {DummyUploadService} from '../../shared/services/dummy-upload.service';

@Component({
  selector: 'app-dummy-upload',
  templateUrl: './dummy-upload.component.html',
  styleUrls: ['./dummy-upload.component.css']
})
export class DummyUploadComponent implements OnInit {

  fileToUpload: File = null;

  constructor(private fileUploadService: DummyUploadService) { }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  uploadFileToActivity() {
    this.fileUploadService.postFile('9942', this.fileToUpload).subscribe(data => {
      // do something, if upload success
      }, error => {
        console.log(error);
      });
  }

}
