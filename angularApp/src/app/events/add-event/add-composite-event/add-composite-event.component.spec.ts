import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCompositeEventComponent } from './add-composite-event.component';
import { AddEventService } from 'src/app/shared/services/add-event.service';
import { DebugElement } from '@angular/core';
import { HomePageComponent } from 'src/app/home/home-page/home-page.component';
import { DummyUploadComponent } from 'src/app/core/dummy-upload/dummy-upload.component';
import { EventComponent } from '../../event/event.component';
import { SingleEventComponent } from '../../event/single-event/single-event.component';
import { CompositeEventComponent } from '../../event/composite-event/composite-event.component';
import { NavbarComponent } from 'src/app/home/navbar/navbar.component';
import { AddLocationComponent } from 'src/app/location/add-location/add-location.component';
import { EventListItemComponent } from '../../event-list/event-list-item/event-list-item.component';
import { EventListComponent } from '../../event-list/event-list.component';
import { SpaceLayoutGridComponent } from 'src/app/space-layout/space-layout-grid/space-layout-grid.component';
import { SelectSeatComponent } from 'src/app/reservations/select-seat/select-seat.component';
import { LocationViewComponent } from 'src/app/location/location-view/location-view.component';
import { AddSpaceComponent } from 'src/app/space/add-space/add-space.component';
import { RegisterUserComponent } from 'src/app/home/register-user/register-user.component';
import { PasswordConfirmationValidatorDirective } from 'src/app/shared/directives/password-confirmation-validator.directive';
import { AddSingleEventComponent } from '../add-single-event/add-single-event.component';
import { EventTreeComponent } from '../event-tree/event-tree.component';
import { AddEventComponent } from '../add-event.component';
import { SpaceLayoutSectorsComponent } from 'src/app/space-layout/space-layout-sectors/space-layout-sectors.component';
import { PayFormComponent } from 'src/app/reservations/pay-form/pay-form.component';
import { AutoCompleteValidatorDirective } from 'src/app/shared/directives/auto-complete-validator.directive';
import { StartDateValidatorDirective } from 'src/app/shared/directives/start-date-validator.directive';
import { ReservationComponent } from 'src/app/reservations/reservation/reservation.component';
import { LoginComponent } from 'src/app/home/login/login.component';
import { UnauthenticatedComponent } from 'src/app/core/unauthenticated/unauthenticated.component';
import { EndDateValidatorDirective } from 'src/app/shared/directives/end-date-validator.directive';
import { SearchComponent } from '../../search/search.component';
import { EventImageUploadComponent } from '../../event-image-upload/event-image-upload.component';
import { ConfirmReservationComponent } from 'src/app/reservations/confirm-reservation/confirm-reservation.component';
import { TicketComponent } from 'src/app/tickets/ticket-list/ticket/ticket.component';
import { TicketListComponent } from 'src/app/tickets/ticket-list/ticket-list.component';
import { UpdateLocationComponent } from 'src/app/location/update-location/update-location.component';
import { UpdateSpaceComponent } from 'src/app/space/update-space/update-space.component';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { LocationListItemComponent } from 'src/app/location/location-list/location-list-item/location-list-item.component';
import { LocationListComponent } from 'src/app/location/location-list/location-list.component';
import { EditEventComponent } from '../../edit-event/edit-event.component';
import { CompletePaymentComponent } from 'src/app/reservations/complete-payment/complete-payment.component';
import { CompleteSinglePaymentComponent } from 'src/app/reservations/complete-single-payment/complete-single-payment.component';
import { ChartViewComponent } from 'src/app/core/chart-view/chart-view.component';
import { EventReportSelectionDialogComponent } from '../../event-report-selection-dialog/event-report-selection-dialog.component';
// tslint:disable-next-line:max-line-length
import { LocationReportSelectionDialogComponent } from 'src/app/location/location-report-selection-dialog/location-report-selection-dialog.component';
import { SnackbarComponent } from 'src/app/core/snackbar/snackbar.component';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { MaterialModule } from 'src/app/material/material.module';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from 'src/app/app-routing/app-routing.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MapComponent } from 'src/app/core/map/map.component';

describe('AddCompositeEventComponent', () => {
  let component: AddCompositeEventComponent;
  let fixture: ComponentFixture<AddCompositeEventComponent>;
  let addEventServiceSpy: jasmine.SpyObj<AddEventService>;
  let nameInput: DebugElement;
  let submitButton: DebugElement;
  let cancelButton: DebugElement;


  beforeEach(async(() => {
    const spyAddEventService = jasmine.createSpyObj('AddEventService', ['pushNewEvent']);

    TestBed.configureTestingModule({
      declarations: [ HomePageComponent,
        DummyUploadComponent,
        EventComponent,
        SingleEventComponent,
        CompositeEventComponent,
        NavbarComponent,
        AddLocationComponent,
        EventListItemComponent,
        NavbarComponent,
        EventListComponent,
        SpaceLayoutGridComponent,
        SelectSeatComponent,
        LocationViewComponent,
        AddSpaceComponent,
        RegisterUserComponent,
        PasswordConfirmationValidatorDirective,
        AddSingleEventComponent,
        AddCompositeEventComponent,
        EventTreeComponent,
        AddEventComponent,
        SpaceLayoutSectorsComponent,
        PayFormComponent,
        AutoCompleteValidatorDirective,
        StartDateValidatorDirective,
        ReservationComponent,
        LoginComponent,
        UnauthenticatedComponent,
        EndDateValidatorDirective,
        SearchComponent,
        EventImageUploadComponent,
        ConfirmReservationComponent,
        TicketComponent,
        TicketListComponent,
        UpdateLocationComponent,
        UpdateSpaceComponent,
        ConfirmationDialogComponent,
        LocationListItemComponent,
        LocationListComponent,
        EditEventComponent,
        CompletePaymentComponent,
        CompleteSinglePaymentComponent,
        ChartViewComponent,
        EventReportSelectionDialogComponent,
        LocationReportSelectionDialogComponent,
        SnackbarComponent,
        MapComponent,
        UnauthenticatedComponent,
        SnackbarComponent,
     ],
      imports: [
        MaterialModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        NgbModule,
        AppRoutingModule,
        AngularOpenlayersModule,
        AppRoutingModule,
          RouterTestingModule.withRoutes([
            {
              path: '',
              component: HomePageComponent,
              pathMatch: 'full'
            }
          ])
      ],
      providers: [
        {provide: AddEventService, useValue: spyAddEventService},
      ]
    })

    .compileComponents();

    fixture = TestBed.createComponent(AddCompositeEventComponent);
    component = fixture.componentInstance;
    addEventServiceSpy = TestBed.get(AddEventService);
    nameInput = fixture.debugElement.query(By.css('#nameInput'));
    submitButton = fixture.debugElement.query(By.css('#addCompositeEventConfirm'));
    cancelButton = fixture.debugElement.query(By.css('#cancelCompositeEventButton'));

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCompositeEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (done) => {
    expect(component).toBeTruthy();
    done();
  });

  it('should validate add composite form correctly', (done) => {

    component.compositeEvent.name = 'Name of event';
    fixture.detectChanges();

    const debugElement = fixture.debugElement;
    const matSelect = debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    matSelect.click();
    fixture.detectChanges();
    debugElement.query(By.css('.mat-option')).nativeElement.click();
    fixture.detectChanges();

    component.compositeEvent.description = 'Description';
    fixture.detectChanges();
    expect(component.form.valid).toBe(true);

    submitButton.nativeElement.click();
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(addEventServiceSpy.pushNewEvent).toHaveBeenCalledTimes(1);
      done();
    });
  });

  it('should cancell and send and empty event correctly', (done) => {
    cancelButton.nativeElement.click();
    fixture.whenStable().then(() => {
      expect(addEventServiceSpy.pushNewEvent).toHaveBeenCalledTimes(1);
      expect(addEventServiceSpy.pushNewEvent).toHaveBeenCalledWith(null);
      done();
    });
  });

});
