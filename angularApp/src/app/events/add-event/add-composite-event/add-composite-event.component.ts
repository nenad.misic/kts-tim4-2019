import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {EventType} from '../../../shared/model/enums';
import {CompositeEvent} from '../../../shared/model/composite-event';
import {AddEventService} from '../../../shared/services/add-event.service';

@Component({
  selector: 'app-add-composite-event',
  templateUrl: './add-composite-event.component.html',
  styleUrls: ['./add-composite-event.component.css']
})
export class AddCompositeEventComponent implements OnInit {

  @ViewChild('addCompositeEventForm') form: NgForm;

  eventTypes: string[];

  compositeEvent: CompositeEvent = new CompositeEvent();

  constructor(private addEventService: AddEventService) { }

  ngOnInit() {
    this.eventTypes = Object.keys(EventType).filter(k => typeof EventType[k as any] === 'number');
  }

  addCompositeEvent(form: NgForm) {
    this.compositeEvent.classType = 'CompositeEvent';
    this.compositeEvent.childEvents = [];
    this.addEventService.pushNewEvent(this.compositeEvent);
    this.compositeEvent = new CompositeEvent();
    form.resetForm();
  }

  cancellAddingEvent(form: NgForm) {
    this.addEventService.pushNewEvent(null);
    this.compositeEvent = new CompositeEvent();
    form.resetForm();
  }

}
