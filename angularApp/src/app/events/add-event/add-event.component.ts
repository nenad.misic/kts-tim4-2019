import { Component, OnInit } from '@angular/core';
import {AddEventService} from '../../shared/services/add-event.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  eventClass: string;

  currentlyWorkingOnEvent: boolean;

  constructor(private addEventService: AddEventService) {}

  ngOnInit() {
    this.currentlyWorkingOnEvent = false;

    this.addEventService.elementClickedObservable.subscribe((buttonName: string) => {
      this.eventClass = buttonName;
      this.currentlyWorkingOnEvent = true;
    });

    this.addEventService.eventObservable.subscribe((event) => {
      this.currentlyWorkingOnEvent = false;
    });
  }

}
