import { Component, OnInit, ViewChild } from '@angular/core';
import { SingleEvent } from '../../../shared/model/single-event';
import { LocationService } from '../../../shared/services/location.service';
import { Location } from '../../../shared/model/location';
import { Space } from '../../../shared/model/space';
import { SpaceLayout } from '../../../shared/model/space-layout';
import { SpaceService } from '../../../shared/services/space.service';
import { AddEventService } from '../../../shared/services/add-event.service';
import {MatDialog} from '@angular/material';
import {SpaceLayoutGridComponent} from '../../../space-layout/space-layout-grid/space-layout-grid.component';
import { EventType } from '../../../shared/model/enums';
import { NgForm } from '@angular/forms';
import { SpaceLayoutService } from '../../../shared/services/space-layout.service';

@Component({
  selector: 'app-add-single-event',
  templateUrl: './add-single-event.component.html',
  styleUrls: ['./add-single-event.component.css']
})
export class AddSingleEventComponent implements OnInit {

  @ViewChild('addSingleEventForm') form: NgForm;

  selectedEntity: any;

  eventTypes: string[];

  singleEvent: SingleEvent = new SingleEvent();

  startDate: any;
  startTime = {hour: 18, minute: 0};

  endDate: any;
  endTime = {hour: 20, minute: 0};

  selectedLocationName = '';
  selectedLocation: Location;
  locations: Array<Location>;
  filteredLocations: Array<Location>;

  selectedSpaceName = '';
  selectedSpace: Space;
  spaces: Array<Space>;
  filteredSpaces: Array<Space>;

  selectedSpaceLayoutName = '';
  selectedSpaceLayout: SpaceLayout;
  spaceLayouts: Array<SpaceLayout>;
  filteredSpaceLayouts: Array<SpaceLayout>;

  constructor(private locationService: LocationService,
    private spaceService: SpaceService,
    private addEventService: AddEventService,
    private spaceLayoutService: SpaceLayoutService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.eventTypes = Object.keys(EventType).filter(k => typeof EventType[k as any] === 'number');

    this.locations = [];
    this.filteredLocations = [];

    this.locationService.getAll()
      .then((locations: Array<Location>) => {
        this.locations = locations;
        this.filteredLocations = locations;
      })
    .catch((err) => {
      alert('Failed to collect locations from server, cannot continue.');
    });
    this.filteredLocations = this.locations;
  }

  addSingleEvent(form) {
    this.singleEvent.classType = 'SingleEvent';
    this.singleEvent.state = 'ForSale';

    this.singleEvent.startDate = new Date(this.startDate);
    this.singleEvent.startDate.setHours(this.startTime.hour);
    this.singleEvent.startDate.setMinutes(this.startTime.minute);
    this.singleEvent.startDate = this.singleEvent.startDate.getTime();

    this.singleEvent.endDate = new Date(this.endDate);
    this.singleEvent.endDate.setHours(this.endTime.hour);
    this.singleEvent.endDate.setMinutes(this.endTime.minute);
    this.singleEvent.endDate = this.singleEvent.endDate.getTime();

    this.singleEvent.space = this.selectedSpace;
    this.singleEvent.spaceId = this.singleEvent.space.id;
    this.singleEvent.spaceLayout = this.selectedSpaceLayout;
    this.singleEvent.spaceLayoutId = this.selectedSpaceLayout.id;
    this.addEventService.pushNewEvent(JSON.parse(JSON.stringify(this.singleEvent)));

    this.singleEvent = new SingleEvent();
    this.selectedSpaceLayout = null;
    this.selectedSpace = null;
    this.selectedLocation = null;
    form.resetForm();

  }

  cancellAddingEvent(form) {
    this.addEventService.pushNewEvent(null);
    this.singleEvent = new SingleEvent();
    form.resetForm();
  }


  displayFunctionLocation(location?: any) {
    return location ? location.name + ', ' + location.city : undefined;
  }

  doFilterLocation() {
    if (typeof this.selectedLocationName === 'string') {
      this.filteredLocations = this.locations.filter(loc => (loc.name + ', ' + loc.city)
        .toLowerCase().includes(this.selectedLocationName.toLowerCase()));
    }
  }

  setSelectedLocation(event) {
    this.selectedLocation = event.option.value;
    this.clearSpaces();
    this.locationService.getById(this.selectedLocation.id)
    .then((location: Location) => {
      this.spaces = location.spaces;
      this.filteredSpaces = this.spaces;
    })
    .catch((err: any) => {
      alert('Failed to get spaces for this location, internetz nou');
    });
  }

  locationInvalidated() {
    this.clearSpaces();
  }

  setSelectedSpace(event) {
    this.selectedSpace = event.option.value;
    this.clearSpaceLayouts();
    this.spaceService.getById(this.selectedSpace.id)
    .then((space: Space) => {
      this.spaceLayouts = space.defaultSpaceLayouts;
      this.filteredSpaceLayouts = this.spaceLayouts;
    })
    .catch((err: any) => {
      alert('Failed to get spaces for this location, internetz nou');
    });
  }

  displayFunctionSpace(space?: any) {
    return space ? space.name : undefined;
  }

  doFilterSpace() {
    if (typeof this.selectedSpaceName === 'string') {
      this.filteredSpaces = this.spaces.filter(space => space.name.toLowerCase().includes(this.selectedSpaceName.toLowerCase()));
    }
  }

  spaceInvalidated() {
    this.clearSpaceLayouts();
  }

  spaceLayoutInvalidated() {
    this.selectedSpaceLayout = null;
  }

  setSelectedSpaceLayout(event) {
    this.spaceLayoutService.getById(event.option.value.id)
    .then((spaceLayout: SpaceLayout) => {
      spaceLayout.groundFloors.forEach(groundFloor => {
        groundFloor.price = null;
        groundFloor.capacity = null;
      });
      spaceLayout.seatSpaces.forEach(seatSpace => {
        seatSpace.price = null;
      });
      this.selectedSpaceLayout = spaceLayout;
    })
    .catch((err: any) => { alert('Internet failed, nou' ); });
  }

  displayFunctionSpaceLayout(spaceLayout?: any) {
    return spaceLayout ? spaceLayout.name : undefined;
  }

  doFilterSpaceLayout() {
    if (typeof this.selectedSpaceLayoutName === 'string') {
      this.filteredSpaceLayouts = this.spaceLayouts.filter(space => space.name.toLowerCase()
        .includes(this.selectedSpaceLayoutName.toLowerCase()));
    }
  }

  startTimeChanged() {
    setTimeout(() => {
      if (this.form.controls.startDate && this.form.controls.endDate) {
        this.form.controls.startDate.updateValueAndValidity();
        this.form.controls.endDate.updateValueAndValidity();
      }
     }, 50);
  }

  endTimeChanged() {
    setTimeout(() => {
      if (this.form.controls.endDate) {
        this.form.controls.endDate.updateValueAndValidity();
      }
    }, 50);
  }

  clearSpaces() {
    this.clearSpaceLayouts();
    this.spaces = [];
    this.filteredSpaces = [];
    this.selectedSpace = null;
    this.selectedSpaceName = '';
  }

  clearSpaceLayouts() {
    this.spaceLayouts = [];
    this.filteredSpaceLayouts = [];
    this.selectedSpaceLayout = null;
    this.selectedSpaceLayoutName = '';
    this.selectedEntity = null;
  }

  openSpaceLayoutDialog(): void {
    const dialogRef = this.dialog.open(SpaceLayoutGridComponent, {
      width: '90vw',
      height: '95vh',
      data: this.selectedSpace
    });

    dialogRef.afterClosed().subscribe(result => {
      this.clearSpaceLayouts();
      this.spaceService.getById(this.selectedSpace.id)
        .then((space: Space) => {
          this.spaceLayouts = space.defaultSpaceLayouts;
          this.filteredSpaceLayouts = this.spaceLayouts;
        })
        .catch((err: any) => {
          alert('Failed to get spaces for this location, internetz nou');
        });
    });
  }

  onSelectedEntityChanged(entity) {
    this.selectedEntity = entity;
  }

}
