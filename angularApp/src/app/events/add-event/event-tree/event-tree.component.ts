import { Component, OnInit } from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import { Event } from '../../../shared/model/event';
import { CompositeEvent } from '../../../shared/model/composite-event';
import { CompositeEventComponent } from '../../event/composite-event/composite-event.component';
import { AddEventService } from '../../../shared/services/add-event.service';
import { EventService } from '../../../shared/services/event.service';
import { Message } from '../../../shared/model/message';
import {Router} from '@angular/router';

interface EventFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  event: Event;
}

@Component({
  selector: 'app-event-tree',
  templateUrl: './event-tree.component.html',
  styleUrls: ['./event-tree.component.css']
})
export class EventTreeComponent implements OnInit {

  treeControl = new FlatTreeControl<EventFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable,
      node => node.classType === 'CompositeEvent' ? (node as CompositeEvent).childEvents : null);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  selectedNode: EventFlatNode;

  currentlyWorkingOnEvent: boolean;

  constructor(private addEventService: AddEventService,
    private eventService: EventService,
    private router: Router) {
    this.currentlyWorkingOnEvent = false;
    this.selectedNode = undefined;
    this.addEventService.eventObservable.subscribe((event) => {
      this.currentlyWorkingOnEvent = false;
      if (event) {
        if (this.dataSource.data.length === 0) {
          this.dataSource.data.push(event);
        } else {
          (this.selectedNode.event as CompositeEvent).childEvents.push(event);
        }
      }
      this.dataSource.data = this.dataSource.data;
      this.treeControl.expandAll();
      this.selectedNode = undefined;
    });
  }

  private _transformer(node: Event, level: number) {
    let expandable = false;
    if (node.classType === 'CompositeEvent') {
      expandable = true;
    }

    return {
      expandable,
      name: node.name,
      level: level,
      event: node
    };
  }

  submit() {
    if (this.dataSource.data.length === 1) {
      const event: Event = this.dataSource.data[0];
      this.eventService.create(event).then((message: Message) => {
        const id: number = +message.message.split(':')[1];
        this.router.navigateByUrl('/eventImage/' + id);
      })
      .catch((err: any) => {
        alert('Failed to add event');
      });
    } else {
      alert('Something went wrong with form submission');
    }
  }

  eventButtonPressed(eventType: string) {
    this.addEventService.pushElementClicked(eventType);
    this.currentlyWorkingOnEvent = true;
  }

  deleteEventButtonPressed() {

    const toDelete: Event = this.selectedNode.event;
    const root: Event = this.dataSource.data[0];

    if (root === toDelete) {
      this.dataSource.data = [];
    } else {
        const recursiveDeleteFunction = function(parent: CompositeEvent) {
          const index: number = parent.childEvents.findIndex(event => event === toDelete);
          if (index > -1 ) {
              parent.childEvents.splice(index, 1);
          } else {
            parent.childEvents.forEach((event: Event) => {
              if (event.classType === 'CompositeEvent') {
                recursiveDeleteFunction(event as CompositeEvent);
              }
            });
          }
        };
        recursiveDeleteFunction(root as CompositeEvent);
    }
    this.dataSource.data = this.dataSource.data;
    this.selectedNode = undefined;
    this.treeControl.expandAll();
  }

  ngOnInit() {
    this.dataSource.data = [];
    this.treeControl.expandAll();
  }

  hasChild = (_: number, node: EventFlatNode) => node.expandable;

  nodeClicked(node: EventFlatNode) {
    if (!this.currentlyWorkingOnEvent) {
      if (this.selectedNode === node) {
        this.selectedNode = undefined;
      } else {
        this.selectedNode = node;
      }
    }
  }

}
