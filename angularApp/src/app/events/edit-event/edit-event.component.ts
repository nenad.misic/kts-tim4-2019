import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Event } from '../../shared/model/event';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {


  @Input() event: Event;

  toEdit = {name: '', description: ''};

  constructor(private dialogRef: MatDialogRef<EditEventComponent>) { }

  ngOnInit() {
    this.toEdit.name = this.event.name;
    this.toEdit.description = this.event.description;
  }

  submit() {
    this.event.name = this.toEdit.name;
    this.event.description = this.toEdit.description;
    this.dialogRef.close('submit');
  }

  cancel() {
    this.dialogRef.close('cancel');
  }

}
