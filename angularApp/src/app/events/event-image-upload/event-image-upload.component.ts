import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EventService} from '../../shared/services/event.service';
import {DummyUploadService} from '../../shared/services/dummy-upload.service';

@Component({
  selector: 'app-event-image-upload',
  templateUrl: './event-image-upload.component.html',
  styleUrls: ['./event-image-upload.component.css']
})
export class EventImageUploadComponent implements OnInit {

  event: any;
  events: Array<any>;
  files: any;
  buttonDisabled = false;


  constructor(private route: ActivatedRoute,
              private eventService: EventService,
              private fileUploadService: DummyUploadService) { }

  ngOnInit() {
    this.buttonDisabled = false;
    const id = this.route.snapshot.params['id'];
    this.eventService.getById(id).then(data => {
      this.event = data;
      this.events = [];
      this.recursivelyGetAllEvents(this.event);
      this.files = {};
      for (const e of this.events) {
        this.files[e.id] = [];
      }
    });
  }

  recursivelyGetAllEvents(event: any) {
    this.events.push(event);
    if (event.childEvents) {
      for ( const child of event.childEvents) {
        this.recursivelyGetAllEvents(child);
      }
    }
  }

  onFileChange(eventId, clickevent) {
    this.files[eventId] = [];
    for (const file of clickevent.target.files) {
      this.files[eventId].push({uploaded: -1, name: file.name, file: file });
    }
  }

  onSubmit() {
    this.buttonDisabled = true;
    for (const eventId in this.files) {
      if (Object.prototype.hasOwnProperty.call(this.files, eventId)) {
        for (const file of this.files[eventId]) {
          file.uploaded = 2;
          this.fileUploadService.postFile(eventId, file.file).subscribe(data => {
            file.uploaded = 1;
          }, error => {
            file.uploaded = 0;
            console.log(error);
          });
        }
      }
    }
  }



}
