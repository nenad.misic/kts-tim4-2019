import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { Event } from '../../../shared/model/event';
import {User} from '../../../shared/model/user';
import {ImageService} from '../../../shared/services/image.service';
import {EventService} from '../../../shared/services/event.service';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {SingleEvent} from '../../../shared/model/single-event';
import {CompositeEvent} from '../../../shared/model/composite-event';
import {ConfirmationDialogComponent} from '../../../core/confirmation-dialog/confirmation-dialog.component';
import {Message} from '../../../shared/model/message';
import {EditEventComponent} from '../../edit-event/edit-event.component';
import {EventReportSelectionDialogComponent} from '../../event-report-selection-dialog/event-report-selection-dialog.component';

@Component({
  selector: 'app-event-list-item',
  templateUrl: './event-list-item.component.html',
  styleUrls: ['./event-list-item.component.css']
})

export class EventListItemComponent implements OnInit {

  @Input()
  event: Event;

  image = 'https://i.ytimg.com/vi/MFd18sF8CNU/maxresdefault.jpg';

  deletable: boolean;

  currentUser: User;

  @Output()
  deletedEmitter = new EventEmitter<number>();

  constructor(private router: Router,
    private imageService: ImageService,
    private dialog: MatDialog,
    private eventService: EventService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    // TODO get image
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.deletable = false;
    this.imageService.getImage(this.event.id).then(data => {
      this.image = environment.imageEndpoint + data.message;
    });
    this.deletable = this.checkIfDeletable(this.event);
  }

  checkIfDeletable(event: Event): boolean {
    if (event.classType === 'single_event') {
      const singleEvent: SingleEvent = event as SingleEvent;
      if (singleEvent.tickets) {
        return singleEvent.tickets.findIndex((ticket) => ticket.userId != null) === -1;
      }
      return true;
    } else if (event.classType === 'CompositeEvent') {
      const compositeEvent: CompositeEvent = event as CompositeEvent;
      if (compositeEvent.childEvents) {
        for (const childEvent of compositeEvent.childEvents) {
          if (!this.checkIfDeletable(childEvent)) {
            return false;
          }
        }
      }
      return true;
    }
  }

  deleteEvent() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((value: string) => {
      if (value === 'yes') {
        this.eventService.deleteById(this.event.id)
        .then((message: Message) => {
          if (message.message === 'Deleted') {
            this.deletedEmitter.emit(this.event.id);
          } else {
            alert('Deletable condition is wrong, or somebody reserved ticket in meantime');
          }
        })
        .catch((err: any) => {
          alert('Something went wrong while deleting event');
        });
      }
    });
  }

  editEvent() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '400px';
    dialogConfig.width = '800px';

    const oldValues = {name: this.event.name, description: this.event.description};

    const dialogRef = this.dialog.open(EditEventComponent, dialogConfig);
    const instance = dialogRef.componentInstance;
    instance.event = this.event;


    dialogRef.afterClosed().subscribe((result: string) => {
      if (result === 'submit') {
        const toSend = JSON.parse(JSON.stringify(this.event));
        if (toSend.childEvents) {
          delete toSend.childEvents;
        }
        this.eventService.update(this.event)
        .then((message: Message) => {
          console.log('Event successfully changed');
        }).catch((err: any) => {
          this.event.name = oldValues.name;
          this.event.description = oldValues.description;
          alert('Failed to change event, reverting to old values');
        });
      }
    });
  }

  reportEvent() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '300px';
    dialogConfig.width = '250px';

    const dialogRef = this.dialog.open(EventReportSelectionDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: string) => {
      if (result) {
        this.router.navigateByUrl('chart/' + result + '/' + this.event.id + '/-1/-1');
      }
    });
  }

}
