import { Component, OnInit, Input } from '@angular/core';
import { e } from '@angular/core/src/render3';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  @Input()
  events: Array<any>;

  constructor() { }

  ngOnInit() {
  }

  onEventDeleted(id: number) {
    this.events = this.events.filter(event => event.id !== id);
  }

  sortEvents() {
    return this.events.sort((e1, e2) => e1.id - e2.id);
  }

}
