import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-event-report-selection-dialog',
  templateUrl: './event-report-selection-dialog.component.html',
  styleUrls: ['./event-report-selection-dialog.component.css']
})
export class EventReportSelectionDialogComponent implements OnInit {

  selectedReport = '';

  constructor(private dialogRef: MatDialogRef<EventReportSelectionDialogComponent>) { }

  ngOnInit() {
  }

  close(answer: any) {
    this.dialogRef.close(answer);
  }

}
