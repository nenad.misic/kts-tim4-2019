import { Component, OnInit, Input } from '@angular/core';
import {ImageService} from '../../../shared/services/image.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-composite-event',
  templateUrl: './composite-event.component.html',
  styleUrls: ['./composite-event.component.css']
})
export class CompositeEventComponent implements OnInit {

  @Input()
  event: any;

  images: string[];

  constructor(private imageService: ImageService) { }

  ngOnInit() {
    this.imageService.getAllImages(this.event.id).then(data => {
      this.images = data.message.split(',').map(image => environment.imageEndpoint + image);
    }, err => {
      console.log(err);
    });
  }

}
