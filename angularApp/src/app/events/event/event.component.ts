import { EventService } from '../../shared/services/event.service';
import { Component, OnInit } from '@angular/core';
import { Event } from '../../shared/model/event';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {


  event: any;

  public dummyEvent: any = {
    id: 1,
    name: 'Koncert Zvogdana',
    description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
    classType: 'SingleEvent',
    eventType: 'Concert',
    startDate: new Date(),
    endDate: new Date(),
    state: 'ForSale',
    space: {
      name: 'Svecana sala',
      location: {
        name: 'Spens',
        city: 'Novi Sad',
        address: 'Maksima Gorkog 5',
        country: 'Serbia',
        longitude: 45.247537,
        latitude: 19.845547,
      }
    }
  };

  constructor(private route: ActivatedRoute,
              private eventService: EventService) { }

  ngOnInit() {
    // TODO get event
    const id = this.route.snapshot.params['id'];
    this.eventService.getById(id).then(data => {
      this.event = data;
    });

    this.route.paramMap.subscribe(params => {
      const new_id = +params.get('id');
      this.eventService.getById(new_id).then(data => {
        this.event = data;
      });
    });
  }

}
