import { Component, OnInit, Input } from '@angular/core';
import { SingleEvent } from '../../../shared/model/single-event';
import { environment } from 'src/environments/environment';
import { ImageService } from '../../../shared/services/image.service';


@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.css']
})



export class SingleEventComponent implements OnInit {

  @Input()
  event: any;

  images: string[];


  constructor(private imageService: ImageService) { }

  ngOnInit() {
    this.imageService.getAllImages(this.event.id).then(data => {
      this.images = data.message.split(',').map(image => environment.imageEndpoint + image);
    }, err => {
      console.log(err);
    });
  }


}
