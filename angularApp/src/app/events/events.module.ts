import { SpaceLayoutModule } from './../space-layout/space-layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventComponent} from './event/event.component';
import {SingleEventComponent} from './event/single-event/single-event.component';
import {CompositeEventComponent} from './event/composite-event/composite-event.component';
import {EventListItemComponent} from './event-list/event-list-item/event-list-item.component';
import {EventListComponent} from './event-list/event-list.component';
import {AddSingleEventComponent} from './add-event/add-single-event/add-single-event.component';
import {AddCompositeEventComponent} from './add-event/add-composite-event/add-composite-event.component';
import {EventTreeComponent} from './add-event/event-tree/event-tree.component';
import {AddEventComponent} from './add-event/add-event.component';
import {EventImageUploadComponent} from './event-image-upload/event-image-upload.component';
import {EditEventComponent} from './edit-event/edit-event.component';
import {EventReportSelectionDialogComponent} from './event-report-selection-dialog/event-report-selection-dialog.component';
import {SearchComponent} from './search/search.component';
import {CoreModule} from '../core/core.module';
import {MaterialModule} from '../material/material.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {FormsModule} from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    EventComponent,
    SingleEventComponent,
    CompositeEventComponent,
    EventListItemComponent,
    EventListComponent,
    AddSingleEventComponent,
    AddCompositeEventComponent,
    EventTreeComponent,
    AddEventComponent,
    EventImageUploadComponent,
    EditEventComponent,
    EventReportSelectionDialogComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    SpaceLayoutModule,
    SharedModule,
  ],
  exports: [
    EventListComponent,
  ]
})
export class EventsModule { }
