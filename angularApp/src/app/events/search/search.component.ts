import { Component, OnInit } from '@angular/core';
import { Event } from '../../shared/model/event';
import {SingleEvent} from '../../shared/model/single-event';
import {SearchService} from '../../shared/services/search.service';
import {Location} from '../../shared/model/location';
import {Space} from '../../shared/model/space';
import {SpaceLayout} from '../../shared/model/space-layout';
import {EventType} from '../../shared/model/enums';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  startDate: string;
  endDate: string;
  eventTypes: string[];
  event: SingleEvent;
  events: Array<Event>;
  searched: boolean;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.eventTypes = Object.keys(EventType).filter(k => typeof EventType[k as any] === 'number');
    this.event = new SingleEvent();
    const space = new Space();
    space.location = new Location();
    this.event.space = space;
    this.event.spaceLayout = new SpaceLayout();
  }


  onSubmit() {
    this.event.startDate = new Date(this.startDate).getTime();
    this.event.endDate = new Date(this.endDate).getTime();
    if (this.event.endDate) {
      this.event.endDate = this.event.endDate + (23 * 60 + 59) * 60000 + 1;
    }

    this.searchService.search(this.event).then((eventList) => {

      this.searched = true;
      this.events = eventList;
    });

  }
  clearSearch() {

    this.event = new SingleEvent();
    const space = new Space();
    space.location = new Location();
    this.event.space = space;
    this.event.spaceLayout = new SpaceLayout();

    this.event.startDate = null;
    this.event.endDate = null;

    this.searchService.search(this.event).then((eventList) => {

      this.searched = false;
      this.events = eventList;
    });
  }
}
