import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { HomePageComponent } from './home-page.component';
import {EventService} from '../../shared/services/event.service';
import {Router} from '@angular/router';
import {SearchService} from '../../shared/services/search.service';
import {MaterialModule} from '../../material/material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBar} from '@angular/material';
import {of} from 'rxjs';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from '../../app-routing/app-routing.module';
import {AngularOpenlayersModule} from 'ngx-openlayers';
import {UnauthenticatedComponent} from '../../core/unauthenticated/unauthenticated.component';
import {EventComponent} from '../../events/event/event.component';
import {LocationViewComponent} from '../../location/location-view/location-view.component';
import {RegisterUserComponent} from '../register-user/register-user.component';
import {LoginComponent} from '../login/login.component';
import {AppComponent} from '../../app.component';
import {DummyUploadComponent} from '../../core/dummy-upload/dummy-upload.component';
import {SingleEventComponent} from '../../events/event/single-event/single-event.component';
import {CompositeEventComponent} from '../../events/event/composite-event/composite-event.component';
import {EventListItemComponent} from '../../events/event-list/event-list-item/event-list-item.component';
import {NavbarComponent} from '../navbar/navbar.component';
import {EventListComponent} from '../../events/event-list/event-list.component';
import {AddLocationComponent} from '../../location/add-location/add-location.component';
import {SpaceLayoutGridComponent} from '../../space-layout/space-layout-grid/space-layout-grid.component';
import {SelectSeatComponent} from '../../reservations/select-seat/select-seat.component';
import {AddSpaceComponent} from '../../space/add-space/add-space.component';
import {PasswordConfirmationValidatorDirective} from '../../shared/directives/password-confirmation-validator.directive';
import {AddSingleEventComponent} from '../../events/add-event/add-single-event/add-single-event.component';
import {AddCompositeEventComponent} from '../../events/add-event/add-composite-event/add-composite-event.component';
import {EventTreeComponent} from '../../events/add-event/event-tree/event-tree.component';
import {AddEventComponent} from '../../events/add-event/add-event.component';
import {SpaceLayoutSectorsComponent} from '../../space-layout/space-layout-sectors/space-layout-sectors.component';
import {PayFormComponent} from '../../reservations/pay-form/pay-form.component';
import {AutoCompleteValidatorDirective} from '../../shared/directives/auto-complete-validator.directive';
import {StartDateValidatorDirective} from '../../shared/directives/start-date-validator.directive';
import {ReservationComponent} from '../../reservations/reservation/reservation.component';
import {EndDateValidatorDirective} from '../../shared/directives/end-date-validator.directive';
import {SearchComponent} from '../../events/search/search.component';
import {EventImageUploadComponent} from '../../events/event-image-upload/event-image-upload.component';
import {ConfirmReservationComponent} from '../../reservations/confirm-reservation/confirm-reservation.component';
import {TicketComponent} from '../../tickets/ticket-list/ticket/ticket.component';
import {TicketListComponent} from '../../tickets/ticket-list/ticket-list.component';
import {UpdateLocationComponent} from '../../location/update-location/update-location.component';
import {UpdateSpaceComponent} from '../../space/update-space/update-space.component';
import {ConfirmationDialogComponent} from '../../core/confirmation-dialog/confirmation-dialog.component';
import {LocationListItemComponent} from '../../location/location-list/location-list-item/location-list-item.component';
import {LocationListComponent} from '../../location/location-list/location-list.component';
import {EditEventComponent} from '../../events/edit-event/edit-event.component';
import {CompletePaymentComponent} from '../../reservations/complete-payment/complete-payment.component';
import {CompleteSinglePaymentComponent} from '../../reservations/complete-single-payment/complete-single-payment.component';
import {ChartViewComponent} from '../../core/chart-view/chart-view.component';
import {EventReportSelectionDialogComponent} from '../../events/event-report-selection-dialog/event-report-selection-dialog.component';
// tslint:disable-next-line:max-line-length
import {LocationReportSelectionDialogComponent} from '../../location/location-report-selection-dialog/location-report-selection-dialog.component';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {MapComponent} from '../../core/map/map.component';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let searchServiceSpy: jasmine.SpyObj<SearchService>;
  let eventsServiceSpy: jasmine.SpyObj<EventService>;

  const starting = [];
  const events = [{
    id: 4,
    name: 'Turneja Zvonka Bogdana',
    description: 'Nezaboravni put Zvonka Bogdana po nasim najmilijim varosima stare Jugoslavije. Ova turneja donosi srecu i begstvo \
    od pakla svakodnevnice u kojoj se nalazi stanovnistvo naseg regiona.',
    classType: 'CompositeEvent',
    eventType: 'Concert',
    childEvents: [{
      id: 1,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }, {
      id: 2,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }, {
      id: 3,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }]
  }, {
      id: 5,
      name: 'Koncert Maye Berovic',
    // tslint:disable-next-line:max-line-length
      description: 'Pored vokalnih sposobnosti, na ovom koncertu mocicete da vidite i ostale atribute lepe i oskudno obucene popularne folk pevacice.',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }, {
      id: 6,
      name: 'Zikina Sarencia Live',
      description: 'Budite u publici najgledanije emisije ljudi starnosne kategorije od 65 do 68 godina!',
      classType: 'SingleEvent',
      eventType: 'TV Programme',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }];

  beforeEach((() => {
    const spySearch = jasmine.createSpyObj('SearchService', ['quickSearch']);
    const spyEvents = jasmine.createSpyObj('EventService', ['getAll', 'getPage']);
    TestBed.configureTestingModule({
      declarations: [ HomePageComponent,
        DummyUploadComponent,
        EventComponent,
        SingleEventComponent,
        CompositeEventComponent,
        NavbarComponent,
        AddLocationComponent,
        EventListItemComponent,
        NavbarComponent,
        EventListComponent,
        SpaceLayoutGridComponent,
        SelectSeatComponent,
        LocationViewComponent,
        AddSpaceComponent,
        RegisterUserComponent,
        PasswordConfirmationValidatorDirective,
        AddSingleEventComponent,
        AddCompositeEventComponent,
        EventTreeComponent,
        AddEventComponent,
        SpaceLayoutSectorsComponent,
        PayFormComponent,
        AutoCompleteValidatorDirective,
        StartDateValidatorDirective,
        ReservationComponent,
        LoginComponent,
        UnauthenticatedComponent,
        EndDateValidatorDirective,
        SearchComponent,
        EventImageUploadComponent,
        ConfirmReservationComponent,
        TicketComponent,
        TicketListComponent,
        UpdateLocationComponent,
        UpdateSpaceComponent,
        ConfirmationDialogComponent,
        LocationListItemComponent,
        LocationListComponent,
        EditEventComponent,
        CompletePaymentComponent,
        CompleteSinglePaymentComponent,
        ChartViewComponent,
        EventReportSelectionDialogComponent,
        LocationReportSelectionDialogComponent,
        SnackbarComponent,
        MapComponent,
      UnauthenticatedComponent,
      ],

      imports: [ MaterialModule,
                BrowserModule,
                BrowserAnimationsModule,
                FormsModule,
                HttpClientModule,
                MaterialModule,
                NgbModule,
                AppRoutingModule,
                AngularOpenlayersModule,
                AppRoutingModule,
                  RouterTestingModule.withRoutes([
                    {
                      path: '',
                      component: HomePageComponent,
                      pathMatch: 'full'
                    }
                  ])
      ],
      providers: [
        EventListComponent,
        EventListItemComponent,
        MatSnackBar,
        {provide: SearchService, useValue: spySearch},
        {provide: EventService, useValue: spyEvents}
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    searchServiceSpy = TestBed.get(SearchService);
    eventsServiceSpy = TestBed.get(EventService);
    searchServiceSpy.quickSearch.and.returnValue(Promise.resolve(events));
    eventsServiceSpy.getAll.and.returnValue(Promise.resolve(starting));
    eventsServiceSpy.getPage.and.returnValue(Promise.resolve(starting));
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct number of app-event-list-item components after the call of quickSearch method', fakeAsync( () => {
    component.ngOnInit();
    tick();
    expect(component.events.length).toEqual(starting.length);
    component.searchTerm = 'sta god';
    component.doSearch();
    tick();
    expect(component.events.length).toEqual(events.length);
  }));
});
