import { EventService } from '../../shared/services/event.service';
import {Component, HostListener, OnInit} from '@angular/core';
import {SearchService} from '../../shared/services/search.service';
import {SingleEvent} from '../../shared/model/single-event';
import {SpaceLayout} from '../../shared/model/space-layout';
import {Space} from '../../shared/model/space';
import {Location} from '../../shared/model/location';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  searchTerm: string;
  searched: boolean;
  page = 1;
  pageSize = 3;
  gotAllData = false;
  events: Array<any>;

  constructor(private eventService: EventService,
              private searchService: SearchService) { }

  ngOnInit() {
    this.events = [];
    this.searched = false;
    this.eventService.getPage(0, this.pageSize).then(data => {
      this.events = data;
    });
  }

  doSearch() {

    const event = new SingleEvent();
    event.spaceLayout = new SpaceLayout();
    event.space = new Space();
    event.space.location = new Location();
    event.description = this.searchTerm;
    event.name = this.searchTerm;
    event.spaceLayout.name = this.searchTerm;
    event.space.name = this.searchTerm;
    event.space.location.name = this.searchTerm;
    event.space.location.address = this.searchTerm;
    event.space.location.city = this.searchTerm;
    event.space.location.country = this.searchTerm;

    this.searchService.quickSearch(event).then((eventList) => {
      this.searched = true;
      this.events = eventList;
    });
  }

  clearSearch() {
    this.eventService.getPage(0, this.pageSize).then(data => {
      this.searched = false;
      this.page = 1;
      this.gotAllData = false;
      this.searchTerm = '';
      this.events = data;
    });
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {
    if (!this.searched) {
      const windowHeight = 'innerHeight' in window ? window.innerHeight
        : document.documentElement.offsetHeight;
      const body = document.body, html = document.documentElement;
      const docHeight = Math.max(body.scrollHeight,
        body.offsetHeight, html.clientHeight,
        html.scrollHeight, html.offsetHeight);
      const windowBottom = windowHeight + window.pageYOffset;
      if (windowBottom >= docHeight && !this.gotAllData) {
        this.eventService.getPage(this.page, this.pageSize)
          .then((data: Array<any>) => {
            data.forEach(newEvent => {
              this.events.push(newEvent);
            });
            this.page++;
            if (data.length === 0) {
              this.gotAllData = true;
            }
          });
      }
    }
  }
}
