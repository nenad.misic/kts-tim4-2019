import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavbarComponent} from './navbar/navbar.component';
import {HomePageComponent} from './home-page/home-page.component';
import {RegisterUserComponent} from './register-user/register-user.component';
import {LoginComponent} from './login/login.component';
import {CoreModule} from '../core/core.module';
import {MaterialModule} from '../material/material.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {FormsModule} from '@angular/forms';
import {EventsModule} from '../events/events.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    EventsModule,
    SharedModule,
  ],
  exports: [
    NavbarComponent
  ],
  declarations: [
    NavbarComponent,
    HomePageComponent,
    RegisterUserComponent,
    LoginComponent,
  ]
})
export class HomeModule { }
