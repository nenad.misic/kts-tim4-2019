import { SnackbarComponent } from '../../core/snackbar/snackbar.component';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  returnUrl: string;

  message: string;

  constructor(private authenticationService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar) {
      this.message = '';
     }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onLogin() {
    console.log('on login');
    this.authenticationService.login(this.username, this.password).then((result) => {
      this.router.navigate([this.returnUrl]);
    }, err => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
    });
  }

}
