import { MapComponent } from '../../core/map/map.component';
import { UnauthenticatedComponent } from '../../core/unauthenticated/unauthenticated.component';
import { UserService } from '../../shared/services/user.service';
import { SnackbarComponent } from '../../core/snackbar/snackbar.component';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { RegisterUserComponent } from './register-user.component';
import { MaterialModule } from '../../material/material.module';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from '../../app-routing/app-routing.module';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { RouterTestingModule } from '@angular/router/testing';
import { HomePageComponent } from '../home-page/home-page.component';
import { MatSnackBar } from '@angular/material';
import { DummyUploadComponent } from '../../core/dummy-upload/dummy-upload.component';
import { EventComponent } from '../../events/event/event.component';
import { SingleEventComponent } from '../../events/event/single-event/single-event.component';
import { CompositeEventComponent } from '../../events/event/composite-event/composite-event.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { AddLocationComponent } from '../../location/add-location/add-location.component';
import { EventListItemComponent } from '../../events/event-list/event-list-item/event-list-item.component';
import { EventListComponent } from '../../events/event-list/event-list.component';
import { SpaceLayoutGridComponent } from '../../space-layout/space-layout-grid/space-layout-grid.component';
import { SelectSeatComponent } from '../../reservations/select-seat/select-seat.component';
import { LocationViewComponent } from '../../location/location-view/location-view.component';
import { AddSpaceComponent } from '../../space/add-space/add-space.component';
import { PasswordConfirmationValidatorDirective } from '../../shared/directives/password-confirmation-validator.directive';
import { AddSingleEventComponent } from '../../events/add-event/add-single-event/add-single-event.component';
import { AddCompositeEventComponent } from '../../events/add-event/add-composite-event/add-composite-event.component';
import { EventTreeComponent } from '../../events/add-event/event-tree/event-tree.component';
import { AddEventComponent } from '../../events/add-event/add-event.component';
import { SpaceLayoutSectorsComponent } from '../../space-layout/space-layout-sectors/space-layout-sectors.component';
import { PayFormComponent } from '../../reservations/pay-form/pay-form.component';
import { AutoCompleteValidatorDirective } from '../../shared/directives/auto-complete-validator.directive';
import { StartDateValidatorDirective } from '../../shared/directives/start-date-validator.directive';
import { ReservationComponent } from '../../reservations/reservation/reservation.component';
import { LoginComponent } from '../login/login.component';
import { EndDateValidatorDirective } from '../../shared/directives/end-date-validator.directive';
import { SearchComponent } from '../../events/search/search.component';
import { EventImageUploadComponent } from '../../events/event-image-upload/event-image-upload.component';
import { ConfirmReservationComponent } from '../../reservations/confirm-reservation/confirm-reservation.component';
import { TicketComponent } from '../../tickets/ticket-list/ticket/ticket.component';
import { TicketListComponent } from '../../tickets/ticket-list/ticket-list.component';
import { UpdateLocationComponent } from '../../location/update-location/update-location.component';
import { UpdateSpaceComponent } from '../../space/update-space/update-space.component';
import { ConfirmationDialogComponent } from '../../core/confirmation-dialog/confirmation-dialog.component';
import { LocationListItemComponent } from '../../location/location-list/location-list-item/location-list-item.component';
import { LocationListComponent } from '../../location/location-list/location-list.component';
import { EditEventComponent } from '../../events/edit-event/edit-event.component';
import { CompletePaymentComponent } from '../../reservations/complete-payment/complete-payment.component';
import { CompleteSinglePaymentComponent } from '../../reservations/complete-single-payment/complete-single-payment.component';
import { ChartViewComponent } from '../../core/chart-view/chart-view.component';
import { EventReportSelectionDialogComponent } from '../../events/event-report-selection-dialog/event-report-selection-dialog.component';
// tslint:disable-next-line:max-line-length
import { LocationReportSelectionDialogComponent } from '../../location/location-report-selection-dialog/location-report-selection-dialog.component';
import { User } from '../../shared/model/user';
import { DebugElement } from '@angular/core';
import { Router } from '@angular/router';

describe('RegisterUserComponent', () => {
  let component: RegisterUserComponent;
  let fixture: ComponentFixture<RegisterUserComponent>;
  let userServiceSpy: jasmine.SpyObj<UserService>;
  let snackBarSpy: jasmine.SpyObj<MatSnackBar>;
  let routerSpy: jasmine.SpyObj<Router>;

  let submitButton: DebugElement;
  let registerForm: DebugElement;
  let usernameInput: DebugElement;
  let emailInput: DebugElement;
  let passwordInput: DebugElement;
  let confirmPasswordInput: DebugElement;
  let nameInput: DebugElement;
  let surnameInput: DebugElement;

  const user: User = {
    username: 'username',
    password: 'password',
    email: 'email@gmail.com',
    name: 'Name',
    surname: 'Surname',
  };


  beforeEach(async(() => {
    const spyUserService = jasmine.createSpyObj('UserService', ['register']);
    const spySnackBar = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']);
    const spyRouter = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      declarations: [ HomePageComponent,
        DummyUploadComponent,
        EventComponent,
        SingleEventComponent,
        CompositeEventComponent,
        NavbarComponent,
        AddLocationComponent,
        EventListItemComponent,
        NavbarComponent,
        EventListComponent,
        SpaceLayoutGridComponent,
        SelectSeatComponent,
        LocationViewComponent,
        AddSpaceComponent,
        RegisterUserComponent,
        PasswordConfirmationValidatorDirective,
        AddSingleEventComponent,
        AddCompositeEventComponent,
        EventTreeComponent,
        AddEventComponent,
        SpaceLayoutSectorsComponent,
        PayFormComponent,
        AutoCompleteValidatorDirective,
        StartDateValidatorDirective,
        ReservationComponent,
        LoginComponent,
        UnauthenticatedComponent,
        EndDateValidatorDirective,
        SearchComponent,
        EventImageUploadComponent,
        ConfirmReservationComponent,
        TicketComponent,
        TicketListComponent,
        UpdateLocationComponent,
        UpdateSpaceComponent,
        ConfirmationDialogComponent,
        LocationListItemComponent,
        LocationListComponent,
        EditEventComponent,
        CompletePaymentComponent,
        CompleteSinglePaymentComponent,
        ChartViewComponent,
        EventReportSelectionDialogComponent,
        LocationReportSelectionDialogComponent,
        SnackbarComponent,
        MapComponent,
        UnauthenticatedComponent,
        SnackbarComponent,
     ],
      imports: [
        MaterialModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        NgbModule,
        AppRoutingModule,
        AngularOpenlayersModule,
        AppRoutingModule,
          RouterTestingModule.withRoutes([
            {
              path: '',
              component: HomePageComponent,
              pathMatch: 'full'
            }
          ])
      ],
      providers: [
        MatSnackBar,
        {provide: UserService, useValue: spyUserService},
        {provide: MatSnackBar, useValue: spySnackBar},
        {provide: Router, useValue: spyRouter},
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterUserComponent);
    component = fixture.componentInstance;
    userServiceSpy = TestBed.get(UserService);
    snackBarSpy = TestBed.get(MatSnackBar);
    routerSpy = TestBed.get(Router);

    component.ngOnInit();
    fixture.detectChanges();
    submitButton = fixture.debugElement.query(By.css('#registerButton'));
    registerForm = fixture.debugElement.query(By.css('form'));
    usernameInput = fixture.debugElement.query(By.css('#usernameInput'));
    emailInput = fixture.debugElement.query(By.css('#emailInput'));
    passwordInput = fixture.debugElement.query(By.css('#passwordInput'));
    confirmPasswordInput = fixture.debugElement.query(By.css('#confirmPasswordInput'));
    nameInput = fixture.debugElement.query(By.css('#nameInput'));
    surnameInput = fixture.debugElement.query(By.css('#surnameInput'));

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;
  });

  it('should create', (done) => {
    expect(component).toBeTruthy();
    done();
  });

  it('should validate register form correctly', (done) => {
    fixture.detectChanges();

    console.log(submitButton);
    expect(usernameInput.nativeElement.validity.valid).toBe(false);
    expect(emailInput.nativeElement.validity.valid).toBe(false);
    expect(passwordInput.nativeElement.validity.valid).toBe(false);
    expect(confirmPasswordInput.nativeElement.validity.valid).toBe(false);
    expect(nameInput.nativeElement.validity.valid).toBe(false);
    expect(surnameInput.nativeElement.validity.valid).toBe(false);

    component.user = user;
    component.confirmedPassword = user.password;
    fixture.detectChanges();
    usernameInput = fixture.debugElement.query(By.css('#usernameInput'));
    emailInput = fixture.debugElement.query(By.css('#emailInput'));
    passwordInput = fixture.debugElement.query(By.css('#passwordInput'));
    confirmPasswordInput = fixture.debugElement.query(By.css('#confirmPasswordInput'));
    nameInput = fixture.debugElement.query(By.css('#nameInput'));
    surnameInput = fixture.debugElement.query(By.css('#surnameInput'));

    fixture.whenStable().then(() => {


      expect(usernameInput.nativeElement.validity.valid).toBe(true);
      expect(emailInput.nativeElement.validity.valid).toBe(true);
      expect(passwordInput.nativeElement.validity.valid).toBe(true);
      expect(confirmPasswordInput.nativeElement.validity.valid).toBe(true);
      expect(nameInput.nativeElement.validity.valid).toBe(true);
      expect(surnameInput.nativeElement.validity.valid).toBe(true);
      done();
    });


  });

  it('should call register in user service and display error', fakeAsync(() => {

    userServiceSpy.register.and.returnValue(Promise.reject('Username already exists'));
    snackBarSpy.openFromComponent.and.returnValue('success');

    component.user = user;
    component.confirmedPassword = user.password;
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#registerButton')).nativeElement.click();

    tick();
    expect(userServiceSpy.register).toHaveBeenCalledTimes(1);
    expect(snackBarSpy.openFromComponent).toHaveBeenCalledTimes(1);
  }));

  it('should call register in user service and succeed', fakeAsync( () => {

    userServiceSpy.register.and.returnValue(Promise.resolve('success'));


    component.user = user;
    component.confirmedPassword = user.password;
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#registerButton')).nativeElement.click();

    tick();
    expect(userServiceSpy.register).toHaveBeenCalledTimes(1);
    expect(routerSpy.navigateByUrl).toHaveBeenCalledTimes(1);
  }));
});
