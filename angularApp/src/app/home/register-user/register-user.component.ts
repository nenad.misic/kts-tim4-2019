import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/model/user';
import { UserService } from '../../shared/services/user.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthenticationService} from '../../shared/services/authentication.service';
import { MatSnackBar } from '@angular/material';
import { SnackbarComponent } from '../../core/snackbar/snackbar.component';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  @ViewChild('addUserForm') form: NgForm;

  user: User = new User();

  confirmedPassword: string;

  constructor(private userService: UserService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  passwordChanged() {
    this.form.controls.confirmedPassword.updateValueAndValidity();
  }

  addUser() {
    this.userService.register(this.user).then(data => {
      this.router.navigateByUrl('/login');
    }, err => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
    });
  }

}
