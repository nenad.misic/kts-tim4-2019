/* tslint:disable:prefer-const */
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { AddLocationComponent } from './add-location.component';
import {HomePageComponent} from '../../home/home-page/home-page.component';
import {DummyUploadComponent} from '../../core/dummy-upload/dummy-upload.component';
import {EventComponent} from '../../events/event/event.component';
import {SingleEventComponent} from '../../events/event/single-event/single-event.component';
import {CompositeEventComponent} from '../../events/event/composite-event/composite-event.component';
import {NavbarComponent} from '../../home/navbar/navbar.component';
import {EventListItemComponent} from '../../events/event-list/event-list-item/event-list-item.component';
import {EventListComponent} from '../../events/event-list/event-list.component';
import {SpaceLayoutGridComponent} from '../../space-layout/space-layout-grid/space-layout-grid.component';
import {SelectSeatComponent} from '../../reservations/select-seat/select-seat.component';
import {LocationViewComponent} from '../location-view/location-view.component';
import {AddSpaceComponent} from '../../space/add-space/add-space.component';
import {RegisterUserComponent} from '../../home/register-user/register-user.component';
import {PasswordConfirmationValidatorDirective} from '../../shared/directives/password-confirmation-validator.directive';
import {AddSingleEventComponent} from '../../events/add-event/add-single-event/add-single-event.component';
import {AddCompositeEventComponent} from '../../events/add-event/add-composite-event/add-composite-event.component';
import {EventTreeComponent} from '../../events/add-event/event-tree/event-tree.component';
import {AddEventComponent} from '../../events/add-event/add-event.component';
import {SpaceLayoutSectorsComponent} from '../../space-layout/space-layout-sectors/space-layout-sectors.component';
import {PayFormComponent} from '../../reservations/pay-form/pay-form.component';
import {AutoCompleteValidatorDirective} from '../../shared/directives/auto-complete-validator.directive';
import {StartDateValidatorDirective} from '../../shared/directives/start-date-validator.directive';
import {ReservationComponent} from '../../reservations/reservation/reservation.component';
import {LoginComponent} from '../../home/login/login.component';
import {UnauthenticatedComponent} from '../../core/unauthenticated/unauthenticated.component';
import {EndDateValidatorDirective} from '../../shared/directives/end-date-validator.directive';
import {SearchComponent} from '../../events/search/search.component';
import {EventImageUploadComponent} from '../../events/event-image-upload/event-image-upload.component';
import {ConfirmReservationComponent} from '../../reservations/confirm-reservation/confirm-reservation.component';
import {TicketComponent} from '../../tickets/ticket-list/ticket/ticket.component';
import {TicketListComponent} from '../../tickets/ticket-list/ticket-list.component';
import {UpdateLocationComponent} from '../update-location/update-location.component';
import {UpdateSpaceComponent} from '../../space/update-space/update-space.component';
import {ConfirmationDialogComponent} from '../../core/confirmation-dialog/confirmation-dialog.component';
import {LocationListItemComponent} from '../location-list/location-list-item/location-list-item.component';
import {LocationListComponent} from '../location-list/location-list.component';
import {EditEventComponent} from '../../events/edit-event/edit-event.component';
import {CompletePaymentComponent} from '../../reservations/complete-payment/complete-payment.component';
import {CompleteSinglePaymentComponent} from '../../reservations/complete-single-payment/complete-single-payment.component';
import {ChartViewComponent} from '../../core/chart-view/chart-view.component';
import {EventReportSelectionDialogComponent} from '../../events/event-report-selection-dialog/event-report-selection-dialog.component';
import {LocationReportSelectionDialogComponent} from '../location-report-selection-dialog/location-report-selection-dialog.component';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';
import {MapComponent} from '../../core/map/map.component';
import {MaterialModule} from '../../material/material.module';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from '../../app-routing/app-routing.module';
import {AngularOpenlayersModule} from 'ngx-openlayers';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBar} from '@angular/material';
import {LocationService} from '../../shared/services/location.service';
import {Location} from '../../shared/model/location';
import {DebugElement} from '@angular/core';

describe('AddLocationComponent', () => {
  let component: AddLocationComponent;
  let fixture: ComponentFixture<AddLocationComponent>;
  let locationServiceSpy: jasmine.SpyObj<LocationService>;
  let snackBarSpy: jasmine.SpyObj<MatSnackBar>;

  let addButton: DebugElement;
  let nameInput: DebugElement;
  let cityInput: DebugElement;
  let addressInput: DebugElement;
  let countryInput: DebugElement;

  const location: Location = {
    id: 1,
    name: 'Nova lokacija',
    address: 'Nova adresa',
    city: 'Novi garod',
    country: 'Nova drzava',
    spaces: [],
    latitude: 18,
    longitude: 41
  };

  beforeEach(async(() => {
    locationServiceSpy = jasmine.createSpyObj('LocationService', ['create']);
    snackBarSpy = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']);
    TestBed.configureTestingModule({
      declarations: [ HomePageComponent,
        DummyUploadComponent,
        EventComponent,
        SingleEventComponent,
        CompositeEventComponent,
        NavbarComponent,
        AddLocationComponent,
        EventListItemComponent,
        NavbarComponent,
        EventListComponent,
        SpaceLayoutGridComponent,
        SelectSeatComponent,
        LocationViewComponent,
        AddSpaceComponent,
        RegisterUserComponent,
        PasswordConfirmationValidatorDirective,
        AddSingleEventComponent,
        AddCompositeEventComponent,
        EventTreeComponent,
        AddEventComponent,
        SpaceLayoutSectorsComponent,
        PayFormComponent,
        AutoCompleteValidatorDirective,
        StartDateValidatorDirective,
        ReservationComponent,
        LoginComponent,
        UnauthenticatedComponent,
        EndDateValidatorDirective,
        SearchComponent,
        EventImageUploadComponent,
        ConfirmReservationComponent,
        TicketComponent,
        TicketListComponent,
        UpdateLocationComponent,
        UpdateSpaceComponent,
        ConfirmationDialogComponent,
        LocationListItemComponent,
        LocationListComponent,
        EditEventComponent,
        CompletePaymentComponent,
        CompleteSinglePaymentComponent,
        ChartViewComponent,
        EventReportSelectionDialogComponent,
        LocationReportSelectionDialogComponent,
        SnackbarComponent,
        MapComponent,
        UnauthenticatedComponent,
        SnackbarComponent,
      ],
      imports: [
        MaterialModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        NgbModule,
        AppRoutingModule,
        AngularOpenlayersModule,
        AppRoutingModule,
        RouterTestingModule.withRoutes([
          {
            path: '',
            component: HomePageComponent,
            pathMatch: 'full'
          }
        ])
      ],
      providers: [
        MatSnackBar,
        {provide: LocationService, useValue: locationServiceSpy},
        {provide: MatSnackBar, useValue: snackBarSpy},
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
    addButton = fixture.debugElement.query(By.css('#add-location'));
    nameInput = fixture.debugElement.query(By.css('#name'));
    addressInput = fixture.debugElement.query(By.css('#address'));
    cityInput = fixture.debugElement.query(By.css('#city'));
    countryInput = fixture.debugElement.query(By.css('#country'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLocationComponent);
    component = fixture.componentInstance;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
    fixture.detectChanges();
  });

  it('should create', (done) => {
    expect(component).toBeTruthy();
    done();
  });

  it('should validate add location form correctly', (done) => {
    fixture.detectChanges();
    expect(nameInput.nativeElement.validity.valid).toBe(false);
    expect(addressInput.nativeElement.validity.valid).toBe(false);
    expect(cityInput.nativeElement.validity.valid).toBe(false);
    expect(countryInput.nativeElement.validity.valid).toBe(false);
    component.location = location;
    fixture.detectChanges();

    nameInput = fixture.debugElement.query(By.css('#name'));
    addressInput = fixture.debugElement.query(By.css('#address'));
    cityInput = fixture.debugElement.query(By.css('#city'));
    countryInput = fixture.debugElement.query(By.css('#country'));

    fixture.whenStable().then(() => {
      expect(nameInput.nativeElement.validity.valid).toBe(true);
      expect(addressInput.nativeElement.validity.valid).toBe(true);
      expect(cityInput.nativeElement.validity.valid).toBe(true);
      expect(countryInput.nativeElement.validity.valid).toBe(true);
      done();
    });
  });

  it('should call create in location service and succeed', fakeAsync( () => {
    locationServiceSpy.create.and.returnValue(Promise.resolve({message: 'success'}));
    component.location = location;
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#add-location')).nativeElement.click();

    tick();
    expect(locationServiceSpy.create).toHaveBeenCalledTimes(1);
    expect(snackBarSpy.openFromComponent).toHaveBeenCalledTimes(1);
  }));
});


