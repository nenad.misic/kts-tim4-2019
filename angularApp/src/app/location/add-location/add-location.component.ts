import { Component, OnInit } from '@angular/core';
import {Location} from '../../shared/model/location';
import { LocationService } from '../../shared/services/location.service';
import { Message } from '../../shared/model/message';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.css']
})
export class AddLocationComponent implements OnInit {

  location: Location = new Location();

  constructor(private locationService: LocationService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.location.longitude = 45.246908;
    this.location.latitude = 19.845978;
  }

  addLocation() {
    this.locationService.create(this.location)
    .then((message: Message) => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: 'Added a location!', status: 'success' }, duration: 3000 });
    })
    .catch((err: any) => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
    });

  }

  updateLocationPosition(position) {
    this.location.latitude = position.lat;
    this.location.longitude = position.lng;
  }
}
