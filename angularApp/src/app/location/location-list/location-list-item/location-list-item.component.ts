import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Location} from '../../../shared/model/location';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {ConfirmationDialogComponent} from '../../../core/confirmation-dialog/confirmation-dialog.component';
import {Message} from '../../../shared/model/message';
import {LocationService} from '../../../shared/services/location.service';
import { LocationReportSelectionDialogComponent } from '../../location-report-selection-dialog/location-report-selection-dialog.component';
import { Router } from '@angular/router';
import {SnackbarComponent} from '../../../core/snackbar/snackbar.component';

@Component({
  selector: 'app-location-list-item',
  templateUrl: './location-list-item.component.html',
  styleUrls: ['./location-list-item.component.css']
})
export class LocationListItemComponent implements OnInit {

  @Input()
  location: Location;

  @Output()
  deletedEmitter = new EventEmitter<number>();

  constructor(private locationService: LocationService,
              private dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  deleteLocation(id: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((value: string) => {
      if (value === 'yes') {
        this.locationService.deleteById(id)
          .then((message: Message) => {
            if (message.message === 'Deleted') {
              this.deletedEmitter.emit(id);
            } else {
              this.snackBar.openFromComponent(SnackbarComponent,
                { data: { message: 'Cannot delete this location. Please delete its spaces first!', status: 'error' }, duration: 3000 });
            }
          })
          .catch((err: any) => {
            this.snackBar.openFromComponent(SnackbarComponent,
              { data: { message: 'Cannot delete this location. Please delete its spaces first!', status: 'error' }, duration: 3000 });
          });
      }
    });
  }


  reportLocation() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(LocationReportSelectionDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((value: any) => {
      if (value) {
        const routePath = 'chart/' + value.selectedReport + '/' + this.location.id + '/' + value.startDate + '/' + value.endDate;
        this.router.navigateByUrl(routePath);
      }
    });
  }
}
