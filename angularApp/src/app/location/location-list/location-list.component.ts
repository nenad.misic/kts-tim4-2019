import {Component, HostListener, OnInit} from '@angular/core';
import {LocationService} from '../../shared/services/location.service';
import {Location} from '../../shared/model/location';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css']
})
export class LocationListComponent implements OnInit {

  locations: Array<Location>;
  page = 1;
  pageSize = 10;
  gotAllData = false;
  searchTerm;
  searched = false;
  canSend = true;

  constructor(private locationService: LocationService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.locationService.getPage(0, this.pageSize)
      .then((locations: Array<Location>) => {
        this.locations = locations;
        this.page = 1;
        this.gotAllData = false;
        this.searched = false;
      });
  }

  onLocationDeleted(id: number) {
    this.locations = this.locations.filter(location => location.id !== id);
  }

  doSearch() {
    this.locationService.getByNamePage(this.searchTerm, 0, this.pageSize)
      .then((locations: Array<Location>) => {
        this.locations = locations;
        this.searched = true;
        this.gotAllData = false;
        this.page = 1;
      });

  }

  clearSearch() {
    this.init();
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {

    const windowHeight = 'innerHeight' in window ? window.innerHeight
      : document.documentElement.offsetHeight;
    const body = document.body, html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight,
      body.offsetHeight, html.clientHeight,
      html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight && !this.gotAllData && this.canSend) {
      this.canSend = false;
      if (!this.searched) {
        this.locationService.getPage(this.page, this.pageSize)
          .then((locations: Array<Location>) => {
            locations.forEach(newLocation => {
              this.locations.push(newLocation);
            });
            this.page++;
            this.canSend = true;
            if (locations.length === 0) {
              this.gotAllData = true;
            }
          });
      } else {
        this.locationService.getByNamePage(this.searchTerm, this.page, this.pageSize)
          .then((locations: Array<Location>) => {
            locations.forEach(newLocation => {
              this.locations.push(newLocation);
            });
            this.page++;
            this.canSend = true;
            if (locations.length === 0) {
              this.gotAllData = true;
            }
          });
      }
    }
  }
}
