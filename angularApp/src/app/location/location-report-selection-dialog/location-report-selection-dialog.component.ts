import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Form, NgForm } from '@angular/forms';

@Component({
  selector: 'app-location-report-selection-dialog',
  templateUrl: './location-report-selection-dialog.component.html',
  styleUrls: ['./location-report-selection-dialog.component.css']
})
export class LocationReportSelectionDialogComponent implements OnInit {

  @ViewChild('chooseTypeForm') form: NgForm;

  constructor(private dialogRef: MatDialogRef<LocationReportSelectionDialogComponent>) { }

  startDate: string;
  endDate: string;
  selectedReport: string;
  startTime =  {hour: 0, minute: 0};
  endTime = {hour: 0, minute: 1};
  ngOnInit() {
  }

  submit() {
    const retVal: any = {};
    retVal.startDate = new Date(this.startDate).getTime();
    retVal.endDate = new Date(this.endDate).getTime();
    retVal.selectedReport = this.selectedReport;
    this.dialogRef.close(retVal);
  }

  cancel() {
    this.dialogRef.close();
  }

  startTimeChanged() {
    setTimeout(() => {
      if (this.form.controls.startDate && this.form.controls.endDate) {
        this.form.controls.endDate.updateValueAndValidity();
      }
     }, 50);
  }
}
