import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Location} from '../../shared/model/location';
import {LocationService} from '../../shared/services/location.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {ConfirmationDialogComponent} from '../../core/confirmation-dialog/confirmation-dialog.component';
import {Message} from '../../shared/model/message';
import {SpaceService} from '../../shared/services/space.service';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';

@Component({
  selector: 'app-location-view',
  templateUrl: './location-view.component.html',
  styleUrls: ['./location-view.component.css']
})
export class LocationViewComponent implements OnInit {

  location: Location;

  constructor(private locationService: LocationService,
              private spaceService: SpaceService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.locationService.getById(id)
      .then((location: Location) => this.location = location);
  }

  deleteSpace(id: number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((value: string) => {
      if (value === 'yes') {
        this.spaceService.deleteById(id)
          .then((message: Message) => {
            if (message.message === 'Deleted') {
              this.location.spaces = this.location.spaces.filter(space => space.id !== id);
            } else {
              this.snackBar.openFromComponent(SnackbarComponent, { data: { message: message, status: 'error' }, duration: 3000 });
            }
          })
          .catch((err: any) => {
            this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
          });
      }
    });
  }
}
