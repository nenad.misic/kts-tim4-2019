import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddLocationComponent} from './add-location/add-location.component';
import {LocationViewComponent} from './location-view/location-view.component';
import {UpdateLocationComponent} from './update-location/update-location.component';
import {LocationListItemComponent} from './location-list/location-list-item/location-list-item.component';
import {LocationListComponent} from './location-list/location-list.component';
import {LocationReportSelectionDialogComponent} from './location-report-selection-dialog/location-report-selection-dialog.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {SpaceModule} from '../space/space.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    CoreModule,
    AppRoutingModule,
    SpaceModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [
    AddLocationComponent,
    LocationViewComponent,
    UpdateLocationComponent,
    LocationListItemComponent,
    LocationListComponent,
    LocationReportSelectionDialogComponent,
  ]
})
export class LocationModule { }
