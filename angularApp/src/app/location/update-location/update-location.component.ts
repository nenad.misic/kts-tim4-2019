import {Component, Input, OnInit} from '@angular/core';
import {Location} from '../../shared/model/location';
import {LocationService} from '../../shared/services/location.service';
import {Message} from '../../shared/model/message';
import {ActivatedRoute} from '@angular/router';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-update-location',
  templateUrl: './update-location.component.html',
  styleUrls: ['./update-location.component.css']
})
export class UpdateLocationComponent implements OnInit {

  location: Location;
  loaded = false;

  constructor(private locationService: LocationService,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.locationService.getById(id)
      .then((location: Location) => {
        this.location = location;
        this.loaded = true;
      });
  }

  updateLocation() {
    console.log(this.location);
    this.locationService.update(this.location)
      .then((message: Message) => {
        this.snackBar.openFromComponent(SnackbarComponent,
          { data: { message: 'Updated the location!', status: 'success' }, duration: 3000 });
      })
      .catch((err: any) => {
        this.snackBar.openFromComponent(SnackbarComponent,
          { data: { message: err, status: 'error' }, duration: 3000 });
      });
  }


  updateLocationPosition(position) {
    this.location.latitude = position.lat;
    this.location.longitude = position.lng;
  }

}
