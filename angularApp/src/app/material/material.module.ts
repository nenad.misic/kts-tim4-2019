import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {
  MatButtonModule, MatInputModule, MatRadioModule, MatToolbarModule,
  MatSelectModule, MatAutocompleteModule, MatDatepickerModule,
  MatNativeDateModule, MatTreeModule, MatIconModule, MatDialogModule,
  MatStepperModule, MatTooltipModule, MatTableModule, MatProgressSpinnerModule, MatSnackBarModule
} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatDividerModule,
    MatRadioModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTreeModule,
    MatIconModule,
    MatDialogModule,
    MatStepperModule,
    MatTooltipModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgbModule,
  ],
  exports: [
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatDividerModule,
    MatRadioModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTreeModule,
    MatIconModule,
    MatDialogModule,
    MatStepperModule,
    MatTooltipModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgbModule,
  ],
  declarations: []
})
export class MaterialModule { }
