import { PaymentService } from '../../shared/services/payment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-complete-payment',
  templateUrl: './complete-payment.component.html',
  styleUrls: ['./complete-payment.component.css']
})
export class CompletePaymentComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private paymentService: PaymentService,
              private router: Router) { }

  ngOnInit() {
    const paymentId = this.route.snapshot.queryParams['paymentId'];
    const payerId = this.route.snapshot.queryParams['PayerID'];
    this.paymentService.completePaymentTickets({
      paymentId,
      payerId,
      tickets: JSON.parse(localStorage.getItem('tickets'))
    }).then(data => {
      this.router.navigateByUrl('/');
    }, err => {
      console.log(err.message);
    });
  }

}
