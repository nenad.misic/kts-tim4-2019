import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentService } from '../../shared/services/payment.service';

@Component({
  selector: 'app-complete-single-payment',
  templateUrl: './complete-single-payment.component.html',
  styleUrls: ['./complete-single-payment.component.css']
})
export class CompleteSinglePaymentComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private paymentService: PaymentService,
              private router: Router) { }

  ngOnInit() {
    const paymentId = this.route.snapshot.queryParams['paymentId'];
    const payerId = this.route.snapshot.queryParams['PayerID'];
    this.paymentService.completePaymentTicket({
      paymentId,
      payerId,
      ticket: JSON.parse(localStorage.getItem('ticket'))
    }).then(data => {
      this.router.navigateByUrl('/');
    }, err => {
      console.log(err.message);
    });
  }
}
