import { PaymentService } from '../../shared/services/payment.service';
import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { Router } from '@angular/router';
import { ImageService } from '../../shared/services/image.service';
import { environment } from 'src/environments/environment';
import { EventService } from '../../shared/services/event.service';
import { Ticket } from '../../shared/model/ticket';
import { Event } from '../../shared/model/event';
import { SingleEvent } from '../../shared/model/single-event';
import { TicketService } from '../../shared/services/ticket.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-confirm-reservation',
  templateUrl: './confirm-reservation.component.html',
  styleUrls: ['./confirm-reservation.component.css']
})
export class ConfirmReservationComponent implements OnInit {

  @Input()
  allSingleEvents: Array<SingleEvent>;
  @Input()
  tickets: Array<Ticket>;
  @Input()
  event: Event;
  @Input()
  totalPrice: number;

  called: number;
  singleEventTicketMap: Array<[SingleEvent, Array<Ticket>]> = [];
  columnsTicket = ['section', 'row', 'seat', 'price'];

  image = 'https://i.ytimg.com/vi/MFd18sF8CNU/maxresdefault.jpg';
  loaded = false;

  constructor(private router: Router,
              private imageService: ImageService,
              private eventService: EventService,
              private ticketService: TicketService,
              private paymentService: PaymentService,
              private location: Location) { }

  ngOnInit() {
    this.called = 0;
    this.imageService.getImage(this.event.id).then(data => {
      this.image = environment.imageEndpoint + data.message;
    });
  }

  payNow() {
    this.paymentService.payForTickets(this.tickets).then(data => {
      localStorage.setItem('tickets', JSON.stringify(this.tickets));
      window.location.href = data.redirect_url;
    });
  }

  confirmReservation() {
    this.ticketService.makeReservation(this.tickets).then(data => {
      this.router.navigateByUrl('tickets');
    }, err => { alert(err.message); });
  }

  cancelReservation() {
    // todo
    console.log('oh nooooo');
  }

  containsTicketForSingleEvent(se: SingleEvent): boolean {
    return this.tickets.findIndex((t) => t.singleEventId === se.id) !== -1;
  }
}
