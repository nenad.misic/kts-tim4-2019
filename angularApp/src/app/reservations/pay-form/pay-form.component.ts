import { Component, OnInit } from '@angular/core';
import { PaymentInfo } from '../../shared/model/payment-info';
import { PaymentService } from '../../shared/services/payment.service';
import { Message } from '../../shared/model/message';

@Component({
  selector: 'app-pay-form',
  templateUrl: './pay-form.component.html',
  styleUrls: ['./pay-form.component.css']
})
export class PayFormComponent implements OnInit {

  ticketIds: Array<number> = [10];
  payment: PaymentInfo = new PaymentInfo();

  constructor(private paymentService: PaymentService) { }

  ngOnInit() {
  }

  pay() {
    this.paymentService.pay(this.payment, this.ticketIds[0])
    .then((message: Message) => {
      alert('Succesfully paid');
    })
    .catch((err: any) => {
      alert('Failed to pay for tickets');
    });
  }

  payLater() {
    console.log('payLater');
  }

}
