import {Component, Input, OnInit} from '@angular/core';
import {SingleEvent} from '../../shared/model/single-event';
import { Event } from '../../shared/model/event';
import {EventService} from '../../shared/services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CompositeEvent} from '../../shared/model/composite-event';
import {Ticket} from '../../shared/model/ticket';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  totalPrice = 0;

  events: Array<SingleEvent>;

  event: Event;

  ticketList: Array<Ticket>;

  changePrice = ((amount: number) => {
    console.log('hehe');
    this.totalPrice = this.totalPrice + amount;
  });

  constructor(private eventService: EventService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    console.log('heh2e');
    this.totalPrice = 0;
    this.ticketList = [];
    this.events = [];
    const eventId = this.route.snapshot.params['eventId'];
    this.eventService.getById(eventId).then((event) => {
      this.events = this.fillEvents(event);
      this.event = event;
    });
  }



  fillEvents(event) {
    const retval = [];
    if (event.classType === 'single_event') {
      return [event];
    } else {
      const compositeEvent: CompositeEvent = event as CompositeEvent;
      for ( const e of compositeEvent.childEvents ) {
        retval.push(this.fillEvents(e));
      }
    }
    return [].concat(...retval);
  }

  sortEvents() {
    return this.events.sort((e1, e2) => e1.id - e2.id);
  }

}
