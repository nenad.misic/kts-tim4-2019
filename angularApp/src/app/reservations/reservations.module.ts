import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReservationComponent} from './reservation/reservation.component';
import {ConfirmReservationComponent} from './confirm-reservation/confirm-reservation.component';
import {SelectSeatComponent} from './select-seat/select-seat.component';
import {PayFormComponent} from './pay-form/pay-form.component';
import {ConfirmationDialogComponent} from '../core/confirmation-dialog/confirmation-dialog.component';
import {CompletePaymentComponent} from './complete-payment/complete-payment.component';
import {CompleteSinglePaymentComponent} from './complete-single-payment/complete-single-payment.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
  ],
  declarations: [
    ReservationComponent,
    ConfirmReservationComponent,
    SelectSeatComponent,
    PayFormComponent,
    ConfirmationDialogComponent,
    CompletePaymentComponent,
    CompleteSinglePaymentComponent,
  ]
})
export class ReservationsModule { }
