import {Component, Input, OnInit} from '@angular/core';
import {SingleEvent} from '../../shared/model/single-event';
import {GroundFloor} from '../../shared/model/ground-floor';
import {EventService} from '../../shared/services/event.service';
import {SeatSpaceService} from '../../shared/services/seat-space.service';
import {Ticket} from '../../shared/model/ticket';
import {SeatSpace} from '../../shared/model/seat-space';

@Component({
  selector: 'app-select-seat',
  templateUrl: './select-seat.component.html',
  styleUrls: ['./select-seat.component.css']
})
export class SelectSeatComponent implements OnInit {

  @Input()
  eventId: number;

  @Input()
  ticketList: Array<Ticket>;

  @Input()
  changePrice: Function;

  numberOfSeats: Array<Array<number>>;
  confirmedNumber: Array<Array<boolean>>;

  event: SingleEvent;

  rowNumber: number;
  columnNumber: number;
  rows: Array<number>;
  columns: Array<number>;

  cellClasses: Array<Array<string>>;

  selected: Array<Array<boolean>>;

  groundFloorHover: Array<Array<boolean>>;


  selectedGroundFloors: Array<GroundFloor>;

  takenSeats: Array<Array<boolean>>;

  ticketMatrix: Array<Array<Ticket>>;



  constructor(private eventService: EventService,
              private seatSpaceService: SeatSpaceService) { }

  ngOnInit() {
    // @ts-ignore
    this.eventService.getById(this.eventId).then(async (event) => {
      this.event = event as SingleEvent;
      console.log(JSON.stringify(this.event));
      this.rowNumber = this.findHeight();
      this.columnNumber = this.findWidth();
      this.numberOfSeats = Array(this.rowNumber).fill(0)
        .map((x, i) => Array(this.columnNumber).fill(0));
      this.confirmedNumber = Array(this.rowNumber).fill(0)
        .map((x, i) => Array(this.columnNumber).fill(false));
      this.rows = Array(this.rowNumber).fill(0).map((x, i) => i);
      this.columns = Array(this.columnNumber).fill(0).map((x, i) => i);
      this.selected = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));
      this.cellClasses = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill('empty'));
      this.groundFloorHover = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));


      console.log('matrix');
      this.ticketMatrix = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(null));


      this.takenSeats = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));

      const fetchedSeatSpaces: Array<SeatSpace> = [];
      for (const ticket of this.event.tickets) {
        if (ticket.seatSpaceId) {
          let seatSpace;
          if (fetchedSeatSpaces.findIndex((ss) => ss.id === ticket.seatSpaceId) === -1) {
            seatSpace = await this.seatSpaceService.getById(ticket.seatSpaceId);
            fetchedSeatSpaces.push(seatSpace);
          } else {
            seatSpace = fetchedSeatSpaces.find((ss) => ss.id === ticket.seatSpaceId);
          }
          this.ticketMatrix[ticket.row + seatSpace.top][ticket.seat + seatSpace.left] = ticket;
          if (ticket.userId) {           // may be problem
            this.takenSeats[ticket.row + seatSpace.top][ticket.seat + seatSpace.left] = true;
          }
        }
      }

      this.selectedGroundFloors = [];
      this.fillClasses();

    });
  }


  fillClasses() {
    console.log('fill');
    for (const groundfloor of this.event.spaceLayout.groundFloors) {
      for (let i = groundfloor.top; i < groundfloor.top + groundfloor.height; i++) {
        for (let j = groundfloor.left; j < groundfloor.left + groundfloor.width; j++) {
          this.cellClasses[i][j] = 'groundfloor';
        }
      }
    }
    for (const seatspace of this.event.spaceLayout.seatSpaces) {
      for (let i = seatspace.top; i < seatspace.top + seatspace.rows; i++) {
        for (let j = seatspace.left; j < seatspace.left + seatspace.seats; j++) {
          this.cellClasses[i][j] = 'seatspace';
        }
      }
    }
  }
  findWidth() {
    let maxWidthGroundFloor = null;
    let maxWidthSeatSpace = null;

    if (this.event.spaceLayout.groundFloors.length > 0) {
      maxWidthGroundFloor = this.event.spaceLayout.groundFloors
        .reduce((prev, current) => (prev.width + prev.left > current.width + current.left) ? prev : current);
    }

    if (this.event.spaceLayout.seatSpaces.length > 0 ) {
      maxWidthSeatSpace = this.event.spaceLayout.seatSpaces
        .reduce((prev, current) => (prev.seats + prev.left > current.seats + current.left) ? prev : current);
    }

    const maxGroundFloorWidth = maxWidthGroundFloor == null ? 1 : maxWidthGroundFloor.left + maxWidthGroundFloor.width;
    const maxSeatSpaceWidth = maxWidthSeatSpace == null ? 1 : maxWidthSeatSpace.left + maxWidthSeatSpace.seats;

    return Math.max(maxGroundFloorWidth, maxSeatSpaceWidth);
  }

  findHeight() {
    let maxHeightGroundFloor = null;
    let maxHeightSeatSpace = null;

    if (this.event.spaceLayout.groundFloors.length > 0) {
      maxHeightGroundFloor = this.event.spaceLayout.groundFloors
        .reduce((prev, current) => (prev.height + prev.top > current.height + current.top) ? prev : current);
    }

    if (this.event.spaceLayout.seatSpaces.length > 0 ) {
      maxHeightSeatSpace = this.event.spaceLayout.seatSpaces
        .reduce((prev, current) => (prev.rows + prev.top > current.rows + current.top) ? prev : current);
    }
    const maxGroundFloorHeight = maxHeightGroundFloor == null ? 1 : maxHeightGroundFloor.top + maxHeightGroundFloor.height;
    const maxSeatSpaceHeight = maxHeightSeatSpace == null ? 1 : maxHeightSeatSpace.top + maxHeightSeatSpace.rows;

    return Math.max(maxGroundFloorHeight, maxSeatSpaceHeight);
  }

  onSeatClick(row, column) {
    if (!this.takenSeats[row][column]) {
      if (this.cellClasses[row][column] === 'seatspace') {
        this.selected[row][column] = !this.selected[row][column];
        if (this.selected[row][column]) {
          this.ticketList.push(this.ticketMatrix[row][column]);
          this.changePrice(this.ticketMatrix[row][column].price);
        } else {
          this.ticketList.splice(this.ticketList.indexOf(this.ticketMatrix[row][column]), 1);
          this.changePrice(-this.ticketMatrix[row][column].price);
        }

      } else if (this.cellClasses[row][column] === 'groundfloor' ) {
        if (this.groundFloorFull(row, column)) {
          return;
        }
        const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
          return row >= groundFloor.top &&
            row <= groundFloor.height + groundFloor.top &&
            column >= groundFloor.left &&
            column <= groundFloor.width + groundFloor.left;
        })[0];


        const indexOfFilt = this.selectedGroundFloors.indexOf(filteredGroundfloor);
        if (indexOfFilt === -1) {
          this.selectedGroundFloors.push(filteredGroundfloor);
        } else {
          this.selectedGroundFloors.splice(indexOfFilt, 1);
        }

        for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
          for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
            this.selected[i][j] = !this.selected[i][j];
          }
        }
      }
    }
  }

  onMouseEnter(row, column) {
    if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];

      for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
        for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
          this.groundFloorHover[i][j] = true;
        }
      }
    }
  }

  onMouseLeave(row, column) {
    if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];

      for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
        for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
          this.groundFloorHover[i][j] = false;
        }
      }
    }
  }

  confirmNumberOfSeats(groundFloor: GroundFloor) {
    if (this.numberOfSeats[groundFloor.top][groundFloor.left] > 0) {
      for (let i = 0; i < this.numberOfSeats[groundFloor.top][groundFloor.left]; i++) {
        const ticket: Ticket = {
          id: null,
          seatSpace: null,
          seatSpaceId: null,
          groundFloor: null,
          groundFloorId: groundFloor.id,
          seat: null,
          row: null,
          singleEventId: this.event.id,
          expirationDate: null,
          paid: false,
          price: groundFloor.price,
          singleEvent: null,
          user: null,
          userId: 9944

        };
        this.ticketList.push(ticket);
        this.changePrice(ticket.price);
      }
    }

    this.confirmedNumber[groundFloor.top][groundFloor.left] = true;

  }

  groundFloorFull(row, column) {
    if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];

      if (filteredGroundfloor.capacity <= filteredGroundfloor.spotsReserved) {
        return true;
      }

    }
  }
}
