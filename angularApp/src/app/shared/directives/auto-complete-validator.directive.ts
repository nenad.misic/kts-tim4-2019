import { Directive } from '@angular/core';
import { ValidatorFn, AbstractControl, Validator, NG_VALIDATORS } from '@angular/forms';
import {ValidationErrors} from '@angular/forms';

function validateAutoCompleteFactory(): ValidatorFn {
  const error = {
    optionSelected: true
  };
  return (c: AbstractControl) => {
    if (c.value == null) {
      return error;
    } else if (typeof c.value === 'string') {
      return error;
    } else if (typeof c.value === 'object') {
      return null;
    } else {
      return error;
    }
  };
}


@Directive({
  selector: '[appAutoCompleteValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: AutoCompleteValidatorDirective, multi: true}]
})
export class AutoCompleteValidatorDirective {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateAutoCompleteFactory();
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.validator(control);
  }
}
