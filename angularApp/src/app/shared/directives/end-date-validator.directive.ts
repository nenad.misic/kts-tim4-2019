import { Directive, OnChanges, Input, ViewChild } from '@angular/core';
import { ValidatorFn, AbstractControl, Validator, NG_VALIDATORS, NgForm } from '@angular/forms';
import {ValidationErrors} from '@angular/forms';


@Directive({
  selector: '[appEndDateValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: EndDateValidatorDirective, multi: true}]
})
export class EndDateValidatorDirective implements Validator {

  @Input() startDate: string;
  @Input() startTime: any;
  @Input() endTime: any;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {

    const err: any = {};

    if (this.endTime) {
      if (this.startTime && this.startDate) {
        const startDate: Date = new Date(this.startDate);
        startDate.setHours(this.startTime.hour);
        startDate.setMinutes(this.startTime.minute);

        const endDate = new Date(control.value);
        endDate.setHours(this.endTime.hour);
        endDate.setMinutes(this.endTime.minute);
        if (startDate.getTime() >= endDate.getTime()) {
          err.endDateBeforeStartTime = true;
        }
      }
    } else {
      err.endTimeRequired = true;
    }
    return err;
  }
}


