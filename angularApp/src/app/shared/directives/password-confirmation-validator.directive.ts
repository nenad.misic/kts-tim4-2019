import { Directive, OnChanges, Input } from '@angular/core';
import { ValidatorFn, AbstractControl, Validator, NG_VALIDATORS } from '@angular/forms';
import {ValidationErrors} from '@angular/forms';




//
@Directive({
  selector: '[appPasswordConfirmationValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordConfirmationValidatorDirective, multi: true}]
})
export class PasswordConfirmationValidatorDirective implements Validator {

  @Input() originalPassword: string;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
    if (control.value !== this.originalPassword) {
      return { passwordConfirmation: true };
    } else {
      return null;
    }
  }

}
