import { Directive, OnChanges, Input, ViewChild } from '@angular/core';
import { ValidatorFn, AbstractControl, Validator, NG_VALIDATORS, NgForm } from '@angular/forms';
import {ValidationErrors} from '@angular/forms';


@Directive({
  selector: '[appStartDateValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: StartDateValidatorDirective, multi: true}]
})
export class StartDateValidatorDirective implements Validator {

  @Input() startTime: any;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
    const err: any = {};
    if (this.startTime) {
      const toValidate: Date = new Date(control.value);
      toValidate.setHours(this.startTime.hour);
      toValidate.setMinutes(this.startTime.minute);
      if (toValidate.getTime() < new Date().getTime()) {
        err.startDateBeforeNow = true;
      }
    } else {
      err.startTimeRequired = true;
    }
    return err;
  }
}
