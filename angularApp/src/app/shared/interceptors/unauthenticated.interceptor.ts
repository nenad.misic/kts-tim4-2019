// errors-handler.ts
import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class UnauthenticatedHandler implements HttpInterceptor {

  constructor(private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(catchError( err => {
      if (err.status === 403) {
        this.router.navigateByUrl('/unauthenticated');
      }
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
