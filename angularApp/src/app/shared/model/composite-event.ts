import { Event } from './event';

export class CompositeEvent extends Event {
    childEvents: Event[];
}
