export class DataPoint {
  y: number;
  label: string;
  constructor(y: number, label: string) {
    this.y = y;
    this.label = label;
  }
}
