export enum EventType {
    Concert, Movie, Music
}
export enum SearchMode {
  Regular, Quick
}
