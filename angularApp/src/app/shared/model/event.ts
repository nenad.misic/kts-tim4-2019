import {CompositeEvent} from './composite-event';

export class Event {
    id: number;
    name: string;
    description: string;
    type: string;
    parentEvent: CompositeEvent;
    classType: string;
}
