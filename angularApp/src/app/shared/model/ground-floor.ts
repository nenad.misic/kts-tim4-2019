import {Ticket} from './ticket';
import {SpaceLayout} from './space-layout';

export class GroundFloor {
  id: number;
  capacity: number;
  spotsReserved: number;
  price: number;
  height: number;
  width: number;
  tickets: Array<Ticket>;
  spaceLayout: SpaceLayout;
  spaceLayoutId: number;
  top: number;
  left: number;
  name: string;
}
