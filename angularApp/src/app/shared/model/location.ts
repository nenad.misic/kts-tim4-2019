import {Space} from './space';

export class Location {
  id: number;
  name: string;
  address: string;
  city: string;
  country: string;
  spaces: Array<Space>;
  latitude: number;
  longitude: number;
}
