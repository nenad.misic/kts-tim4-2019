export class PaymentInfo {
    cardNumber: String;
    securityNumber: String;
    expirationDate: String;
}
