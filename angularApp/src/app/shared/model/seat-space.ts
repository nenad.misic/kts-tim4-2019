import {Ticket} from './ticket';
import {SpaceLayout} from './space-layout';

export class SeatSpace {
  id: number;
  rows: number;
  seats: number;
  price: number;
  spaceLayout: SpaceLayout;
  spaceLayoutId: number;
  tickets: Array<Ticket>;
  top: number;
  left: number;
  name: string;
}
