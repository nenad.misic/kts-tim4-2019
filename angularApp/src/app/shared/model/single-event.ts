import { SpaceLayout } from './space-layout';
import { Space } from './space';
import { Ticket } from './ticket';
import { Event } from './event';

export class SingleEvent extends Event {
    state: string;
    startDate: any;
    endDate: any;
    tickets: Array<Ticket>;
    spaceId: number;
    space: Space;
    spaceLayoutId: number;
    spaceLayout: SpaceLayout;
}
