import { Space } from './space';
import { SeatSpace } from './seat-space';
import { GroundFloor } from './ground-floor';
import { SingleEvent } from './single-event';

export class SpaceLayout {
    id: number;
    name: string;
    space: Space;
    spaceId: number;
    seatSpaces: Array<SeatSpace>;
    groundFloors: Array<GroundFloor>;
    singleEvent: SingleEvent;
    eventId: number;
}
