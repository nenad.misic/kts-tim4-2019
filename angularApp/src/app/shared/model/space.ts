import {SpaceLayout} from './space-layout';
import {SingleEvent} from './single-event';
import { Location } from './location';

export class Space {
  id: number;
  name: string;
  location: Location;
  locationId: number;
  defaultSpaceLayouts: Array<SpaceLayout>;
  events: Array<SingleEvent>;

}
