import { User } from './user';
import { GroundFloor } from './ground-floor';
import { SeatSpace } from './seat-space';
import { SingleEvent } from './single-event';

export class Ticket {
    price: number;
    id: number;
    paid: boolean;
    expirationDate: Date;
    user: User;
    userId: number;
    groundFloor: GroundFloor;
    groundFloorId: number;
    seatSpace: SeatSpace;
    seatSpaceId: number;
    singleEventId: number;
    singleEvent: SingleEvent;
    row: number;
    seat: number;
}

