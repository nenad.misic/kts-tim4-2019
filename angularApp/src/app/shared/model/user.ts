import { Ticket } from './ticket';

export class User {
    id?: number;
    username: string;
    email: string;
    password: string;
    name: string;
    surname: string;
    role?: string;
    tickets?: Array<Ticket>;
}
