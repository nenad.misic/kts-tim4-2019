import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Event  } from '../model/event';


@Injectable({
  providedIn: 'root'
})
export class AddEventService {

  private eventSubject: Subject<Event>;
  eventObservable: Observable<Event>;

  private elementClickedSubject: Subject<string>;
  elementClickedObservable: Observable<string>;

  constructor() {
    this.eventSubject = new Subject<Event>();
    this.eventObservable = this.eventSubject.asObservable();
    this.elementClickedSubject = new Subject<string>();
    this.elementClickedObservable = this.elementClickedSubject.asObservable();
  }

  pushNewEvent(event: Event) {
    this.eventSubject.next(event);
  }

  pushElementClicked(elementName: string) {
    this.elementClickedSubject.next(elementName);
  }

}
