import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DummyUploadService {

  constructor(private httpClient: HttpClient) { }

  postFile(eventId: string, fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('eventId', eventId);
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.httpClient.post('http://localhost:8081/api/image/upload', formData);
  }
}
