import { TestBed } from '@angular/core/testing';

import { EventService } from './event.service';
import { HttpClient } from '@angular/common/http';
import { Event } from '../model/event';
import {of} from 'rxjs';

describe('EventService', () => {

  let eventService: EventService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(() => {

    const spy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      providers: [EventService, {provide: HttpClient, useValue: spy}]
    });

    eventService = TestBed.get(EventService);
    httpClientSpy = TestBed.get(HttpClient);
  });

  const event: Event = {
    id: 1,
    name: 'Name',
    description: 'Description',
    type: 'Movie',
    classType: 'CompositeEvent',
    parentEvent: null
  };

  it('should be created', () => {
    const service: EventService = TestBed.get(EventService);
    expect(service).toBeTruthy();
  });

  it('shoud post composite event once', () => {
    httpClientSpy.post.and.returnValue(of(({status: 'success'})));

    eventService.create(event).then(result => {
      expect(result).toEqual({status: 'success'});
    });

    expect(httpClientSpy.post.calls.count()).toBe(1, 'oneCall');
  });


});
