import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Message } from '../model/message';
import { Event } from '../model/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<Event>> {
    return (this.httpClient.get(environment.apiEndpoint + 'event') as Observable<Array<Event>>).toPromise();
  }

  getPage(page: number, size: number): Promise<Array<Event>> {
    // tslint:disable-next-line:max-line-length
    return (this.httpClient.get(environment.apiEndpoint + 'event/page?page=' + page + '&size=' + size) as Observable<Array<Event>>).toPromise();
  }

  getById(id: number): Promise<Event> {
    return (this.httpClient.get(environment.apiEndpoint + 'event/' + id) as Observable<Event>).toPromise();
  }

  create(event: Event): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'event/custom', event) as Observable<Message>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'event/' + id )as Observable<Message>).toPromise();
  }

  update(event: Event): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'event/custom', event) as Observable<Message>).toPromise();
  }

  getEarnings(id: number): Promise<any> {
    return (this.httpClient.get(environment.apiEndpoint + 'event/report/earnings/' + id) as Observable<any>).toPromise();
  }

  getOccupancy(id: number): Promise<any> {
    return (this.httpClient.get(environment.apiEndpoint + 'event/report/occupancy/' + id) as Observable<any>).toPromise();
  }
}
