import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GroundFloor } from '../model/ground-floor';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Message } from '../model/message';

@Injectable({
  providedIn: 'root'
})
export class GroundFloorService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<GroundFloor>> {
    return (this.httpClient.get(environment.apiEndpoint + 'groundFloor') as Observable<Array<GroundFloor>>).toPromise();
  }

  getById(id: number): Promise<GroundFloor> {
    return (this.httpClient.get(environment.apiEndpoint + 'groundFloor/' + id) as Observable<GroundFloor>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'groundFloor/' + id) as Observable<Message>).toPromise();
  }

  create(groundFloor: GroundFloor): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'groundFloor', groundFloor) as Observable<Message>).toPromise();
  }

  update(groundFloor: GroundFloor): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'groundFloor', groundFloor) as Observable<Message>).toPromise();
  }
}
