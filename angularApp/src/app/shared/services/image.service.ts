import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  imageUrl = 'http://localhost:8081/api/image/';

  constructor(private httpClient: HttpClient) { }


  postFile(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.httpClient.post('http://localhost:8081/api/upload', formData);
  }

  getImage(eventId: number): Promise<any> {
    return (this.httpClient.get(this.imageUrl + eventId) as Observable<any>).toPromise();
  }

  getAllImages(eventId: number): Promise<any> {
    return (this.httpClient.get(this.imageUrl + 'all/' + eventId) as Observable<any>).toPromise();
  }
}
