import { TestBed } from '@angular/core/testing';

import { LocationService } from './location.service';
import {HttpClient} from '@angular/common/http';
import {Location} from '../model/location';
import {of} from 'rxjs';

describe('LocationService', () => {

  let locationService: LocationService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete', 'put']);
    TestBed.configureTestingModule({
      providers: [LocationService, {provide: HttpClient, useValue: httpSpy}]
    });
    locationService = TestBed.get(LocationService);
    httpClientSpy = TestBed.get(HttpClient);
  });

  const location: Location = {
    id: 1,
    name: 'Nova lokacija',
    address: 'Nova adresa',
    city: 'Novi garod',
    country: 'Nova drzava',
    spaces: [],
    latitude: 18,
    longitude: 41
  };

  const location1: Location = {
    id: 2,
    name: 'Nova lokacija 1',
    address: 'Nova adresa 1',
    city: 'Novi garod 1',
    country: 'Nova drzava 1',
    spaces: [],
    latitude: 18,
    longitude: 41
  };

  it('should be created', () => {
    const service: LocationService = TestBed.get(LocationService);
    expect(service).toBeTruthy();
  });

  it('should get locations', () => {
    httpClientSpy.get.and.returnValue(of([location, location1]));
    locationService.getAll().then(result => {
      expect(result).toContain(location);
      expect(result).toContain(location1);
    });
  });

  it('should post location once', () => {
    httpClientSpy.post.and.returnValue(of({message: 'Location created'}));
    locationService.create(location).then(result => {
      expect(result).toEqual({message: 'Location created'});
    });
  });

  it('should get locations page', () => {
    httpClientSpy.get.and.returnValue(of([location, location1]));
    locationService.getPage(0, 2).then(result => {
      expect(result).toContain(location);
      expect(result).toContain(location1);
    });
  });

  it('should get locations page by name', () => {
    httpClientSpy.get.and.returnValue(of([location1]));
    locationService.getByNamePage('Nova lokacija 1', 0, 2).then(result => {
      expect(result).toContain(location1);
    });
  });

  it('should get location by id', () => {
    httpClientSpy.get.and.returnValue(of(location));
    locationService.getById(location.id).then(result => {
      expect(result).toEqual(location);
    });
  });

  it('should get locations earnings report', () => {
    httpClientSpy.get.and.returnValue(of({message: 'earnings report'}));
    locationService.getEarningsInPeriod(location.id, '1.1.2015', '1.1.2025').then(result => {
      expect(result).toEqual({message: 'earnings report'});
    });
  });

  it('should get locations occupancy report', () => {
    httpClientSpy.get.and.returnValue(of({message: 'occupancy report'}));
    locationService.getOccupancyInPeriod(location.id, '1.1.2015', '1.1.2025').then(result => {
      expect(result).toEqual({message: 'occupancy report'});
    });
  });

  it('should delete location', () => {
    httpClientSpy.delete.and.returnValue(of({message: 'deleted location'}));
    locationService.deleteById(location.id).then(result => {
      expect(result).toEqual({message: 'deleted location'});
    });
  });

  it('should update location', () => {
    httpClientSpy.put.and.returnValue(of({message: 'updated location'}));
    locationService.update(location).then(result => {
      expect(result).toEqual({message: 'updated location'});
    });
  });
});

