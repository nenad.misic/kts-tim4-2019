/* tslint:disable:max-line-length */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Message } from '../model/message';
import { Location } from '../model/location';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<Location>> {
    return (this.httpClient.get(environment.apiEndpoint + 'location') as Observable<Array<Location>>).toPromise();
  }

  getPage(page: number, size: number): Promise<Array<Location>> {
    return (this.httpClient.get(environment.apiEndpoint + 'location/page?page=' + page + '&size=' + size) as Observable<Array<Location>>).toPromise();
  }

  getById(id: number): Promise<Location> {
    return (this.httpClient.get(environment.apiEndpoint + 'location/' + id) as Observable<Location>).toPromise();
  }

  getByNamePage(name: string, page: number, size: number): Promise<Array<Location>> {
    return (this.httpClient.get(environment.apiEndpoint + 'location/findByNamePage?name=' + name + '&page=' + page + '&size=' + size) as Observable<Array<Location>>).toPromise();
  }

  getEarningsInPeriod(id: number, startDate: string, endDate: string): Promise<any> {
    return (this.httpClient.get(environment.apiEndpoint + 'location/report/earnings/' + id + '?startDate=' + startDate + '&endDate=' + endDate) as Observable<any>).toPromise();
  }

  getOccupancyInPeriod(id: number, startDate: string, endDate: string): Promise<any> {
    return (this.httpClient.get(environment.apiEndpoint + 'location/report/occupancy/' + id + '?startDate=' + startDate + '&endDate=' + endDate) as Observable<any>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'location/' + id) as Observable<Message>).toPromise();
  }

  create(location: Location): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'location', location) as Observable<Message>).toPromise();
  }

  update(location: Location): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'location', location) as Observable<Message>).toPromise();
  }

}
