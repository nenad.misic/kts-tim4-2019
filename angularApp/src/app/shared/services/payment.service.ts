/* tslint:disable:max-line-length */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentInfo } from '../model/payment-info';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Message } from '../model/message';
import { Ticket } from '../model/ticket';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private httpClient: HttpClient) { }

  payForTickets(tickets: Array<Ticket>) {
    return (this.httpClient.post(environment.apiEndpoint + 'ticket/pay' , tickets) as Observable<any>).toPromise();
  }

  completePaymentTickets(payment: any) {
    return (this.httpClient.post(environment.apiEndpoint + 'ticket/complete', payment) as Observable<any>).toPromise();
  }

  payForTicket(ticketId: number) {
    return (this.httpClient.post(environment.apiEndpoint + 'ticket/' + ticketId + '/pay', {}) as Observable<any>).toPromise();
  }

  completePaymentTicket(payment: any) {
    return (this.httpClient.post(environment.apiEndpoint + 'ticket/' + payment.ticket.id + '/complete', payment) as Observable<any>)
      .toPromise();
  }

  pay(paymentInfo: PaymentInfo, ticketId: number) {
    return (this.httpClient.post(environment.apiEndpoint + 'ticket/' + ticketId + '/pay', 'sumy') as Observable<Message>)
      .toPromise();
  }
}
