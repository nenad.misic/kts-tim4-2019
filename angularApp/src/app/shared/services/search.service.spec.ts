import { TestBed } from '@angular/core/testing';

import { SearchService } from './search.service';
import {HttpClient} from '@angular/common/http';

import {Event} from '../model/event';
import {of} from 'rxjs';
describe('SearchService', () => {
  let searchService: SearchService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  const events: Array<any> = [{
    id: 4,
    name: 'Turneja Zvonka Bogdana',
    description: 'Nezaboravni put Zvonka Bogdana po nasim najmilijim varosima stare Jugoslavije. Ova turneja donosi srecu i begstvo \
    od pakla svakodnevnice u kojoj se nalazi stanovnistvo naseg regiona.',
    classType: 'CompositeEvent',
    eventType: 'Concert',
    childEvents: [{
      id: 1,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }, {
      id: 2,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }, {
      id: 3,
      name: 'Koncert Zvogdana',
      description: 'Koncert klasicnih hitova naseg omiljenog tradiciolanog zabavnjaka, Zvonka Bogdana',
      classType: 'SingleEvent',
      eventType: 'Concert',
      startDate: new Date(),
      endDate: new Date(),
      state: 'ForSale',
      space: {
        name: 'Svecana sala',
        location: {
          name: 'Spens',
          city: 'Novi Sad',
          address: 'Maksima Gorkog 5',
          country: 'Serbia',
          longitude: 45.247537,
          latitude: 19.845547,
        }
      }
    }]
  }, {
    id: 5,
    name: 'Koncert Maye Berovic',
    description: 'Pored vokalnih sposobnosti, na ovom koncertu mocicete da vidite i ostale atribute lepe ' +
      'i oskudno obucene popularne folk pevacice.',
    classType: 'SingleEvent',
    eventType: 'Concert',
    startDate: new Date(),
    endDate: new Date(),
    state: 'ForSale',
    space: {
      name: 'Svecana sala',
      location: {
        name: 'Spens',
        city: 'Novi Sad',
        address: 'Maksima Gorkog 5',
        country: 'Serbia',
        longitude: 45.247537,
        latitude: 19.845547,
      }
    }
  }, {
    id: 6,
    name: 'Zikina Sarencia Live',
    description: 'Budite u publici najgledanije emisije ljudi starnosne kategorije od 65 do 68 godina!',
    classType: 'SingleEvent',
    eventType: 'TV Programme',
    startDate: new Date(),
    endDate: new Date(),
    state: 'ForSale',
    space: {
      name: 'Svecana sala',
      location: {
        name: 'Spens',
        city: 'Novi Sad',
        address: 'Maksima Gorkog 5',
        country: 'Serbia',
        longitude: 45.247537,
        latitude: 19.845547,
      }
    }
  }];

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      providers: [SearchService, {provide: HttpClient, useValue: spy}]
    });
    searchService = TestBed.get(SearchService);
    httpClientSpy = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    const service: SearchService = TestBed.get(SearchService);
    expect(service).toBeTruthy();
  });

  it('should return page (HttpClient called once)', () => {
    httpClientSpy.post.and.returnValue(of(events));

    searchService.quickSearch({name: 'sta god'}).then(
      result => expect(result).toEqual(events, 'failed'),
      fail
    );

    expect(httpClientSpy.post.calls.count()).toBe(1, 'oneCall');
  });

});
