import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Location} from '../model/location';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Event} from '../model/event';
import {SearchMode} from '../model/enums';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private httpClient: HttpClient) { }

  search(eventExample): Promise<Array<Event>> {
    return (
      this.httpClient
      .post(
        environment.apiEndpoint + 'event/search',
        eventExample,
        { params: new HttpParams().set('mode', 'Regular')}
        ) as Observable<Array<Event>>
    ).toPromise();
  }

  quickSearch(eventExample): Promise<Array<Event>> {
    return (
      this.httpClient
        .post(
          environment.apiEndpoint + 'event/search',
          eventExample,
          { params: new HttpParams().set('mode', 'Quick')}
        ) as Observable<Array<Event>>
    ).toPromise();
  }
}
