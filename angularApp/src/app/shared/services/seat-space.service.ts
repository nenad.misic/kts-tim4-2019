import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SeatSpace } from '../model/seat-space';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Message } from '../model/message';

@Injectable({
  providedIn: 'root'
})
export class SeatSpaceService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<SeatSpace>> {
    return (this.httpClient.get(environment.apiEndpoint + 'seatSpace') as Observable<Array<SeatSpace>>).toPromise();
  }

  getById(id: number): Promise<SeatSpace> {
    return (this.httpClient.get(environment.apiEndpoint + 'seatSpace/' + id) as Observable<SeatSpace>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'seatSpace/' + id) as Observable<Message>).toPromise();
  }

  create(seatSpace: SeatSpace): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'seatSpace', seatSpace) as Observable<Message>).toPromise();
  }

  update(seatSpace: SeatSpace): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'seatSpace', seatSpace) as Observable<Message>).toPromise();
  }
}
