import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpaceLayout } from '../model/space-layout';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Message } from '../model/message';

@Injectable({
  providedIn: 'root'
})
export class SpaceLayoutService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<SpaceLayout>> {
    return (this.httpClient.get(environment.apiEndpoint + 'spaceLayout') as Observable<Array<SpaceLayout>>).toPromise();
  }
  getById(id: number): Promise<SpaceLayout> {
    return (this.httpClient.get(environment.apiEndpoint + 'spaceLayout/' + id) as Observable<SpaceLayout>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'spaceLayout/' + id) as Observable<Message>).toPromise();
  }

  create(spaceLayout: SpaceLayout): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'spaceLayout', spaceLayout) as Observable<Message>).toPromise();
  }

  update(spaceLayout: SpaceLayout): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'spaceLayout', spaceLayout) as Observable<Message>).toPromise();
  }
}
