import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Message } from '../model/message';
import { Observable } from 'rxjs';
import { Space } from '../model/space';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<Space>> {
    return (this.httpClient.get(environment.apiEndpoint + 'space') as Observable<Array<Space>>).toPromise();
  }

  getById(id: number): Promise<Space> {
    return (this.httpClient.get(environment.apiEndpoint + 'space/' + id) as Observable<Space>).toPromise();
  }

  deleteById(id: number): Promise<Message> {
    return (this.httpClient.delete(environment.apiEndpoint + 'space/' + id) as Observable<Message>).toPromise();
  }

  create(space: Space): Promise<Message> {
    return (this.httpClient.post(environment.apiEndpoint + 'space', space) as Observable<Message>).toPromise();
  }

  update(space: Space): Promise<Message> {
    return (this.httpClient.put(environment.apiEndpoint + 'space', space) as Observable<Message>).toPromise();
  }
}
