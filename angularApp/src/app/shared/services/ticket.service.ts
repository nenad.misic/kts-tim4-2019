import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Ticket } from '../model/ticket';
import {Location} from '../model/location';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Promise<Array<Ticket>> {
    return (this.httpClient.get(environment.apiEndpoint + 'ticket') as Observable<Array<Ticket>>).toPromise();
  }

  getPage(page: number, size: number): Promise<Array<Ticket>> {
    return (this.httpClient.get(environment.apiEndpoint + 'ticket/page?page=' + page + '&size=' + size) as Observable<Array<Ticket>>)
      .toPromise();
  }

  getUserTickets(page: number, size: number): Promise<Array<Ticket>> {
    return (this.httpClient.get(environment.apiEndpoint + 'ticket/user?page=' + page +  '&size=' + size) as Observable<Array<Ticket>>)
      .toPromise();
  }

  makeReservation(tickets: Array<Ticket>) {
    return (this.httpClient.post(`${environment.apiEndpoint}ticket/custom`, tickets)).toPromise();
  }

  cancelReservation(ticket: Ticket) {
    return (this.httpClient.delete(`${environment.apiEndpoint}ticket/${ticket.id}`)).toPromise();
  }
}
