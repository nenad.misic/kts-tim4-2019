import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private httpClient: HttpClient) { }

  postFile(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('eventId', '9942');
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.httpClient.post('http://localhost:8081/api/upload', formData);
  }

}
