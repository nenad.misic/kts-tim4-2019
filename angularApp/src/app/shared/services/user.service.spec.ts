import { TestBed } from '@angular/core/testing';
import {HttpClient} from '@angular/common/http';

import { UserService } from './user.service';
import {of} from 'rxjs';
import { User } from '../model/user';

describe('UserService', () => {
  let userService: UserService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      providers: [UserService, {provide: HttpClient, useValue: spy}]
    });
    userService = TestBed.get(UserService);
    httpClientSpy = TestBed.get(HttpClient);
  });

  const user: User = {
    username: 'username',
    password: 'password',
    email: 'email@email.com',
    name: 'Name',
    surname: 'Surname',
  };

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('should post user once', () => {
    httpClientSpy.post.and.returnValue(of({status: 'success'}));

    userService.register(user).then(result => {
      expect(result).toEqual({status: 'success'});
    });
    expect(httpClientSpy.post.calls.count()).toBe(1, 'oneCall');
  });
});
