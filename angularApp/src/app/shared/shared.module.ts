import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PasswordConfirmationValidatorDirective} from './directives/password-confirmation-validator.directive';
import {AutoCompleteValidatorDirective} from './directives/auto-complete-validator.directive';
import {StartDateValidatorDirective} from './directives/start-date-validator.directive';
import {EndDateValidatorDirective} from './directives/end-date-validator.directive';

@NgModule({
  imports: [
    CommonModule
  ],
    exports: [
        AutoCompleteValidatorDirective,
        EndDateValidatorDirective,
        StartDateValidatorDirective,
        PasswordConfirmationValidatorDirective
    ],
  declarations: [
    PasswordConfirmationValidatorDirective,
    AutoCompleteValidatorDirective,
    StartDateValidatorDirective,
    EndDateValidatorDirective,
  ]
})
export class SharedModule { }
