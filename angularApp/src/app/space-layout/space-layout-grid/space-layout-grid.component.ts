import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SeatSpace} from '../../shared/model/seat-space';
import {GroundFloor} from '../../shared/model/ground-floor';
import {SpaceLayout} from '../../shared/model/space-layout';
import {Space} from '../../shared/model/space';
import {SpaceLayoutService} from '../../shared/services/space-layout.service';

@Component({
  selector: 'app-space-layout-grid',
  templateUrl: './space-layout-grid.component.html',
  styleUrls: ['./space-layout-grid.component.css']
})
export class SpaceLayoutGridComponent implements OnInit {

  seatSpace = '1';

  start = {row: -1, column: -1};

  rowNumber = 15;
  columnNumber = 40;

  height: number;
  width: number;
  name: string;

  rows: Array<number>;
  columns: Array<number>;

  addedSeatSpaces: Array<SeatSpace>;
  addedGroundFloors: Array<GroundFloor>;
  spaceLayout: SpaceLayout;

  addOrder = [];

  cellClasses: any;
  invalidCells: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Space,
              public dialogRef: MatDialogRef<SpaceLayoutGridComponent>,
              private spaceLayoutService: SpaceLayoutService) { }

  ngOnInit() {
    this.height = this.rowNumber;
    this.width = this.columnNumber;

    this.addedSeatSpaces = [];
    this.addedGroundFloors = [];
    this.cellClasses = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill('notsel'));
    this.invalidCells = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill(false));

    this.spaceLayout = new SpaceLayout();
    // TO-DO space;
    this.rows = Array(this.rowNumber).fill(0).map((x, i) => i);
    this.columns = Array(this.columnNumber).fill(0).map((x, i) => i);
  }

  resetInvalids() {
    this.invalidCells = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill(false));
  }
  onSeatClick(row, column) {
    if (this.clickActive()) {
      let startRow = this.start.row;
      let endRow = row;
      let startColumn = this.start.column;
      let endColumn = column;
      if (this.start.row > row) {
        startRow = row;
        endRow = this.start.row;
      }
      if (this.start.column > column) {
        startColumn = column;
        endColumn = this.start.column;
      }

      let e;
      let fail = false;
      const listOfElements = [];
      for (let i = startRow; i <= endRow; i++) {
        for (let j = startColumn; j <= endColumn; j++) {
          e = this.cellClasses[i][j];
          if (e.includes('selected')) {
            this.start = { row: -1, column: -1 };
            this.removeAllOfClass('prepared');
            this.resetInvalids();
            fail = true;
          } else {
            listOfElements.push({row: i, column: j});
          }
        }
      }

      if (!fail) {
        if (this.seatSpace === '1') {
          this.addSeatSpace(startRow, startColumn, endRow - startRow, endColumn - startColumn);
        } else {
          this.addGroundFloor(startRow, startColumn, endRow - startRow, endColumn - startColumn);
        }

        for (const elemToChange of listOfElements) {
          if (this.seatSpace === '1') {
            this.cellClasses[elemToChange.row][elemToChange.column] = 'selectedseatspace';
          } else {
            this.cellClasses[elemToChange.row][elemToChange.column] = 'selectedgroundfloor';
          }
        }
        this.start = { row: -1, column: -1 };
      }
    } else {
      if (!this.cellClasses[row][column].includes('selected')) {
        this.start = { row: row, column: column };
        this.cellClasses[row][column] = 'prepared';
      }
    }
  }

  onSeatMouseEnter(row, column) {
    this.removeAllOfClass('prepared');
    this.resetInvalids();
    if (this.clickActive()) {
      let startRow = this.start.row;
      let endRow = row;
      let startColumn = this.start.column;
      let endColumn = column;
      if (this.start.row > row) {
        startRow = row;
        endRow = this.start.row;
      }
      if (this.start.column > column) {
        startColumn = column;
        endColumn = this.start.column;
      }

      for (let i = startRow; i <= endRow; i++) {
        for (let j = startColumn; j <= endColumn; j++) {
          if (this.cellClasses[i][j].includes('selected')) {
            this.invalidCells[i][j] = true;
          } else {
            this.cellClasses[i][j] = 'prepared';
          }
        }
      }
    }
  }

  clickActive() {
    return this.start.row !== -1 && this.start.column !== -1;
  }

  addSeatSpace(top, left, height, width) {
    this.addOrder.push(true);
    // @ts-ignore
    const seatSpace: SeatSpace = {
      top: top,
      left: left,
      rows: height + 1,
      seats: width + 1,
      tickets: [],
      // spaceLayout: this.spaceLayout
    };
    this.addedSeatSpaces.push(seatSpace);
  }

  addGroundFloor(top, left, height, width) {
    this.addOrder.push(false);
    // @ts-ignore
    const groundFloor: GroundFloor = {
      top: top,
      left: left,
      height: height + 1,
      width: width + 1,
      spotsReserved: 0,
      tickets: [],
      // spaceLayout: this.spaceLayout
    };
    this.addedGroundFloors.push(groundFloor);
  }

  onConfirm() {
    let name = 'A';
    for ( const gf of this.addedGroundFloors ) {
      gf.name = name;
      name = String.fromCharCode(name.charCodeAt(0) + 1);
      gf.spaceLayoutId = this.spaceLayout.id;
    }

    for ( const ss of this.addedSeatSpaces ) {
      ss.name = name;
      name = String.fromCharCode(name.charCodeAt(0) + 1);
    }

    this.spaceLayout.groundFloors = this.addedGroundFloors;
    this.spaceLayout.seatSpaces = this.addedSeatSpaces;
    this.spaceLayout.space = this.data;
    this.spaceLayout.spaceId = this.data.id;
    this.spaceLayout.name = this.name;
    this.spaceLayoutService.create(this.spaceLayout);


  }

  onReset() {
    this.start = { row: -1, column: -1 };
    for (let i = 0; i < this.rowNumber; i++) {
      for (let j = 0; j < this.columnNumber; j++) {
       this.cellClasses[i][j] = 'notsel';
      }
    }
    this.addedSeatSpaces = [];
    this.addedGroundFloors = [];
    this.addOrder = [];
  }

  removeAllOfClass(className: string) {
    for (let i = 0; i < this.rowNumber; i++) {
      for (let j = 0; j < this.columnNumber; j++) {
        if (this.cellClasses[i][j].includes(className)) {
          this.cellClasses[i][j] = 'notsel';
        }
      }
    }
  }

  resizeAndReset() {
    this.rowNumber = this.height;
    this.columnNumber = this.width;

    this.addedSeatSpaces = [];
    this.addedGroundFloors = [];
    this.cellClasses = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill('notsel'));
    this.invalidCells = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill(false));

    this.spaceLayout = new SpaceLayout();
    // TO-DO space;
    this.rows = Array(this.rowNumber).fill(0).map((x, i) => i);
    this.columns = Array(this.columnNumber).fill(0).map((x, i) => i);
  }

  minimum(a, b) {
    return Math.min(a, b);
  }

  getScreenHeight() {
    return screen.height;
  }

  getScreenWidth() {
    return screen.width;
  }

  onUndo() {
    let top = -1;
    let left = -1;
    let height = -1;
    let width = -1;
    const lastAddedSeatSpace = this.addOrder.pop();
    if (lastAddedSeatSpace) {
      const seatSpace = this.addedSeatSpaces.pop();
      top = seatSpace.top;
      left = seatSpace.left;
      height = seatSpace.rows;
      width = seatSpace.seats;
    } else {
      const groundFloor = this.addedGroundFloors.pop();
      top = groundFloor.top;
      left = groundFloor.left;
      height = groundFloor.height;
      width = groundFloor.width;
    }
    for (let i = top; i < top + height; i++) {
      for (let j = left; j < left + width; j++) {
          this.cellClasses[i][j] = 'notsel';
      }
    }
  }
}
