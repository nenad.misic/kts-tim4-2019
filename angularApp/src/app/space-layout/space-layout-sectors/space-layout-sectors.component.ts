import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {SingleEvent} from '../../shared/model/single-event';
import {SpaceLayout} from '../../shared/model/space-layout';
import {SpaceLayoutService} from '../../shared/services/space-layout.service';

@Component({
  selector: 'app-space-layout-sectors',
  templateUrl: './space-layout-sectors.component.html',
  styleUrls: ['./space-layout-sectors.component.css']
})
export class SpaceLayoutSectorsComponent implements OnInit {

  @Input()
  event: SingleEvent;

  @Input()
  spaceLayout: SpaceLayout;

  @Output()
  selectedEmitter = new EventEmitter<any>();

  rowNumber: number;
  columnNumber: number;
  rows: Array<number>;
  columns: Array<number>;

  cellClasses: Array<Array<string>>;

  selected: Array<Array<boolean>>;

  hoveredSectors: Array<Array<boolean>>;

  selectedEntity: any;

  takenSeats: Array<Array<boolean>>;

  constructor(private spaceLayoutService: SpaceLayoutService) { }

  ngOnInit() {
      this.event.spaceLayout = this.spaceLayout;
      this.rowNumber = this.findHeight();
      this.columnNumber = this.findWidth();
      this.rows = Array(this.rowNumber).fill(0).map((x, i) => i);
      this.columns = Array(this.columnNumber).fill(0).map((x, i) => i);
      this.selected = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));
      this.cellClasses = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill('empty'));
      this.hoveredSectors = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));
      this.takenSeats = Array(this.rowNumber).fill(null)
        .map((x, i) => Array(this.columnNumber).fill(false));

      this.fillClasses();

  }


  fillClasses() {
    console.log('fill');
    for (const groundfloor of this.event.spaceLayout.groundFloors) {
      for (let i = groundfloor.top; i < groundfloor.top + groundfloor.height; i++) {
        for (let j = groundfloor.left; j < groundfloor.left + groundfloor.width; j++) {
          this.cellClasses[i][j] = 'groundfloor';
        }
      }
    }
    for (const seatspace of this.event.spaceLayout.seatSpaces) {
      for (let i = seatspace.top; i < seatspace.top + seatspace.rows; i++) {
        for (let j = seatspace.left; j < seatspace.left + seatspace.seats; j++) {
          this.cellClasses[i][j] = 'seatspace';
        }
      }
    }
  }
  findWidth() {
    let maxWidthGroundFloor = null;
    let maxWidthSeatSpace = null;

    if (this.event.spaceLayout.groundFloors.length > 0) {
      maxWidthGroundFloor = this.event.spaceLayout.groundFloors
        .reduce((prev, current) => (prev.width + prev.left > current.width + current.left) ? prev : current);
    }

    if (this.event.spaceLayout.seatSpaces.length > 0 ) {
      maxWidthSeatSpace = this.event.spaceLayout.seatSpaces
        .reduce((prev, current) => (prev.seats + prev.left > current.seats + current.left) ? prev : current);
    }

    const maxGroundFloorWidth = maxWidthGroundFloor == null ? 1 : maxWidthGroundFloor.left + maxWidthGroundFloor.width;
    const maxSeatSpaceWidth = maxWidthSeatSpace == null ? 1 : maxWidthSeatSpace.left + maxWidthSeatSpace.seats;

    return Math.max(maxGroundFloorWidth, maxSeatSpaceWidth);
  }

  findHeight() {
    let maxHeightGroundFloor = null;
    let maxHeightSeatSpace = null;

    if (this.event.spaceLayout.groundFloors.length > 0) {
      maxHeightGroundFloor = this.event.spaceLayout.groundFloors
        .reduce((prev, current) => (prev.height + prev.top > current.height + current.top) ? prev : current);
    }

    if (this.event.spaceLayout.seatSpaces.length > 0 ) {
      maxHeightSeatSpace = this.event.spaceLayout.seatSpaces
        .reduce((prev, current) => (prev.rows + prev.top > current.rows + current.top) ? prev : current);
    }
    const maxGroundFloorHeight = maxHeightGroundFloor == null ? 1 : maxHeightGroundFloor.top + maxHeightGroundFloor.height;
    const maxSeatSpaceHeight = maxHeightSeatSpace == null ? 1 : maxHeightSeatSpace.top + maxHeightSeatSpace.rows;

    return Math.max(maxGroundFloorHeight, maxSeatSpaceHeight);
  }

  onSeatClick(row, column) {
    if (this.cellClasses[row][column] === 'seatspace') {
      const filteredSeatSpace = this.event.spaceLayout.seatSpaces.filter((seatSpace) => {
        return row >= seatSpace.top &&
          row <= seatSpace.rows + seatSpace.top &&
          column >= seatSpace.left &&
          column <= seatSpace.seats + seatSpace.left;
      })[0];


      this.resetSelected();
      this.selectedEntity = filteredSeatSpace;
      this.selectedEmitter.emit(this.selectedEntity);

      for (let i = filteredSeatSpace.top; i < filteredSeatSpace.top + filteredSeatSpace.rows; i++) {
        for (let j = filteredSeatSpace.left; j < filteredSeatSpace.left + filteredSeatSpace.seats; j++) {
          this.selected[i][j] = !this.selected[i][j];
        }
      }
    } else if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];



      this.selectedEntity = filteredGroundfloor;
      this.selectedEmitter.emit(this.selectedEntity);

      this.resetSelected();
      for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
        for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
          this.selected[i][j] = true;
        }
      }
    }

  }

  onMouseEnter(row, column) {
    if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];

      for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
        for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
          this.hoveredSectors[i][j] = true;
        }
      }
    } else if (this.cellClasses[row][column] === 'seatspace') {
      const filteredSeatSpace = this.event.spaceLayout.seatSpaces.filter((seatSpace) => {
        return row >= seatSpace.top &&
          row <= seatSpace.rows + seatSpace.top &&
          column >= seatSpace.left &&
          column <= seatSpace.seats + seatSpace.left;
      })[0];

      for (let i = filteredSeatSpace.top; i < filteredSeatSpace.top + filteredSeatSpace.rows; i++) {
        for (let j = filteredSeatSpace.left; j < filteredSeatSpace.left + filteredSeatSpace.seats; j++) {
          this.hoveredSectors[i][j] = true;
        }
      }
    }
  }

  resetSelected() {
    this.selected = Array(this.rowNumber).fill(null)
      .map((x, i) => Array(this.columnNumber).fill(false));
  }
  minimum(a, b) {
    return Math.min(a, b);
  }

  getScreenHeight() {
    return screen.height;
  }

  getScreenWidth() {
    return screen.width;
  }
  onMouseLeave(row, column) {
    if (this.cellClasses[row][column] === 'groundfloor') {
      const filteredGroundfloor = this.event.spaceLayout.groundFloors.filter((groundFloor) => {
        return row >= groundFloor.top &&
          row <= groundFloor.height + groundFloor.top &&
          column >= groundFloor.left &&
          column <= groundFloor.width + groundFloor.left;
      })[0];

      for (let i = filteredGroundfloor.top; i < filteredGroundfloor.top + filteredGroundfloor.height; i++) {
        for (let j = filteredGroundfloor.left; j < filteredGroundfloor.left + filteredGroundfloor.width; j++) {
          this.hoveredSectors[i][j] = false;
        }
      }
    } else if (this.cellClasses[row][column] === 'seatspace') {
      const filteredSeatSpace = this.event.spaceLayout.seatSpaces.filter((seatSpace) => {
        return row >= seatSpace.top &&
          row <= seatSpace.rows + seatSpace.top &&
          column >= seatSpace.left &&
          column <= seatSpace.seats + seatSpace.left;
      })[0];

      for (let i = filteredSeatSpace.top; i < filteredSeatSpace.top + filteredSeatSpace.rows; i++) {
        for (let j = filteredSeatSpace.left; j < filteredSeatSpace.left + filteredSeatSpace.seats ; j++) {
          this.hoveredSectors[i][j] = false;
        }
      }
    }
  }


}
