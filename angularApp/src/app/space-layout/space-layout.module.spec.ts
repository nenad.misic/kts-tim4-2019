import { SpaceLayoutModule } from './space-layout.module';

describe('SpaceLayoutModule', () => {
  let spaceLayoutModule: SpaceLayoutModule;

  beforeEach(() => {
    spaceLayoutModule = new SpaceLayoutModule();
  });

  it('should create an instance', () => {
    expect(spaceLayoutModule).toBeTruthy();
  });
});
