import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SpaceLayoutSectorsComponent} from './space-layout-sectors/space-layout-sectors.component';
import {SpaceLayoutGridComponent} from './space-layout-grid/space-layout-grid.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [
    SpaceLayoutSectorsComponent,
    SpaceLayoutGridComponent,
  ],
  exports: [
    SpaceLayoutSectorsComponent,
  ]
})
export class SpaceLayoutModule { }
