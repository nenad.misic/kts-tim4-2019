import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LocationService } from '../../shared/services/location.service';
import { Location } from '../../shared/model/location';
import { Space } from '../../shared/model/space';
import { SpaceService } from '../../shared/services/space.service';
import { Message } from '../../shared/model/message';
import { stringify } from 'querystring';
import {MatAutocomplete, MatSnackBar} from '@angular/material';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';


@Component({
  selector: 'app-add-space',
  templateUrl: './add-space.component.html',
  styleUrls: ['./add-space.component.css']
})
export class AddSpaceComponent implements OnInit {

  @ViewChild('auto') _auto: MatAutocomplete;

  space: Space = new Space();
  selectedLocation: Location;

  selectedName = '';

  filteredLocations: Array<Location>;
  locations: Array<Location>;

  constructor(private locationService: LocationService,
              private spaceService: SpaceService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.locations = [];
    this.filteredLocations = [];

    this.locationService.getAll()
    .then((locations: Array<Location>) => {
      this.locations = locations;
      this.filteredLocations = locations;
    })
    .catch((err) => {
      alert('Failed to collect locations from server, cannot continue.');
    });
    this.filteredLocations = this.locations;
  }

  doFilter() {
    if (typeof this.selectedName === 'string') {
      this.filteredLocations = this.locations.filter(loc => (loc.name + ', ' + loc.city)
        .toLowerCase().includes(this.selectedName.toLowerCase()));
    }
  }

  setSelected(event) {
    this.selectedLocation = event.option.value;
  }


  displayFunction(location?: any) {
    return location ? location.name + ', ' + location.city : undefined;
  }

  addSpace() {
    this.space.location = this.selectedLocation;
    this.space.locationId = this.selectedLocation.id;
    this.spaceService.create(this.space)
    .then((message: Message) => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: 'Added a space!', status: 'success' }, duration: 3000 });
    })
    .catch((err: any) => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
    });
  }
}
