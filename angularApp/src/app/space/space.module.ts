import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddSpaceComponent} from './add-space/add-space.component';
import {UpdateSpaceComponent} from './update-space/update-space.component';
import {MaterialModule} from '../material/material.module';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [
    AddSpaceComponent,
    UpdateSpaceComponent,
  ]
})
export class SpaceModule { }
