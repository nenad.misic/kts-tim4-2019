import {Component, OnInit, ViewChild} from '@angular/core';
import {MatAutocomplete, MatSnackBar} from '@angular/material';
import {Space} from '../../shared/model/space';
import {Location} from '../../shared/model/location';
import {LocationService} from '../../shared/services/location.service';
import {SpaceService} from '../../shared/services/space.service';
import {Message} from '../../shared/model/message';
import {ActivatedRoute} from '@angular/router';
import {SnackbarComponent} from '../../core/snackbar/snackbar.component';

@Component({
  selector: 'app-update-space',
  templateUrl: './update-space.component.html',
  styleUrls: ['./update-space.component.css']
})
export class UpdateSpaceComponent implements OnInit {

  space: Space = new Space();
  selectedName = '';

  constructor(private locationService: LocationService,
              private spaceService: SpaceService,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.spaceService.getById(id)
      .then((space: Space) => {
        this.space = space;
        this.selectedName = this.space.location.name;
      });
  }

  updateSpace() {
    this.spaceService.update(this.space)
      .then((message: Message) => {
        this.snackBar.openFromComponent(SnackbarComponent, { data: { message: 'Updated the space!', status: 'success' }, duration: 3000 });
      })
      .catch((err: any) => {
        this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
      });
  }

}
