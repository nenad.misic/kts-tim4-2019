import {Component, HostListener, OnInit} from '@angular/core';
import {Ticket} from '../../shared/model/ticket';
import {TicketService} from '../../shared/services/ticket.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {

  tickets: Array<Ticket>;
  page = 1;
  pageSize = 10;
  gotAllData = false;

  constructor(private ticketService: TicketService) { }

  ngOnInit() {
    this.ticketService.getUserTickets(0, this.pageSize)
      .then((tickets: Array<Ticket>) => {
        this.tickets = tickets;
      });
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {
    const windowHeight = 'innerHeight' in window ? window.innerHeight
      : document.documentElement.offsetHeight;
    const body = document.body, html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight,
      body.offsetHeight, html.clientHeight,
      html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight && !this.gotAllData) {
      this.ticketService.getUserTickets(this.page, this.pageSize)
        .then((data: Array<any>) => {
          data.forEach(newTicket => {
            this.tickets.push(newTicket);
          });
          this.page++;
          if (data.length === 0) {
            this.gotAllData = true;
          }
        });
    }
  }
}
