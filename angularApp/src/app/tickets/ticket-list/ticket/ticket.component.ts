import { SnackbarComponent } from '../../../core/snackbar/snackbar.component';
import {Component, Input, OnInit} from '@angular/core';
import { Ticket } from '../../../shared/model/ticket';
import { TicketService } from '../../../shared/services/ticket.service';
import { Space } from '../../../shared/model/space';
import { SpaceLayoutService } from '../../../shared/services/space-layout.service';
import { SpaceLayout } from '../../../shared/model/space-layout';
import { Location } from '../../../shared/model/location';
import {LocationService} from '../../../shared/services/location.service';
import {SpaceService} from '../../../shared/services/space.service';
import { PaymentService } from '../../../shared/services/payment.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  @Input()
  ticket: Ticket;
  spaceLayout: SpaceLayout;
  location: Location;
  space: Space;
  loaded = false;

  constructor(private ticketService: TicketService,
              private spaceLayoutService: SpaceLayoutService,
              private locationService: LocationService,
              private spaceService: SpaceService,
              private paymentService: PaymentService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.spaceLayoutService.getById(this.ticket.singleEvent.spaceLayoutId)
      .then((spaceLayout: SpaceLayout) => {
        this.spaceLayout = spaceLayout;
        return this.spaceService.getById(this.spaceLayout.spaceId);
      })
      .then((space: Space) => {
        this.space = space;
        return this.locationService.getById(this.space.locationId);
      })
      .then((location: Location) => {
        this.location = location;
        this.loaded = true;
      }) ;
  }

  payTicket(ticket: Ticket) {
    this.paymentService.payForTicket(ticket.id).then(data => {
      localStorage.setItem('ticket', JSON.stringify(ticket));
      window.location.href = data.redirect_url;
    });
  }

  cancelReservation(ticket: Ticket) {
    this.ticketService.cancelReservation(ticket).then(data => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: 'Reservation canceled', status: 'success' }, duration: 3000 });
    }, err => {
      this.snackBar.openFromComponent(SnackbarComponent, { data: { message: err, status: 'error' }, duration: 3000 });
    });
  }

}
