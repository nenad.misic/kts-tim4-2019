import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TicketComponent} from './ticket-list/ticket/ticket.component';
import {TicketListComponent} from './ticket-list/ticket-list.component';
import {MaterialModule} from '../material/material.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
  ],
  declarations: [
    TicketComponent,
    TicketListComponent,
  ]
})
export class TicketsModule { }
