package ktsnvt.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AddEventPage {

    private WebDriver driver;

    public AddEventPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "addSingleEvent")
    private WebElement addSingleEventButton;

    @FindBy(id = "addCompositeEvent")
    private WebElement addCompositeEventButton;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "type")
    private WebElement typeInput;

    @FindBy(name = "description")
    private WebElement descriptionInput;

    @FindBy(name = "startDate")
    private WebElement startDateInput;

    @FindBy(name = "endDate")
    private WebElement endDateInput;

    @FindBy(name = "location")
    private WebElement locationInput;

    @FindBy(name = "space")
    private WebElement spaceInput;

    @FindBy(name = "spaceLayout")
    private WebElement spaceLayoutInput;

    @FindBy(id = "addSpaceLayout")
    private WebElement addSpaceLayoutButton;

    @FindBy(id = "addSingleEventConfirm")
    private WebElement addSingleEventConfirmButton;

    @FindBy(id = "addCompositeEventConfirm")
    private WebElement addCompositeEventConfirmButton;

    @FindBy(xpath = "//mat-option/span")
    private  WebElement firstOption;

    @FindBy(css = ".mat-tree-node")
    private List<WebElement> treeNodes;

    @FindBy(xpath = "//mat-option/span[text()=' spaceLayoutTesting ']")
    private  WebElement addedSpaceLayoutOption;

    @FindBy(xpath = "//mat-option/span[text()=' Somborska Arena, Sombor ']")
    private  WebElement somborskaArenaOption;

    @FindBy(xpath = "//mat-option/span[text() =' space layout 15 ']")
    private WebElement spaceLayout15Option;

    @FindBy(id = "deleteEventButton")
    private WebElement deleteEventButton;

    @FindBy(id = "groundFloorCapacity0")
    private WebElement gfCapacity;
    @FindBy(id = "groundFloorPrice0")
    private WebElement gfPrice;
    @FindBy(id = "seatSpacePrice0")
    private WebElement ssPrice1;
    @FindBy(id = "seatSpacePrice1")
    private WebElement ssPrice2;


    @FindBy(id="treeSubmit")
    private WebElement treeSubmit;

    public void ensureTreeInOrder(int expectedNumber) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(".mat-tree-node"), expectedNumber));
    }

    public WebElement getTreeSubmit() {
        return treeSubmit;
    }

    public WebElement getGfCapacity() {
        return gfCapacity;
    }


    public WebElement getGfPrice() {
        return gfPrice;
    }

    public WebElement getSsPrice1() {
        return ssPrice1;
    }

    public WebElement getSsPrice2() {
        return ssPrice2;
    }

    public WebElement getFirstOption() {
        return firstOption;
    }

    public WebElement getAddSingleEventButton() {
        return addSingleEventButton;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getTypeInput() {
        return typeInput;
    }

    public WebElement getDescriptionInput() {
        return descriptionInput;
    }

    public WebElement getStartDateInput() {
        return startDateInput;
    }

    public WebElement getEndDateInput() {
        return endDateInput;
    }

    public WebElement getLocationInput() {
        return locationInput;
    }

    public WebElement getSpaceInput() {
        return spaceInput;
    }

    public WebElement getSpaceLayoutInput() {
        return spaceLayoutInput;
    }

    public WebElement getAddSpaceLayoutButton() {
        return addSpaceLayoutButton;
    }

    public WebElement getAddSingleEventConfirmButton() {
        return addSingleEventConfirmButton;
    }

    public WebElement getAddCompositeEventConfirmButton() { return addCompositeEventConfirmButton; }

    public WebElement getAddedSpaceLayoutOption() {
        return addedSpaceLayoutOption;
    }

    public WebElement getSomborskaArenaOption() { return somborskaArenaOption; }

    public WebElement getSpaceLayout15Element() { return spaceLayout15Option; }

    public WebElement getAddCompositeEventButton() { return addCompositeEventButton; }

    public List<WebElement> getTreeNodes() { return treeNodes; }

    public WebElement getDeleteElementButton() { return deleteEventButton; }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureCompositeFormSubmittable() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(addCompositeEventConfirmButton));
    }

    public void ensureCompositeFormNotSubmittable() {
        JavascriptExecutor javascriptExecutor =  (JavascriptExecutor) this.driver;
        javascriptExecutor.executeScript("arguments[0].click();", this.nameInput);
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(addCompositeEventConfirmButton)));
    }

    public void ensureSingleFormSubmittable() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(addSingleEventConfirmButton));
    }

    public void ensureSingleFormNotSubmittable() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(addSingleEventConfirmButton)));
    }



    public void clearInputManually(WebElement element, int charactersToClear) {
        element.click();
        for (int i = 0; i < charactersToClear; i++) {
            element.sendKeys(Keys.BACK_SPACE);
        }
    }

    public void waitABit(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
