package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AddLocationPage {
    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "city")
    private WebElement cityInput;

    @FindBy(name = "address")
    private WebElement addressInput;

    @FindBy(name = "country")
    private WebElement countryInput;

    @FindBy(name = "add-location")
    private WebElement addLocationButton;

    public AddLocationPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getCityInput() {
        return cityInput;
    }

    public WebElement getAddressInput() {
        return addressInput;
    }

    public WebElement getCountryInput() {
        return countryInput;
    }

    public WebElement getAddLocationButton() {
        return addLocationButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

}
