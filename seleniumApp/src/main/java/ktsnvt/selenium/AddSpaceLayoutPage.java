package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddSpaceLayoutPage {

    private WebDriver driver;

    public AddSpaceLayoutPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "row0column0")
    private WebElement topLeftSeat1;

    @FindBy(id = "row3column3")
    private  WebElement bottomRightSeat1;

    @FindBy(id = "row4column4")
    private  WebElement topLeftSeat2;

    @FindBy(id = "row7column7")
    private  WebElement bottomRightSeat2;

    @FindBy(xpath = "//mat-radio-button[last()]/label/div")
    private WebElement groundFloorRadio;

    @FindBy(id = "row8column8")
    private  WebElement topLeftGround1;

    @FindBy(id = "row10column10")
    private  WebElement bottomRightGround1;

    @FindBy(id = "addSpaceLayoutConfirm")
    private WebElement addSpaceLayoutConfirm;




    @FindBy(xpath = "(//input[@name='name'])[last()]")
    private WebElement spaceLayoutNameInput;

    public WebElement getSpaceLayoutNameInput() {
        return spaceLayoutNameInput;
    }

    public WebElement getTopLeftSeat1() {
        return topLeftSeat1;
    }

    public WebElement getBottomRightSeat1() {
        return bottomRightSeat1;
    }

    public WebElement getTopLeftSeat2() {
        return topLeftSeat2;
    }

    public WebElement getBottomRightSeat2() {
        return bottomRightSeat2;
    }

    public WebElement getGroundFloorRadio() {
        return groundFloorRadio;
    }

    public WebElement getTopLeftGround1() {
        return topLeftGround1;
    }

    public WebElement getBottomRightGround1() {
        return bottomRightGround1;
    }

    public WebElement getAddSpaceLayoutConfirm() {
        return addSpaceLayoutConfirm;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }



}
