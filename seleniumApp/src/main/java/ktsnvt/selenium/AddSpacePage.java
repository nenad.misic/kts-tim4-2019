package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddSpacePage {

    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "location")
    private WebElement locationInput;

    @FindBy(name = "add-space")
    private WebElement addSpaceButton;

    @FindBy(css = ".option")
    private WebElement firstOption;

    public AddSpacePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getLocationInput() {
        return locationInput;
    }

    public WebElement getAddSpaceButton() {
        return addSpaceButton;
    }

    public WebElement getFirstOption() {
        return firstOption;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
