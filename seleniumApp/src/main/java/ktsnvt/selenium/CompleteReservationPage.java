package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CompleteReservationPage {

    private WebDriver driver;

    @FindBy(id="make-reservation-button")
    private WebElement makeReservationButton;

    public CompleteReservationPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getMakeReservationButton() {
        return makeReservationButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
