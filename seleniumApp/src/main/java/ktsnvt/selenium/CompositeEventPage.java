package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CompositeEventPage {

    private WebDriver driver;

    @FindBy(css = ".eventListItem")
    private List<WebElement> eventListItems;

    @FindBy(css = ".compositeEventTitle")
    private WebElement compositeEventTitle;

    @FindBy(css = ".detailsButton")
    private List<WebElement> detailsButtons;

    @FindBy(id = "makeReservationButton")
    private WebElement makeReservationButton;

    public CompositeEventPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getEventListItems() {
        return eventListItems;
    }

    public WebElement getCompositeEventTitle() {
        return compositeEventTitle;
    }

    public List<WebElement> getDetailsButtons() {
        return detailsButtons;
    }

    public WebElement getMakeReservationButton() {
        return makeReservationButton;
    }

    public void ensureDisplayed() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".compositeEventTitle")));
    }


    public void ensureDisplayed(String id) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
    }
}
