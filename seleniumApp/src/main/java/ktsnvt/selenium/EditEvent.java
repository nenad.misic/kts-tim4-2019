package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditEvent {

    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "description")
    private WebElement descriptionInput;

    @FindBy(id = "editEventSubmitButton")
    private WebElement submitEditButton;

    public EditEvent(WebDriver driver) {this.driver = driver;}

    public WebElement getNameInput() {return nameInput;}

    public WebElement getDescriptionInput() {return descriptionInput; }

    public WebElement getSubmitEditButton() {return submitEditButton; }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
