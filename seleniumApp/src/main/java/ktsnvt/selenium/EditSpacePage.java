package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditSpacePage {

    WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "location")
    private WebElement locationInput;

    @FindBy(name = "update-space")
    private WebElement updateSpaceButton;

    public EditSpacePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getLocationInput() {
        return locationInput;
    }

    public WebElement getUpdateSpaceButton() {
        return updateSpaceButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
