package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class EventDetailsPage {

    private WebDriver driver;

    public EventDetailsPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(className = "carousel-item")
    private List<WebElement> carouselItems;

    @FindBy(id = "makeReservationButton")
    private WebElement makeReservationButton;

    public WebElement getMakeReservationButton() {
        return makeReservationButton;
    }

    public List<WebElement> getCarouselItems() {
        return carouselItems;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }


    public void ensureAllPicturesSuccessfullyLoaded() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(".carousel-item"), 5));
    }



}
