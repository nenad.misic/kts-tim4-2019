package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.function.Predicate;

public class HomePage {

    private WebDriver driver;

    @FindBy(name = "search")
    private WebElement searchInput;

    @FindBy(id = "clear-search")
    private WebElement clearSearchButton;

    @FindBy(id = "do-search")
    private WebElement doSearchButton;

    @FindBy(css = "app-event-list-item")
    private List<WebElement> listItems;


    @FindBy(css = ".detailsButton")
    private WebElement details;


    @FindBy(css = ".deleteButton")
    private WebElement deleteButton;

    @FindBy(css = ".editButton")
    private WebElement editButton;


    @FindBy(id = "yes-dialog-button")
    private WebElement yesDialogButton;
    
    @FindBy(css = ".detailsButton")
    private List<WebElement> detailsButtons;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }


    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getEditButton() { return editButton; }

    public WebElement getYesDialogButton() {
        return yesDialogButton;
    }

    public WebElement getDetails() {
        return details;
    }

    public WebElement getSearchInput() {
        return searchInput;
    }

    public WebElement getClearSearchButton() {
        return clearSearchButton;
    }

    public WebElement getDoSearchButton() {
        return doSearchButton;
    }

    public List<WebElement> getListItems() {
        return listItems;
    }

    public List<WebElement> getDetailsButtons() {
        return detailsButtons;
    }


    public void setDeleteButton() {
        this.deleteButton = driver.findElement(By.cssSelector(".deleteButton"));
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureAjaxFinished(int expectedNumber) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("app-event-list-item"), expectedNumber));
    }

    public void waitABit(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
