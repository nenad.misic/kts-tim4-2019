package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ImageUploadPage {

    private WebDriver driver;

    public ImageUploadPage(WebDriver driver) {
        this.driver = driver;
    }



    @FindBy(className = "upload")
    private WebElement upload;

    @FindBy(className = "custom-file-upload")
    private WebElement customUpload;

    @FindBy(id = "confirm-upload")
    private WebElement confirmUpload;


    public WebElement getCustomUpload() {
        return customUpload;
    }


    public WebElement getConfirmUpload() {
        return confirmUpload;
    }

    public WebElement getUpload() {
        return upload;
    }


    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }


    public void ensureAllPicturesSuccessfullyUploaded() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.xpath("//img[@alt='successful']"), 5));
    }

    public void waitABit(){
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
