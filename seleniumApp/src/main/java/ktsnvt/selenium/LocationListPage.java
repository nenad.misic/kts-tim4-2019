package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LocationListPage {

    private WebDriver driver;

    @FindBy(name = "search")
    private WebElement searchInput;

    @FindBy(id = "clear-search")
    private WebElement clearSearchButton;

    @FindBy(id = "do-search")
    private WebElement doSearchButton;

    @FindBy(css = "app-location-list-item")
    private List<WebElement> listItems;

    @FindBy(css = ".detailsButton")
    private List<WebElement> detailsButtons;

    @FindBy(css = ".deleteButton")
    private List<WebElement> deleteButtons;

    @FindBy(css = ".editButton")
    private List<WebElement> editButtons;

    @FindBy(css = ".reportButton")
    private List<WebElement> reportButtons;

    @FindBy(id = "yes-dialog-button")
    private WebElement yesDialogButton;

    public LocationListPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getSearchInput() {
        return searchInput;
    }

    public WebElement getClearSearchButton() {
        return clearSearchButton;
    }

    public WebElement getDoSearchButton() {
        return doSearchButton;
    }

    public List<WebElement> getListItems() {
        return listItems;
    }

    public List<WebElement> getDetailsButtons() {
        return detailsButtons;
    }

    public List<WebElement> getDeleteButtons() {
        return deleteButtons;
    }

    public List<WebElement> getEditButtons() {
        return editButtons;
    }

    public List<WebElement> getReportButtons() {
        return reportButtons;
    }


    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureAjaxFinished(int expectedNumber) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("app-location-list-item"), expectedNumber));
    }

    public WebElement getYesDialogButton() {
        return yesDialogButton;
    }
}
