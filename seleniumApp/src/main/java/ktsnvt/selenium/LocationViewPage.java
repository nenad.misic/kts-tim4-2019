package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LocationViewPage {

    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "city")
    private WebElement cityInput;

    @FindBy(name = "address")
    private WebElement addressInput;

    @FindBy(name = "country")
    private WebElement countryInput;

    @FindBy(css = ".space-row")
    private List<WebElement> spaceRows;

    @FindBy(name = "edit-button")
    private WebElement editButton;

    @FindBy(name = "delete-button")
    private WebElement deleteButton;

    @FindBy(xpath = "//tr[./td=\"Novo ime\"]/td/button[@name=\"edit-button\"]")
    private WebElement editNewSpaceButton;

    @FindBy(xpath = "//tr[./td=\"Novo ime novije\"]/td/button[@name=\"delete-button\"]")
    private WebElement deleteNewSpaceButton;

    @FindBy(id = "yes-dialog-button")
    private WebElement yesDialogButton;

    public LocationViewPage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getCityInput() {
        return cityInput;
    }

    public WebElement getAddressInput() {
        return addressInput;
    }

    public WebElement getCountryInput() {
        return countryInput;
    }

    public List<WebElement> getSpaceRows() {
        return spaceRows;
    }

    public WebElement getEditNewSpaceButton() {
        return editNewSpaceButton;
    }

    public WebElement getDeleteNewSpaceButton() {
        return deleteNewSpaceButton;
    }

    public WebElement getEditButton() {
        return editButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getYesDialogButton() {
        return yesDialogButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureAjaxFinished(int expectedNumber) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(".space-row"), expectedNumber));
    }
}
