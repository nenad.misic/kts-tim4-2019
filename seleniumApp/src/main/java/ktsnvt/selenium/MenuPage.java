package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuPage {
	
	private WebDriver driver;

	@FindBy(css = ".navbar-toggler-icon")
	private WebElement mainMenuLink;

	@FindBy(css = "li > a:first-of-type")
	private WebElement studentsLink;
	
	public MenuPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getMainMenuLink() {
		return mainMenuLink;
	}

	public WebElement getStudentsLink() {
		return studentsLink;
	}

	public void ensureIsDisplayed() {
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.elementToBeClickable(mainMenuLink));
	}
}
