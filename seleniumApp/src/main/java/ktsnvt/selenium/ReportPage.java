package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReportPage {

    private WebDriver driver;

    @FindBy(id = "chartContainer")
    private WebElement earningsRadioButton;

    public ReportPage(WebDriver webDriver) {
        this.driver = webDriver;
    }

    public WebElement getEarningsRadioButton() {
        return earningsRadioButton;
    }

    public void ensureLoaded() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(earningsRadioButton));
    }
}
