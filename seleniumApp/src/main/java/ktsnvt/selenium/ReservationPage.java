package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ReservationPage {

    private WebDriver driver;

    @FindBy(css = ".mat-step-header")
    private List<WebElement> headerSteps;

    public ReservationPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getHeaderSteps() {
        return headerSteps;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureIsDisplayed() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("mat-step-header")));
    }
}

