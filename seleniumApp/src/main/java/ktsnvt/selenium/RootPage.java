package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RootPage {

    private WebDriver driver;

    @FindBy(id = "iconSuccess")
    private WebElement successSnackbar;

    @FindBy(id = "iconError")
    private WebElement errorSnackbar;

    @FindBy(id = "navbarLoginButton")
    private WebElement navbarLoginButton;

    @FindBy(id = "navbarLogoutButton")
    private WebElement navbarLogoutButton;

    @FindBy(id = "navbarTicketsButton")
    private WebElement navbarTicketsButton;

    @FindBy(id = "navbarHomeButton")
    private WebElement navbarHomeButton;

    @FindBy(id = "navbarSearchButton")
    private WebElement navbarSearchButton;

    @FindBy(id = "navbarLocationsButton")
    private WebElement navbarLocationsButton;

    @FindBy(id = "navbarAddLocationButton")
    private WebElement navbarAddLocationButton;

    @FindBy(id = "navbarAddSpaceButton")
    private WebElement navbarAddSpaceButton;

    @FindBy(id = "navbarAddEventButton")
    private WebElement navbarAddEventButton;

    public RootPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getSuccessSnackbar() {
        return successSnackbar;
    }

    public WebElement getErrorSnackbar() {
        return errorSnackbar;
    }

    public WebElement getNavbarLoginButton() {
        return navbarLoginButton;
    }

    public WebElement getNavbarLogoutButton() {
        return navbarLogoutButton;
    }

    public WebElement getNavbarTicketsButton() {
        return navbarTicketsButton;
    }

    public WebElement getNavbarHomeButton() {
        return navbarHomeButton;
    }

    public WebElement getNavbarSearchButton() {
        return navbarSearchButton;
    }

    public WebElement getNavbarAddEventButton() {
        return navbarAddEventButton;
    }

    public WebElement getNavbarLocationsButton() {
        return navbarLocationsButton;
    }

    public WebElement getNavbarAddLocationButton() {
        return navbarAddLocationButton;
    }

    public WebElement getNavbarAddSpaceButton() {
        return navbarAddSpaceButton;
    }

    public void ensureSuccess() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("iconSuccess")));
    }

    public void ensureError() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("iconError")));
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitABit(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
