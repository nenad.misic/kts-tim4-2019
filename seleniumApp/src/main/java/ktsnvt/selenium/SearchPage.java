package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchPage {

    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "description")
    private WebElement descriptionInput;

    @FindBy(name = "spaceName")
    private WebElement spaceNameInput;

    @FindBy(name = "locationName")
    private WebElement locationNameInput;

    @FindBy(name = "spaceLayoutName")
    private WebElement spaceLayoutNameInput;

    @FindBy(name = "address")
    private WebElement addressInput;

    @FindBy(name = "city")
    private WebElement cityInput;

    @FindBy(name = "country")
    private WebElement countryInput;

    @FindBy(name = "dateFrom")
    private WebElement dateFromInput;

    @FindBy(name = "dateTo")
    private WebElement dateToInput;

    @FindBy(id = "do-search")
    private WebElement doSearchButton;

    @FindBy(id = "clear-search")
    private WebElement clearSearchButton;

    @FindBy(css = "app-event-list-item")
    private List<WebElement> listItems;



    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getDoSearchButton() {
        return doSearchButton;
    }

    public List<WebElement> getListItems() {
        return listItems;
    }

    public WebElement getDescriptionInput() {
        return descriptionInput;
    }


    public WebElement getSpaceNameInput() {
        return spaceNameInput;
    }

    public WebElement getLocationNameInput() {
        return locationNameInput;
    }

    public WebElement getSpaceLayoutNameInput() {
        return spaceLayoutNameInput;
    }

    public WebElement getAddressInput() {
        return addressInput;
    }

    public WebElement getCityInput() {
        return cityInput;
    }

    public WebElement getCountryInput() {
        return countryInput;
    }

    public WebElement getDateFromInput() {
        return dateFromInput;
    }

    public WebElement getDateToInput() {
        return dateToInput;
    }

    public WebElement getClearSearchButton() {
        return clearSearchButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureAjaxFinished(int expectedNumber) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("app-event-list-item"), expectedNumber));
    }

}
