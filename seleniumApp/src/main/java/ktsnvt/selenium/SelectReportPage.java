package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SelectReportPage {

    private WebDriver driver;

    @FindBy(name = "earnings")
    private WebElement earningsRadioButton;

    @FindBy(name = "startDate")
    private WebElement startDateInput;

    @FindBy(name = "endDate")
    private WebElement endDateInput;

    @FindBy(name = "continue-button")
    private WebElement continueButton;

    public SelectReportPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getEarningsRadioButton() {
        return earningsRadioButton;
    }

    public WebElement getStartDateInput() {
        return startDateInput;
    }

    public WebElement getEndDateInput() {
        return endDateInput;
    }

    public WebElement getContinueButton() {
        return continueButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
