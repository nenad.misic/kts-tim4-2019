package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageUtility;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SelectSeatPage {

    @FindBy(css=".groundfloor")
    private List<WebElement> groundFloorSquares;

    @FindBy(css=".seatspace")
    private List<WebElement> seatSpaceSquares;

    @FindBy(css=".gf-ticket-number")
    private List<WebElement> gfTicketNumberInputs;

    @FindBy(css=".gf-tickets-confirm-button")
    private List<WebElement> gfTicketConfirmButtons;

    @FindBy(css=".space-layout-table")
    private List<WebElement> spaceLayoutTable;

    private WebDriver driver;

    public SelectSeatPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getGroundFloorSquares() {
        return groundFloorSquares;
    }

    public List<WebElement> getSeatSpaceSquares() {
        return seatSpaceSquares;
    }

    public List<WebElement> getGfTicketNumberInputs() {
        return gfTicketNumberInputs;
    }

    public List<WebElement> getGfTicketConfirmButtons() {
        return gfTicketConfirmButtons;
    }


    public List<WebElement> getSpaceLayoutTable() {
        return spaceLayoutTable;
    }

    public void ensureIsDisplayed() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("space-layout-table")));
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureElementDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public void ensureSteadiness() {
        (new WebDriverWait(driver, 10))
                .until(PageUtility.steadinessOfElementLocated(By.id("space-layout-table")));
    }

    public int getDisplayedGFSquaresCount() {
        int counter = 0;
        for (WebElement gfs : this.groundFloorSquares) {
            if (gfs.isDisplayed()) counter++;
        }
        return counter;
    }

    public int getDisplayedSSSquaresCount() {
        int counter = 0;
        for (WebElement sss : this.seatSpaceSquares) {
            if (sss.isDisplayed()) counter++;
        }
        return counter;
    }
}

