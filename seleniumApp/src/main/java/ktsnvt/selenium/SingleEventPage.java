package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SingleEventPage {

    @FindBy(id = "singleEventTitle")
    private WebElement singleEventTitle;

    @FindBy(id = "description")
    private WebElement description;

    @FindBy(id = "makeReservationButton")
    private WebElement makeReservationButton;

    private WebDriver driver;

    public SingleEventPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getSingleEventTitle() {
        return singleEventTitle;
    }

    public WebElement getDescription() {
        return description;
    }

    public WebElement getMakeReservationButton() {
        return makeReservationButton;
    }

    public void ensureDisplayed() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("singleEventTitle")));
    }
}
