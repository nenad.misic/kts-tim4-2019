package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentEditPage {
	private WebDriver driver;
	
	@FindBy(id = "field1c")
	private WebElement inputCard;
	
	@FindBy(id = "field2c")
	private WebElement inputFirstName;
	
	@FindBy(id = "field3c")
	private WebElement inputLastName;
	
	@FindBy(xpath = "//button[2]")
	private WebElement okButton;

	public StudentEditPage(WebDriver driver) {
		this.driver = driver;
	}

	public void ensureIsDisplayed() {
		//wait for card number input field to be present
		(new WebDriverWait(driver, 10))
				  .until(ExpectedConditions.presenceOfElementLocated(
						  By.id("cardNumber")));
	}

	public WebElement getInputCard() {
		return inputCard;
	}

	public void setInputCard(String value) {
		WebElement el = getInputCard();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getInputFirstName() {
		return inputFirstName;
	}

	public void setInputFirstName(String value) {
		WebElement el = getInputFirstName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getInputLastName() {
		return inputLastName;
	}

	public void setInputLastName(String value) {
		WebElement el = getInputLastName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getOkButton() {
		return okButton;
	}
}
