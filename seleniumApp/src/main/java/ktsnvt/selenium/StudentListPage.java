package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentListPage {
	private WebDriver driver;
	
	@FindBy(css = "button[aria-label=\"Add\"]")
	private WebElement addButton;
	
	@FindBy(xpath = "//tr[last()]/td[1]")
	private WebElement lastTdCard;
	
	@FindBy(xpath = "//tr[last()]/td[2]")
	private WebElement lastTdFirstName;
	
	@FindBy(xpath = "//tr[last()]/td[3]")
	private WebElement lastTdLastName;

	@FindBy(xpath = "//tr[last()]/td[4]/button[2]")
	private WebElement deleteButton;
	
	public StudentListPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getAddButton() {
		return addButton;
	}

	public WebElement getLastTdCard() {
		return lastTdCard;
	}

	public WebElement getLastTdFirstName() {
		return lastTdFirstName;
	}

	public WebElement getLastTdLastName() {
		return lastTdLastName;
	}

	public WebElement getDeleteButton() {
		return deleteButton;
	}

	public void ensureIsDisplayed() {
		//wait for add button to be present
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.visibilityOf(addButton));
	}
	
	public void ensureIsAdded(int previousNoOfStudents) {
		//wait for add button to be present
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.numberOfElementsToBe(
					By.cssSelector("tr"), previousNoOfStudents + 1));
	}
	
	public void ensureIsDeleted(int previousNoOfStudents) {
		(new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.invisibilityOfElementLocated(
				  By.xpath("//tr[" + (previousNoOfStudents + 1) + "]")));
	}
	
	public int getStudentsTableSize() {
		return driver.findElements(By.cssSelector("tr")).size();
	}
}
