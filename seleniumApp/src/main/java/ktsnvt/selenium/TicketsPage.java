package ktsnvt.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TicketsPage {

    private WebDriver driver;

    @FindBy(css=".ticketListItem")
    private List<WebElement> ticketListItems;

    @FindBy(css=".cancel-reservation-button")
    private List<WebElement> cancelReservationButtons;

    public TicketsPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getTicketListItems() {
        return ticketListItems;
    }

    public List<WebElement> getCancelReservationButtons() {
        return cancelReservationButtons;
    }


    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void ensureIsDisplayed() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("ticketListItem")));
    }
}
