package ktsnvt.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UpdateLocationPage {

    private WebDriver driver;

    @FindBy(name = "name")
    private WebElement nameInput;

    @FindBy(name = "city")
    private WebElement cityInput;

    @FindBy(name = "address")
    private WebElement addressInput;

    @FindBy(name = "country")
    private WebElement countryInput;

    @FindBy(name = "update-location")
    private WebElement updateLocationButton;

    public UpdateLocationPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getCityInput() {
        return cityInput;
    }

    public WebElement getAddressInput() {
        return addressInput;
    }

    public WebElement getCountryInput() {
        return countryInput;
    }

    public WebElement getUpdateLocationButton() {
        return updateLocationButton;
    }

    public void ensureClickable(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
