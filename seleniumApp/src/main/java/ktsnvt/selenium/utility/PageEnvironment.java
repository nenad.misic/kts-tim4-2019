package ktsnvt.selenium.utility;

public class PageEnvironment {

    public static final String DRIVER_PATH = "src/test/resources/chromedriver.exe";

    public static final String LOGIN_PAGE_URL = "http://localhost:4200/login";
    public static final String HOME_PAGE_URL = "http://localhost:4200/";
    public static final String EVENT_PAGE_URL = "http://localhost:4200/event";
    public static final String EVENT_RESERVATION_URL = "http://localhost:4200/reservation/";
    public static final String TICKETS_URL = "http://localhost:4200/tickets";
}
