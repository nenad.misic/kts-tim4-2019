package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddEventTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    AddEventPage addEventPage;
    HomePage homePage;
    EditEvent editEvent;


    @BeforeMethod
    public void setupSelenium() {
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");
        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        addEventPage = PageFactory.initElements(browser, AddEventPage.class);
        homePage = PageFactory.initElements(browser, HomePage.class);
        editEvent = PageFactory.initElements(browser, EditEvent.class);
    }

    @Test
    public void compositeEventFormValidation() {
        Utility.login(rootPage, loginPage, "admin", "password");
        rootPage.ensureClickable(rootPage.getNavbarAddEventButton());
        rootPage.getNavbarAddEventButton().click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.ensureTreeInOrder(0);
        addEventPage.getAddCompositeEventButton().click();


        fillInCompositeEventForm("Composite event");

        //name is required
        addEventPage.clearInputManually(addEventPage.getNameInput(), 20);
        addEventPage.getNameInput().sendKeys(Keys.TAB);
        addEventPage.ensureCompositeFormNotSubmittable();
        addEventPage.getNameInput().sendKeys("Composite Event, the ultimate one");
        addEventPage.ensureCompositeFormSubmittable();

        //description is required
        addEventPage.clearInputManually(addEventPage.getDescriptionInput(), 20);
        addEventPage.ensureCompositeFormNotSubmittable();
        addEventPage.getDescriptionInput().sendKeys("Description");

        addEventPage.ensureCompositeFormSubmittable();
        addEventPage.getAddCompositeEventConfirmButton().click();

        //tree now has the composite event in tree
        addEventPage.ensureTreeInOrder(1);
    }

    @Test
    public void treeDelete() {
        Utility.login(rootPage, loginPage, "admin", "password");
        rootPage.ensureClickable(rootPage.getNavbarAddEventButton());
        rootPage.getNavbarAddEventButton().click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.ensureTreeInOrder(0);
        addEventPage.getAddCompositeEventButton().click();

        fillInCompositeFormAndSubmit("1");

        addEventPage.ensureTreeInOrder(1);

        addEventPage.getTreeNodes().get(0).click();
        addEventPage.getDeleteElementButton().click();
        addEventPage.ensureTreeInOrder(0);

        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.ensureTreeInOrder(0);
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("1");
        addEventPage.ensureTreeInOrder(1);

        addEventPage.getTreeNodes().get(0).click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("1.1");
        addEventPage.ensureTreeInOrder(2);

        addEventPage.getTreeNodes().get(0).click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("1.2");
        addEventPage.ensureTreeInOrder(3);

        addEventPage.getTreeNodes().get(1).click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("1.1.1");
        addEventPage.ensureTreeInOrder(4);

        addEventPage.getTreeNodes().get(3).click();
        addEventPage.ensureClickable(addEventPage.getDeleteElementButton());
        addEventPage.getDeleteElementButton().click();
        addEventPage.ensureTreeInOrder(3);

        addEventPage.getTreeNodes().get(1).click();
        addEventPage.ensureClickable(addEventPage.getDeleteElementButton());
        addEventPage.getDeleteElementButton().click();
        addEventPage.ensureTreeInOrder(1);

        addEventPage.getTreeNodes().get(0).click();
        addEventPage.ensureClickable(addEventPage.getDeleteElementButton());
        addEventPage.getDeleteElementButton().click();
        addEventPage.ensureTreeInOrder(0);
    }

    @Test
    public void eventSubmission() {
        Utility.login(rootPage, loginPage, "admin", "password");
        rootPage.ensureClickable(rootPage.getNavbarAddEventButton());
        rootPage.getNavbarAddEventButton().click();

        addEventPage.ensureTreeInOrder(0);
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("Very specific name 1");

        addEventPage.ensureTreeInOrder(1);
        addEventPage.getTreeNodes().get(0).click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.getAddCompositeEventButton().click();
        fillInCompositeFormAndSubmit("Composite child of root");

        addEventPage.ensureTreeInOrder(2);
        addEventPage.getTreeNodes().get(0).click();
        addEventPage.ensureClickable(addEventPage.getAddSingleEventButton());
        addEventPage.getAddSingleEventButton().click();
        fillInSingleEventFormAndSubmit("Single child of root");

        addEventPage.ensureClickable(addEventPage.getTreeSubmit());
        addEventPage.getTreeSubmit().click();

        homePage.waitABit();

        Utility.searchEvent(rootPage, homePage, "Very specific name 1");

        homePage.ensureAjaxFinished(1);
        homePage.ensureClickable(homePage.getEditButton());
        homePage.getEditButton().click();

        editEvent.getNameInput().clear();
        editEvent.getNameInput().sendKeys("Changed very specific name 13");
        editEvent.ensureClickable(editEvent.getSubmitEditButton());
        editEvent.getSubmitEditButton().click();

        homePage.waitABit();
        homePage.waitABit();

        Utility.searchEvent(rootPage, homePage, "Changed very specific name 13");
        homePage.ensureAjaxFinished(1);

        homePage.waitABit();
        homePage.waitABit();

        homePage.getDeleteButton().click();
        homePage.ensureClickable(homePage.getYesDialogButton());
        homePage.getYesDialogButton().click();


        homePage.waitABit();
        homePage.waitABit();
    }


    @Test
    public void singleEventFormValidation() {
        Utility.login(rootPage, loginPage, "admin", "password");
        rootPage.ensureClickable(rootPage.getNavbarAddEventButton());
        rootPage.getNavbarAddEventButton().click();
        addEventPage.ensureClickable(addEventPage.getAddCompositeEventButton());
        addEventPage.ensureTreeInOrder(0);
        addEventPage.getAddSingleEventButton().click();

        fillInSingleEventForm("Single event");

        // now to test validation

        // seat space price required
        addEventPage.clearInputManually(addEventPage.getSsPrice1(), 10);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        // seat space price must be positive
        addEventPage.getSsPrice1().clear();
        addEventPage.getSsPrice1().sendKeys("-33");
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //ground floor capacity required
        addEventPage.clearInputManually(addEventPage.getGfCapacity(), 10);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //ground floor capacity must be positive
        addEventPage.getGfCapacity().clear();
        addEventPage.getGfCapacity().sendKeys("-12");
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //ground floor price is required
        addEventPage.clearInputManually(addEventPage.getGfPrice(), 10);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //ground floor prince has to be positive
        addEventPage.getGfPrice().clear();
        addEventPage.getGfPrice().sendKeys("-123");
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //valid space layout is required
        addEventPage.clearInputManually(addEventPage.getSpaceLayoutInput(), 1);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //valid space is required
        addEventPage.clearInputManually(addEventPage.getSpaceInput(), 1);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        //valid location is required
        addEventPage.clearInputManually(addEventPage.getLocationInput(), 1);
        addEventPage.ensureSingleFormNotSubmittable();

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();

        // end date is required
        addEventPage.clearInputManually(addEventPage.getEndDateInput(), 15);
        addEventPage.ensureSingleFormNotSubmittable();

        // valid end date is required
        addEventPage.getEndDateInput().sendKeys("a");
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getEndDateInput().clear();
        addEventPage.getEndDateInput().sendKeys("6/5/2021");
        addEventPage.ensureSingleFormSubmittable();

        //start date is required
        addEventPage.clearInputManually(addEventPage.getStartDateInput(), 15);
        addEventPage.ensureSingleFormNotSubmittable();

        //valid start date is required
        addEventPage.getStartDateInput().sendKeys("a");
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getStartDateInput().clear();
        addEventPage.getStartDateInput().sendKeys("6/5/2021");
        addEventPage.ensureSingleFormSubmittable();

        //start date has to be before end date
        addEventPage.getStartDateInput().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
        addEventPage.getStartDateInput().sendKeys("7/5/2021");
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getEndDateInput().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
        addEventPage.getEndDateInput().sendKeys("8/5/2021");
        addEventPage.ensureSingleFormSubmittable();

        //description is required
        addEventPage.clearInputManually(addEventPage.getDescriptionInput(), 15);
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getDescriptionInput().sendKeys("Description");
        addEventPage.ensureSingleFormSubmittable();

        //name is required
        addEventPage.clearInputManually(addEventPage.getNameInput(), 15);
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getNameInput().sendKeys("Name");
        addEventPage.ensureSingleFormSubmittable();

        addEventPage.getAddSingleEventConfirmButton().click();


        addEventPage.ensureTreeInOrder(1);
    }

    public void fillInCompositeFormAndSubmit(String name) {
        fillInCompositeEventForm(name);
        addEventPage.ensureCompositeFormSubmittable();
        addEventPage.getAddCompositeEventConfirmButton().click();
    }


    public void fillInCompositeEventForm(String name) {
        addEventPage.ensureCompositeFormNotSubmittable();

        addEventPage.getNameInput().click();
        addEventPage.getNameInput().sendKeys(name);
        addEventPage.getDescriptionInput().click();
        addEventPage.getDescriptionInput().sendKeys("Description");

        //type is required
        addEventPage.ensureCompositeFormNotSubmittable();

        addEventPage.getTypeInput().click();
        addEventPage.ensureClickable(addEventPage.getFirstOption());
        addEventPage.getFirstOption().click();

        addEventPage.ensureCompositeFormSubmittable();
    }

    public void fillInSingleEventFormAndSubmit(String name) {
        fillInSingleEventForm(name);

        addEventPage.ensureSingleFormSubmittable();
        addEventPage.getAddSingleEventConfirmButton().click();
    }

    public void fillInSingleEventForm(String name) {
        addEventPage.ensureSingleFormNotSubmittable();

        addEventPage.getNameInput().clear();
        addEventPage.getNameInput().sendKeys("Name");

        addEventPage.getTypeInput().click();
        addEventPage.ensureClickable(addEventPage.getFirstOption());
        addEventPage.getFirstOption().click();

        addEventPage.getDescriptionInput().click();
        addEventPage.getDescriptionInput().sendKeys("Description");

        addEventPage.getStartDateInput().click();
        addEventPage.getStartDateInput().clear();
        addEventPage.getStartDateInput().sendKeys("6/5/2021");

        addEventPage.getEndDateInput().click();
        addEventPage.getEndDateInput().clear();
        addEventPage.getEndDateInput().sendKeys("6/5/2021");

        fillDynamicPartOfForm();
        addEventPage.ensureSingleFormSubmittable();
    }

    public void fillDynamicPartOfForm() {
        addEventPage.getLocationInput().clear();
        addEventPage.getLocationInput().click();
        addEventPage.ensureClickable(addEventPage.getSomborskaArenaOption());
        addEventPage.getSomborskaArenaOption().click();

        addEventPage.getSpaceInput().clear();
        addEventPage.getSpaceInput().click();
        addEventPage.ensureClickable(addEventPage.getFirstOption());
        addEventPage.getFirstOption().click();

        addEventPage.getSpaceLayoutInput().clear();
        addEventPage.getSpaceLayoutInput().click();
        addEventPage.ensureClickable(addEventPage.getSpaceLayout15Element());
        addEventPage.getSpaceLayout15Element().click();

        // wait for dynamic part of form to pop up
        addEventPage.ensureClickable(addEventPage.getGfPrice());

        addEventPage.getGfPrice().clear();
        addEventPage.getGfPrice().sendKeys("100");

        addEventPage.getGfCapacity().clear();
        addEventPage.getGfCapacity().sendKeys("100");

        addEventPage.getSsPrice1().clear();
        addEventPage.getSsPrice1().sendKeys("250");
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
