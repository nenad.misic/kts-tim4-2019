package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class DetailedSearchEventsTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    SearchPage searchPage;

    @BeforeMethod
    public void setupSelenium() {
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");
        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        searchPage = PageFactory.initElements(browser, SearchPage.class);
    }

    @Test
    public void searchTest() {
        Utility.login(rootPage, loginPage, "username", "password");
        rootPage.ensureClickable(rootPage.getNavbarSearchButton());
        rootPage.getNavbarSearchButton().click();

        searchPage.ensureClickable(searchPage.getDoSearchButton());

        doSearch(
                "eXiT","","","","",
                "","","","","",1);
        clearSearch();

        doSearch(
                "","gORI","","","",
                "","","","","",2);
        clearSearch();

        doSearch(
                "","","beograd","","",
                "","","","","",2);
        clearSearch();

        doSearch(
                "","B","beograd","","",
                "","","","","",1);
        clearSearch();

        doSearch(
                "","","","","",
                "","","","01012021","01022021",1);
        clearSearch();

        doSearch(
                "Turneja","","","","",
                "","","","01012021","01022021",0);
        clearSearch();

        Utility.logout(rootPage);
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }

    private void doSearch(
            String name,
            String description,
            String spaceName,
            String locationName,
            String spaceLayoutName,
            String address,
            String city,
            String country,
            String dateFrom,
            String dateTo,
            int expectedNumber
    ){
        searchPage.getNameInput().clear();
        searchPage.getNameInput().sendKeys(name);
        searchPage.getDescriptionInput().clear();
        searchPage.getDescriptionInput().sendKeys(description);
        searchPage.getSpaceNameInput().clear();
        searchPage.getSpaceNameInput().sendKeys(spaceName);
        searchPage.getLocationNameInput().clear();
        searchPage.getLocationNameInput().sendKeys(locationName);
        searchPage.getSpaceLayoutNameInput().clear();
        searchPage.getSpaceLayoutNameInput().sendKeys(spaceLayoutName);
        searchPage.getAddressInput().clear();
        searchPage.getAddressInput().sendKeys(address);
        searchPage.getCityInput().clear();
        searchPage.getCityInput().sendKeys(city);
        searchPage.getCountryInput().clear();
        searchPage.getCountryInput().sendKeys(country);
        searchPage.getDateFromInput().clear();
        searchPage.getDateFromInput().sendKeys(dateFrom);
        searchPage.getDateToInput().clear();
        searchPage.getDateToInput().sendKeys(dateTo);
        searchPage.getDoSearchButton().click();

        searchPage.ensureAjaxFinished(expectedNumber);
        List<WebElement> eventList = searchPage.getListItems();
        int size = eventList.size();
        assertEquals(expectedNumber, size);

    }

    private void clearSearch(){
        searchPage.getClearSearchButton().click();

        searchPage.ensureAjaxFinished(3);
        List<WebElement> eventList = searchPage.getListItems();
        int size = eventList.size();
        assertEquals(3, size);
    }
}
