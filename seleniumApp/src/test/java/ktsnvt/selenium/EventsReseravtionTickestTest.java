package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EventsReseravtionTickestTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    HomePage homePage;
    CompositeEventPage compositeEventPage;
    SingleEventPage singleEventPage;
    ReservationPage reservationPage;
    SelectSeatPage selectSeatPage;
    CompleteReservationPage completeReservationPage;
    TicketsPage ticketsPage;

    @BeforeMethod
    public void setupSelenium() {
        //instantiate browser
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        //maximize window
        browser.manage().window().maximize();
        //navigate
        browser.navigate().to("http://localhost:4200");

        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        homePage = PageFactory.initElements(browser, HomePage.class);
        compositeEventPage = PageFactory.initElements(browser, CompositeEventPage.class);
        singleEventPage = PageFactory.initElements(browser, SingleEventPage.class);
        reservationPage = PageFactory.initElements(browser, ReservationPage.class);
        selectSeatPage = PageFactory.initElements(browser, SelectSeatPage.class);
        completeReservationPage = PageFactory.initElements(browser, CompleteReservationPage.class);
        ticketsPage = PageFactory.initElements(browser, TicketsPage.class);

    }

    @Test
    public void testEventsReservationAndTickets() {
        homePage.ensureClickable(homePage.getDoSearchButton());
        assertEquals(homePage.getListItems().size(), 3);

        int exitAndSeadanceId = 9942;
        String exitAndSeadanceTitle = "Exit and seadance festivals";
        int exitId = 9940;
        int exitDay1Id = 999;
        String exitDay1Title = "Prvi dan Exit festivala";
        String exitDay1Description = "Prvi dan Exit festivala u Novom Sadu";
        int koncertZvonkaBogdanaId = 9943;

        homePage.getDetailsButtons().get(1).click();
        assertEquals(PageEnvironment.EVENT_PAGE_URL + "/" + exitAndSeadanceId, browser.getCurrentUrl());

        compositeEventPage.ensureDisplayed();
        assertEquals(compositeEventPage.getCompositeEventTitle().getText(), exitAndSeadanceTitle);
        assertEquals(compositeEventPage.getEventListItems().size(), 2);

        compositeEventPage.getDetailsButtons().get(0).click();
        assertEquals(PageEnvironment.EVENT_PAGE_URL + "/" + exitId, browser.getCurrentUrl());

        compositeEventPage.ensureDisplayed("title" + exitId);
        compositeEventPage.getDetailsButtons().get(0).click();

        singleEventPage.ensureDisplayed();
        assertEquals(PageEnvironment.EVENT_PAGE_URL + "/" + exitDay1Id, browser.getCurrentUrl());
        assertEquals(singleEventPage.getSingleEventTitle().getText(), exitDay1Title);
        assertEquals(singleEventPage.getDescription().getText(), exitDay1Description);

        singleEventPage.ensureDisplayed();
        Actions actions = new Actions(browser);
        actions.moveToElement(singleEventPage.getMakeReservationButton());
        actions.perform();
        singleEventPage.getMakeReservationButton().click();

        loginPage.ensureClickable(loginPage.getLoginButton());
        loginPage.getUsernameInput().sendKeys("test");
        loginPage.getPasswordInput().sendKeys("test");
        loginPage.getLoginButton().click();

        reservationPage.ensureIsDisplayed();
        assertEquals(browser.getCurrentUrl(), PageEnvironment.EVENT_RESERVATION_URL + exitDay1Id);
        assertEquals(reservationPage.getHeaderSteps().size(), 2);

        selectSeatPage.ensureIsDisplayed();
        selectSeatPage.getGroundFloorSquares().get(0).click();

        selectSeatPage.ensureClickable(selectSeatPage.getGfTicketConfirmButtons().get(0));
        selectSeatPage.getGfTicketNumberInputs().get(0).sendKeys("1");
        selectSeatPage.getGfTicketConfirmButtons().get(0).click();

        reservationPage.getHeaderSteps().get(1).click();
        completeReservationPage.ensureClickable(completeReservationPage.getMakeReservationButton());
        completeReservationPage.getMakeReservationButton().click();

        ticketsPage.ensureIsDisplayed();
        assertEquals(browser.getCurrentUrl(), PageEnvironment.TICKETS_URL);
        assertEquals(ticketsPage.getTicketListItems().size(), 1);

        ticketsPage.getCancelReservationButtons().get(0).click();
        rootPage.ensureSuccess();

        rootPage.getNavbarHomeButton().click();
        homePage.ensureAjaxFinished(3);
        actions.moveToElement(homePage.getDetailsButtons().get(2));
        actions.perform();
        homePage.getDetailsButtons().get(2).click();
        compositeEventPage.ensureDisplayed();
        actions.moveToElement(compositeEventPage.getMakeReservationButton());
        actions.perform();
        compositeEventPage.getMakeReservationButton().click();

        reservationPage.ensureIsDisplayed();
        assertEquals(browser.getCurrentUrl(), PageEnvironment.EVENT_RESERVATION_URL + koncertZvonkaBogdanaId);
        assertEquals(reservationPage.getHeaderSteps().size(), 3);
        selectSeatPage.ensureIsDisplayed();

        selectSeatPage.getGroundFloorSquares().get(0).click();
        selectSeatPage.getGfTicketNumberInputs().get(0).sendKeys("2");
        selectSeatPage.getGfTicketConfirmButtons().get(0).click();

        selectSeatPage.getSeatSpaceSquares().get(0).click();
        selectSeatPage.getSeatSpaceSquares().get(4).click();

        reservationPage.getHeaderSteps().get(1).click();
        selectSeatPage.ensureElementDisplayed(selectSeatPage.getSpaceLayoutTable().get(1));
        assertEquals(selectSeatPage.getDisplayedGFSquaresCount(), 0);
        assertEquals(selectSeatPage.getDisplayedSSSquaresCount(), 0);

        reservationPage.getHeaderSteps().get(2).click();
        completeReservationPage.ensureClickable(completeReservationPage.getMakeReservationButton());
        completeReservationPage.getMakeReservationButton().click();

        ticketsPage.ensureIsDisplayed();
        assertEquals(ticketsPage.getTicketListItems().size(), 4);
        for (WebElement button : ticketsPage.getCancelReservationButtons()) {
            actions.moveToElement(button);
            actions.perform();
            button.click();
            rootPage.ensureSuccess();
        }

        rootPage.getNavbarLogoutButton().click();

    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
