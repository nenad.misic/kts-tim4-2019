package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class ImageUploadAndSpaceLayoutTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    HomePage homePage;
    AddEventPage addEventPage;
    AddSpaceLayoutPage addSpaceLayoutPage;
    ImageUploadPage imageUploadPage;
    EventDetailsPage eventDetailsPage;
    SelectSeatPage selectSeatPage;

    @BeforeMethod
    public void setupSelenium() {
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");
        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        addEventPage = PageFactory.initElements(browser, AddEventPage.class);
        addSpaceLayoutPage = PageFactory.initElements(browser, AddSpaceLayoutPage.class);
        imageUploadPage = PageFactory.initElements(browser, ImageUploadPage.class);
        homePage = PageFactory.initElements(browser, HomePage.class);
        eventDetailsPage = PageFactory.initElements(browser, EventDetailsPage.class);
        selectSeatPage = PageFactory.initElements(browser, SelectSeatPage.class);
    }

    @Test
    public void testImageUpload() throws Exception {
        Utility.login(rootPage, loginPage, "admin", "password");
        rootPage.ensureClickable(rootPage.getNavbarAddEventButton());
        rootPage.getNavbarAddEventButton().click();

        addEventPage.ensureClickable(addEventPage.getAddSingleEventButton());
        addEventPage.getAddSingleEventButton().click();

        addEventPage.getNameInput().click();
        addEventPage.getNameInput().clear();
        addEventPage.getNameInput().sendKeys("imageupload");
        addEventPage.getTypeInput().click();
        addEventPage.getFirstOption().click();
        addEventPage.getDescriptionInput().click();
        addEventPage.getDescriptionInput().clear();
        addEventPage.getDescriptionInput().sendKeys("image upload test");
        addEventPage.getStartDateInput().click();
        addEventPage.getStartDateInput().clear();
        addEventPage.getStartDateInput().sendKeys("6/5/2020");
        addEventPage.getEndDateInput().click();
        addEventPage.getEndDateInput().clear();
        addEventPage.getEndDateInput().sendKeys("6/5/2020");
        addEventPage.getLocationInput().click();
        addEventPage.getSomborskaArenaOption().click();
        addEventPage.getSpaceInput().click();
        addEventPage.getFirstOption().click();
        addEventPage.getAddSpaceLayoutButton().click();
        addSpaceLayoutPage.ensureClickable(addSpaceLayoutPage.getTopLeftSeat1());
        addEventPage.waitABit();
        addSpaceLayoutPage.getTopLeftSeat1().click();
        addSpaceLayoutPage.getBottomRightSeat1().click();
        addSpaceLayoutPage.getTopLeftSeat2().click();
        addSpaceLayoutPage.getBottomRightSeat2().click();
        addSpaceLayoutPage.getGroundFloorRadio().click();
        addSpaceLayoutPage.getTopLeftGround1().click();
        addSpaceLayoutPage.getBottomRightGround1().click();

        addSpaceLayoutPage.getSpaceLayoutNameInput().click();
        addSpaceLayoutPage.getSpaceLayoutNameInput().clear();
        addSpaceLayoutPage.getSpaceLayoutNameInput().sendKeys("spaceLayoutTesting");
        addSpaceLayoutPage.getAddSpaceLayoutConfirm().click();

        addEventPage.getSpaceLayoutInput().click();
        addEventPage.getAddedSpaceLayoutOption().click();


        addEventPage.ensureClickable(addEventPage.getGfCapacity());
        Actions actions = new Actions(browser);
        actions.moveToElement(addEventPage.getSsPrice2());
        actions.perform();

        addEventPage.getGfCapacity().click();
        addEventPage.getGfCapacity().clear();
        addEventPage.getGfCapacity().sendKeys("10");
        addEventPage.getGfPrice().click();
        addEventPage.getGfPrice().clear();
        addEventPage.getGfPrice().sendKeys("11");
        addEventPage.getSsPrice1().click();
        addEventPage.getSsPrice1().clear();
        addEventPage.getSsPrice1().sendKeys("12");
        addEventPage.getSsPrice2().click();
        addEventPage.getSsPrice2().clear();
        addEventPage.getSsPrice2().sendKeys("13");

        addEventPage.getAddSingleEventConfirmButton().click();

        addEventPage.getTreeSubmit().click();


        imageUploadPage.ensureClickable(imageUploadPage.getConfirmUpload());
        ((JavascriptExecutor)browser).executeScript("arguments[0].style.display = 'block';", imageUploadPage.getUpload());
        imageUploadPage.getCustomUpload().click();

        imageUploadPage.waitABit();


        Robot mister_freaking_robot = new Robot();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_ENTER);
        mister_freaking_robot.keyRelease(KeyEvent.VK_ENTER);
        imageUploadPage.waitABit();

        StringSelection ss = new StringSelection("Desktop");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);


        mister_freaking_robot.keyPress(KeyEvent.VK_CONTROL);
        mister_freaking_robot.keyPress(KeyEvent.VK_V);
        mister_freaking_robot.keyRelease(KeyEvent.VK_V);
        mister_freaking_robot.keyRelease(KeyEvent.VK_CONTROL);
        imageUploadPage.waitABit();

        mister_freaking_robot.keyPress(KeyEvent.VK_ENTER);
        mister_freaking_robot.keyRelease(KeyEvent.VK_ENTER);
        imageUploadPage.waitABit();


        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_TAB);
        mister_freaking_robot.keyRelease(KeyEvent.VK_TAB);
        imageUploadPage.waitABit();

        StringSelection ss2 = new StringSelection("\"1.png\" \"2.png\" \"3.png\" \"4.png\" \"5.png\"");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss2, null);

        mister_freaking_robot.keyPress(KeyEvent.VK_CONTROL);
        mister_freaking_robot.keyPress(KeyEvent.VK_V);
        mister_freaking_robot.keyRelease(KeyEvent.VK_V);
        mister_freaking_robot.keyRelease(KeyEvent.VK_CONTROL);
        imageUploadPage.waitABit();
        mister_freaking_robot.keyPress(KeyEvent.VK_ENTER);
        mister_freaking_robot.keyRelease(KeyEvent.VK_ENTER);

        imageUploadPage.ensureClickable(imageUploadPage.getConfirmUpload());
        imageUploadPage.getConfirmUpload().click();

        imageUploadPage.ensureAllPicturesSuccessfullyUploaded();


        Utility.goToEvent(rootPage, homePage,"imageupload");
        eventDetailsPage.ensureAllPicturesSuccessfullyLoaded();

        assertEquals(eventDetailsPage.getCarouselItems().size(), 5);

        eventDetailsPage.ensureClickable(eventDetailsPage.getMakeReservationButton());
        eventDetailsPage.getMakeReservationButton().click();

        selectSeatPage.ensureIsDisplayed();

        assertEquals(selectSeatPage.getDisplayedGFSquaresCount(), 9);
        assertEquals(selectSeatPage.getDisplayedSSSquaresCount(), 32);


        Utility.deleteEvent(rootPage, homePage,"imageupload");



    }


    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
