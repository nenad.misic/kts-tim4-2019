package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.*;

public class LocationFlowTest {

    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    LocationListPage locationListPage;
    AddLocationPage addLocationPage;
    UpdateLocationPage updateLocationPage;
    LocationViewPage locationViewPage;
    SelectReportPage selectReportPage;
    ReportPage reportPage;

    @BeforeMethod
    public void setupSelenium() {
        //instantiate browser
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        //maximize window
        browser.manage().window().maximize();
        //navigate
        browser.navigate().to("http://localhost:4200");

        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        locationListPage = PageFactory.initElements(browser, LocationListPage.class);
        addLocationPage = PageFactory.initElements(browser, AddLocationPage.class);
        updateLocationPage = PageFactory.initElements(browser, UpdateLocationPage.class);
        locationViewPage = PageFactory.initElements(browser, LocationViewPage.class);
        selectReportPage = PageFactory.initElements(browser, SelectReportPage.class);
        reportPage = PageFactory.initElements(browser, ReportPage.class);
    }

    @Test
    public void flowTest() throws Exception {
        Utility.login(rootPage, loginPage, "admin", "password");
        getToLocations(8);
        assertEquals(locationListPage.getListItems().size(), 8);
        //test adding a location
        addLocationWithValidation();
        getToLocations(9);
        assertEquals(locationListPage.getListItems().size(), 9);
        //test editing a location
        editLocation(8);
        getToLocations(9);
        //test deleting a location
        deleteLocation(8);
        rootPage.waitABit();
        getToLocations(8);
        //test details page
        checkDetailsPage(0, 2);
        getToLocations(8);
        //test report page
        checkReport(4);
        getToLocations(8);
        //test search
        checkSearch("Spens", 1);
        cancelSearch(8);
        checkSearch("a", 7);
        cancelSearch(8);
        checkSearch("o", 5);
        cancelSearch(8);
        //test deleting a location fail
        deleteLocationFail(4);
        Utility.logout(rootPage);
    }

    private void addLocationWithValidation() {
        rootPage.ensureClickable(rootPage.getNavbarAddLocationButton());
        rootPage.getNavbarAddLocationButton().click();
        assertFalse(addLocationPage.getAddLocationButton().isEnabled());
        addLocationPage.getNameInput().sendKeys("Nova lokacija");
        assertFalse(addLocationPage.getAddLocationButton().isEnabled());
        addLocationPage.getCityInput().sendKeys("Novi grad");
        assertFalse(addLocationPage.getAddLocationButton().isEnabled());
        addLocationPage.getAddressInput().sendKeys("Nova adresa");
        assertFalse(addLocationPage.getAddLocationButton().isEnabled());
        addLocationPage.getCountryInput().sendKeys("Nova zemlja");
        addLocationPage.ensureClickable(addLocationPage.getAddLocationButton());
        addLocationPage.getAddLocationButton().click();
        rootPage.ensureClickable(rootPage.getSuccessSnackbar());
    }

    private void deleteLocation(int index) {
        locationListPage.ensureClickable(locationListPage.getDeleteButtons().get(index));
        locationListPage.getDeleteButtons().get(index).click();
        locationListPage.ensureClickable(locationListPage.getYesDialogButton());
        rootPage.waitABit();
        locationListPage.getYesDialogButton().click();
        locationListPage.ensureAjaxFinished(index);
    }

    private void deleteLocationFail(int index) {
        locationListPage.ensureClickable(locationListPage.getDeleteButtons().get(index));
        locationListPage.getDeleteButtons().get(index).click();
        locationListPage.ensureClickable(locationListPage.getYesDialogButton());
        rootPage.waitABit();
        locationListPage.getYesDialogButton().click();
        rootPage.ensureClickable(rootPage.getErrorSnackbar());
    }

    private void editLocation(int index) {
        locationListPage.ensureClickable(locationListPage.getEditButtons().get(index));
        locationListPage.getEditButtons().get(index).click();
        updateLocationPage.ensureClickable(updateLocationPage.getUpdateLocationButton());
        updateLocationPage.getNameInput().sendKeys(" novije");
        updateLocationPage.getCityInput().sendKeys(" novije");
        updateLocationPage.getCountryInput().sendKeys(" novije");
        updateLocationPage.getAddressInput().sendKeys(" novije");
        updateLocationPage.getUpdateLocationButton().click();
        rootPage.ensureClickable(rootPage.getSuccessSnackbar());
    }

    private void getToLocations(int expectedNumberOfLocations) {
        rootPage.ensureClickable(rootPage.getNavbarLocationsButton());
        rootPage.getNavbarLocationsButton().click();
        locationListPage.ensureAjaxFinished(expectedNumberOfLocations);
    }

    private void checkDetailsPage(int index, int expectedSpacesNumber) {
        locationListPage.ensureClickable(locationListPage.getDetailsButtons().get(index));
        locationListPage.getDetailsButtons().get(index).click();
        locationViewPage.ensureAjaxFinished(expectedSpacesNumber);
        assertEquals("Spens", locationViewPage.getNameInput().getText());
        assertEquals("Maksima Gorkog 22", locationViewPage.getAddressInput().getText());
        assertEquals("Novi Sad", locationViewPage.getCityInput().getText());
        assertEquals("Srbija", locationViewPage.getCountryInput().getText());
    }

    private void checkReport(int index) {
        locationListPage.ensureClickable(locationListPage.getReportButtons().get(index));
        locationListPage.getReportButtons().get(index).click();
        selectReportPage.ensureClickable(selectReportPage.getEarningsRadioButton());
        assertFalse(selectReportPage.getContinueButton().isEnabled());
        rootPage.waitABit();
        selectReportPage.getEarningsRadioButton().click();
        assertFalse(selectReportPage.getContinueButton().isEnabled());
        selectReportPage.getStartDateInput().sendKeys("1/1/2019");
        assertFalse(selectReportPage.getContinueButton().isEnabled());
        selectReportPage.getEndDateInput().sendKeys("1/1/2023");
        selectReportPage.getEarningsRadioButton().click();
        selectReportPage.ensureClickable(selectReportPage.getContinueButton());
        selectReportPage.getContinueButton().click();
        reportPage.ensureLoaded();
    }

    private void checkSearch(String query, int numberOfExpectedResults) {
        locationListPage.ensureClickable(locationListPage.getDoSearchButton());
        locationListPage.getSearchInput().sendKeys(query);
        locationListPage.getDoSearchButton().click();
        locationListPage.ensureAjaxFinished(numberOfExpectedResults);
        locationListPage.getSearchInput().clear();
    }

    private void cancelSearch(int numberOfExpectedResults) {
        locationListPage.ensureClickable(locationListPage.getClearSearchButton());
        locationListPage.getClearSearchButton().click();
        locationListPage.ensureAjaxFinished(numberOfExpectedResults);
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
