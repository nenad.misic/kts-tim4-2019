package ktsnvt.selenium;

import static org.testng.AssertJUnit.assertEquals;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;

    @BeforeMethod
    public void setupSelenium() {
        //instantiate browser
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        //maximize window
        browser.manage().window().maximize();
        //navigate
        browser.navigate().to("http://localhost:4200");

        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
    }

    @Test
    public void testLogin() {
        rootPage.ensureClickable(rootPage.getNavbarLoginButton());
        rootPage.getNavbarLoginButton().click();

        assertEquals(PageEnvironment.LOGIN_PAGE_URL, browser.getCurrentUrl());
        loginPage.ensureClickable(loginPage.getLoginButton());


        loginPage.getLoginButton().click();
        rootPage.ensureError();

        loginPage.getUsernameInput().sendKeys("asdf");
        loginPage.getPasswordInput().sendKeys("asdf");
        loginPage.getLoginButton().click();
        rootPage.ensureError();

        loginPage.getUsernameInput().clear();
        loginPage.getUsernameInput().sendKeys("username");
        loginPage.getPasswordInput().clear();
        loginPage.getPasswordInput().sendKeys("password");
        loginPage.getLoginButton().click();

        rootPage.ensureClickable(rootPage.getNavbarLogoutButton());

        assertEquals(PageEnvironment.HOME_PAGE_URL, browser.getCurrentUrl());

        rootPage.getNavbarLogoutButton().click();
        rootPage.ensureClickable(rootPage.getNavbarLoginButton());
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
