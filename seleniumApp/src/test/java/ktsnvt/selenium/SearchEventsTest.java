package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class SearchEventsTest {
    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    HomePage homePage;

    @BeforeMethod
    public void setupSelenium() {
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200");
        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        homePage = PageFactory.initElements(browser, HomePage.class);
    }

    @Test
    public void searchTest() {
        Utility.login(rootPage, loginPage, "username", "password");
        rootPage.ensureClickable(rootPage.getNavbarHomeButton());
        rootPage.getNavbarHomeButton().click();

        homePage.ensureClickable(homePage.getDoSearchButton());

        doSearch("eXiT", 1);

        clearSearch();

        doSearch("gOrI", 2);

        clearSearch();

        doSearch("ljnihubgvyfctygvbhuijnmokjuy", 0);

        Utility.logout(rootPage);
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }

    private void doSearch(String term, int expectedResults){
        homePage.getSearchInput().clear();
        homePage.getSearchInput().sendKeys(term);
        homePage.getDoSearchButton().click();
        homePage.ensureAjaxFinished(expectedResults);
        List<WebElement> eventList = homePage.getListItems();
        int size = eventList.size();
        assertEquals(expectedResults, size);
    }

    private void clearSearch(){
        homePage.getClearSearchButton().click();
        homePage.ensureAjaxFinished(3);
        List<WebElement> eventList = homePage.getListItems();
        int size = eventList.size();
        assertEquals(3, size);
    }
}
