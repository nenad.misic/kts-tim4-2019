package ktsnvt.selenium;

import ktsnvt.selenium.utility.PageEnvironment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class SpaceFlowTest {

    private WebDriver browser;

    RootPage rootPage;
    LoginPage loginPage;
    LocationListPage locationListPage;
    LocationViewPage locationViewPage;
    AddSpacePage addSpacePage;
    EditSpacePage editSpacePage;

    @BeforeMethod
    public void setupSelenium() {
        //instantiate browser
        System.setProperty("webdriver.chrome.driver", PageEnvironment.DRIVER_PATH);
        browser = new ChromeDriver();
        //maximize window
        browser.manage().window().maximize();
        //navigate
        browser.navigate().to("http://localhost:4200");

        rootPage = PageFactory.initElements(browser, RootPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        locationListPage = PageFactory.initElements(browser, LocationListPage.class);
        locationViewPage = PageFactory.initElements(browser, LocationViewPage.class);
        addSpacePage = PageFactory.initElements(browser, AddSpacePage.class);
        editSpacePage = PageFactory.initElements(browser, EditSpacePage.class);
    }

    @Test
    public void flowTest() throws Exception {
        Utility.login(rootPage, loginPage, "admin", "password");
        getToSpace(0, 8, 2);
        //test adding a new space
        addSpaceWithValidaton();
        getToSpace(0, 8, 3);
        //test editing the space
        editSpace();
        getToSpace(0, 8, 3);
        //test deleting the space
        deleteSpace();
        rootPage.waitABit();
        getToSpace(0, 8, 2);
        //test deleting a space fail
        getToSpace(4, 8, 1);
        deleteSpaceFail();
        Utility.logout(rootPage);
    }

    private void addSpaceWithValidaton() {
        rootPage.ensureClickable(rootPage.getNavbarAddSpaceButton());
        rootPage.getNavbarAddSpaceButton().click();
        assertFalse(addSpacePage.getAddSpaceButton().isEnabled());
        addSpacePage.getNameInput().sendKeys("Novo ime");
        assertFalse(addSpacePage.getAddSpaceButton().isEnabled());
        addSpacePage.getLocationInput().sendKeys("Nepostojeca lokacije");
        assertFalse(addSpacePage.getAddSpaceButton().isEnabled());
        addSpacePage.getLocationInput().clear();
        addSpacePage.getLocationInput().sendKeys("Spens, Novi Sad");
        addSpacePage.ensureClickable(addSpacePage.getFirstOption());
        addSpacePage.getFirstOption().click();
        addSpacePage.ensureClickable(addSpacePage.getAddSpaceButton());
        addSpacePage.getAddSpaceButton().click();
        rootPage.ensureClickable(rootPage.getSuccessSnackbar());
    }

    private void getToSpace(int index, int expectedNumberOfLocations, int expectedNumberOfSpaces) {
        rootPage.ensureClickable(rootPage.getNavbarLocationsButton());
        rootPage.getNavbarLocationsButton().click();
        locationListPage.ensureAjaxFinished(expectedNumberOfLocations);
        locationListPage.ensureClickable(locationListPage.getDetailsButtons().get(index));
        locationListPage.getDetailsButtons().get(index).click();
        locationViewPage.ensureAjaxFinished(expectedNumberOfSpaces);
    }

    private void editSpace() {
        locationViewPage.ensureClickable(locationViewPage.getEditNewSpaceButton());
        locationViewPage.getEditNewSpaceButton().click();
        editSpacePage.getNameInput().sendKeys(" novije");
        editSpacePage.getUpdateSpaceButton().click();
        rootPage.ensureClickable(rootPage.getSuccessSnackbar());
    }

    private void deleteSpace() {
        locationViewPage.ensureClickable(locationViewPage.getDeleteNewSpaceButton());
        locationViewPage.getDeleteNewSpaceButton().click();
        locationViewPage.ensureClickable(locationViewPage.getYesDialogButton());
        rootPage.waitABit();
        locationViewPage.getYesDialogButton().click();
    }

    private void deleteSpaceFail() {
        locationViewPage.ensureClickable(locationViewPage.getDeleteButton());
        locationViewPage.getDeleteButton().click();
        locationViewPage.ensureClickable(locationViewPage.getYesDialogButton());
        rootPage.waitABit();
        locationViewPage.getYesDialogButton().click();
        rootPage.ensureClickable(rootPage.getErrorSnackbar());
    }

    @AfterMethod
    public void closeSelenium() {
        // Shutdown the browser
        browser.quit();
    }
}
