package ktsnvt.selenium;

import org.openqa.selenium.chrome.ChromeDriver;

import static org.testng.AssertJUnit.assertEquals;

public class Utility {

    public static void login(RootPage rootPage, LoginPage loginPage, String username, String password) {

        rootPage.ensureClickable(rootPage.getNavbarLoginButton());
        rootPage.getNavbarLoginButton().click();

        loginPage.ensureClickable(loginPage.getLoginButton());


        loginPage.getUsernameInput().clear();
        loginPage.getUsernameInput().sendKeys(username);
        loginPage.getPasswordInput().clear();
        loginPage.getPasswordInput().sendKeys(password);
        loginPage.getLoginButton().click();

        rootPage.ensureClickable(rootPage.getNavbarLogoutButton());
    }

    public static void goToEvent(RootPage rootPage, HomePage homePage, String eventName) {

        rootPage.ensureClickable(rootPage.getNavbarHomeButton());
        rootPage.getNavbarHomeButton().click();

        homePage.ensureClickable(homePage.getSearchInput());


        homePage.getSearchInput().click();
        homePage.getSearchInput().clear();
        homePage.getSearchInput().sendKeys(eventName);
        homePage.getDoSearchButton().click();
        homePage.ensureAjaxFinished(1);
        homePage.getDetails().click();

    }

    public static void searchEvent(RootPage rootPage, HomePage homePage, String eventName) {

        rootPage.ensureClickable(rootPage.getNavbarHomeButton());
        rootPage.getNavbarHomeButton().click();

        homePage.ensureClickable(homePage.getSearchInput());


        homePage.getSearchInput().click();
        homePage.getSearchInput().clear();
        homePage.getSearchInput().sendKeys(eventName);
        homePage.getDoSearchButton().click();
        homePage.ensureAjaxFinished(1);

    }

    public static void deleteEvent(RootPage rootPage, HomePage homePage, String eventName) {

        rootPage.ensureClickable(rootPage.getNavbarHomeButton());
        rootPage.getNavbarHomeButton().click();

        homePage.ensureClickable(homePage.getSearchInput());


        homePage.getSearchInput().click();
        homePage.getSearchInput().clear();
        homePage.getSearchInput().sendKeys(eventName);
        homePage.getDoSearchButton().click();
        homePage.ensureAjaxFinished(1);

        homePage.getDeleteButton().click();
        homePage.ensureClickable(homePage.getYesDialogButton());
        homePage.getYesDialogButton().click();

        homePage.waitABit();
        homePage.waitABit();

    }


    public static void logout(RootPage rootPage) {
        rootPage.ensureClickable(rootPage.getNavbarLogoutButton());
        rootPage.getNavbarLogoutButton().click();
        rootPage.ensureClickable(rootPage.getNavbarLoginButton());
    }
}
