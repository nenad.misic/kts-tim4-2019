package ktsnvt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
public class Main {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200");
            }
        };
    }

    public static void main(String[] args) {
        String bone = "llllllllllllllloooooooooooooooooooooooooooooooooooooooooooooooddoooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo\n" +
                "lllllllllloooooooooooooooooooooooooooooooooddooooooooooollooooodddddddddooodddddddddddddoodddoooodddddddddddddddddddoooooooooooooooooooooooooooooooooo\n" +
                "lllllloooooooooooooooooooooooooooooodddoodddoooollccccccloolododdddddddddddddddddddddooooddooddddddddddddddddddddddddddddddddddddddddddddddddddddddddd\n" +
                "oooooooooooooooooooooooodddddddddddddddddddoolllllcc:::;;:cccloodddddddooooollllcclloooooolloodddddddddddddddddddddddddddddddddddddddddddddddddddddddd\n" +
                "ooooooooooooooooooodddddddddddddddddddddddollcc:::;;;;;;,'''';cloloooollc:;;;,,'''',;::ccccclodddddddddddddddddddddddddddddddddddddddddddddddddddddddd\n" +
                "ooooooooooooooooddddddddddddddddddddddoolc:;,,,''''''''........',,;;;;,'''...........',;::cclllloooooddddddddddddddddddddddddddddddddddddddddddddddddd\n" +
                "ooooooooooooooddddddddddddddddddddooolc::;,,''.........................................,,,,'',,;;::ccclloddddddddddddddddddddddddddddddddddddddddddddd\n" +
                "oooooooooooodddddddddddddddddddddlcc;;,,,,'.............  .................       ..............',,;;::ccclodddddddddddddddddddddddddddddddddddddddddd\n" +
                "ooooooooodddddddddddddddddddddddol:;,''''...........         .................      .................',;:cc:codddddddddddddddddddddddddddddddddddddddd\n" +
                "oooooooodddddddddddddddddddddddolc;,,''.......  ...          ......................     ...............',;::::lodddddddddddddddddddddddddddddddddddddd\n" +
                "oooooooddddddddddddddddddddddddlc:;,'..........            ..........................          ..........',;:;;coddddddddddddddddddddddddddddddddddddd\n" +
                "oooodddddddddddddddddddddddddolc::;,'............   ....................    ..  .......          .      ...''',;:ldddddddddddddddddddddddddddddddddddd\n" +
                "ooddddddddddddddddddddddddddolc:;,,'''....................'''.............          ....           .      ....'',:lodddddddddddddddddddddddddddddddddd\n" +
                "ddddddddddddddddddddddddddddolc:,,''''..................',,,,,'............            ....                ......,cloddddddddddddddddddddddddddddddddd\n" +
                "dddddddddddddddddddddddddddolcc:;,''''..........'',;;:::cccccc:;,'........               .           .       .....;clodddddddddddddddddddddddddddddddd\n" +
                "dddddddddddddddddddddddddddoc:ccc;,,,''...''',;:ccllloooodoooolc:;,''.'....               ....                   .',:odddddddddddddddddddddddddddddddd\n" +
                "dddddddddddddddddddddddddddoc;:::;,'''..',;;clloooddddxxxxddddool:;;,''''.....         ........      .        .  ...,ldddddddddddddddddddddddddddddddd\n" +
                "dddddddddddddddddddddddddddol:::;,,,''';:clodxxxxxxxxxxxxxxxxxdoolc:;,''''......  ..                ..        ..  ...:oddddddddddddddddddddddddddddddd\n" +
                "ddddddddddddddddddddddddddddlc:;;;,'',;codxxkkkkkkkkkkkxxxxxxxxddol::;,,''''.........        ......            .  ...;oddddddddddddddddddddddddddddddd\n" +
                "dddddddddddddddddddddddddxddoc;;;,'',;cdxkkkOOOOOOOOkkxxxxxxxxxdddoc:;;;,,,''...........      ....            .. ....;oddddddddddxxxxxxxxxxxxxxxxxxxxx\n" +
                "oooddddddddddddddddddddddddddl:;;,'.,cdkkkOOOOOOOOOOkkxxxxxxxxxxddolc:;;;;,,,'...........           .        ..  ....cdddddddddddxxxxxxxxxxxxxxxxxxxxx\n" +
                "cloodddddddddddddddddddxxddxddl;,,'';okOOOOO0000OOOOkkxxxxxxxxxxxddollc:;,,,,,...........      ..  ...      ... ....;ldddddddddxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "';clooddddddddddddddddddxddxddl,,;,,ckOO000000000OOOkkkkxxxxxxdxdddoolllc:;;;,'.......................     ... ....;oddddddddxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "..';:looddddddddddddddddxxdxxdl;,,;lx000000000000OOOOkkkxxxxxxdddddooooolllcc:,'.......................    ..  ...,ldddddddxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "....';clodddddddddddddxxxxddxxoc:;cdkxkO000000Okxolcc::ccccllloodddooolooollllcc;,,''.......................  ....:ddddddddxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "......,:loddddddddddddxxxxxxxddl:::;;;;cdkO000kdlc:::::::cccccc:ccclooooooolllllc::,,'............................cddddddddxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                ".......':lodddddddddddxxxdxxxxxxoloddolccoxO0Okdoooooddddxddddoooc::c:clllllllllcll:,,'.'''''....................,oddxddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "  ......';lodddddddddxxxxxdddxddxkkkkxddooxOOxdollloolloolcclloxOxllc:looooooooolllc;,''''',,.....'''...........'lddxxxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                ".........';lodddddddddxxxooxkxocoxxxxdlldccxkxc;:clooloxd:,;:ccoOx:clcclllllllooolllc:::;;;;,...';cl:,..........;oddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                ".. .......':loddddddddxxxodkOxo:;cldkkddxloOOdc;;:looodxdollllcd0Olcllllllllllllllll:;;;,,;;'....';ll:,.........:ddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "   ........,coddddddddxxxdx0KOkl,lxkOkkOOkOOkxollodddxxxxxxddddkOxoooooooooodddooddoc;,,...,;::::clooolc:......,oxddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "   ........';lodddddddxxxxx0K0Od;ckOOO00K00OxddoddxxkOOkkkkkkxxxxxxddddddddddddddddol:;,'':clooddoooollllc,....:dxdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "   .........,:ldddddddxxxxxxkkkxlok000000OkkxdddddxxkOOOOOOOOkkkkkxxxxdddddddddddddoolc:;cloooddoccoddlcloc...'lxddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "   .........';lodddddddxxxxxxxxxxkO000000OkxddddoodxkOOOOOOOOOOOkkkxxxxxdddddddddddddoollooollolclodddollol'..;dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "   ..........,coddddddddddxxxxxxxkO00KKK0OxxdoooolodkOOOOOOOOOOOkkkxxxxxddddddddddddddoooooodol:,;lodddlcoc..,oxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                ".  ..........':lodddddddxxxxxxxxxk000KK00kxdddddddlldkOOOOOOOOOkkkkxxxxxddddddddddddddddododxo:::clloddoll,..:dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "..............;loddddddddddxxxxxxkOK000Okxocc::codlcokOOOOOOOOkkkkkxxxxdddddddddddddddddddddoc::clllodddl;..,oxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "..............,codddddddxddxxxxxxxk0K00OkxdolcclllodkOOOOOOOOOkkkkxxxxxddddddddddddddddddddollddoddddddo;..'cdxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "......  ......,cldddddddddddxxxxxxk0000kkkkkxddodxxkkOOOOOOOOOkkkxxxxxdddddddddddddddddxdoodxxxxxxxxddl;...;dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "......  ......':loddddddddddxxxxxxk00OkkOkkkxxxddxxxxkkkkkkOOkkkkxxxxddddddddddddddddddxdoodxxxxxxxxdc'...;lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "......  ......';coddddddddddxxxxxxxkOOOOOkkkkxxxdxxxdxxkkkkkkkkxxxxdxddddddooddoodddodxxxdoodddddxxxl,...;oxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                " .....  .......,coddddddddddxxxxxxxxkOOkkkkxdddoooddxxxxxxxxxxxxxxdddddddddooooooodoodxxxdlllooodxxdl,..:dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                " ....   .......,:lddddddddddxxxxxxxxkkxdxdollllcccccldxkxxxxxxxxxxddddddddooooooooooddxxdooooddddxxdc..:dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                " ...    .......';lodddddddddxxxxxxxxOOOkOkkkxxdooooodxkkkxxxxxdddddddddooooooooooooddxxxdooddddxxxxdc.;dxxxxkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "...    .........,cooddddddddxxxxxxxxk00OOOOkkxdddxxxkkOkkxxxddddddddooooooooooooooooddddoodddxxxxxxdc,cxxxxkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" +
                "...    .........,:loddddddddxxxxxxxxxO00Okkkkkkkkkkkkkkkxddddoddoddoooooooollloooooodddooddddxxxxxxxl:lxxxxkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdd\n" +
                "..     .........';coddddddddxxxxxxxxxkO0Okxxxkkkkkkkxxxddooooooddooollllllllllllloodooooddddddxxxxxxoldxxxkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdd\n" +
                "       ..........,clodddddddxxxxxxxxxxk00OkkkkOOkkxddoolccccllloolllccccccccccllloooooodddddddxxxxxxddxxkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdd\n" +
                "       ..........,:loddddddddxxdxxxxxxxkOO00OOOkxxdolllcc;;::::cc:ccc::::::::cllllooooddddxxdxxxxxxdddxxkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddddddd\n" +
                "       ..........';coddddddddxxxxxxxxxdxOOOOOOkxdolccccc:;,,;;;;;;:::;:::ccllllllooodddddxxxddxddddddxkkkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddd\n" +
                "       ...........,:loddddddddxxxxxxxxddxkkkkxxdolc:;;;,,,'',,,,;;;::ccllllllloooodddddxxxxxxxddddxxk00Okkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddd\n" +
                "      ............,:loddddddddxxxxxxxxxdodxdddolccc;,,'.''''',;;:ccllllloloooooodddxxxxxxxxxxddxkO00OOOkkkkkkkkkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddd\n" +
                ".     ............';cooddddddxxxxxxxxxxddoolllcc::;,,,,,,;;:ccclllooooooooooooodxxxxxxxxxxxxxkO0K0OkxxkOOkkxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddd\n" +
                "..    ............',:loddddddxxxxxxxxxxxxxdollllcccccloooooooooooooooooooddddddxxxkkxkkkkxxk0K0OO0OOOO00Okxxkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddddddddd\n" +
                "..    .............,:loddddddddxxxxxxxxxxxxxxxxxxxxxxxxxkOkkxxxxxdddddddxxxxxxxxxkkkkkkkkOKK0KKkk0000Okxdddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdddddddddddd\n" +
                "..    .............';cloddddddddxxxxxxxxxxxxxxxxxxxxxxxxk00OOkkkkkxxxxxkkkkkkkkkkkkkkkkOKXK0OO00KK0kxxddddddxxxxxxxxxxxxxxxxxxxxxxxxxxxxxddddddddddddd\n" +
                "...   ..............';coodddddddxxxxxxxxxxxxxxxxxxxxxxxxk0K00OOOOOkkkOOOOOOOOOOOkkOOkOKKKOO00KKK0kkxxxxxdddddddxxxxxxxxxxxxxxxxxxxxxxxxddxdddddddddddd\n" +
                "....  ..        .....,:codddddddxxxxxxxxxxxxxxxxxxxxxxxxxO0K0OOOOOOOOO00OOOOOOOOOOOO0XK00O0KXK0Okkkkkxxdddollllllooooddxdddxxxxxxxxxxddddddddddddddddd\n" +
                "...              ....',:lodddddddddxxxxxxxxxxxxxxxxxxxxxxxO00OOOOO000000000000000O0XXXOk0KXK0OOOkxoc:;;,,''........'',;lddddxxxddxdddddddddddddddddddd\n" +
                ".                .....';coddddddddddxxxxxxxxxxxxxxxxxxxxxxxkOOOO00000000000000000KNNKK0KXX0OOOxl:,'.''..................;ldxddxxdddddddddddddddddddddd\n" +
                "                  .....,clodddddddddddxxxxxxxxxxxxxxxxxxxxxxkO00000000KKKKKK000KXNKKKKXX000kdl;'....''''''................,coddddddddddddddddddddddddd\n" +
                ".      ....       .....';clodddddxxdddxxxxxxxxxxxxxxxxxxxxxxkO0000000KKKKK000KXNNNXKKXK0Oxo:,'..'',,''''''''''..............;odddddddddddddddddddddddd\n" +
                "..     .....        .....';:loodddxxddxxxxxxxxxxxxxxxxxxxxxxxk0K0000KKKKK000KXNNNNNNX0kdl;,''''',,,,,,'''''................'';lddddddddddddddddddddddd\n" +
                "...     ..................',;:loddddddxxdxxxxxxxxxxxxxxxxxxxxk0K0000KKKKKKKXNNNNNNKOdlc;,'',,,,,,,,,,,'''.....................,coddddddddddddddddddddd\n" +
                "..      .................',,,;:loddddxxxddxxxxxxxxxxxxxxxxxxxxOKK0KKKKKKKXNXK0KNXOo:;,',,,,,,,,,,,,,,''.'.......................,coddddddddddddddddddd\n" +
                ".    ......',,,''......',,;;;:cloddddxxddddxxxxxxxxxxxxxxxxxxxk0KKKKKKKKNNKK000kl:;,,,,;;,,,,,;;;,,,,,''''......''...............';ldddddddddddddddddd\n" +
                ".........';::::;;,,,,,,;;;::::clodddddddddddddxxxxxxxxxxxxxxxxk0KKKKK0XNXKKXKkl;,,;;;;;;,,,,,;;,,,,,'''''''''''''..................':odddddddddddddddd\n" +
                "......'',:cllllcc::::::::::::ccloddddddddddddddxxxxxxxxxxxxxkxx0KKKKKXXKKKkdl;,;;;;;,,,,,,,;;,,,,,,''''''''''........................;lddddddddddddddd\n" +
                ".''',;;:ccloooooolcccccc::::ccccloodddddddddddxxxxxxxxxxxxxxxolOKK00XXKKOd:;;:;;;;,,,,,,;;;,,,,,,,,'''','''..........................';ldddddddddddddd\n" +
                ",;;;::clloodddddoolcc:::::ccccccclloooooodddddddddxxxxxxxxxxocoO0xx0XKXOl::::;;,,,,,,,,,;;,,,,,,,,'''''...............................',looodddddddddd\n" +
                "ccccllooddddddooollc::::::::::::cccclllooooodddddddxxxxxxxxoccloloOKKXkc::;;;,,,,,,,,,,;;;,,;,,,,''....................................';cddoddddddddd\n" +
                "oooooodddddddoooollcc:::::;;;::::ccccllllllooddddddxxxxdddl:c::clOXKKxc::;;;,,,,,,,,,;;;,,,;;,''......................    ..............';lododddddddd\n" +
                "ooddddddddddooooollllc::::;;;;:::ccccllllllloddddddddddoll::c::ckKK0xc::;,,,,,,,,,,,;;;,,;,,'...........................     ............';loodddddddd\n" +
                "dddddddddddoooolllllllc::;;;;;::::ccccllllllooodddddddlclc:ccclkKX0dc::;,,,,,,,,,',;;,,,,,''''''............................    ..........';lddddddddd\n" +
                "dddddxxddddoollllccccccc:;;;;;:::::cccclllllloodddddlccll:ccclxXN0dc:;;,,,,,;,,',,;;,;,'.''',''''...............................   ........':loddddddd\n" +
                "ddddddddddooollcccc::ccc:;;;;;;::::cccccllcclloooool::ooc:ccldKWKoc:;,;,,;;;,,'',;;;,,''','''''''..................... .    ......    ......':oooddddd\n" +
                "ddddddddddooolllcc:::::::::;;;;;:::::cccccccccllooc::odl:ccldKWKdc:;;,,,;;,''',,;;,,'.',,,'''''.........................      ......    .....,:ooooodd\n" +
                "ddddddddooooollllcc:::::c::;;;;;:::::::ccccccccllc::ldl:ccco0N0dc:;;;;;,,'''',,,,''..','''''''''...................... ..      ..............',cododdo\n" +
                "dddoooooooolllcclccc:::::::;;;;:::::::::cccc::ccc::lxo:cccoOX0dc:;,;;;,,'',,,,,,....',,''''''''...........................       .............';loddoo\n" +
                "ooooooooolllllllccc:::::::::;;;;::::::::ccccc::c::lxo::cclkX0dc:;;;;;,''',,,,,,....',,''....''........................... ..      .............':oodoo\n" +
                "ooolllllllllllllcccc:::::cc:;;;;;::::::::cccccc::cdd:cc:ckX0o:;;;;;,''',,,,,''....',,'......'''.........................   ..      ............';loooo\n" +
                "lllllcccllllllllcc:::::::ccc:;;;;::::;;::cccccc:cdkl:::cdK0o:;;;;;,''',,',''.....',,'......'''.................................      ...........'coooo\n" +
                "lllcccccccllllllcccc::::cccc::;;;;;;;;;;:::cccc:okd::::o0Oo:;;;;,,'',,,''''......,,'......''''......................  ...........     ...........:oddo\n" +
                "lcccccccccccclllcccccc::ccc:::;;;;;;;;;;::::c::ckkc:::cO0o:;;;;,,,,,,,'''.......,,'........''.......................     ........      ..........,lddd\n" +
                "ccc:::::::ccccccccccccc:::cc::;;;;;;;;;;;::::::xOo;:::xOl:;;;,,'',,,''''..... .','.......'..........................       ........     ..........cdxd\n" +
                "c:::;;;::::ccccccc:::::::::::::;;;;;;;;;;;::::oOd:;::dOo;;;;,,',,'''''''..    .''...................................        ........     .........;odo\n" +
                "::;;;;;;;;::cccccc:::::::::::;;;;,,,;;;;;;:;;lOOc;;;okl;;;;,''','''''''...   ..'... ................................          ........    ........,lol\n" +
                ":;;;;;;;;;::ccclcccc:::::::::::;;;;;;;;;;;:::x0o;;;cxo;;;;,,,,,,''''',,'.    .''.......''................................................ ........';:;\n" +
                "OkkkkkkkkkOOOOO0OOOOOOOOkOOOOOOOkkkkkkkkkkkkOXNOkkk0KOkkkkxxkkkxxxxxxkkxdoooddxxxddddxxxxxxxxxxdddxxxxxxdddddddddddddddddddddddddddddddddodddddxddxkkx";
        System.out.println(bone);
        SpringApplication.run(Main.class, args);
    }
}


