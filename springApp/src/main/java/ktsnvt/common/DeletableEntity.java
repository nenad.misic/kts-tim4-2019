package ktsnvt.common;

import org.hibernate.annotations.Where;

import javax.persistence.*;


public interface DeletableEntity extends IDeletable {
    public abstract void setId(Long id);
    public boolean isDeleted();
}
