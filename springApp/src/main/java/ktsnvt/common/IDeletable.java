package ktsnvt.common;

public interface IDeletable {
    public abstract void logicalDelete();
    public abstract boolean isDeleted();
}
