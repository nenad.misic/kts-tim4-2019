package ktsnvt.common;

import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MailTemplates {

    public static String formatRegistrationHtml(String firstName, String registrationLink) throws IOException {
        StringBuilder contentBuilder = new StringBuilder();
        BufferedReader in = new BufferedReader(new FileReader(ResourceUtils.getFile("classpath:registrationMail.html")));
        String str;
        while ((str = in.readLine()) != null) {
            contentBuilder.append(str);
        }
        in.close();
        String content = contentBuilder.toString();

        return content.replaceAll("%firstName", firstName).replaceAll("%registrationLink", registrationLink);
    }
}
