package ktsnvt.common.exceptions;

public class AddException extends Exception {

    public AddException() {
        super();
    }

    public AddException(String msg) {
        super(msg);
    }
}
