package ktsnvt.common.exceptions;


public class DeleteException extends Exception {

    public DeleteException() {
        super();
    }

    public DeleteException(String msg) {
        super(msg);
    }
}
