package ktsnvt.common.exceptions;


public class FetchException extends Exception {

    public FetchException() {
        super();
    }

    public FetchException(String msg) {
        super(msg);
    }
}
