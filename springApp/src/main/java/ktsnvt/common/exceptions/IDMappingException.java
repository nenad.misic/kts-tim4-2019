package ktsnvt.common.exceptions;

public class IDMappingException extends Exception {

    public IDMappingException() {
        super();
    }

    public IDMappingException(String msg) {
        super(msg);
    }
}
