package ktsnvt.common.exceptions;

public class PaymentException extends Exception {

    public PaymentException() { super();  }

    public PaymentException(String message) { super(message); }
}
