package ktsnvt.controller;

import ktsnvt.common.DeletableEntity;
import ktsnvt.common.exceptions.*;
import ktsnvt.dto.EventDto;
import ktsnvt.dto.MessageDto;
import ktsnvt.entity.Event;
import ktsnvt.mapper.IMapper;
import ktsnvt.service.CRUDService;
import ktsnvt.service.EventService;
import org.hibernate.sql.Update;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public abstract class CRUDController<E extends DeletableEntity, DTO> implements IController<DTO> {

        protected CRUDService<E> service;

        protected IMapper<E, DTO> mapper;

        @RequestMapping(path = "/{id}", method = RequestMethod.GET)
        public ResponseEntity<DTO> getById(@PathVariable Long id) {
                try {
                        E entity = service.getById(id);
                        DTO dto = mapper.toDto(entity);
                        if (dto == null) {
                                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                        }
                        return new ResponseEntity<>(dto, HttpStatus.OK);
                } catch (DataAccessException | FetchException | IDMappingException e) {
                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
        }

        @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
        public ResponseEntity<MessageDto> deleteById(@PathVariable Long id) {
                try {
                        service.deleteById(id);
                        return new ResponseEntity<MessageDto>(new MessageDto("Deleted"), HttpStatus.OK);
                } catch (DataAccessException | DeleteException | FetchException e) {
                        return new ResponseEntity<MessageDto>(new MessageDto(e.getMessage()), HttpStatus.BAD_REQUEST);
                }
        }

        @RequestMapping(method = RequestMethod.GET)
        public ResponseEntity<List<DTO>> getAll() {
                try {
                        List<E> entities = service.getAll();
                        List<DTO> dtos = new LinkedList<DTO>();
                        for (E entity: entities) {
                                dtos.add(mapper.toDto((entity)));
                        }
                        return new ResponseEntity<List<DTO>>(dtos, HttpStatus.OK);
                } catch (DataAccessException | IDMappingException e) {
                        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }


        @RequestMapping(path = "/page", method = RequestMethod.GET)
        public ResponseEntity<List<DTO>> getPage(@RequestParam("page") int page, @RequestParam("size") int size) {
                try {
                        Pageable pageable = PageRequest.of(page, size);
                        Page<E> entities = this.service.getPage(pageable);
                        List<E> entitiesList = entities.getContent();
                        List<DTO> dtos = new LinkedList<>();
                        for (E e: entitiesList) {
                                dtos.add(this.mapper.toDto(e));
                        }
                        return new ResponseEntity<List<DTO>>(dtos, HttpStatus.OK);
                } catch (DataAccessException | IDMappingException e) {
                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
        }


        @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<MessageDto> create(@RequestBody DTO dto) {
                try {
                        E entity = mapper.fromDto(dto);
                        entity.setId(-1L);
                        this.service.add(entity);
                        return new ResponseEntity<MessageDto>(new MessageDto("Created"), HttpStatus.OK);
                } catch (DataAccessException | IDMappingException | AddException e) {
                        return new ResponseEntity<MessageDto>(new MessageDto(e.getMessage()),HttpStatus.BAD_REQUEST);
                }
        }

        @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<MessageDto> update(@RequestBody DTO dto){
                try {
                        E entity = mapper.fromDto(dto);
                        this.service.update(entity);
                        return new ResponseEntity<MessageDto>(new MessageDto("Updated"), HttpStatus.OK);
                } catch (DataAccessException | IDMappingException | UpdateException | AddException e) {
                        return new ResponseEntity<MessageDto>(new MessageDto(e.getMessage()), HttpStatus.BAD_REQUEST);
                }
        }

}
