package ktsnvt.controller;

import javafx.util.Pair;
import ktsnvt.common.SearchMode;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.controller.contracts.IEventController;
import ktsnvt.dto.EventDto;
import ktsnvt.dto.MessageDto;
import ktsnvt.entity.Event;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.mapper.EventMapper;
import ktsnvt.service.EventService;
import ktsnvt.service.SpaceLayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/api/event")
public class EventController extends CRUDController<Event, EventDto> implements IEventController {

    @Autowired
    public void setMapper(EventMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setService(EventService service) {
        this.service = service;
    }

    @Autowired
    private SpaceLayoutService spaceLayoutService;

    //todo fix transaction
    @RequestMapping(path="/custom", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDto> create(@RequestBody Map<String, Object> eventMap){
        try {
            Queue<Pair<SpaceLayout, SingleEvent>> toSave = new LinkedList<Pair<SpaceLayout, SingleEvent>>();
            Event event = ((EventMapper) mapper).fromMapObject(eventMap, toSave);
            event.setId(-1L);
            event = ((EventService) service).addAndReturn(event, toSave);
            return new ResponseEntity<>(new MessageDto("Created, id:" + event.getId()), HttpStatus.OK);
        } catch (DataAccessException | IDMappingException | AddException e) {
            return new ResponseEntity<>(new MessageDto(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path="/custom", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@RequestBody Map<String, Object> eventMap) throws AddException {
        try {
            Queue<Pair<SpaceLayout, SingleEvent>> toSave = new LinkedList<Pair<SpaceLayout, SingleEvent>>();
            Event event = ((EventMapper) mapper).fromMapObject(eventMap, toSave);
            Event old = this.service.getById(event.getId());

            event = ((EventService)service).attachOriginalSpaceLayout(event,
                    old);
//            old.setName(event.getName());
//            old.setDescription(event.getDescription());
            this.service.update(event);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (DataAccessException | IDMappingException | UpdateException | FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path="/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDto>> search(@RequestBody Map<String, Object> eventMap, @RequestParam SearchMode mode) {
        try{
            eventMap.put("classType", "single_event");
            Event singleEvent = ((EventMapper) mapper).fromMapObjectForSearch(eventMap);
            Event compositeEvent = null;
            if(canMapToCompositeEvent(eventMap, mode)){
                eventMap.put("classType", "CompositeEvent");
                compositeEvent = ((EventMapper) mapper).fromMapObjectForSearch(eventMap);
            }

            List<Event> entities = ((EventService)service).search(singleEvent, compositeEvent, mode);
            List<EventDto> dtos = new LinkedList<EventDto>();
            for (Event entity: entities) {
                dtos.add(mapper.toDto((entity)));
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } catch (DataAccessException | IDMappingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path = "/report/earnings/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> eventEarningsReport(@PathVariable Long id) {
        try {
            Map<String, Object> retVal = ((EventService)this.service).reportEarningsOfChildren(id);
            return new ResponseEntity<Map<String, Object>>(retVal, HttpStatus.OK);
        } catch (FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(path = "/report/occupancy/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> eventOccupancyReport(@PathVariable Long id) {
        try {
            Map<String, Object> retVal = ((EventService)this.service).reportOccupancyOfChildren(id);
            return new ResponseEntity<Map<String, Object>>(retVal, HttpStatus.OK);
        } catch (FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    private boolean canMapToCompositeEvent(Map<String, Object> eventMap, SearchMode mode) {
        Map<String, Object> spaceMap = null;
        Map<String, Object> spaceLayoutMap = null;
        Map<String, Object> locationMap = null;
        if(eventMap.containsKey("space")){
            spaceMap = (Map<String, Object>) eventMap.get("space");
            if(spaceMap.containsKey("location")) {
                locationMap = (Map<String, Object>) spaceMap.get("location");
            }
        }
        if(eventMap.containsKey("spaceLayout")){
            spaceLayoutMap = (Map<String, Object>) eventMap.get("spaceLayout");
        }

        return  (       mode == SearchMode.Quick) ||
                (       spaceMap==null &&
                        spaceLayoutMap==null) ||
                (       !(spaceMap != null && spaceMap.containsKey("name")) &&
                        !(spaceLayoutMap != null && spaceLayoutMap.containsKey("name")) &&
                        !(locationMap != null && locationMap.containsKey("name")));
    }

}
