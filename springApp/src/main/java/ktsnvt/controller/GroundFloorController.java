package ktsnvt.controller;

import ktsnvt.controller.contracts.IGroundFloorController;
import ktsnvt.dto.GroundFloorDto;
import ktsnvt.entity.GroundFloor;
import ktsnvt.mapper.GroundFloorMapper;
import ktsnvt.service.GroundFloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/groundFloor")
public class GroundFloorController extends CRUDController<GroundFloor, GroundFloorDto> implements IGroundFloorController {
    @Autowired
    public void setService(GroundFloorService service) { this.service = service; }

    @Autowired
    public void setMapper(GroundFloorMapper mapper) { this.mapper = mapper; }
}
