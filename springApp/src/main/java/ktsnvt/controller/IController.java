package ktsnvt.controller;

import ktsnvt.dto.MessageDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IController<TD> {

    public ResponseEntity<TD> getById(Long id);
    public ResponseEntity<MessageDto> deleteById(Long id);
    public ResponseEntity<List<TD>> getAll();
    public ResponseEntity<MessageDto> create(TD dto);
    public ResponseEntity<MessageDto> update(TD dto);
}
