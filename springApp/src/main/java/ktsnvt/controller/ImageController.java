package ktsnvt.controller;

import ktsnvt.dto.MessageDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/image")
public class ImageController {

    private static String UPLOADED_FOLDER_PERMANENT = "src/main/resources/public/";
    private static String UPLOADED_FOLDER_LIVE = "target/classes/public/";

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public ResponseEntity<MessageDto> singleFileUpload(@RequestParam("eventId") Long eventId, @RequestParam("file") MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            Path folder_path_permanent = Paths.get(UPLOADED_FOLDER_PERMANENT + eventId );
            Path folder_path_live = Paths.get(UPLOADED_FOLDER_LIVE + eventId);
            if (!Files.exists(folder_path_permanent)) {
                new File(folder_path_permanent.toString()).mkdir();
            }
            if (!Files.exists(folder_path_live)) {
                new File(folder_path_live.toString()).mkdir();
            }
            Path path_permanent = Paths.get(UPLOADED_FOLDER_PERMANENT + eventId + "/" + file.getOriginalFilename());
            Path path_live = Paths.get(UPLOADED_FOLDER_LIVE + eventId + "/" + file.getOriginalFilename());
            Files.write(path_permanent, bytes);
            Files.write(path_live, bytes);
            return new ResponseEntity<>(new MessageDto("File uploaded"), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/all/{id}", method = RequestMethod.GET)
    public ResponseEntity<MessageDto> getAllImagesForEvent(@PathVariable("id") Long id) {
        try{
            String path = UPLOADED_FOLDER_LIVE + String.valueOf(id);

            File file = new File(path);
            File[] images = file.listFiles();
            if (images.length == 0) {
                return new ResponseEntity<>(new MessageDto("default.jpg"), HttpStatus.OK);
            }
            ArrayList<String> imageNames = new ArrayList<>();
            for (File image: images) {
                imageNames.add(id + "/" + image.getName());
            }

            String retval = String.join(",", imageNames);
            return new ResponseEntity<>(new MessageDto(retval), HttpStatus.OK);

        } catch (NullPointerException e) {
            return new ResponseEntity<>(new MessageDto("default.jpg"), HttpStatus.OK);
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<MessageDto> getFirstImageForEvent(@PathVariable("id") Long id) {

        try{
            String path = UPLOADED_FOLDER_LIVE + String.valueOf(id);

            File file = new File(path);
            File[] images = file.listFiles();
            if (images.length == 0) {
                return new ResponseEntity<>(new MessageDto("default.jpg"), HttpStatus.OK);
            }
            String retval = id + "/" + images[0].getName();
            return new ResponseEntity<>(new MessageDto(retval), HttpStatus.OK);

        } catch (NullPointerException e) {
            return new ResponseEntity<>(new MessageDto("default.jpg"), HttpStatus.OK);
        }

    }

}
