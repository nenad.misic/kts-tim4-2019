package ktsnvt.controller;

import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.controller.contracts.ILocationController;
import ktsnvt.dto.LocationDto;
import ktsnvt.entity.Location;
import ktsnvt.mapper.LocationMapper;
import ktsnvt.service.LocationService;
import org.hibernate.annotations.Fetch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/location")
public class LocationController extends CRUDController<Location, LocationDto> implements ILocationController {

    @Autowired
    public void setService(LocationService service) {
        this.service = service;
    }

    @Autowired
    public void setMapper(LocationMapper mapper) {
        this.mapper = mapper;
    }

    @RequestMapping(path="/report/earnings/{id}", method=RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> locationEarningsDuringTimePeriod
            (@PathVariable Long id,
             @RequestParam("startDate")@DateTimeFormat(pattern = "dd.MM.yyyy") Date startDate,
             @RequestParam("endDate")@DateTimeFormat(pattern = "dd.MM.yyyy") Date endDate) {
        try {
            Map<String, Object> retVal = ((LocationService)this.service).reportEarningsDuringTimePeriod(id, startDate, endDate);
            return new ResponseEntity<Map<String, Object>>(retVal, HttpStatus.OK);
        } catch (FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path="/report/occupancy/{id}", method=RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> locationOccupancyDuringTimePeriod
            (@PathVariable Long id,
             @RequestParam("startDate")@DateTimeFormat(pattern = "dd.MM.yyyy") Date startDate,
             @RequestParam("endDate")@DateTimeFormat(pattern = "dd.MM.yyyy") Date endDate) {
        try {
            Map<String, Object> retVal = ((LocationService)this.service).reportOccupancyDuringTimePeriod(id, startDate, endDate);
            return new ResponseEntity<Map<String, Object>>(retVal, HttpStatus.OK);
        } catch (FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path = "/findByNamePage", method = RequestMethod.GET)
    public ResponseEntity<List<LocationDto>> findByNamePage(@RequestParam("name") String name, @RequestParam("page") int page, @RequestParam("size") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Location> entities = ((LocationService)this.service).findPageByName(name, pageable);
            List<Location> entitiesList = entities.getContent();
            List<LocationDto> dtos = new LinkedList<>();
            for (Location e: entitiesList) {
                dtos.add(this.mapper.toDto(e));
            }
            return new ResponseEntity<List<LocationDto>>(dtos, HttpStatus.OK);
        } catch (DataAccessException | IDMappingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
