package ktsnvt.controller;

import ktsnvt.common.exceptions.FetchException;
import ktsnvt.controller.contracts.ISeatSpaceController;
import ktsnvt.dto.SeatDto;
import ktsnvt.dto.SeatSpaceDto;
import ktsnvt.entity.SeatSpace;
import ktsnvt.mapper.SeatSpaceMapper;
import ktsnvt.service.SeatSpaceService;
import ktsnvt.service.contracts.ISeatSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;
import java.util.List;

@Repository
@RequestMapping("/api/seatSpace")
public class SeatSpaceController extends CRUDController<SeatSpace, SeatSpaceDto> implements ISeatSpaceController {
    @Autowired
    public void setService(SeatSpaceService service) { this.service = service; }

    @Autowired
    public void setMapper(SeatSpaceMapper mapper) { this.mapper = mapper; }

    @GetMapping(path = "/{id}/seatsTaken")
    public ResponseEntity<List<SeatDto>> getSeatsTaken(@PathVariable Long id) {
        try {
            List<SeatDto> seats = ((SeatSpaceService) service).getSeatsTaken(id);
            return new ResponseEntity<>(seats, HttpStatus.OK);
        } catch (FetchException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
