package ktsnvt.controller;

import ktsnvt.controller.contracts.ISpaceController;
import ktsnvt.dto.SpaceDto;
import ktsnvt.entity.Space;
import ktsnvt.mapper.SpaceMapper;
import ktsnvt.service.SpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/space")
public class SpaceController extends CRUDController<Space, SpaceDto> implements ISpaceController {

    @Autowired
    public void setService(SpaceService service) {
        this.service = service;
    }

    @Autowired
    public void setMapper(SpaceMapper mapper) {
        this.mapper = mapper;
    }

}
