package ktsnvt.controller;

import ktsnvt.controller.contracts.ISpaceLayoutController;
import ktsnvt.dto.SpaceLayoutDto;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.mapper.SpaceLayoutMapper;
import ktsnvt.service.SpaceLayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/spaceLayout")
public class SpaceLayoutController extends CRUDController<SpaceLayout, SpaceLayoutDto> implements ISpaceLayoutController {
    @Autowired
    public void setService(SpaceLayoutService service) { this.service = service; }

    @Autowired
    public void setMapper(SpaceLayoutMapper mapper) { this.mapper = mapper; }
}
