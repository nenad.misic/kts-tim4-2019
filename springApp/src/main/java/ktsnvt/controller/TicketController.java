package ktsnvt.controller;

import com.paypal.base.rest.PayPalRESTException;
import ktsnvt.common.exceptions.*;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.controller.contracts.ITicketController;
import ktsnvt.dto.MessageDto;
import ktsnvt.dto.PaymentDto;
import ktsnvt.dto.SinglePaymentDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.Ticket;
import ktsnvt.mapper.TicketMapper;
import ktsnvt.service.PaymentService;
import ktsnvt.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ticket")
public class TicketController extends CRUDController<Ticket, TicketDto> implements ITicketController {

    @Autowired
    public void setService(TicketService service) {this.service = service;}

    @Autowired
    public void setMapper(TicketMapper mapper) {this.mapper = mapper;}

    @Autowired
    private PaymentService paymentService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public ResponseEntity<MessageDto> create(@RequestBody TicketDto dto) {
        return new ResponseEntity<>(new MessageDto("Invalid api endpoint."), HttpStatus.BAD_REQUEST);

    }


    @RequestMapping(path = "/custom", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDto> reserveTickets(@RequestBody List<TicketDto> ticketDtos) {
        try {
            ((TicketService)service).add(ticketDtos);
            return new ResponseEntity<>(new MessageDto("Tickets reserved"), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new MessageDto(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    //todo make it work with paypal
    @RequestMapping(method = RequestMethod.POST, path = "/{id}/pay")
    public ResponseEntity<Map<String, Object>> payForTicket(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(this.paymentService.createPayment(id), HttpStatus.OK);
        } catch (Exception e) {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("message", e.getMessage());
            result.put("status", "failed");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}/complete")
    public ResponseEntity<Map<String, Object>> completePaymentTicket(@RequestBody SinglePaymentDto singlePaymentDto) {
        try {
            return new ResponseEntity<>(this.paymentService.completePayment(singlePaymentDto), HttpStatus.OK);
        } catch (UpdateException | AddException | PaymentException | FetchException | PayPalRESTException | IDMappingException e) {
            Map<String, Object> result = new HashMap<>();
            result.put("message", e.getMessage());
            result.put("status", "failed");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "pay", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> payForTickets(@RequestBody ArrayList<TicketDto> tickets) {
        try {
            return new ResponseEntity<>(this.paymentService.createPayment(tickets), HttpStatus.OK);
        } catch (FetchException e) {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("message", e.getMessage());
            result.put("status", "failed");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "complete", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> completePaymentTickets(@RequestBody PaymentDto paymentDto) {
        try {
            return new ResponseEntity<>(this.paymentService.completePayment(paymentDto), HttpStatus.OK);
        } catch (AddException | PaymentException | FetchException | PayPalRESTException | IDMappingException e) {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("message", e.getMessage());
            result.put("status", "failed");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path="/user")
    public ResponseEntity<List<TicketDto>> getUserTickets(@RequestParam("page") int page, @RequestParam("size") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            List<Ticket> ticketList = ((TicketService) this.service).getUserTickets(pageable);
            List<TicketDto> ticketDtos = new LinkedList<>();
            for (Ticket ticket: ticketList) {
                ticketDtos.add(this.mapper.toDto(ticket));
            }
            return new ResponseEntity<List<TicketDto>>(ticketDtos, HttpStatus.OK);
        } catch (IDMappingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

//    @RequestMapping(path="/custom", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Void> create(@RequestBody Map<String, Object> eventMap){
}
