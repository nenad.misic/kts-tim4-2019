package ktsnvt.controller;

import ktsnvt.controller.contracts.IUserController;
import ktsnvt.dto.LoginDTO;
import ktsnvt.dto.MessageDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.User;
import ktsnvt.mapper.UserMapper;
import ktsnvt.repository.UserRepository;
import ktsnvt.security.TokenUtils;
import ktsnvt.service.UserService;
import ktsnvt.service.contracts.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
//import sun.misc.Request;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;

@RestController
@RequestMapping("api/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController extends CRUDController<User, UserDto> implements IUserController {

    @Autowired
    public void setService(UserService userService) {this.service = userService;}

    @Autowired
    public void setMapper(UserMapper userMapper) {this.mapper = userMapper;}

    @Autowired
    private UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    TokenUtils tokenUtils;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<MessageDto> login(@RequestBody LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            UserDetails details = ((IUserService)service).loadUserByUsername(loginDTO.getUsername());

            User user = this.userRepository.findByUsername(loginDTO.getUsername());

            return new ResponseEntity<MessageDto>(new MessageDto(tokenUtils.generateToken(user)), HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<MessageDto>(new MessageDto("Invalid username or password"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@Valid @RequestBody UserDto user) {
        try{
            User userEntity = ((IUserService)service).register(user);
            UserDto userDto = mapper.toDto(userEntity);
            return new ResponseEntity<>(userDto, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/verify/{token}", method = RequestMethod.GET)
    public ResponseEntity<MessageDto> verifyUser(@PathVariable String token){
        try{
            ((IUserService)service).verifyUser(token);
            return new ResponseEntity<>(new MessageDto("User verified"), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(new MessageDto(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
