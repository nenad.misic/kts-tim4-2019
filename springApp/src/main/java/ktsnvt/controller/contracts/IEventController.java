package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.EventDto;

public interface IEventController extends IController<EventDto> {
}
