package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.GroundFloorDto;

public interface IGroundFloorController extends IController<GroundFloorDto> {
}
