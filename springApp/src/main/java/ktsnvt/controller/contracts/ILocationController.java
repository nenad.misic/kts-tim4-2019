package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.LocationDto;

public interface ILocationController extends IController<LocationDto> {
}
