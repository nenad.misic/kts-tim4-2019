package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.SeatSpaceDto;

public interface ISeatSpaceController extends IController<SeatSpaceDto> {
}
