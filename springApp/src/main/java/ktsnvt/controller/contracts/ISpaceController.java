package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.SpaceDto;

public interface ISpaceController extends IController<SpaceDto> {
}
