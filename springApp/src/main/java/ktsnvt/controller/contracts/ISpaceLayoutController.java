package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.SpaceLayoutDto;

public interface ISpaceLayoutController extends IController<SpaceLayoutDto> {
}
