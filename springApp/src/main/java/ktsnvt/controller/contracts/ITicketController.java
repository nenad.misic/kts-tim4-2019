package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.TicketDto;

public interface ITicketController extends IController<TicketDto> {
}
