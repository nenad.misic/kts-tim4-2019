package ktsnvt.controller.contracts;

import ktsnvt.controller.IController;
import ktsnvt.dto.UserDto;

public interface IUserController extends IController<UserDto> {
}
