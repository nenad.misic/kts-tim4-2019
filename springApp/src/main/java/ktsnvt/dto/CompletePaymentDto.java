package ktsnvt.dto;

import ktsnvt.entity.Ticket;

import java.util.ArrayList;

public class CompletePaymentDto {

    private String paymentId;
    private ArrayList<TicketDto> tickets;

    public CompletePaymentDto() {
        tickets = new ArrayList<>();
    }

    public CompletePaymentDto(String paymentId, ArrayList<TicketDto> tickets) {
        this.paymentId = paymentId;
        this.tickets = tickets;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public ArrayList<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<TicketDto> tickets) {
        this.tickets = tickets;
    }
}
