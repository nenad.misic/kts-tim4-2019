package ktsnvt.dto;

import java.util.Set;

public class CompositeEventDto extends EventDto {

    private static final long serialVersionUID = -6201452787277650212L;
    private Set<EventDto> childEvents;

    public CompositeEventDto() {
        super();
    }

    public Set<EventDto> getChildEvents() {
        return childEvents;
    }

    public void setChildEvents(Set<EventDto> childEvents) {
        this.childEvents = childEvents;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
