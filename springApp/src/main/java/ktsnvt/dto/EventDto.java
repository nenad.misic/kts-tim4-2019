package ktsnvt.dto;

import ktsnvt.entity.CompositeEvent;
import ktsnvt.entity.EventType;

import java.io.Serializable;

public class EventDto implements Serializable {

    private static final long serialVersionUID = -836262944383722572L;
    private Long id;

    private String name;

    private String description;

    private EventType type;

    private CompositeEventDto parentEvent;

    private String classType;

    public EventDto() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public CompositeEventDto getParentEvent() {
        return parentEvent;
    }


    public void setParentEvent(CompositeEventDto parentEvent) {
        this.parentEvent = parentEvent;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
