package ktsnvt.dto;

import ktsnvt.entity.Ticket;

import java.io.Serializable;
import java.util.Set;

public class GroundFloorDto implements Serializable {
    private Long id;
    private int capacity;
    private double price;
    private int top;
    private int left;
    private String name;
    private double height;
    private double width;
    private Integer spotsReserved;
    private Long spaceLayoutId;
    private Set<TicketDto> tickets;

    public GroundFloorDto() { super(); }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Long getSpaceLayoutId() {
        return spaceLayoutId;
    }

    public void setSpaceLayoutId(Long spaceLayoutId) {
        this.spaceLayoutId = spaceLayoutId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 2L;

    public Set<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(Set<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public Integer getSpotsReserved() {
        return spotsReserved;
    }

    public void setSpotsReserved(Integer spotsReserved) {
        this.spotsReserved = spotsReserved;
    }
    
    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
