package ktsnvt.dto;

import java.io.Serializable;
import java.util.Set;

public class LocationDto implements Serializable {
    private Long id;
    private String country;
    private String city;
    private String address;
    private String name;
    private Set<SpaceDto> spaces;
    private Double longitude;
    private Double latitude;


    public LocationDto(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SpaceDto> getSpaces() {
        return spaces;
    }

    public void setSpaces(Set<SpaceDto> spaces) {
        this.spaces = spaces;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 2401745303204690548L;
}
