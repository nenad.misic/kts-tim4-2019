package ktsnvt.dto;

import java.util.ArrayList;

public class PaymentDto {

    private String paymentId;
    private String payerId;
    private ArrayList<TicketDto> tickets;

    public PaymentDto() {
    }

    public PaymentDto(String paymentId, String payerId, ArrayList<TicketDto> tickets) {
        this.paymentId = paymentId;
        this.payerId = payerId;
        this.tickets = tickets;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public ArrayList<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<TicketDto> tickets) {
        this.tickets = tickets;
    }
}
