package ktsnvt.dto;

import java.io.Serializable;

public class SeatDto implements Serializable {

    private int row;
    private int seat;

    public SeatDto() {
        super();
    }

    public SeatDto(int row, int seat) {
        this.row = row;
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
}
