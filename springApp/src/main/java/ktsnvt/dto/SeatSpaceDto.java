package ktsnvt.dto;

import java.io.Serializable;
import java.util.Set;

public class SeatSpaceDto implements Serializable {
    private Long id;
    private int rows;
    private int seats;
    private double price;
    private int top;
    private int left;
    private String name;
    private Long spaceLayoutId;
    private Set<TicketDto> tickets;


    public SeatSpaceDto() { super(); }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getSpaceLayoutId() {
        return spaceLayoutId;
    }

    public void setSpaceLayoutId(Long spaceLayoutId) {
        this.spaceLayoutId = spaceLayoutId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 3L;

    public Set<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(Set<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
