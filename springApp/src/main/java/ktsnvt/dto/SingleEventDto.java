package ktsnvt.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import ktsnvt.entity.EventState;

import java.util.Date;
import java.util.Set;

public class SingleEventDto extends EventDto {

    private static final long serialVersionUID = -2531123730961647225L;
    private EventState state;
    @JsonFormat(shape=JsonFormat.Shape.NUMBER)
    private Date startDate;
    @JsonFormat(shape=JsonFormat.Shape.NUMBER)
    private Date endDate;
    private Set<TicketDto> tickets;
    private Long spaceId;
    private SpaceDto space;
    private Long spaceLayoutId;
    private SpaceLayoutDto spaceLayout;

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<TicketDto> getTickets() {
        return tickets;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }

    public Long getSpaceLayoutId() {
        return spaceLayoutId;
    }

    public void setSpaceLayoutId(Long spaceLayoutId) {
        this.spaceLayoutId = spaceLayoutId;
    }

    public SpaceDto getSpace() {
        return space;
    }

    public void setSpace(SpaceDto space) {
        this.space = space;
    }

    public SpaceLayoutDto getSpaceLayout() {
        return spaceLayout;
    }

    public void setSpaceLayout(SpaceLayoutDto spaceLayout) {
        this.spaceLayout = spaceLayout;
    }

    public void setTickets(Set<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
