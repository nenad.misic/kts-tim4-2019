package ktsnvt.dto;

public class SinglePaymentDto {

    private String paymentId;
    private String payerId;
    private TicketDto ticket;

    public SinglePaymentDto(String paymentId, String payerId, TicketDto ticket) {
        this.paymentId = paymentId;
        this.payerId = payerId;
        this.ticket = ticket;
    }

    public SinglePaymentDto() {
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public TicketDto getTicket() {
        return ticket;
    }

    public void setTicket(TicketDto ticket) {
        this.ticket = ticket;
    }
}
