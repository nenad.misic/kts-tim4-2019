package ktsnvt.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ktsnvt.entity.SingleEvent;

import java.io.Serializable;
import java.util.Set;

public class SpaceDto implements Serializable {
    private Long id;
    private String name;
    private LocationDto location;
    private Long locationId;
    private Set<SpaceLayoutDto> defaultSpaceLayouts;
    private Set<SingleEventDto> events;

    public SpaceDto(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SpaceLayoutDto> getDefaultSpaceLayouts() {
        return defaultSpaceLayouts;
    }

    public void setDefaultSpaceLayouts(Set<SpaceLayoutDto> defaultSpaceLayouts) {
        this.defaultSpaceLayouts = defaultSpaceLayouts;
    }

    public Set<SingleEventDto> getEvents() {
        return events;
    }

    public void setEvents(Set<SingleEventDto> events) {
        this.events = events;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 2401745303204690549L;
}
