package ktsnvt.dto;

import java.io.Serializable;
import java.util.Set;

public class SpaceLayoutDto implements Serializable {
    private Long id;
    private SpaceDto space;
    private Long spaceId;
    private Set<SeatSpaceDto> seatSpaces;
    private Set<GroundFloorDto> groundFloors;
    private SingleEventDto singleEvent;
    private Long singleEventId;
    private String name;

    public SpaceLayoutDto() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<SeatSpaceDto> getSeatSpaces() {
        return seatSpaces;
    }

    public void setSeatSpaces(Set<SeatSpaceDto> seatSpaces) {
        this.seatSpaces = seatSpaces;
    }

    public Set<GroundFloorDto> getGroundFloors() {
        return groundFloors;
    }

    public void setGroundFloors(Set<GroundFloorDto> groundFloors) {
        this.groundFloors = groundFloors;
    }

    public SpaceDto getSpace() {
        return space;
    }

    public void setSpace(SpaceDto space) {
        this.space = space;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }

    public SingleEventDto getSingleEvent() {
        return singleEvent;
    }

    public void setSingleEvent(SingleEventDto singleEvent) {
        this.singleEvent = singleEvent;
    }

    public Long getSingleEventId() {
        return singleEventId;
    }

    public void setSingleEventId(Long singleEventId) {
        this.singleEventId = singleEventId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
