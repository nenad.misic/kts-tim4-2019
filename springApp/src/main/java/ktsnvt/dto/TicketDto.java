package ktsnvt.dto;


import java.io.Serializable;
import java.util.Date;

public class TicketDto implements  Serializable {

    private double price;

    private Long id;

    private boolean paid;

    private Date expirationDate;

    private UserDto user;

    private Long userId;

    private GroundFloorDto groundFloor;

    private Long groundFloorId;

    private SeatSpaceDto seatSpace;

    private Long seatSpaceId;

    private Long singleEventId;

    private SingleEventDto singleEvent;

    private Integer row;

    private Integer seat;

    public TicketDto() {super();}

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private static final long serialVersionUID = 566009487672156439L;

    public GroundFloorDto getGroundFloor() {
        return groundFloor;
    }

    public void setGroundFloor(GroundFloorDto groundFloor) {
        this.groundFloor = groundFloor;
    }

    public Long getGroundFloorId() {
        return groundFloorId;
    }

    public void setGroundFloorId(Long groundFloorId) {
        this.groundFloorId = groundFloorId;
    }

    public SeatSpaceDto getSeatSpace() {
        return seatSpace;
    }

    public void setSeatSpace(SeatSpaceDto seatSpace) {
        this.seatSpace = seatSpace;
    }

    public Long getSeatSpaceId() {
        return seatSpaceId;
    }

    public void setSeatSpaceId(Long seatSpaceId) {
        this.seatSpaceId = seatSpaceId;
    }

    public Long getSingleEventId() {
        return singleEventId;
    }

    public void setSingleEventId(Long singleEventId) {
        this.singleEventId = singleEventId;
    }

    public SingleEventDto getSingleEvent() {
        return singleEvent;
    }

    public void setSingleEvent(SingleEventDto singleEvent) {
        this.singleEvent = singleEvent;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
}
