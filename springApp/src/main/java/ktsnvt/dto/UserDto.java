package ktsnvt.dto;

import ktsnvt.entity.UserRole;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Set;

public class UserDto implements Serializable {

    private Long id;

    private String username;

    private String password;

    private String name;

    private String surname;

    private boolean verified;

    private String email;

    private UserRole role;

    private Set<TicketDto> tickets;

    public UserDto() {super();}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Set<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(Set<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    private static final long serialVersionUID = 4814959865004673036L;
}
