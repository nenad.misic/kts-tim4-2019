package ktsnvt.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Entity
@DiscriminatorValue("CompositeEvent")
public class CompositeEvent extends Event  {




    @OneToMany(cascade = CascadeType.MERGE)
    @Where(clause = "deleted = 'false'")
    @JoinTable(name="event_child_parent",
            joinColumns = {
                    @JoinColumn(name="parent", referencedColumnName = "id"),
            },
            inverseJoinColumns = {
                    @JoinColumn(name="child", referencedColumnName = "id")
            })
    private Set<Event> childEvents;


    public CompositeEvent() {
        super();
    }

    public Set<Event> getChildEvents() {
        return childEvents;
    }

    public void setChildEvents(Set<Event> childEvents) {
        this.childEvents = childEvents;
    }
}
