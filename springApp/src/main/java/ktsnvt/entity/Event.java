package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="event_type", discriminatorType = DiscriminatorType.STRING)
@Table(name="event")
@Where(clause = "deleted = 'false'")
public abstract class Event implements DeletableEntity {

    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="type")
    private EventType type;

    @Column(name="event_type", insertable = false, updatable = false)
    private String classType;


    @ManyToOne(cascade = CascadeType.PERSIST)
    //@JoinColumn(name="parent_event")
    @JoinTable(name="event_child_parent",
    joinColumns = {
            @JoinColumn(name="child", referencedColumnName = "id"),
    },
    inverseJoinColumns = {
            @JoinColumn(name="parent", referencedColumnName = "id")
    })
    private CompositeEvent parentEvent;

    public Event() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public CompositeEvent getParentEvent() {
        return parentEvent;
    }

    public void setParentEvent(CompositeEvent parentEvent) {
        this.parentEvent = parentEvent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    @Override
    public void logicalDelete() {
        this.deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
