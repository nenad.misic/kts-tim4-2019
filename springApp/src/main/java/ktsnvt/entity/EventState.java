package ktsnvt.entity;

public enum EventState {
    ForSale, Sold, Cancelled, Over
}
