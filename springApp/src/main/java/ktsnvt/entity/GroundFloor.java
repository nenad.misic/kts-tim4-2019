package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

// TO-DO dodati top i left atribute (za prikaz)


@Entity
@Table(name="ground_floor")
@Where(clause = "deleted = 'false'")
public class GroundFloor implements DeletableEntity {
    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="capacity")
    private Integer capacity;

    @Column(name="spots_reserved", columnDefinition = "int default 0")
    private Integer spotsReserved;

    @Column(name="topCoordinate")
    private int top;

    @Column(name="leftCoordinate")
    private int left;

    @Column(name="name")
    private String name;
    @Column(name="price")
    private double price;
    @Column(name="height")
    private double height;
    @Column(name="width")
    private double width;

    @OneToMany(mappedBy="groundFloor")
    private Set<Ticket> tickets;

    @ManyToOne()
    @JoinColumn(name="space_layout")
    private SpaceLayout spaceLayout;

    public SpaceLayout getSpaceLayout() {
        return spaceLayout;
    }

    public void setSpaceLayout(SpaceLayout spaceLayout) {
        this.spaceLayout = spaceLayout;
    }

    public GroundFloor() { super(); }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void logicalDelete() {
        this.deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getSpotsReserved() {
        return spotsReserved;
    }

    public void setSpotsReserved(Integer spotsReserved) {
        this.spotsReserved = spotsReserved;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
