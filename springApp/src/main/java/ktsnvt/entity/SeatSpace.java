package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

// TO-DO dodati top i left atribute (za prikaz)

@Entity
@Table(name="seat_space")
@Where(clause = "deleted = 'false'")
public class SeatSpace implements DeletableEntity {

    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="rows")
    private Integer rows;

    @Column(name="seats")
    private Integer seats;

    @Column(name="price")
    private double price;


    @Column(name="topCoordinate")
    private int top;

    @Column(name="leftCoordinate")
    private int left;

    @Column(name="name")
    private String name;

    @ManyToOne()
    @JoinColumn(name="space_layout")
    private SpaceLayout spaceLayout;

    @OneToMany(mappedBy="seatSpace", cascade = CascadeType.MERGE)
    private Set<Ticket> tickets;

    public SeatSpace() { super(); }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public SpaceLayout getSpaceLayout() {
        return spaceLayout;
    }

    public void setSpaceLayout(SpaceLayout spaceLayout) {
        this.spaceLayout = spaceLayout;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void logicalDelete() {
        this.deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
