package ktsnvt.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@DiscriminatorValue("single_event")
public class SingleEvent extends Event {

    @Column(name = "state")
    private EventState state;


    @Column(name="start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name="end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @ManyToOne()
    @JoinColumn(name = "space")
    private Space space;

    @OneToOne( cascade = CascadeType.MERGE)
    @JoinColumn(name = "spaceLayout")
    private SpaceLayout spaceLayout;

    @OneToMany(mappedBy = "singleEvent")
    private Set<Ticket> tickets;


    public SingleEvent() {
        super();
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public SpaceLayout getSpaceLayout() {
        return spaceLayout;
    }

    public void setSpaceLayout(SpaceLayout spaceLayout) {
        this.spaceLayout = spaceLayout;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> ticket) {
        this.tickets = ticket;
    }
}
