package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="space")
@Where(clause="deleted = false")
public class Space implements DeletableEntity {

    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name")
    private String name;

    @ManyToOne()
    @JoinColumn(name="location")
    private Location location;

    @OneToMany(mappedBy="space")
    @Where(clause="deleted = false")
    private Set<SpaceLayout> defaultSpaceLayouts;

    @OneToMany(mappedBy="space")
    @Where(clause="deleted = false")
    private Set<SingleEvent> events;

    public Space(){
        super();
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SpaceLayout> getDefaultSpaceLayouts() {
        return defaultSpaceLayouts;
    }

    public void setDefaultSpaceLayouts(Set<SpaceLayout> defaultSpaceLayouts) {
        this.defaultSpaceLayouts = defaultSpaceLayouts;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Set<SingleEvent> getEvents() {
        return events;
    }

    public void setEvents(HashSet<SingleEvent> events) {
        this.events = events;
    }

    @Override
    public void logicalDelete() {
        this.deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
