package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="space_layout")
@Where(clause = "deleted = 'false'")
public class SpaceLayout implements DeletableEntity {
    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy="spaceLayout", cascade = CascadeType.MERGE)
    @Where(clause="deleted = false")
    private Set<GroundFloor> groundFloors;

    @OneToMany(mappedBy="spaceLayout", cascade = CascadeType.MERGE)
    @Where(clause="deleted = false")
    private Set<SeatSpace> seatSpaces;

    @ManyToOne()
    @JoinColumn(name="space")
    private Space space;

    @OneToOne()
    @JoinColumn(name="singleEvent")
    @Where(clause="deleted = false")
    private SingleEvent singleEvent;

    public SpaceLayout() { super(); }

    public Set<GroundFloor> getGroundFloors() {
        return groundFloors;
    }

    public void setGroundFloors(Set<GroundFloor> groundFloors) {
        this.groundFloors = groundFloors;
    }

    public Set<SeatSpace> getSeatSpaces() {
        return seatSpaces;
    }

    public void setSeatSpaces(Set<SeatSpace> seatSpaces) {
        this.seatSpaces = seatSpaces;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public SingleEvent getSingleEvent() {
        return singleEvent;
    }

    public void setSingleEvent(SingleEvent singleEvent) {
        this.singleEvent = singleEvent;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void logicalDelete() {
        this.deleted = true;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
