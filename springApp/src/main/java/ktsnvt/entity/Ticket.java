package ktsnvt.entity;

import ktsnvt.common.DeletableEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ticket")
@Where(clause = "deleted = 'false'")
public class Ticket implements DeletableEntity {

    @Column(name="deleted")
    private boolean deleted;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="price")
    private double price;

    @Column(name="paid")
    private boolean paid;

    @Column(name="expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @ManyToOne()
    @JoinColumn(name="customer")
    private User user;

    @ManyToOne()
    @JoinColumn(name="ground_floor")
    private GroundFloor groundFloor;

    @ManyToOne()
    @JoinColumn(name="seat_space")
    private SeatSpace seatSpace;

    @Column(name="seatRow")
    private int row;

    @Column
    private int seat;

    @ManyToOne()
    @JoinColumn(name="singleEvent")
    private SingleEvent singleEvent;

    public Ticket() {super();}

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public GroundFloor getGroundFloor() {
        return groundFloor;
    }

    public void setGroundFloor(GroundFloor groundFloor) {
        this.groundFloor = groundFloor;
    }

    public SeatSpace getSeatSpace() {
        return seatSpace;
    }

    public void setSeatSpace(SeatSpace seatSpace) {
        this.seatSpace = seatSpace;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void logicalDelete() { this.deleted = true; }

    public SingleEvent getSingleEvent() {
        return singleEvent;
    }

    public void setSingleEvent(SingleEvent singleEvent) {
        this.singleEvent = singleEvent;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

}
