package ktsnvt.entity;

import org.hibernate.usertype.UserType;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Set;

@Entity
public class UserAuthority implements GrantedAuthority {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@Enumerated(EnumType.STRING)
	private UserRole role;

	@ManyToMany(mappedBy = "userAuthorities")
	private Set<User> users;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	@Override
	public String getAuthority() {
		return role.toString();
	}
}
