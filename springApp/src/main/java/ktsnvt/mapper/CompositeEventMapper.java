package ktsnvt.mapper;

import ktsnvt.dto.CompositeEventDto;
import ktsnvt.entity.CompositeEvent;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class CompositeEventMapper extends GenericMapper<CompositeEvent, CompositeEventDto> {

    public CompositeEventMapper() {
        super(CompositeEvent.class, CompositeEventDto.class);
        this.addMappings(new PropertyMap<CompositeEvent, CompositeEventDto>() {
            @Override
            public void configure() {
                skip().setParentEvent(null);
            }
        });

        this.addMappings(new PropertyMap<CompositeEventDto, CompositeEvent>() {
            @Override
            public void configure() {
                skip().setParentEvent(null);
            }
        });



        this.addMappings(new PropertyMap<CompositeEvent, CompositeEventDto>() {
            @Override
            public void configure() {
                skip().setChildEvents(null);
            }
        });



        this.addMappings(new PropertyMap<CompositeEventDto, CompositeEvent>() {
            @Override
            public void configure() {
                skip().setChildEvents(null);
            }
        });


    }
}
