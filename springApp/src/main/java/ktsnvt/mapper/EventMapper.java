package ktsnvt.mapper;

import javafx.util.Pair;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.dto.CompositeEventDto;
import ktsnvt.dto.EventDto;
import ktsnvt.dto.SingleEventDto;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceLayoutRepository;
import ktsnvt.repository.SpaceRepository;
import ktsnvt.service.EventService;
import ktsnvt.service.SpaceLayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Component
public class EventMapper implements IMapper<Event, EventDto> {

    @Autowired
    private CompositeEventMapper compositeEventMapper;

    @Autowired
    private SingleEventMapper singleEventMapper;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private SpaceLayoutService spaceLayoutService;

    @Autowired
    private SpaceLayoutRepository spaceLayoutRepository;

    @Autowired
    private EventService eventService;

    @Override
    public Event fromDto(EventDto dto) throws IDMappingException {
        if ("CompositeEvent".equals(dto.getClassType())) {
            CompositeEventDto parentDto = dto.getParentEvent();
            parentDto.setChildEvents(null);
            dto.setParentEvent(parentDto);
            return compositeEventMapper.fromDto((CompositeEventDto) dto);
        } else {
            CompositeEventDto parentDto = dto.getParentEvent();
            parentDto.setChildEvents(null);
            dto.setParentEvent(parentDto);
            return singleEventMapper.fromDto((SingleEventDto) dto);
        }
    }

    @Override
    public EventDto toDto(Event entity) {
        if (entity == null) {
            return null;
        }
        if (entity.getClass().equals(CompositeEvent.class)) {
            HashSet<EventDto> events = new HashSet<>();
            for(Event event: ((CompositeEvent) entity).getChildEvents()) {
                events.add(this.toDto(event));
            }
            CompositeEventDto compositeEventDto = compositeEventMapper.toDto((CompositeEvent) entity);
            compositeEventDto.setChildEvents(events);
            compositeEventDto = (CompositeEventDto) EventMapper.setParentsChildrenToNullRecursively(compositeEventDto);
            return compositeEventDto;
        } else {
            SingleEventDto singleEventDto = singleEventMapper.toDto((SingleEvent) entity);
            singleEventDto = (SingleEventDto) EventMapper.setParentsChildrenToNullRecursively(singleEventDto);
            return singleEventDto;
        }
    }

    private static EventDto setParentsChildrenToNullRecursively(EventDto e){
        if(e.getParentEvent() != null){
            setParentsChildrenToNullRecursively(e.getParentEvent());
            e.getParentEvent().setChildEvents(null);
        }
        return e;
    }


    public Event fromMapObjectForSearch(Map<String, Object> eventMap) {
        return fromMapObjectForSearch(eventMap, null);
    }

    private Event fromMapObjectForSearch(Map<String, Object> eventMap, CompositeEvent parentEvent)
    {
        Event event;
        if (eventMap.get("classType").equals("CompositeEvent")) {
            event = new CompositeEvent();
            Set<Event> childEvents = new HashSet<Event>();
            if(eventMap.containsKey("childEvents") && eventMap.get("childEvents") != null) {
                for (Map<String, Object> childMap : (ArrayList<Map<String, Object>>) eventMap.get("childEvents")) {
                    childEvents.add(fromMapObjectForSearch(childMap, parentEvent));
                }
            }
            ((CompositeEvent)event).setChildEvents(childEvents);

        } else {
            SingleEvent singleEvent = new SingleEvent();
            if(eventMap.get("state") != null)
                singleEvent.setState(EventState.valueOf(eventMap.get("state").toString()));

            if(eventMap.get("startDate") != null){
                Instant dateStart =
                        Instant.ofEpochMilli(Long.parseLong(eventMap.get("startDate").toString())).atZone(ZoneId.systemDefault()).toInstant();
                singleEvent.setStartDate(Date.from(dateStart));
            }


            if(eventMap.get("endDate") != null){
                Instant dateEnd =
                        Instant.ofEpochMilli(Long.parseLong(eventMap.get("endDate").toString())).atZone(ZoneId.systemDefault()).toInstant();
                singleEvent.setEndDate(Date.from(dateEnd));
            }

            if(eventMap.get("space") != null) {
                Map<String,Object> spaceMap = (Map<String, Object>) eventMap.get("space");
                Space space = new Space();
                if (spaceMap.get("name") != null)
                    space.setName(spaceMap.get("name").toString());
                if(spaceMap.get("location") != null){
                    Map<String,Object> locationMap = (Map<String, Object>) spaceMap.get("location");
                    Location location = new Location();
                    if (locationMap.get("country") != null)
                        location.setCountry(locationMap.get("country").toString());
                    if (locationMap.get("city") != null)
                        location.setCity(locationMap.get("city").toString());
                    if (locationMap.get("address") != null)
                        location.setAddress(locationMap.get("address").toString());
                    if (locationMap.get("name") != null)
                        location.setName(locationMap.get("name").toString());
                    space.setLocation(location);
                }
                singleEvent.setSpace(space);
            }
            if(eventMap.get("spaceLayout") != null) {
                Map<String,Object> spaceLayoutMap = (Map<String, Object>) eventMap.get("spaceLayout");
                SpaceLayout spaceLayout = new SpaceLayout();
                if (spaceLayoutMap.get("name") != null)
                    spaceLayout.setName(spaceLayoutMap.get("name").toString());
                singleEvent.setSpaceLayout(spaceLayout);
            }
            event = singleEvent;
        }
        if(eventMap.get("description") != null)
            event.setDescription(eventMap.get("description").toString());
        if(eventMap.get("name") != null)
            event.setName(eventMap.get("name").toString());
        if(eventMap.get("type") != null)
            event.setType(EventType.valueOf(eventMap.get("type").toString()));

        return event;
    }

    public Event fromMapObject(Map<String, Object> eventMap,  Queue<Pair<SpaceLayout, SingleEvent>> toSave) throws IDMappingException {
        try{
            return fromMapObject(eventMap, null, toSave);
        } catch (IDMappingException e ) {
            throw new IDMappingException(e.getMessage());
        } catch (Exception e) {
            throw new IDMappingException("Exception code: 638501. Please contact tech support.");
        }
    }
    
    private Event fromMapObject(Map<String, Object> eventMap, CompositeEvent parentEvent, Queue<Pair<SpaceLayout, SingleEvent>> toSave)
            throws IDMappingException, AddException, FetchException {
        Event event;
        if (eventMap.get("classType").equals("CompositeEvent")) {
            event = new CompositeEvent();
            Set<Event> childEvents = new HashSet<Event>();
            if(eventMap.containsKey("childEvents") && eventMap.get("childEvents") != null) {
                for (Map<String, Object> childMap : (ArrayList<Map<String, Object>>) eventMap.get("childEvents")) {
                    childEvents.add(fromMapObject(childMap, parentEvent, toSave));
                }
            }
            ((CompositeEvent)event).setChildEvents(childEvents);
            
        } else {
            SingleEvent singleEvent = new SingleEvent();
            if(eventMap.get("state") != null)
                singleEvent.setState(EventState.valueOf(eventMap.get("state").toString()));

//            Calendar calendar = Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            calendar.setTimeInMillis(Long.parseLong(eventMap.get("startDate").toString()));
//            String startDatetime =
//                            calendar.get(Calendar.YEAR) + "-" +
//                            calendar.get(Calendar.MONTH) + "-" +
//                            calendar.get(Calendar.DAY_OF_MONTH) + " " +
//                            calendar.get(Calendar.HOUR) + ":" +
//                            calendar.get(Calendar.MINUTE) + ":" +
//                            calendar.get(Calendar.SECOND);
            if(eventMap.get("startDate") != null){
                Instant dateStart =
                        Instant.ofEpochMilli(Long.parseLong(eventMap.get("startDate").toString())).atZone(ZoneId.systemDefault()).toInstant();
                singleEvent.setStartDate(Date.from(dateStart));
            }


            if(eventMap.get("endDate") != null){
                Instant dateEnd =
                        Instant.ofEpochMilli(Long.parseLong(eventMap.get("endDate").toString())).atZone(ZoneId.systemDefault()).toInstant();

                singleEvent.setEndDate(Date.from(dateEnd));
            }

            if(eventMap.get("spaceId") != null) {
                Optional<Space> space = this.spaceRepository.findById(Long.valueOf(eventMap.get("spaceId").toString()));
                if (space.isPresent() && !space.get().isDeleted()) {
                    singleEvent.setSpace(space.get());
                } else {
                    throw new IDMappingException("Unable to find space with ID: " + eventMap.get("spaceId"));
                }
                Long spaceLayoutId = Long.valueOf(eventMap.getOrDefault("spaceLayoutId", -1).toString());
//                SpaceLayout sl = this.spaceLayoutService.getById(spaceLayoutId);
                SpaceLayout sl = new SpaceLayout();
                sl.setGroundFloors(new HashSet<GroundFloor>());
                sl.setSeatSpaces(new HashSet<SeatSpace>());
                sl.setId(null);
                if (!eventMap.containsKey("spaceLayout")) {
                    throw new AddException("Event has no spaceLayout");
                }
                Map<String, Object> spaceLayoutMap = (Map<String, Object>) eventMap.get("spaceLayout");

                for (Map<String, Object> groundFloor : (ArrayList<Map<String, Object>>) spaceLayoutMap.get("groundFloors")) {
                    GroundFloor gf = new GroundFloor();
                    gf.setPrice(castToDouble(groundFloor.get("price")));
                    gf.setCapacity((Integer) groundFloor.get("capacity"));
                    gf.setHeight(castToDouble(groundFloor.get("height")));
                    gf.setWidth(castToDouble(groundFloor.get("width")));
                    gf.setSpotsReserved(0);
                    gf.setName((String) groundFloor.get("name"));
                    gf.setTop((Integer) groundFloor.get("top"));
                    gf.setLeft((Integer) groundFloor.get("left"));
                    sl.getGroundFloors().add(gf);
                    gf.setSpaceLayout(sl);
                }

                for (Map<String, Object> seatSpace : (ArrayList<Map<String, Object>>) spaceLayoutMap.get("seatSpaces")) {
                    SeatSpace ss = new SeatSpace();
                    ss.setPrice(castToDouble(seatSpace.get("price")));
                    ss.setRows((Integer) seatSpace.get("rows"));
                    ss.setSeats((Integer) seatSpace.get("seats"));
                    ss.setName((String) seatSpace.get("name"));
                    ss.setTop((Integer) seatSpace.get("top"));
                    ss.setLeft((Integer) seatSpace.get("left"));
                    ss.setTickets(new HashSet<Ticket>());
                    sl.getSeatSpaces().add(ss);
                    ss.setSpaceLayout(sl);
                }

                toSave.add(new Pair<SpaceLayout, SingleEvent>(sl, singleEvent));

            }
            event = singleEvent;
        }
        if(eventMap.get("description") != null)
            event.setDescription(eventMap.get("description").toString());
        if(eventMap.get("name") != null)
            event.setName(eventMap.get("name").toString());
        if(eventMap.get("parentEvent") != null){
            try{
            event.setParentEvent((CompositeEvent) eventService.getById(Long.parseLong(((Map<String, Object>)eventMap.get("parentEvent")).get("id").toString())));
            } catch (FetchException e ){
                event.setParentEvent((CompositeEvent) fromMapObject((Map<String, Object>)eventMap.get("parentEvent"), null, toSave));
            }
        }
        if(eventMap.get("type") != null)
            event.setType(EventType.valueOf(eventMap.get("type").toString()));
        if(eventMap.get("id") != null)
            event.setId(Long.valueOf(eventMap.get("id").toString()));
        else
            event.setId(-1L);

        return event;
    }

    private Double castToDouble(Object o) {
        try {
            return Double.valueOf((Integer) o);
        }catch (Exception e) {
            return (Double) o;
        }
    }
}
