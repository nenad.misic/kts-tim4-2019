package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;
import org.modelmapper.ModelMapper;

public abstract class GenericMapper<E,D> extends ModelMapper implements IMapper<E,D> {

    private final Class<E> entityType;
    private final Class<D> dtoType;

    public GenericMapper(Class<E> entityType, Class<D> dtoType) {
        this.entityType = entityType;
        this.dtoType = dtoType;
    }

    public E fromDto(D dto) throws IDMappingException {
        return this.map(dto, entityType);
    }

    public D toDto(E entity) {
        return this.map(entity, dtoType);
    }
}
