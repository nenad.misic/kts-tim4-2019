package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;

import ktsnvt.dto.GroundFloorDto;
import ktsnvt.dto.SpaceLayoutDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.repository.SpaceLayoutRepository;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GroundFloorMapper extends GenericMapper<GroundFloor, GroundFloorDto> {

    @Autowired
    private SpaceLayoutRepository spaceLayoutRepository;

    public GroundFloorMapper() {
        super(GroundFloor.class, GroundFloorDto.class);
        this.addMappings(new PropertyMap<User, UserDto>() {
            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });
        this.addMappings(new PropertyMap<Ticket, TicketDto>() {
            @Override
            protected void configure() {
                skip().setGroundFloor(null);
                skip().setSingleEvent(null);
            }
        });
        this.addMappings(new PropertyMap<SpaceLayout, SpaceLayoutDto>() {
            @Override
            protected void configure() {
                skip().setGroundFloors(null);
                skip().setSeatSpaces(null);
                skip().setSingleEvent(null);
            }
        });
        this.addMappings(new PropertyMap<GroundFloorDto, GroundFloor>() {
            @Override
            public void configure() {
                skip().setSpaceLayout(null);
            }
        });
    }

    @Override
    public GroundFloor fromDto(GroundFloorDto dto) throws IDMappingException {
        GroundFloor groundFloor = super.fromDto(dto);
        Optional<SpaceLayout> spaceLayout = spaceLayoutRepository.findById(dto.getSpaceLayoutId());
        if (spaceLayout.isPresent() && !spaceLayout.get().isDeleted()) {
            groundFloor.setSpaceLayout(spaceLayout.get());
        } else {
            throw new IDMappingException("Unable to find space layout with ID: " + dto.getSpaceLayoutId());
        }
        return groundFloor;
    }
}
