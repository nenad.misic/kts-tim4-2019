package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;

public interface IMapper<E,D> {

    public E fromDto(D dto) throws IDMappingException;

    public D toDto(E entity) throws IDMappingException;
}
