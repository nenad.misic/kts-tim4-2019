package ktsnvt.mapper;

import ktsnvt.dto.CompositeEventDto;
import ktsnvt.dto.LocationDto;
import ktsnvt.dto.SpaceDto;
import ktsnvt.entity.CompositeEvent;
import ktsnvt.entity.Location;
import ktsnvt.entity.Space;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class LocationMapper extends GenericMapper<Location, LocationDto>{

    public LocationMapper() {
        super(Location.class, LocationDto.class);
        this.addMappings(new PropertyMap<SpaceDto, Space>() {
            public void configure() {
                skip().setLocation(null);
            }
        });
        this.addMappings(new PropertyMap<Space, SpaceDto>() {
            public void configure() {
                skip().setEvents(null);
                skip().setLocation(null);
                skip().setDefaultSpaceLayouts(null);
            }
        });
    }
}
