package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.dto.SeatSpaceDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.SeatSpace;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.repository.SpaceLayoutRepository;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SeatSpaceMapper extends GenericMapper<SeatSpace, SeatSpaceDto>{

    @Autowired
    private SpaceLayoutRepository spaceLayoutRepository;

    public SeatSpaceMapper() {
        super(SeatSpace.class, SeatSpaceDto.class);
        this.addMappings(new PropertyMap<User, UserDto>() {
            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });
        this.addMappings(new PropertyMap<SeatSpaceDto, SeatSpace>() {
            @Override
            public void configure() {
                skip().setSpaceLayout(null);
            }
        });
        this.addMappings(new PropertyMap<Ticket, TicketDto>() {

            @Override
            protected void configure() {
                skip().setSeatSpace(null);
                skip().setGroundFloor(null);
                skip().setSingleEvent(null);
            }
        });

    }

    @Override
    public SeatSpace fromDto(SeatSpaceDto dto) throws IDMappingException {
        SeatSpace seatSpace = super.fromDto(dto);
        Optional<SpaceLayout> spaceLayout = spaceLayoutRepository.findById(dto.getSpaceLayoutId());
        if (spaceLayout.isPresent() && !spaceLayout.get().isDeleted()) {
            seatSpace.setSpaceLayout(spaceLayout.get());
        } else {
            throw new IDMappingException("Unable to find space layout with ID: " + dto.getSpaceLayoutId());
        }
        return seatSpace;

    }
}
