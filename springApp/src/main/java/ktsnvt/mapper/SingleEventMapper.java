package ktsnvt.mapper;

import ktsnvt.dto.*;
import ktsnvt.entity.*;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class SingleEventMapper extends GenericMapper<SingleEvent, SingleEventDto> {

    public SingleEventMapper() {
        super(SingleEvent.class, SingleEventDto.class);


        this.addMappings(new PropertyMap<Location, LocationDto>() {
            @Override
            protected void configure() {
                skip().setSpaces(null);
            }
        });

        this.addMappings(new PropertyMap<Space, SpaceDto>() {
            @Override
            protected void configure() {
                skip().setEvents(null);
                skip().setDefaultSpaceLayouts(null);
            }
        });

        this.addMappings(new PropertyMap<SpaceLayout, SpaceLayoutDto>() {
            @Override
            protected void configure() {
                skip().setSingleEvent(null);
                skip().setSpace(null);
            }
        });


        this.addMappings(new PropertyMap<SeatSpace, SeatSpaceDto>() {
            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });


        this.addMappings(new PropertyMap<GroundFloor, GroundFloorDto>() {
            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });

        this.addMappings(new PropertyMap<Ticket, TicketDto>() {
            @Override
            public void configure() {
                skip().setSingleEvent(null);
                skip().setUser(null);
                skip().setSeatSpace(null);
                skip().setGroundFloor(null);
            }
        });

//        this.addMappings(new PropertyMap<SingleEvent, SingleEventDto>() {
//            @Override
//            public void configure() {
//                skip().setParentEvent(null);
//            }
//        });

        this.addMappings(new PropertyMap<SingleEventDto, SingleEvent>() {
            @Override
            public void configure() {
                //skip().setParentEvent(null);
                skip().setSpace(null);
                skip().setSpaceLayout(null);
            }
        });

    }

}
