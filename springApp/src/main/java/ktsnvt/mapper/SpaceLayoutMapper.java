package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.dto.SpaceDto;
import ktsnvt.dto.SpaceLayoutDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.Space;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.SingleEventRepository;
import ktsnvt.repository.SpaceRepository;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class SpaceLayoutMapper extends GenericMapper<SpaceLayout, SpaceLayoutDto> {

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private SingleEventRepository singleEventRepository;

    public SpaceLayoutMapper() {
        super(SpaceLayout.class, SpaceLayoutDto.class);
        this.addMappings(new PropertyMap<Ticket, TicketDto>() {
            @Override
            protected void configure() {
                skip().setUser(null);
                skip().setGroundFloor(null);
                skip().setSeatSpace(null);
                skip().setSingleEvent(null);
            }
        });
        this.addMappings(new PropertyMap<SpaceLayoutDto, SpaceLayout>() {
            @Override
            public void configure() {
                skip().setSpace(null);
                skip().setSingleEvent(null);
            }
        });

        this.addMappings(new PropertyMap<SpaceDto, Space>() {
            @Override
            protected void configure() {
                skip().setDefaultSpaceLayouts(null);
                skip().setLocation(null);
            }
        });
        this.addMappings(new PropertyMap<Space, SpaceDto>() {
            @Override
            protected void configure() {
                skip().setDefaultSpaceLayouts(null);
                skip().setLocation(null);
                skip().setEvents(null);
            }
        });
        //do we want to see the event from space layout
        this.addMappings(new PropertyMap<SpaceLayout, SpaceLayoutDto>() {
            @Override
            public void configure() {
                skip().setSingleEvent(null);
            }
        });
    }

    @Override
    public SpaceLayout fromDto(SpaceLayoutDto dto) throws IDMappingException {
        SpaceLayout spaceLayout = super.fromDto(dto);
        if (dto.getSpaceId() != null) {
            Optional<Space> space = spaceRepository.findById(dto.getSpaceId());
            if (space.isPresent() && !space.get().isDeleted()) {
                spaceLayout.setSpace(space.get());
            } else {
                throw new IDMappingException("Cannot find Space with id " + dto.getSpaceId());
            }
        } else {
            throw new IDMappingException("No space in request body");
        }

        return spaceLayout;
    }
}
