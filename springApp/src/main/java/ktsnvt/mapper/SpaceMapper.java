package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.dto.LocationDto;
import ktsnvt.dto.SingleEventDto;
import ktsnvt.dto.SpaceDto;
import ktsnvt.dto.SpaceLayoutDto;
import ktsnvt.entity.Location;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Space;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.repository.LocationRepository;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpaceMapper extends GenericMapper<Space, SpaceDto>  implements IMapper<Space, SpaceDto> {

    @Autowired
    private LocationRepository locationRepository;

    public SpaceMapper() {
        super(Space.class, SpaceDto.class);
        this.addMappings(new PropertyMap<SpaceDto, Space>() {
            @Override
            public void configure() {
                skip().setLocation(null);
                skip().setDefaultSpaceLayouts(null);
            }
        });
        this.addMappings(new PropertyMap<LocationDto, Location>() {
            @Override
            public void configure() {
                skip().setSpaces(null);      }
        });
        this.addMappings(new PropertyMap<Location, LocationDto>() {
            @Override
            public void configure() {
                skip().setSpaces(null);      }
        });
        this.addMappings(new PropertyMap<SpaceLayout, SpaceLayoutDto>() {
            @Override
            public void configure() {
                skip().setSpace(null);
                skip().setSingleEvent(null);
                skip().setSeatSpaces(null);
                skip().setGroundFloors(null);
            }
        });
        //unnecessary pos
        this.addMappings(new PropertyMap<SpaceLayoutDto, SpaceLayout>() {
            @Override
            public void configure() {
                skip().setSpace(null);
                skip().setSingleEvent(null);
            }
        });
        this.addMappings(new PropertyMap<SingleEvent, SingleEventDto>() {
            @Override
            public void configure() {
            skip().setSpace(null);      }
        });
        this.addMappings(new PropertyMap<Space, SpaceDto>() {
            @Override
            public void configure() {
                skip().setEvents(null);
            }
        });
    }

    @Override
    public Space fromDto(SpaceDto dto) throws IDMappingException{
        Space space = super.fromDto(dto);
        Optional<Location> location = locationRepository.findById(dto.getLocationId());
        if (location.isPresent() && !location.get().isDeleted()) {
            space.setLocation(location.get());
        } else {
            throw new IDMappingException("Unable to find location with ID: " + dto.getLocationId());
        }
        return space;

    }
}
