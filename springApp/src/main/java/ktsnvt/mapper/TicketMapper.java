package ktsnvt.mapper;

import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.dto.*;
import ktsnvt.entity.*;
import ktsnvt.repository.*;
import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.modelmapper.PropertyMap;

import java.util.Optional;

//TODO add recursion fixes
@Component
public class TicketMapper extends GenericMapper<Ticket, TicketDto> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SeatSpaceRepository seatSpaceRepository;

    @Autowired
    private GroundFloorRepository groundFloorRepository;

    @Autowired
    private SingleEventRepository singleEventRepository;

    public TicketMapper() {
        super(Ticket.class, TicketDto.class);
        this.addMappings(new PropertyMap<Location, LocationDto>() {
            @Override
            protected void configure() {
                skip().setSpaces(null);
            }
        });
        this.addMappings(new PropertyMap<CompositeEvent, CompositeEventDto>() {
            @Override
            protected void configure() {
                skip().getParentEvent().setChildEvents(null);
            }
        });
        this.addMappings(new PropertyMap<TicketDto, Ticket>() {
            @Override
            public void configure() {
                skip().setUser(null);
                skip().setGroundFloor(null);
                skip().setSeatSpace(null);
                skip().setSingleEvent(null);
            }
        });
        this.addMappings(new PropertyMap<User, UserDto>() {
            @Override
            public void configure() {
                skip().setTickets(null);
            }
        });
        this.addMappings(new PropertyMap<SeatSpace, SeatSpaceDto>() {

            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });
        this.addMappings(new PropertyMap<GroundFloor, GroundFloorDto>() {

            @Override
            protected void configure() {
                skip().setTickets(null);
            }
        });
        this.addMappings(new PropertyMap<SingleEvent, SingleEventDto>() {
            @Override
            protected void configure() {
                skip().setTickets(null);
                skip().setSpace(null);
                skip().setSpaceLayout(null);
                skip().setParentEvent(null);
            }
        });


    }

    @Override
    public Ticket fromDto(TicketDto dto) throws IDMappingException {
        Ticket ticket = super.fromDto(dto);

        if (dto.getSingleEventId() == null) {
            throw new IDMappingException("EventId is null");
        }
        Optional<SingleEvent> event = singleEventRepository.findById(dto.getSingleEventId());
        if (event.isPresent() && !event.get().isDeleted()){
            ticket.setSingleEvent(event.get());
        } else {
            throw new IDMappingException("Unable to find event with ID: " + dto.getSingleEventId());
        }

        if (dto.getUserId() == null) {
            throw new IDMappingException("UserId is null");
        }
        Optional<User> user = userRepository.findById(dto.getUserId());
        if (user.isPresent() && !user.get().isDeleted()) {
            ticket.setUser(user.get());
        } else {
            throw new IDMappingException("Unable to find user with ID: " + dto.getUserId());
        }

        Optional<SeatSpace> seatSpace = Optional.empty();
        Optional<GroundFloor> groundFloor = Optional.empty();
        if (dto.getSeatSpaceId() != null) {
            seatSpace = seatSpaceRepository.findById(dto.getSeatSpaceId());
        } else if (dto.getGroundFloorId() != null) {
            groundFloor = groundFloorRepository.findById(dto.getGroundFloorId());
        } else {
            throw new IDMappingException("Both groundFloorId and seatSpaceId are null");
        }
        if (seatSpace.isPresent() && !seatSpace.get().isDeleted()) {
            ticket.setSeatSpace(seatSpace.get());
        } else if (groundFloor.isPresent() && !groundFloor.get().isDeleted()) {
            ticket.setGroundFloor(groundFloor.get());
        } else {
            throw new IDMappingException("Unable to find ground floor with ID: " + dto.getGroundFloorId());
        }
        return ticket;
    }

}
