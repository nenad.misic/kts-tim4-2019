package ktsnvt.mapper;

import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.repository.UserRepository;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends GenericMapper<User, UserDto> {

    @Autowired
    private UserRepository userRepository;

    public UserMapper() {
        super(User.class, UserDto.class);
        this.addMappings(new PropertyMap<Ticket, TicketDto>() {
            @Override
            public void configure() {
                skip().setUser(null);
                skip().setGroundFloor(null);
                skip().setSeatSpace(null);
                skip().setSingleEvent(null);
            }
        });
    }
}
