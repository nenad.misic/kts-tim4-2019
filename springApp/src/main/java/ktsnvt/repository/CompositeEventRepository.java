package ktsnvt.repository;

import ktsnvt.entity.CompositeEvent;
import ktsnvt.entity.Event;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.ICompositeEventRepository;

import org.springframework.data.domain.Pageable;
import java.util.List;

@Repository
public interface CompositeEventRepository extends ICompositeEventRepository {
    public List<CompositeEvent> findByParentEvent(Event parent);
    public List<CompositeEvent> findByParentEvent(Event parent, Pageable pageable);

}
