package ktsnvt.repository;

import ktsnvt.entity.GroundFloor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.IGroundFloorRepository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.Optional;

@Repository
public interface GroundFloorRepository extends IGroundFloorRepository {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = "5000")})
    @Query("SELECT g FROM GroundFloor g WHERE g.id=?1")
    public Optional<GroundFloor> fetchByIdAndLock(Long id);
}
