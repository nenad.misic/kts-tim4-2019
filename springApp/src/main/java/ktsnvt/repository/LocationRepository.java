package ktsnvt.repository;

import ktsnvt.entity.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import ktsnvt.repository.contracts.ILocationRepository;

@Repository
public interface LocationRepository extends ILocationRepository {
    public Page<Location> findByNameContainingIgnoreCase(String name, Pageable pageable);
}