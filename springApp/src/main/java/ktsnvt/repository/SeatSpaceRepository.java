package ktsnvt.repository;

import ktsnvt.entity.SeatSpace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ktsnvt.repository.contracts.ISeatSpaceRepository;

@Repository
public interface SeatSpaceRepository extends ISeatSpaceRepository {
}
