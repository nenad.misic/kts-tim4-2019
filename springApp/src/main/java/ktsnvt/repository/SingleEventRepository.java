package ktsnvt.repository;

import ktsnvt.entity.Event;
import ktsnvt.entity.SingleEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.ISingleEventRepository;

import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

@Repository
public interface SingleEventRepository extends ISingleEventRepository {

    public List<SingleEvent> findByParentEvent(Event parent);

    public List<SingleEvent> findByParentEvent(Event parent, Pageable pageable);

    public List<SingleEvent> findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(Long spaceId, Date startDate, Date endDate);
}
