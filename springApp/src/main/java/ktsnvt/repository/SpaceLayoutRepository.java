package ktsnvt.repository;

import ktsnvt.entity.SpaceLayout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.ISpaceLayoutRepository;

@Repository
public interface SpaceLayoutRepository extends ISpaceLayoutRepository {
}
