package ktsnvt.repository;

import ktsnvt.entity.Space;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.ISpaceRepository;

@Repository
public interface SpaceRepository extends ISpaceRepository {
}
