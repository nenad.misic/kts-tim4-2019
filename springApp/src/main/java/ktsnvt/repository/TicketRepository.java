package ktsnvt.repository;

import ktsnvt.dto.SeatDto;
import ktsnvt.entity.EventState;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Ticket;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.ITicketRepository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.Date;
import java.util.List;

@Repository
public interface TicketRepository extends ITicketRepository {

    public boolean existsBySeatSpaceIdAndRowAndSeat(Long seatSpaceId, Integer row, Integer seat);

    public boolean existsBySingleEvent_StateAndGroundFloorId(EventState state, Long id);

    public boolean existsBySingleEvent_StateAndSeatSpaceId(EventState state, Long id);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = "5000")})
    public Ticket findBySeatSpaceIdAndRowAndSeat(Long seatSpaceId, Integer row, Integer seat);

    public List<Ticket> findBySeatSpaceIdAndUserIsNotNull(Long seatSpaceId);

    public List<Ticket> findByUserId(Long userId, Pageable pageable);

    public List<Ticket>findAllByExpirationDateGreaterThanEqualAndExpirationDateLessThanEqualAndUserNotNullAndPaidFalse(Date date1, Date date2);

}
