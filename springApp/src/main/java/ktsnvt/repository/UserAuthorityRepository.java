package ktsnvt.repository;

import ktsnvt.entity.UserAuthority;
import ktsnvt.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {

    public UserAuthority findByRole(UserRole role);
}
