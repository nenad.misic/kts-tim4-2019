package ktsnvt.repository;

import ktsnvt.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ktsnvt.repository.contracts.IUserRepository;

@Repository
public interface UserRepository  extends IUserRepository {

    public User findByUsername(String username);
}
