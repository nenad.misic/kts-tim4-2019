package ktsnvt.repository.contracts;

import ktsnvt.entity.CompositeEvent;
import ktsnvt.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface ICompositeEventRepository extends QueryByExampleExecutor<CompositeEvent>, JpaRepository<CompositeEvent, Long> {

    public List<CompositeEvent> findByParentEvent(Event parent);
}
