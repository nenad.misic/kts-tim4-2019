package ktsnvt.repository.contracts;

import ktsnvt.entity.GroundFloor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IGroundFloorRepository extends JpaRepository<GroundFloor, Long> {
}
