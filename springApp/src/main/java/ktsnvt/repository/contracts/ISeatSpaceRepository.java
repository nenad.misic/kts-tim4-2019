package ktsnvt.repository.contracts;

import ktsnvt.entity.SeatSpace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISeatSpaceRepository extends JpaRepository<SeatSpace, Long> {
}
