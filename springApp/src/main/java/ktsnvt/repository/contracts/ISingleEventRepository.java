package ktsnvt.repository.contracts;

import ktsnvt.entity.CompositeEvent;
import ktsnvt.entity.Event;
import ktsnvt.entity.SingleEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface ISingleEventRepository extends JpaRepository<SingleEvent, Long>, QueryByExampleExecutor<SingleEvent> {

    public List<SingleEvent> findByParentEvent(Event parent);
}
