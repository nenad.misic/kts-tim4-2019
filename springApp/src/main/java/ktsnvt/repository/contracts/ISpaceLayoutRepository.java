package ktsnvt.repository.contracts;

import ktsnvt.entity.SpaceLayout;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISpaceLayoutRepository extends JpaRepository<SpaceLayout, Long> {
}
