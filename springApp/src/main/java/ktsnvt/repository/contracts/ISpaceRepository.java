package ktsnvt.repository.contracts;

import ktsnvt.entity.Space;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISpaceRepository extends JpaRepository<Space, Long> {
}
