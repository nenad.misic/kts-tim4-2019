package ktsnvt.repository.contracts;

import ktsnvt.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository  extends JpaRepository<User, Long> {

    public User findByUsername(String username);
}
