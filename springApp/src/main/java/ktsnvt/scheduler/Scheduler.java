package ktsnvt.scheduler;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.EventRepository;
import ktsnvt.repository.SingleEventRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.service.EmailService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.TemporalAmount;
import java.util.Date;
import java.util.List;

@Component
public class Scheduler {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private EmailService emailService;

    //real: "0 0 */1 * * *"
    //test: "* * */1 * * *"
    @Scheduled(cron = "0 0 */1 * * *")
    public void reportCurrentTime() throws IOException, MessagingException {
        System.out.println("Scheduled task running...");

        Instant dateStart = Instant.ofEpochMilli(System.currentTimeMillis());
        dateStart = dateStart.minus(Duration.ofHours(3));
        Date date1 = Date.from(dateStart);

        Instant dateEnd = Instant.ofEpochMilli(System.currentTimeMillis());
        dateEnd = dateEnd.plus(Duration.ofHours(30));
        Date date2 = Date.from(dateEnd);

        List<Ticket> tickets = ticketRepository.findAllByExpirationDateGreaterThanEqualAndExpirationDateLessThanEqualAndUserNotNullAndPaidFalse(date1, date2);

        Instant instantNow = Instant.ofEpochMilli(System.currentTimeMillis());
        Date dateNow = Date.from(instantNow);

        for (Ticket ticket: tickets) {
            if (ticket.getExpirationDate().before(dateNow)) {
                emailService.sendTicketCancelled(ticket);
                ticket.setUser(null);
                ticketRepository.save(ticket);
            } else {
                long hours = (ticket.getExpirationDate().getTime() - dateNow.getTime()) / (1000 * 3600);
                if (hours == 23) {
                    emailService.sendReminder(ticket);
                }
            }
        }

    }


}
