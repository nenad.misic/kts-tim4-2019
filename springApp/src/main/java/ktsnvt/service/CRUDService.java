package ktsnvt.service;

import ktsnvt.common.IDeletable;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

public abstract class CRUDService<T extends IDeletable> implements IService<T> {

    protected JpaRepository<T, Long> repository;

    @Override
    public T getById(Long id) throws FetchException {
        Optional<T> t = repository.findById(id);
        if (t.isPresent() && !t.get().isDeleted()) {
            return t.get();
        } else {
            throw new FetchException("Cannot find entity with id " + id);
        }
    }

    @Override
    public List<T> getAll() {
        return repository.findAll();
    }


    public Page<T> getPage(Pageable pageable) {
        return this.repository.findAll(pageable);
    }


    @Override
    public void add(T t) throws AddException {
        repository.save(t);
    }

    @Override
    public void deleteById(Long id) throws DeleteException, FetchException {
        T t = this.getById(id);
        t.logicalDelete();
        repository.save(t);

    }

    @Override
    public void update(T t) throws UpdateException, AddException {
        repository.save(t);
    }

}
