package ktsnvt.service;

import ktsnvt.common.MailTemplates;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.entity.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String emailAddress;

    @Async
    public void sendVerificationMessage(User user, VerificationToken verificationToken) throws IOException, MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        helper.setText(MailTemplates.formatRegistrationHtml(user.getName(), "http://localhost:8081/api/user/verify/" + verificationToken.getToken()), true);
        helper.setTo(user.getEmail());
        helper.setSubject("Tickets:) verification email");
        helper.setFrom(emailAddress);
        javaMailSender.send(mimeMessage);
    }

    @Async
    public void sendReminder(Ticket ticket) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        String text = String.format("Dear %s %s, your reservation is about to expire in less than a day\n",
                ticket.getUser().getName(), ticket.getUser().getSurname());
        text += getTicketInfo(ticket);
        text += "If you wish to keep the reservation, pay for it.\nKind regards,\nTim Sljub";
        helper.setText(text);
        helper.setTo(ticket.getUser().getEmail());
        helper.setSubject("Reservation reminder");
        helper.setFrom(emailAddress);
        javaMailSender.send(mimeMessage);
    }

    private String getTicketInfo(Ticket ticket) {
        String retVal = "";
        retVal += String.format("Event name: %s\n",ticket.getSingleEvent().getName());
        if (ticket.getGroundFloor() != null) {
            retVal += String.format("Location: Groundfloor %s\n", ticket.getGroundFloor().getName());
        } else {
            retVal += String.format("Seat: SeatSpace %s row: %d seat: %d\n",
                    ticket.getSeatSpace().getName(), ticket.getRow(), ticket.getSeat());
        }
        return retVal;
    }

    @Async
    public void sendTicketCancelled(Ticket ticket) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
        String text = String.format("Dear %s %s, your reservation has been cancelled.\n",
                ticket.getUser().getName(), ticket.getUser().getSurname());
        text += getTicketInfo(ticket);
        text += "Kind regards,\nTim Sljub";
        helper.setText(text); // Use this or above line.
        helper.setTo(ticket.getUser().getEmail());
        helper.setSubject("Reservation cancelled");
        helper.setFrom(emailAddress);
        javaMailSender.send(mimeMessage);
    }
}
