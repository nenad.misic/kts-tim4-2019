package ktsnvt.service;

import javafx.util.Pair;
import ktsnvt.common.SearchMode;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.entity.*;
import ktsnvt.repository.CompositeEventRepository;
import ktsnvt.repository.EventRepository;
import ktsnvt.repository.SingleEventRepository;
import ktsnvt.repository.SpaceLayoutRepository;
import ktsnvt.service.contracts.IEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EventService extends CRUDService<Event> implements IEventService {

    @Autowired
    private void setRepository(EventRepository repository) {
        this.repository = repository;
    }

    @Autowired
    private CompositeEventRepository compositeEventRepository;

    @Autowired
    private SingleEventRepository singleEventRepository;

    @Autowired
    private SeatSpaceService seatSpaceService;

    @Autowired
    private SpaceLayoutRepository spaceLayoutRepository;

    @Autowired
    private SpaceLayoutService spaceLayoutService;

    @Override
    public void add(Event event) throws AddException {
        event.setId(-1L);
        if (event.getClass() == CompositeEvent.class) {
            recursively_SetId_andRemoveParentReference(event, -1L);
            event.setClassType("CompositeEvent");
            CompositeEvent saved = this.compositeEventRepository.save((CompositeEvent) event);
            recursively_addTickets(saved);
        } else if (event.getClass() == SingleEvent.class) {
            SingleEvent saved = this.singleEventRepository.save((SingleEvent) event);
            // adding tickets to all seat spaces
            for (SeatSpace ss : ((SingleEvent) event).getSpaceLayout().getSeatSpaces()) {
                seatSpaceService.addTickets(ss.getId(), saved);
            }
        }
    }

    @Override
    public void update(Event event) throws UpdateException, AddException {
        if (event.getId() == null) {
            this.add(event);
            return;
        }
        if (event.getClass() == CompositeEvent.class) {
            CompositeEvent compositeEvent = (CompositeEvent) event;
            Optional<CompositeEvent> oldEvent = this.compositeEventRepository.findById(event.getId());
            if (!oldEvent.isPresent() || oldEvent.get().isDeleted()) {
                throw new UpdateException("Unable to update event with ID: " + event.getId());
            }
            compositeEvent.setChildEvents(oldEvent.get().getChildEvents());
            this.compositeEventRepository.save((CompositeEvent) event);

        } else if (event.getClass() == SingleEvent.class) {
            if (this.singleEventRepository.existsById(event.getId()))
                this.singleEventRepository.save((SingleEvent) event);
            else
                throw new UpdateException("Unable to update event with ID: " + event.getId());
        }
    }


    public Event addAndReturn(Event event, Queue<Pair<SpaceLayout, SingleEvent>> toSave) throws AddException {

        Iterator<Pair<SpaceLayout, SingleEvent>> iterator = toSave.iterator();
        while (iterator.hasNext()) {
            Pair<SpaceLayout, SingleEvent> pair = iterator.next();
            SpaceLayout sl = spaceLayoutRepository.save(pair.getKey());
            pair.getValue().setSpaceLayout(sl);
        }
        spaceLayoutRepository.flush();

        if (event.getClass() == CompositeEvent.class) {
            recursively_SetId_andRemoveParentReference(event, -1L);
            CompositeEvent saved = this.compositeEventRepository.save((CompositeEvent) event);

            recursively_addTickets(saved);
            return saved;
        } else if (event.getClass() == SingleEvent.class) {
            SingleEvent saved = this.singleEventRepository.save((SingleEvent) event);
            // adding tickets to all seat spaces
            for (SeatSpace ss : ((SingleEvent) event).getSpaceLayout().getSeatSpaces()) {
                seatSpaceService.addTickets(ss.getId(), saved);
            }
            return saved;
        } else {
            throw new AddException("Something went wrong");
        }
    }

    public Map<String, Object> reportEarningsOfChildren(Long id) throws FetchException {
        Optional<SingleEvent> singleEventOptional = singleEventRepository.findById(id);
        Optional<CompositeEvent> compositeEventOptional = compositeEventRepository.findById(id);
        Map<String, Object> retVal = new HashMap<String, Object>();
        if (singleEventOptional.isPresent() || compositeEventOptional.isPresent()) {
            retVal.put("unit", "earnings(dollars)");
            Map<String, Object> data = new HashMap<String, Object>();
            retVal.put("data", data);
            if (singleEventOptional.isPresent()) {
                SingleEvent singleEvent = singleEventOptional.get();
                retVal.put("title", singleEvent.getName());
                data.put(singleEvent.getName(), calculateEarnings(singleEvent));
            } else {
                CompositeEvent compositeEvent = compositeEventOptional.get();
                retVal.put("title", compositeEvent.getName());
                for (Event e: compositeEvent.getChildEvents()) {
                    data.put(e.getName(), calculateEarnings(e));
                }
            }

        } else {
            throw new FetchException("Unable to find event with ID: " + id);
        }
        return  retVal;
    }

    public double calculateEarnings(Event event) {
        double retVal = 0;
        if (event instanceof  SingleEvent) {
            SingleEvent singleEvent = (SingleEvent) event;
            for (Ticket ticket: singleEvent.getTickets()) {
                if (ticket.isPaid()) {
                    retVal += ticket.getPrice();
                }
            }
        } else  {
            CompositeEvent compositeEvent = (CompositeEvent) event;
            for (Event childEvent: compositeEvent.getChildEvents()) {
                retVal += calculateEarnings(childEvent);
            }
        }
        return retVal;
    }

    public Map<String, Object> reportOccupancyOfChildren(Long id) throws FetchException {
        Optional<SingleEvent> singleEventOptional = singleEventRepository.findById(id);
        Optional<CompositeEvent> compositeEventOptional = compositeEventRepository.findById(id);
        Map<String, Object> retVal = new HashMap<String, Object>();
        if (singleEventOptional.isPresent() || compositeEventOptional.isPresent()) {
            retVal.put("unit", "tickets bought");
            Map<String, Object> data = new HashMap<String, Object>();
            retVal.put("data", data);
            if (singleEventOptional.isPresent()) {
                SingleEvent singleEvent = singleEventOptional.get();
                retVal.put("title", singleEvent.getName());
                data.put(singleEvent.getName(), calculateOccupancy(singleEvent));
            } else {
                CompositeEvent compositeEvent = compositeEventOptional.get();
                retVal.put("title", compositeEvent.getName());
                for (Event e: compositeEvent.getChildEvents()) {
                    data.put(e.getName(), calculateOccupancy(e));
                }
            }

        } else {
            throw new FetchException("Unable to find event with ID: " + id);
        }
        return  retVal;
    }

    public int calculateOccupancy(Event event) {
        int retVal = 0;
        if (event instanceof  SingleEvent) {
            SingleEvent singleEvent = (SingleEvent) event;
            for (Ticket ticket: singleEvent.getTickets()) {
                if (ticket.isPaid()) {
                    retVal++;
                }
            }
        } else  {
            CompositeEvent compositeEvent = (CompositeEvent) event;
            for (Event childEvent: compositeEvent.getChildEvents()) {
                retVal += calculateOccupancy(childEvent);
            }
        }
        return retVal;
    }




    @Override
    //@Transactional(Transactional.TxType.REQUIRED)
    public List<Event> getAll() {
        List<CompositeEvent> compositeEvents = compositeEventRepository.findByParentEvent(null);
        List<Event> events = new ArrayList<>(compositeEvents);
        List<SingleEvent> singleEvents = singleEventRepository.findByParentEvent(null);

        events.addAll(singleEvents);
        return events;
    }

    @Override
    public Page<Event> getPage(Pageable pageable) {
        List<Event> retVal = new ArrayList<Event>();
        retVal.addAll(compositeEventRepository.findByParentEvent(null, pageable));
        retVal.addAll(singleEventRepository.findByParentEvent(null, pageable));
        return new PageImpl<>(retVal);
    }

    @Override
    public Event getById(Long id) throws FetchException {
        Optional<SingleEvent> event = singleEventRepository.findById(id);
        if (event.isPresent() && !event.get().isDeleted()) {
            return event.get();
        } else {
            Optional<CompositeEvent> compositeEvent = compositeEventRepository.findById(id);
            if(compositeEvent.isPresent() && !compositeEvent.get().isDeleted()) {
                return compositeEvent.get();
            }
            else {
                throw new FetchException("Unable to find event with ID: " + id);
            }
        }

    }

    @Override
    public void deleteById(Long id) throws FetchException {
        Event event = this.getById(id);
        deleteEvent(event);
    }

    public Event attachOriginalSpaceLayout(Event event, Event oldEvent) throws FetchException {

        if(event.getClass().equals(CompositeEvent.class)) {
            //for(Event childEvent : ((CompositeEvent) event).getChildEvents()) {
            //    if(childEvent.getClass().equals(CompositeEvent.class)) {
            //        attachOriginalSpaceLayout(childEvent, -1L);
            //    }else {
            //        attachOriginalSpaceLayout(childEvent, ((SingleEvent) childEvent).getSpaceLayout().getId());
            //    }
            //}
        }else{
            ((SingleEvent)event).setSpaceLayout(((SingleEvent)oldEvent).getSpaceLayout());
        }
        // If SpaceLayout with id exist and has no event, then its a Default Layout and should be copied

        return event;
    }

    public List<Event> search(Event singleEvent, Event compositeEvent, SearchMode mode) {
        ExampleMatcher matcher = null;
        switch (mode) {
            case Regular:
                matcher = ExampleMatcher.matching();
                break;
            case Quick:
                matcher = ExampleMatcher.matchingAny();
                break;
        }
        matcher = matcher
                .withIgnorePaths("id")
                .withIgnorePaths("startDate")
                .withIgnorePaths("endDate")
                .withIgnorePaths("deleted")
                .withIgnorePaths("space.deleted")
                .withIgnorePaths("spaceLayout.deleted")
                .withIgnorePaths("space.location.deleted")
                .withNullHandler(ExampleMatcher.NullHandler.IGNORE)
                .withIgnoreCase()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

        List<CompositeEvent> compositeEvents = new ArrayList<>();
        if(compositeEvent != null){
            Example<CompositeEvent> compositeExample = Example.of((CompositeEvent)compositeEvent, matcher);
            compositeEvents = compositeEventRepository.findAll(compositeExample);
        }

        Example<SingleEvent> singleExample = Example.of((SingleEvent)singleEvent, matcher);
        List<SingleEvent> singleEvents = singleEventRepository.findAll(singleExample);


        if(((SingleEvent) singleEvent).getStartDate() != null){
            compositeEvents.removeIf(c -> {
                Date start = getStartDateOfCompositeEvent(c);
                if (start != null){
                    return start.before(((SingleEvent) singleEvent).getStartDate());
                }
                return true;
            });
            singleEvents.removeIf(c -> c.getStartDate().before(((SingleEvent) singleEvent).getStartDate()));
        }

        if(((SingleEvent) singleEvent).getEndDate() != null){
            compositeEvents.removeIf(c -> {
                Date end = getEndDateOfCompositeEvent(c);
                if (end != null){
                    return end.after(((SingleEvent) singleEvent).getEndDate());
                }
                return true;
            });
            singleEvents.removeIf(c -> c.getEndDate().after(((SingleEvent) singleEvent).getEndDate()));
        }


        List<Event> events = new ArrayList<>(compositeEvents);
        events.addAll(singleEvents);

        events = removeNotRootEvents(events);

        return new ArrayList<>(events);
    }

    private List<Event> removeNotRootEvents(List<Event> events){
        HashSet<Event> result = new HashSet<>();
        for (Event e : events){
            result.add(recursivelyFindRoot(e));
        }

        return new ArrayList<>(result);
    }

    private Event recursivelyFindRoot(Event e){
        if (e.getParentEvent() == null){
            return e;
        }else {
            return recursivelyFindRoot(e.getParentEvent());
        }
    }

    private Date getStartDateOfCompositeEvent(CompositeEvent e){
        List<SingleEvent> singleEvents = getSingleEventsOfCompositeEvent(e);
        Object[] dates = singleEvents.stream().map(SingleEvent::getStartDate).sorted().toArray();
        if (dates.length > 0){
            return (Date)dates[0];
        } else {
            return null;
        }
    }
    private Date getEndDateOfCompositeEvent(CompositeEvent e){
        List<SingleEvent> singleEvents = getSingleEventsOfCompositeEvent(e);
        Object[] dates = singleEvents.stream().map(SingleEvent::getEndDate).sorted().toArray();
        if (dates.length > 0){
            return (Date)dates[dates.length - 1];
        } else {
            return null;
        }
    }

    private List<SingleEvent> getSingleEventsOfCompositeEvent(CompositeEvent e){
        List<SingleEvent> result = new ArrayList<>();
        recursivelyGetChildren(e, result);
        return result;
    }

    private void recursivelyGetChildren(CompositeEvent e, List<SingleEvent> result){
        for (Event child : e.getChildEvents()) {
            if(child.getClass().equals(SingleEvent.class)){
                result.add((SingleEvent)child);
            } else {
                recursivelyGetChildren((CompositeEvent)child, result);
            }
        }
    }
    private void deleteEvent(Event event) {
        if (event.getClass().equals(CompositeEvent.class)){
            event.logicalDelete();
            this.compositeEventRepository.save((CompositeEvent) event);
            for (Event child: ((CompositeEvent) event).getChildEvents()) {
                this.deleteEvent(child);
            }
        } else {
            event.logicalDelete();
            this.singleEventRepository.save((SingleEvent) event);
        }
    }

    private void recursively_SetId_andRemoveParentReference(Event e, Long id){
        if(e.getClass().equals(CompositeEvent.class)){
            e.setId(id);
            e.setParentEvent(null);
            for(Event childEvent : ((CompositeEvent)e).getChildEvents()){
                recursively_SetId_andRemoveParentReference(childEvent, id);

            }
        }else{
            e.setParentEvent(null);
            e.setId(id);
        }
    }

    private void recursively_addTickets(Event event) throws AddException {
        if (event.getClass() == SingleEvent.class) {
            // adding tickets to all seat spaces
            for (SeatSpace ss : ((SingleEvent) event).getSpaceLayout().getSeatSpaces()) {
                seatSpaceService.addTickets(ss.getId(), (SingleEvent)event);
            }
        } else {
            for (Event child : ((CompositeEvent)event).getChildEvents()) {
                recursively_addTickets(child);
            }
        }
    }
}
