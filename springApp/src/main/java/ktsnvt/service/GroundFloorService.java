package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.GroundFloor;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.service.contracts.IGroundFloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GroundFloorService extends CRUDService<GroundFloor> implements IGroundFloorService {
    @Autowired
    public void setRepository(GroundFloorRepository groundFloorRepository) { this.repository = groundFloorRepository; }

    @Autowired
    private TicketRepository ticketRepository;


    @Override
    public void deleteById(Long id) throws DeleteException, FetchException {
        Optional<GroundFloor> groundFloorOptional = repository.findById(id);
        if (!groundFloorOptional.isPresent() || groundFloorOptional.get().isDeleted()) {
            throw new FetchException("No GroundFloor with id: " + id + "to delete\"");
        }
        GroundFloor groundFloor = groundFloorOptional.get();
        if (groundFloor.getSpaceLayout().getSingleEvent() != null) {
            throw new DeleteException("Cannot delete GroundFloor with id " + id +
                    " GroundFloor is tied to an event");
        }
        this.repository.deleteById(id);
    }
}
