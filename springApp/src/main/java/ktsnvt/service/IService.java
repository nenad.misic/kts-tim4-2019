package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;

import java.util.List;

public interface IService<T> {

    public T getById(Long id) throws FetchException;

    public List<T> getAll();

    public void add(T space) throws AddException;

    public void deleteById(Long id) throws DeleteException, FetchException;

    public void update(T space) throws UpdateException, AddException;

}
