package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.Location;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Space;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.LocationRepository;
import ktsnvt.repository.SingleEventRepository;
import ktsnvt.service.contracts.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.util.resources.cldr.ebu.CalendarData_ebu_KE;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LocationService extends CRUDService<Location> implements ILocationService {

    @Autowired
    public void setRepository(LocationRepository repository) {
        this.repository = repository;
    }

    @Autowired
    private SingleEventRepository singleEventRepository;

    public Page<Location> findPageByName(String name, Pageable pageable) {
        return ((LocationRepository) this.repository).findByNameContainingIgnoreCase(name, pageable);
    }

    public Map<String, Object> reportEarningsDuringTimePeriod(Long id, Date startDate, Date endDate) throws FetchException {
        Optional<Location> locationOptional = repository.findById(id);
        Map<String, Object> retVal = new HashMap<>();
        if (locationOptional.isPresent()) {
            Location location = locationOptional.get();
            Map<String, Double> data = new HashMap<>();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            retVal.put("data", data);
            retVal.put("title", location.getName() + " earnings for time period: " + sdf.format(startDate) + "-" + sdf.format(endDate));
            retVal.put("unit", "earnings(dollars)");
            for (Space space: location.getSpaces()) {
                List<SingleEvent> spaceEvents = singleEventRepository.
                        findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual
                        (space.getId(), startDate, endDate);
                for (SingleEvent singleEvent: spaceEvents) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(singleEvent.getStartDate());
                    String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    String year = Integer.toString(calendar.get(Calendar.YEAR));
                    String key = month + '/' + year;
                    //todo dodaj godinu posle meseca
                    Double currentValue = data.getOrDefault(key, 0d);
                    Double eventEarnings = 0d;
                    for (Ticket t: singleEvent.getTickets()) {
                        if (t.isPaid()) {
                            eventEarnings += t.getPrice();
                        }
                    }
                    data.put(key, currentValue + eventEarnings);
                }
            }
            return retVal;
        } else {
            throw new FetchException("Unable to find location with ID: " + id);
        }
    }


    public Map<String, Object> reportOccupancyDuringTimePeriod(Long id, Date startDate, Date endDate) throws FetchException {
        Optional<Location> locationOptional = repository.findById(id);
        Map<String, Object> retVal = new HashMap<>();
        if (locationOptional.isPresent()) {
            Location location = locationOptional.get();
            Map<String, Integer> data = new HashMap<>();
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            retVal.put("data", data);
            retVal.put("title", location.getName() + " visitors for time period: " + sdf.format(startDate) + "-" + sdf.format(endDate));
            retVal.put("unit", "tickets bought");
            for (Space space: location.getSpaces()) {
                List<SingleEvent> spaceEvents = singleEventRepository.
                        findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual
                                (space.getId(), startDate, endDate);
                for (SingleEvent singleEvent: spaceEvents) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(singleEvent.getStartDate());
                    String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    String year = Integer.toString(calendar.get(Calendar.YEAR));
                    String key = month + '/' + year;
                    Integer currentValue = data.getOrDefault(key, 0);
                    Integer additionalTickets = 0;
                    for (Ticket ticket: singleEvent.getTickets()) {
                        if (ticket.isPaid()) {
                            additionalTickets++;
                        }
                    }
                    data.put(key, currentValue + additionalTickets);
                }
            }
            return retVal;
        } else {
            throw new FetchException("Unable to find location with ID: " + id);
        }
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DeleteException, FetchException{
        Optional<Location> location = this.repository.findById(id);
        if (location.isPresent() && !location.get().isDeleted()) {
            if (location.get().getSpaces().size() > 0) {
                throw new DeleteException("Cannot delete Location with id " + id +
                        ": Location has relationships to one or more Spaces");
            } else {
                this.repository.deleteById(id);
            }
        } else {
            throw new FetchException("No Location with id "+ id + "to delete");
        }
    }
}
