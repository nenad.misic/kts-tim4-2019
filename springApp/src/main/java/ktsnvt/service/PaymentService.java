package ktsnvt.service;

import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import ktsnvt.common.exceptions.*;
import ktsnvt.dto.PaymentDto;
import ktsnvt.dto.SinglePaymentDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.SeatSpace;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.SeatSpaceRepository;
import ktsnvt.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class PaymentService {
    String clientId = "AdBp8qmNu6I6832u-D-w9gMBkQHgy8wJ2GrpVwHLstsyOldxVD85o7CF2Vs-ZLDKqF1Wuo-qA_vyEgDP";
    String clientSecret = "EKk9nflqOg50E6WlRFLOj4ODd02dqmxu9MyK7T7McwC42741gN8MY1JhD2PUz2kVfyoK0Mogbw3TC6rG";

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private GroundFloorRepository groundFloorRepository;

    @Autowired
    private SeatSpaceRepository seatSpaceRepository;

    public Map<String, Object> createPayment(List<TicketDto> tickets) throws FetchException {
        double totalPrice = getTotalPrice(tickets);

        Map<String, Object> response = new HashMap<String, Object>();
        Amount amount = new Amount();
        amount.setCurrency("USD");
        amount.setTotal(String.valueOf(totalPrice));
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("http://localhost:4200/payment/cancel");
        redirectUrls.setReturnUrl("http://localhost:4200/payment/complete");
        payment.setRedirectUrls(redirectUrls);
        Payment createdPayment;
        try {
            String redirectUrl = "";
            APIContext context = new APIContext(clientId, clientSecret, "sandbox");
            createdPayment = payment.create(context);
            if(createdPayment!=null){
                List<Links> links = createdPayment.getLinks();
                for (Links link:links) {
                    if(link.getRel().equals("approval_url")){
                        redirectUrl = link.getHref();
                        break;
                    }
                }
                response.put("status", "success");
                response.put("redirect_url", redirectUrl);
            }
        } catch (PayPalRESTException e) {
            System.out.println("Error happened during payment creation!");
        }
        return response;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, readOnly = false)
    public Map<String, Object> completePayment(PaymentDto paymentDto)
            throws PayPalRESTException, FetchException, PaymentException, AddException, IDMappingException {
        Map<String, Object> response = new HashMap<>();
        Payment payment = new Payment();
        payment.setId(paymentDto.getPaymentId());

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(paymentDto.getPayerId());
        APIContext context = new APIContext(clientId, clientSecret, "sandbox");
        ticketService.createPayedTickets(paymentDto.getTickets());
        Payment createdPayment = payment.execute(context, paymentExecution);
        if(createdPayment!=null){
            double payedTotal = 0d;
            for (Transaction t : createdPayment.getTransactions()) {
                payedTotal += Double.parseDouble(t.getAmount().getTotal());
            }
            if (payedTotal < getTotalPrice(paymentDto.getTickets())) {
                throw new PaymentException("Payment sum does not match total ticket price.");
            }
            response.put("status", "success");
        }
        return response;
    }


    private Double getTotalPrice(List<TicketDto> tickets) throws FetchException {
        double totalPrice = 0d;
        for (TicketDto dto: tickets) {
            if (dto.getGroundFloorId() != null) {
                Optional<GroundFloor> gf = this.groundFloorRepository.findById(dto.getGroundFloorId());
                if (!gf.isPresent()) {
                    throw new FetchException("Cannot find ground floor with id " + dto.getGroundFloorId());
                }
                totalPrice += gf.get().getPrice();
            } else if (dto.getSeatSpaceId() != null) {
                Optional<SeatSpace> ss = this.seatSpaceRepository.findById(dto.getSeatSpaceId());
                if (!ss.isPresent()) {
                    throw new FetchException("Cannot find seat space with id " + dto.getSeatSpaceId());
                }
                totalPrice += ss.get().getPrice();
            } else {
                throw new FetchException("Cannot find sector for ticket");
            }
        }
        return totalPrice;
    }


    public Map<String, Object> createPayment(Long id) throws FetchException {
        Optional<Ticket> ticket = this.ticketRepository.findById(id);
        if (!ticket.isPresent()) {
            throw new FetchException("Cannot find ticket with id: " + id);
        }

        Map<String, Object> response = new HashMap<String, Object>();
        Amount amount = new Amount();
        amount.setCurrency("USD");
        amount.setTotal(String.valueOf(ticket.get().getPrice()));
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("http://localhost:4200/singlePayment/cancel");
        redirectUrls.setReturnUrl("http://localhost:4200/singlePayment/complete");
        payment.setRedirectUrls(redirectUrls);
        Payment createdPayment;
        try {
            String redirectUrl = "";
            APIContext context = new APIContext(clientId, clientSecret, "sandbox");
            createdPayment = payment.create(context);
            if(createdPayment!=null){
                List<Links> links = createdPayment.getLinks();
                for (Links link:links) {
                    if(link.getRel().equals("approval_url")){
                        redirectUrl = link.getHref();
                        break;
                    }
                }
                response.put("status", "success");
                response.put("redirect_url", redirectUrl);
            }
        } catch (PayPalRESTException e) {
            System.out.println("Error happened during payment creation!");
        }
        return response;
    }

    public Map<String, Object> completePayment(SinglePaymentDto paymentDto)
            throws PayPalRESTException, FetchException, PaymentException, AddException, IDMappingException, UpdateException {
        Map<String, Object> response = new HashMap<>();
        Payment payment = new Payment();
        payment.setId(paymentDto.getPaymentId());

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(paymentDto.getPayerId());
        APIContext context = new APIContext(clientId, clientSecret, "sandbox");
        Payment createdPayment = payment.execute(context, paymentExecution);
        if(createdPayment!=null){
            double payedTotal = 0d;
            for (Transaction t : createdPayment.getTransactions()) {
                payedTotal += Double.parseDouble(t.getAmount().getTotal());
            }
            Optional<Ticket> ticket = this.ticketRepository.findById(paymentDto.getTicket().getId());
            if (!ticket.isPresent()) {
                throw new FetchException("Cannot find ticket with id " + paymentDto.getTicket().getId());
            }
            if (payedTotal < ticket.get().getPrice()) {
                throw new PaymentException("Payment sum does not match total ticket price.");
            }
            response.put("status", "success");
            ticketService.payForTicket(paymentDto.getTicket().getId());
        }
        return response;
    }
}
