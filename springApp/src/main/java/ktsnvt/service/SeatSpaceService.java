package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.dto.SeatDto;
import ktsnvt.entity.*;
import ktsnvt.repository.SeatSpaceRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.service.contracts.ISeatSpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SeatSpaceService extends CRUDService<SeatSpace> implements ISeatSpaceService {

    @Autowired
    public void setRepository(SeatSpaceRepository seatSpaceRepository) { this.repository = seatSpaceRepository; }

    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public void deleteById(Long id) throws DeleteException, FetchException {
        Optional<SeatSpace> seatSpaceOptional = this.repository.findById(id);
        if (!seatSpaceOptional.isPresent() || seatSpaceOptional.get().isDeleted()) {
            throw new FetchException("No SeatSpace with id:" + id + " to delete");
        }
        SeatSpace seatSpace = seatSpaceOptional.get();
        if (seatSpace.getSpaceLayout().getSingleEvent() != null) {
            throw new DeleteException("Cannot delete SeatSpace with id " + id +
                    " SeatSpace is tied to an event");
        }
        this.repository.deleteById(id);
    }


    public List<SeatDto> getSeatsTaken(Long seatSpaceId) throws FetchException {
        if (this.repository.existsById(seatSpaceId)) {
            List<Ticket> tickets = ticketRepository.findBySeatSpaceIdAndUserIsNotNull(seatSpaceId);
            List<SeatDto> seats = new ArrayList<>();
            for (Ticket t : tickets) {
                SeatDto seat = new SeatDto(t.getRow(), t.getSeat());
                seats.add(seat);
            }
            return seats;
        } else {
            throw new FetchException("Seat space with id " + seatSpaceId + " was not found.");
        }
    }

    public void addTickets(Long seatSpaceId, SingleEvent event) throws AddException {
        Optional<SeatSpace> seatSpaceOpt = repository.findById(seatSpaceId);
        if (!seatSpaceOpt.isPresent()) {
            throw new AddException("Seat space with id " + seatSpaceId.toString() + " does not exist.");
        }

        SeatSpace seatSpace = seatSpaceOpt.get();

        for (int i = 0; i < seatSpace.getRows(); i++) {
            for (int j = 0; j < seatSpace.getSeats(); j++) {
                Ticket ticket = new Ticket();
                ticket.setSeatSpace(seatSpace);
                ticket.setRow(i);
                ticket.setSeat(j);
                ticket.setDeleted(false);
                ticket.setSingleEvent(event);
                seatSpace.getTickets().add(ticket);
            }
        }
        repository.save(seatSpace);
    }
}
