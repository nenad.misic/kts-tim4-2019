package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceLayoutRepository;
import ktsnvt.service.contracts.ISpaceLayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class SpaceLayoutService extends CRUDService<SpaceLayout> implements ISpaceLayoutService {
    @Autowired
    public void setRepository(SpaceLayoutRepository spaceLayoutRepository) { this.repository = spaceLayoutRepository; }

    @Autowired
    private GroundFloorService groundFloorService;

    @Autowired
    private SeatSpaceService seatSpaceService;

    @Autowired
    private EventService eventService;

    @Override
    public void add(SpaceLayout spaceLayout) {
        for (GroundFloor gf : spaceLayout.getGroundFloors()) {
            gf.setSpaceLayout(spaceLayout);
        }
        for (SeatSpace ss : spaceLayout.getSeatSpaces()) {
            ss.setSpaceLayout(spaceLayout);
        }
        this.repository.save(spaceLayout);
    }

    @Transactional
    @Override
    public void deleteById(Long id) throws DeleteException, FetchException {
        // deletes space layout and corresponding ground floors and seat spaces
        // if no active tickets are assigned
        Optional<SpaceLayout> spaceLayoutOptional = this.repository.findById(id);

        if (spaceLayoutOptional.isPresent() && !spaceLayoutOptional.get().isDeleted()) {
            SpaceLayout spaceLayout = spaceLayoutOptional.get();
            //Only default spaceLayouts should be deletable, so you can delete a spaceLayout only if its event is null
            if (spaceLayout.getSingleEvent() != null) {
                throw new DeleteException("Cannot delete spaceLayout with id: " + spaceLayout.getId() + ", it is tied to an event");
            }
            Set<GroundFloor> groundFloors = spaceLayout.getGroundFloors();
            if (groundFloors != null) {
                for (GroundFloor gf: groundFloors) {
                    // will throw Delete exception if ground floor has active tickets
                    this.groundFloorService.deleteById(gf.getId());
                }
            }
            Set<SeatSpace> seatSpaces = spaceLayout.getSeatSpaces();
            if (seatSpaces != null) {
                for(SeatSpace ss: seatSpaces) {
                    // will throw Delete exception if seat space has active tickets
                    this.seatSpaceService.deleteById(ss.getId());
                }
            }

            this.repository.deleteById(id);
        } else {
            throw new FetchException("No SpaceLayout with id "+ id + "to delete");
        }
    }

}
