package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.dto.SpaceDto;
import ktsnvt.entity.Space;
import ktsnvt.repository.SpaceRepository;
import ktsnvt.service.contracts.ISpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SpaceService extends CRUDService<Space> implements ISpaceService {

    @Autowired
    public void setRepository(SpaceRepository spaceRepository) {
        this.repository = spaceRepository;
    }

    @Override
    public void deleteById(Long id) throws DeleteException, FetchException {
        Optional<Space> space = this.repository.findById(id);
        if (space.isPresent() && !space.get().isDeleted()) {
            if (space.get().getEvents().size() > 0) {
                throw new DeleteException("Cannot delete Space with id " + id +
                        ": Space has relationships to one or more events");
            } else {
                this.repository.deleteById(id);
            }
        } else {
            throw new FetchException("No Space with id "+ id + "to delete");
        }
    }

}
