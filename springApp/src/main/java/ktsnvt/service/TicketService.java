package ktsnvt.service;

import com.zaxxer.hikari.util.IsolationLevel;
import ktsnvt.common.exceptions.*;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.mapper.TicketMapper;
import ktsnvt.mapper.UserMapper;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.repository.UserRepository;
import ktsnvt.service.contracts.ITicketService;
import org.hibernate.sql.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class TicketService extends CRUDService <Ticket> implements ITicketService {

    @Autowired
    public void setRepository(TicketRepository ticketRepository) {this.repository = ticketRepository;}

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TicketMapper ticketMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroundFloorRepository groundFloorRepository;

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, readOnly = false)
    public void add(List<TicketDto> ticketDtos) throws AddException, IDMappingException, FetchException, InterruptedException {
        User user = this.getCurrentUser();

        if (user == null) {
            throw new FetchException("Unable to find logged in user");
        }

        ArrayList<Ticket> tickets = new ArrayList<>();
        HashMap<GroundFloor, Integer> groundFloorsMap = new HashMap<>();
        for (TicketDto ticketDto : ticketDtos) {

            if (ticketDto.getGroundFloorId() != null && ticketDto.getSeatSpaceId() != null) {
                throw new AddException("Unable to make reservation on both seat space and ground floor");
            }

            if (ticketDto.getSeatSpaceId() != null) {
                Ticket ticket = ((TicketRepository) repository).findBySeatSpaceIdAndRowAndSeat(ticketDto.getSeatSpaceId(),
                        ticketDto.getRow(), ticketDto.getSeat());

                if (ticket == null) {
                    throw new FetchException("Invalid ticket");
                }

                if (ticket.getUser() != null) {
                    throw new AddException("Ticket is already reserved");
                }

                if (ticket.getSingleEvent().getStartDate().before(new Date())) {
                    throw new AddException("Unable to make reservation for event that has already happened.");
                }


                Calendar calendar = Calendar.getInstance();
                calendar.setTime(ticket.getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                if (calendar.getTime().before(new Date())) {
                    throw new AddException("Unable to make reservation 24h before the event.");
                }

                if (tickets.contains(ticket)) {
                    throw new AddException("Unable to make reservation for the same seat twice.");
                }

                tickets.add(ticket);
            } else if (ticketDto.getGroundFloorId() != null) {

                Optional<GroundFloor> gf = groundFloorRepository.fetchByIdAndLock(ticketDto.getGroundFloorId());

                if (!gf.isPresent()) {
                    throw new FetchException("Invalid ground floor id in ticket");
                }

                if (gf.get().getSpaceLayout().getSingleEvent() == null) {
                    throw new AddException("Ground floor is part of default space layout.");
                }

                if (gf.get().getSpaceLayout().getSingleEvent().getStartDate().before(new Date())){
                    throw new AddException("Unable to make reservation for event that has already happened.");
                }


                Calendar calendar = Calendar.getInstance();
                calendar.setTime(gf.get().getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                if (calendar.getTime().before(new Date())) {
                    throw new AddException("Unable to make reservation 24h before the event.");
                }

                if (groundFloorsMap.containsKey(gf.get())) {
                    groundFloorsMap.put(gf.get(), groundFloorsMap.get(gf.get())+ 1);
                } else {
                    groundFloorsMap.put(gf.get(), 1);
                }
            } else {
                throw new AddException("Unable to make reservation. Neither seatSpace nor GroundFloor selected");
            }
        }


        for (GroundFloor gf : groundFloorsMap.keySet()) {
            if (gf.getSpotsReserved() + groundFloorsMap.get(gf) >= gf.getCapacity()) {
                throw new AddException("Not enough space in ground fljoor");
            }
        }


        for (Ticket ticket : tickets) {
            ticket.setUser(user);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(ticket.getSingleEvent().getStartDate());
            calendar.add(Calendar.DAY_OF_MONTH, - 1);
            ticket.setExpirationDate(calendar.getTime());
            ticket.setPaid(false);
            repository.save(ticket);
        }

        for (GroundFloor gf : groundFloorsMap.keySet()) {
            for (int i = 0; i < groundFloorsMap.get(gf); i++) {
                Ticket ticket = new Ticket();
                ticket.setGroundFloor(gf);
                ticket.setSingleEvent(gf.getSpaceLayout().getSingleEvent());
                ticket.setPrice(gf.getPrice());
                ticket.setPaid(false);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(ticket.getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                ticket.setExpirationDate(calendar.getTime());
                ticket.setUser(user);
                this.repository.save(ticket);
            }
            gf.setSpotsReserved(gf.getSpotsReserved() + groundFloorsMap.get(gf));
            this.groundFloorRepository.save(gf);
        }
//        Thread.sleep(2000);
    }

    @Override
    public void deleteById(Long id)  throws DeleteException, FetchException {
        Optional<Ticket> ticketOptional = this.repository.findById(id);
        if (ticketOptional.isPresent() && !ticketOptional.get().isDeleted()) {
            User user = getCurrentUser();
            Ticket ticket = ticketOptional.get();
            if (!ticket.getUser().getUsername().equals(user.getUsername())) {
                throw new DeleteException("Ticket does not belong to this user.");
            }
            if (!ticket.isPaid()) {
                if (ticket.getGroundFloor() != null) {
                    ticket.logicalDelete();
                    ticket.getGroundFloor().setSpotsReserved(ticket.getGroundFloor().getSpotsReserved()-1);
                    this.groundFloorRepository.save(ticket.getGroundFloor());
                } else {
                    ticket.setUser(null);
                }
                this.repository.save(ticket);
            } else {
                throw new DeleteException("Ticket already purchased");
            }
        } else {
            throw new FetchException("Ticket with id: " + id + "does not exist.");
        }
    }

    public void payForTicket(Long id) throws UpdateException, FetchException {
        Optional<Ticket> ticketOptional = this.repository.findById(id);
        if (ticketOptional.isPresent() && !ticketOptional.get().isDeleted()) {
            Ticket ticket = ticketOptional.get();

            if (ticket.isPaid()) {
                throw new UpdateException("Ticket already paid for.");
            }
            if (!ticket.getUser().getId().equals(this.getCurrentUser().getId())) {
                    throw new UpdateException("Ticket does not belong to this user.");
            }

            ticket.setPaid(true);
            repository.save(ticket);
        } else {
            throw new FetchException("Ticket with id: " + id + " does not exist.");
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class,
            readOnly = false, propagation = Propagation.MANDATORY)
    public void createPayedTickets(List<TicketDto> ticketDtos) throws AddException, FetchException, IDMappingException {
        User user = this.getCurrentUser();

        if (user == null) {
            throw new FetchException("Unable to find logged in user");
        }

        ArrayList<Ticket> tickets = new ArrayList<>();
        HashMap<GroundFloor, Integer> groundFloorsMap = new HashMap<>();
        for (TicketDto ticketDto : ticketDtos) {

            if (ticketDto.getGroundFloorId() != null && ticketDto.getSeatSpaceId() != null) {
                throw new AddException("Unable to make reservation on both seat space and ground floor");
            }

            if (ticketDto.getSeatSpaceId() != null) {

                Ticket ticket = ((TicketRepository) repository).findBySeatSpaceIdAndRowAndSeat(ticketDto.getSeatSpaceId(),
                        ticketDto.getRow(), ticketDto.getSeat());

                if (ticket == null) {
                    throw new FetchException("Invalid ticket");
                }

                if (ticket.getUser() != null) {
                    throw new AddException("Ticket is already reserved");
                }

                if (ticket.getSingleEvent().getStartDate().before(new Date())) {
                    throw new AddException("Unable to make reservation for event that has already happened.");
                }

                if (tickets.contains(ticket)) {
                    throw new AddException("Unable to make reservation for the same seat twice.");
                }

                tickets.add(ticket);
            } else if (ticketDto.getGroundFloorId() != null) {

                Optional<GroundFloor> gf = groundFloorRepository.fetchByIdAndLock(ticketDto.getGroundFloorId());

                if (!gf.isPresent()) {
                    throw new FetchException("Invalid ground floor id in ticket");
                }

                if (gf.get().getSpaceLayout().getSingleEvent() == null) {
                    throw new AddException("Ground floor is part of default space layout.");
                }

                if (gf.get().getSpaceLayout().getSingleEvent().getStartDate().before(new Date())){
                    throw new AddException("Unable to make reservation for event that has already happened.");
                }

                if (groundFloorsMap.containsKey(gf.get())) {
                    groundFloorsMap.put(gf.get(), groundFloorsMap.get(gf.get())+ 1);
                } else {
                    groundFloorsMap.put(gf.get(), 1);
                }
            } else {
                throw new AddException("Unable to make reservation. Neither seatSpace nor GroundFloor selected");
            }
        }


        for (GroundFloor gf : groundFloorsMap.keySet()) {
            if (gf.getSpotsReserved() + groundFloorsMap.get(gf) >= gf.getCapacity()) {
                throw new AddException("Not enough space in ground fljoor");
            }
        }


        for (Ticket ticket : tickets) {
            ticket.setUser(user);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(ticket.getSingleEvent().getStartDate());
            calendar.add(Calendar.DAY_OF_MONTH, - 1);
            ticket.setExpirationDate(calendar.getTime());
            ticket.setPaid(true);
            repository.save(ticket);
        }

        for (GroundFloor gf : groundFloorsMap.keySet()) {
            for (int i = 0; i < groundFloorsMap.get(gf); i++) {
                Ticket ticket = new Ticket();
                ticket.setGroundFloor(gf);
                ticket.setSingleEvent(gf.getSpaceLayout().getSingleEvent());
                ticket.setPrice(gf.getPrice());
                ticket.setPaid(false);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(ticket.getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                ticket.setExpirationDate(calendar.getTime());
                ticket.setPaid(true);
                ticket.setUser(user);
                this.repository.save(ticket);
            }
            gf.setSpotsReserved(gf.getSpotsReserved() + groundFloorsMap.get(gf));
            this.groundFloorRepository.save(gf);
        }

    }

    public List<Ticket> getUserTickets(Pageable pageable) {
        return ((TicketRepository) repository).findByUserId(getCurrentUser().getId(), pageable);
    }

    private User getCurrentUser() {
        String username = ((org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return this.userRepository.findByUsername(username);
    }

}
