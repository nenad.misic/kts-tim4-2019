package ktsnvt.service;

import ktsnvt.dto.UserDto;
import ktsnvt.entity.*;
import ktsnvt.repository.UserAuthorityRepository;
import ktsnvt.repository.UserRepository;
import ktsnvt.repository.VerificationTokenRepository;
import ktsnvt.repository.contracts.IUserRepository;
import ktsnvt.service.contracts.IUserService;
import org.hibernate.usertype.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.Email;
import java.util.*;

@Service
public class UserService extends CRUDService<User> implements IUserService, UserDetailsService {

    @Autowired
    public void setRepository(UserRepository userRepository) {this.repository = userRepository;}

    @Autowired
    private EmailService emailService;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserAuthorityRepository userAuthorityRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = ((IUserRepository) repository).findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            for (UserAuthority ua: user.getUserAuthorities()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(ua.getAuthority()));
            }

            return new org.springframework.security.core.userdetails.User(
                    user.getUsername(),
                    user.getPassword(),
                    grantedAuthorities);
        }
    }


    public User register(UserDto userDto) throws Exception {
        if(((IUserRepository)repository).findByUsername(userDto.getUsername())!= null) {
            throw new Exception("Username already taken!");
        }
        User user = new User();
        user.setId(null);
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setVerified(false);
        user.setRole(UserRole.Regular);
        Set<UserAuthority> authorities = new HashSet<>();
        UserAuthority a = this.userAuthorityRepository.findByRole(UserRole.Regular);
        if (a == null) {
            a = new UserAuthority();
            a.setRole(UserRole.Regular);
            a = this.userAuthorityRepository.save(a);
        }
        authorities.add(a);
        user.setUserAuthorities(authorities);

        repository.save(user);
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setToken(UUID.randomUUID().toString());
        emailService.sendVerificationMessage(user,verificationToken);
        verificationTokenRepository.save(verificationToken);
        return user;
    }
    public void verifyUser(String token) throws Exception {
        VerificationToken vt = verificationTokenRepository.findByToken(token);
        if(vt==null){
            throw new Exception("The link is invalid or broken!");
        }
        User u = ((IUserRepository)repository).findByUsername(vt.getUser().getUsername());
        u.setVerified(true);
        repository.save(u);
    }
}
