package ktsnvt.service.contracts;

import ktsnvt.entity.Event;
import ktsnvt.service.IService;

public interface IEventService extends IService<Event> {
}
