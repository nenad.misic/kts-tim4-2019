package ktsnvt.service.contracts;

import ktsnvt.entity.GroundFloor;
import ktsnvt.service.IService;

public interface IGroundFloorService extends IService<GroundFloor> {
}
