package ktsnvt.service.contracts;

import ktsnvt.entity.Location;
import ktsnvt.service.IService;

public interface ILocationService extends IService<Location> {
}
