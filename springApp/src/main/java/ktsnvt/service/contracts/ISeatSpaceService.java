package ktsnvt.service.contracts;

import ktsnvt.entity.SeatSpace;
import ktsnvt.service.IService;

public interface ISeatSpaceService extends IService<SeatSpace> {
}
