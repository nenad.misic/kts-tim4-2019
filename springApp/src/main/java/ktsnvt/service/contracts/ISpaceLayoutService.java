package ktsnvt.service.contracts;

import ktsnvt.entity.SpaceLayout;
import ktsnvt.service.IService;

public interface ISpaceLayoutService extends IService<SpaceLayout> {
}
