package ktsnvt.service.contracts;

import ktsnvt.entity.Space;
import ktsnvt.service.IService;

public interface ISpaceService extends IService<Space> {
}
