package ktsnvt.service.contracts;

import ktsnvt.entity.Ticket;
import ktsnvt.service.IService;

public interface ITicketService extends IService<Ticket> {
}
