package ktsnvt.service.contracts;

import ktsnvt.dto.UserDto;
import ktsnvt.entity.User;
import ktsnvt.service.IService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.persistence.EntityNotFoundException;

public interface IUserService extends IService<User>, UserDetailsService {

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    public User register(UserDto user) throws Exception;
    public void verifyUser(String token) throws Exception;
}
