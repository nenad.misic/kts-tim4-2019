package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import ktsnvt.common.DeletableEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.Assert.*;
import java.text.ParseException;
import java.util.List;

public abstract class CRUDControllerGenericTest<Controller extends CRUDController<E, DTO>, E extends DeletableEntity, DTO, Repository extends JpaRepository<E, Long>> {

    @Autowired
    protected Repository repository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mvc;

    @PostConstruct
    public void setup() throws ParseException {
        this.mvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getByIdNotExists() throws Exception {
        Long nonExistingId = -1L;
        mvc.perform(MockMvcRequestBuilders
        .get(getAPIPath() + "/" + nonExistingId))
        .andExpect(status().isBadRequest());
    }

    @Test
    public void getByIdExists() throws Exception {
        E object = createSampleEntity();
        E o = repository.save(object);
        Long id = getId(o);
        mvc.perform(MockMvcRequestBuilders
        .get(getAPIPath() + "/" + id)
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
        repository.deleteById(id);
    }

    @Test
    public void deleteById() throws Exception {
        E object = createSampleEntity();
        E o = repository.save(object);
        Long id = getId(o);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));
        List<E> allEntities = repository.findAll();
        for (E e: allEntities) {
            assertFalse(getId(e).equals(id));
        }
    }

    @Test
    public void deleteByIdNotExists() throws Exception {
        Long nonExistingId = -1L;
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + nonExistingId))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAll() throws Exception {
        List<E> allEntities = repository.findAll();
        int length = allEntities.size();
        Integer[] ids = new Integer[length];
        for (int i = 0; i < length; i++) {
            ids[i] = Integer.parseInt(getId(allEntities.get(i)).toString());
        }
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(length)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(ids)));
    }

    @Test
    public void getPage() throws Exception {
        int length = repository.findAll().size();
        Pageable pageable = PageRequest.of(0, length - 1);
        //get first page which contains all but 1 element
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/page?page=" + 0 + "&size=" + (length - 1))
            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(length - 1)));

        //get next page, and expect it to contain only 1 element
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/page?page=" + 1 + "&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

        //get the third page and expect it to be empty
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/page?page=" + 2 + "&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void create() throws Exception {
        DTO dto = createSampleDto();
        String dtoJson = getJson(dto);
        List<E> allEntities = repository.findAll();
        int length = allEntities.size();
        mvc.perform(MockMvcRequestBuilders
            .post(getAPIPath())
            .contentType(MediaType.APPLICATION_JSON)
            .content(dtoJson)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.message").value("Created"));
        assertEquals(length + 1, repository.findAll().size());
        //we want to delete the newly added entity
        Long newId = -1L;
        List<E> allEntitiesNew = repository.findAll();
        for (E entityNew: allEntitiesNew) {
            boolean entityAlreadyExisted = false;
            for (E entityOld: allEntities) {
                if (getId(entityNew).equals(getId(entityOld))) {
                    entityAlreadyExisted = true;
                }
            }
            if (!entityAlreadyExisted) {
                newId = getId(entityNew);
                break;
            }
        }
        repository.deleteById(newId);
    }


    @Test
    public void update() throws Exception {
        E object = createSampleEntity();
        E o = repository.save(object);
        Long id = getId(o);
        DTO dto = createSampleDto();
        setId(dto, id);
        String dtoJson = getJson(dto);
        mvc.perform(MockMvcRequestBuilders
            .put(getAPIPath())
            .contentType(MediaType.APPLICATION_JSON)
            .content(dtoJson))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.message").value("Updated"));
        //todo check if our object has been changed
        repository.deleteById(id);
    }

    protected abstract void setId(DTO dto, Long id);

    protected abstract E createSampleEntity();

    protected abstract DTO createSampleDto();

    protected abstract String getAPIPath();

    protected abstract Class getDTOType();

    protected abstract Long getId(DTO dto);

    protected abstract Long getId(E entity);

    protected abstract String getJson(DTO dto) throws JsonProcessingException;
}
