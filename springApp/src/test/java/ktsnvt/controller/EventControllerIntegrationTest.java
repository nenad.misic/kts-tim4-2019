package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.IDMappingException;
import ktsnvt.mapper.TicketMapper;
import ktsnvt.repository.TicketRepository;
import ktsnvt.service.TicketService;
import ktsnvt.utility.TestUtil;
import ktsnvt.entity.*;
import ktsnvt.mapper.CompositeEventMapper;
import ktsnvt.mapper.SingleEventMapper;
import ktsnvt.service.SpaceLayoutService;
import ktsnvt.service.SpaceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class EventControllerIntegrationTest {

    private static final String URL_PREFIX = "/api/event";
    @Autowired
    private SingleEventMapper singleEventMapper;

    @Autowired
    private CompositeEventMapper compositeEventMapper;

    @Autowired
    protected SpaceService spaceService;

    @Autowired
    protected SpaceLayoutService spaceLayoutService;


    @Autowired
    protected TicketRepository ticketRepository;

    private Space tvrdjava;
    private SpaceLayout exitDay4SpaceLayout;

    private Event exitDay1;
    private Event exitDay2;
    private Event exitDay3;
    private Event exitDay4;
    private Event seaDanceDay1;
    private Event seaDanceDay2;
    private Event seaDanceDay3;
    private Event exit;
    private Event seaDance;
    private Event exitAndSeaDance;
    private Event koncertZvonkaBogdana1;
    private Event koncertZvonkaBogdana2;
    private Event turnejaZvonkaBogdana;
    private Event koncertMayeBerovic;

    private Event add_josJednaTurnejaZvonkaBogdana;
    private Event add_josJedanKoncertZvonkaBogdana1;
    private Event add_josJedanKoncertZvonkaBogdana2;
    private Event add_koncertLaneDelRey;
    private Event add_exitDay5;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @PostConstruct
    public void setup() throws ParseException, FetchException, AddException, IDMappingException {
        setUpAttributes();
        this.mvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).build();
    }




    @Test
    @Transactional
    @Rollback(true)
    public void getById_NotExists() throws Exception {
        Long id = 5000L;
        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_SingleEvent_NoParent_Exists() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + koncertMayeBerovic.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + koncertMayeBerovic.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(koncertMayeBerovic.getId()))
                .andExpect(jsonPath("$.name").value(koncertMayeBerovic.getName()))
                .andExpect(jsonPath("$.description").value(koncertMayeBerovic.getDescription()))
                .andExpect(jsonPath("$.type").value(koncertMayeBerovic.getType().toString()))
                .andExpect(jsonPath("$.state").value(((SingleEvent)koncertMayeBerovic).getState().toString()))
                .andExpect(jsonPath("$.startDate").value(((SingleEvent)koncertMayeBerovic).getStartDate().getTime()))
                .andExpect(jsonPath("$.endDate").value(((SingleEvent)koncertMayeBerovic).getEndDate().getTime()))
                .andExpect(jsonPath("$.spaceId").value(((SingleEvent)koncertMayeBerovic).getSpace().getId()))
                .andExpect(jsonPath("$.spaceLayout.id").value(((SingleEvent)koncertMayeBerovic).getSpaceLayout().getId()))
                .andExpect(jsonPath("$.spaceLayout").exists())
                .andExpect(jsonPath("$.space").exists())
                .andExpect(jsonPath("$.parentEvent").doesNotExist())
                .andExpect(jsonPath("$.tickets").exists());
    }


    @Test
    @Transactional
    @Rollback(true)
    public void getById_SingleEvent_WithParent_Exists() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + exitDay3.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + exitDay3.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(exitDay3.getId()))
                .andExpect(jsonPath("$.name").value(exitDay3.getName()))
                .andExpect(jsonPath("$.description").value(exitDay3.getDescription()))
                .andExpect(jsonPath("$.type").value(exitDay3.getType().toString()))
                .andExpect(jsonPath("$.state").value(((SingleEvent)exitDay3).getState().toString()))
                .andExpect(jsonPath("$.startDate").value(((SingleEvent)exitDay3).getStartDate().getTime()))
                .andExpect(jsonPath("$.endDate").value(((SingleEvent)exitDay3).getEndDate().getTime()))
                .andExpect(jsonPath("$.spaceId").value(((SingleEvent)exitDay3).getSpace().getId()))
                .andExpect(jsonPath("$.spaceLayout.id").value(((SingleEvent)exitDay3).getSpaceLayout().getId()))
                .andExpect(jsonPath("$.spaceLayout").exists())
                .andExpect(jsonPath("$.space").exists())
                .andExpect(jsonPath("$.parentEvent").exists())
                .andExpect(jsonPath("$.parentEvent.name").value(exitDay3.getParentEvent().getName()))
                .andExpect(jsonPath("$.parentEvent.childEvents").doesNotExist())
                .andExpect(jsonPath("$.tickets").exists());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_NoParent_Exists() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(turnejaZvonkaBogdana.getId()))
                .andExpect(jsonPath("$.name").value(turnejaZvonkaBogdana.getName()))
                .andExpect(jsonPath("$.description").value(turnejaZvonkaBogdana.getDescription()))
                .andExpect(jsonPath("$.type").value(turnejaZvonkaBogdana.getType().toString()))
                .andExpect(jsonPath("$.childEvents.length()").value(((CompositeEvent)turnejaZvonkaBogdana).getChildEvents().size()));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_WithParent_Exists() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(turnejaZvonkaBogdana.getId()))
                .andExpect(jsonPath("$.name").value(turnejaZvonkaBogdana.getName()))
                .andExpect(jsonPath("$.description").value(turnejaZvonkaBogdana.getDescription()))
                .andExpect(jsonPath("$.type").value(turnejaZvonkaBogdana.getType().toString()))
                .andExpect(jsonPath("$.childEvents.length()").value(((CompositeEvent)turnejaZvonkaBogdana).getChildEvents().size()));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_Complex_Exists() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + exitAndSeaDance.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + exitAndSeaDance.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(exitAndSeaDance.getId()))
                .andExpect(jsonPath("$.name").value(exitAndSeaDance.getName()))
                .andExpect(jsonPath("$.description").value(exitAndSeaDance.getDescription()))
                .andExpect(jsonPath("$.type").value(exitAndSeaDance.getType().toString()))
                .andExpect(jsonPath("$.childEvents.length()").value(((CompositeEvent)exitAndSeaDance).getChildEvents().size()))
                .andExpect(jsonPath("$.parentEvent").doesNotExist());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getAll_Success() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        turnejaZvonkaBogdana.getName(),
                        koncertMayeBerovic.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        turnejaZvonkaBogdana.getDescription(),
                        koncertMayeBerovic.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString(),
                        koncertMayeBerovic.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent(),
                        koncertMayeBerovic.getParentEvent())));

    }

    @Test
    @Transactional
    @Rollback(true)
    public void deleteById_IdDoesNotExist() throws Exception {
        Long id = 5000L;
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test()
    @Transactional
    @Rollback(true)
    public void deleteById_SingleEvent_WithNoParent() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + koncertMayeBerovic.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test()
    @Transactional
    @Rollback(true)
    public void deleteById_SingleEvent_WithParent() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + exitDay1.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test()
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_WithNoParent() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent())));
    }

    @Test()
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_WithParent() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + exit.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }


    @Test()
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_Complex() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .delete(URL_PREFIX + "/" + exitAndSeaDance.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(koncertMayeBerovic.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        koncertMayeBerovic.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        koncertMayeBerovic.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        koncertMayeBerovic.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        koncertMayeBerovic.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_SingleEvent_WithNoParent_Success() throws Exception {
//        Space s = new Space();
//        SpaceLayout sl = new SpaceLayout();
//        s.setId(99302L);
//        sl.setId(99304L);
        ((SingleEvent)add_koncertLaneDelRey).setSpace(tvrdjava);
        ((SingleEvent)add_koncertLaneDelRey).setSpaceLayout(exitDay4SpaceLayout);
        String json = constructEventJson(add_koncertLaneDelRey);
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName(),
                        add_koncertLaneDelRey.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription(),
                        add_koncertLaneDelRey.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString(),
                        add_koncertLaneDelRey.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent(),
                        add_koncertLaneDelRey.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_CompositeEvent_WithNoParent_Success() throws Exception {
//        Space space1 = new Space();
//        SpaceLayout spaceLayout1 = new SpaceLayout();
//        Space space2 = new Space();
//        SpaceLayout spaceLayout2 = new SpaceLayout();
//        space1.setId(99303L);
//        spaceLayout1.setId(99305L);
//        space2.setId(99303L);
//        spaceLayout2.setId(99306L);
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana1).setSpace(tvrdjava);
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana2).setSpace(tvrdjava);
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana1).setSpaceLayout(exitDay4SpaceLayout);
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana2).setSpaceLayout(exitDay4SpaceLayout);
        ((CompositeEvent)add_josJednaTurnejaZvonkaBogdana).setChildEvents(new HashSet<>(Arrays.asList(add_josJedanKoncertZvonkaBogdana1, add_josJedanKoncertZvonkaBogdana2)));
        String json = constructEventJson(add_josJednaTurnejaZvonkaBogdana);
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName(),
                        add_josJednaTurnejaZvonkaBogdana.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription(),
                        add_josJednaTurnejaZvonkaBogdana.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString(),
                        add_josJednaTurnejaZvonkaBogdana.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent(),
                        add_josJednaTurnejaZvonkaBogdana.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_SingleEvent_WithParent_Success() throws Exception {
//        Space s = new Space();
//        SpaceLayout sl = new SpaceLayout();
//        s.setId(992L);
//        sl.setId(99313L);
        ((SingleEvent)add_exitDay5).setSpace(tvrdjava);
        ((SingleEvent)add_exitDay5).setSpaceLayout(exitDay4SpaceLayout);

        String json = constructEventJson(add_exitDay5);
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_SingleEvent_NoParent_Success() throws Exception {
        koncertMayeBerovic.setDescription("Promenjen opis");
        koncertMayeBerovic.setName("Promenjeno ime");

        String json = constructEventJson(koncertMayeBerovic);
        mvc.perform( MockMvcRequestBuilders
                .put(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get(URL_PREFIX + "/" + koncertMayeBerovic.getId())).andReturn();
        String response = result.getResponse().getContentAsString();

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + koncertMayeBerovic.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(koncertMayeBerovic.getId()))
                .andExpect(jsonPath("$.name").value(koncertMayeBerovic.getName()))
                .andExpect(jsonPath("$.description").value(koncertMayeBerovic.getDescription()))
                .andExpect(jsonPath("$.type").value(koncertMayeBerovic.getType().toString()))
                .andExpect(jsonPath("$.state").value(((SingleEvent)koncertMayeBerovic).getState().toString()))
                .andExpect(jsonPath("$.startDate").value(((SingleEvent)koncertMayeBerovic).getStartDate().getTime()))
                .andExpect(jsonPath("$.endDate").value(((SingleEvent)koncertMayeBerovic).getEndDate().getTime()))
                .andExpect(jsonPath("$.spaceId").value(((SingleEvent)koncertMayeBerovic).getSpace().getId()))
                .andExpect(jsonPath("$.spaceLayout.groundFloors.length()").value(((SingleEvent)koncertMayeBerovic).getSpaceLayout().getGroundFloors().size()))
                .andExpect(jsonPath("$.spaceLayout.seatSpaces.length()").value(((SingleEvent)koncertMayeBerovic).getSpaceLayout().getSeatSpaces().size()))
                .andExpect(jsonPath("$.spaceLayout").exists())
                .andExpect(jsonPath("$.space").exists())
                .andExpect(jsonPath("$.parentEvent").doesNotExist())
                .andExpect(jsonPath("$.tickets").doesNotExist());


    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_NoParent_Success() throws Exception {
        turnejaZvonkaBogdana.setDescription("Promenjen opis");
        turnejaZvonkaBogdana.setName("Promenjeno ime");

        String json = constructEventJson(turnejaZvonkaBogdana);
        mvc.perform( MockMvcRequestBuilders
                .put(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(turnejaZvonkaBogdana.getId()))
                .andExpect(jsonPath("$.name").value(turnejaZvonkaBogdana.getName()))
                .andExpect(jsonPath("$.description").value(turnejaZvonkaBogdana.getDescription()))
                .andExpect(jsonPath("$.type").value(turnejaZvonkaBogdana.getType().toString()))
                .andExpect(jsonPath("$.childEvents.length()").value(((CompositeEvent)turnejaZvonkaBogdana).getChildEvents().size()));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_SingleEvent_NoParent_SetParent_Success() throws Exception {
        koncertMayeBerovic.setDescription("Promenjen opis");
        koncertMayeBerovic.setName("Promenjeno ime");
        koncertMayeBerovic.setType(EventType.Movie);
        ((SingleEvent)koncertMayeBerovic).setState(EventState.Cancelled);
        koncertMayeBerovic.setParentEvent((CompositeEvent) exit);
        String json = constructEventJson(koncertMayeBerovic);
        mvc.perform( MockMvcRequestBuilders
                .put(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + koncertMayeBerovic.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(koncertMayeBerovic.getId()))
                .andExpect(jsonPath("$.name").value(koncertMayeBerovic.getName()))
                .andExpect(jsonPath("$.description").value(koncertMayeBerovic.getDescription()))
                .andExpect(jsonPath("$.type").value(koncertMayeBerovic.getType().toString()))
                .andExpect(jsonPath("$.state").value(((SingleEvent)koncertMayeBerovic).getState().toString()))
                .andExpect(jsonPath("$.startDate").value(((SingleEvent)koncertMayeBerovic).getStartDate().getTime()))
                .andExpect(jsonPath("$.endDate").value(((SingleEvent)koncertMayeBerovic).getEndDate().getTime()))
                .andExpect(jsonPath("$.spaceId").value(((SingleEvent)koncertMayeBerovic).getSpace().getId()))
                .andExpect(jsonPath("$.spaceLayout.groundFloors.length()").value(((SingleEvent)koncertMayeBerovic).getSpaceLayout().getGroundFloors().size()))
                .andExpect(jsonPath("$.spaceLayout.seatSpaces.length()").value(((SingleEvent)koncertMayeBerovic).getSpaceLayout().getSeatSpaces().size()))
                .andExpect(jsonPath("$.spaceLayout").exists())
                .andExpect(jsonPath("$.space").exists())
                .andExpect(jsonPath("$.parentEvent").exists())
                .andExpect(jsonPath("$.tickets").doesNotExist());

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        turnejaZvonkaBogdana.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        turnejaZvonkaBogdana.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_NoParent_SetParent_Success() throws Exception {
        turnejaZvonkaBogdana.setParentEvent((CompositeEvent) exitAndSeaDance);
        String json = constructEventJson(turnejaZvonkaBogdana);
        mvc.perform( MockMvcRequestBuilders
                .put(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + turnejaZvonkaBogdana.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(turnejaZvonkaBogdana.getId()))
                .andExpect(jsonPath("$.name").value(turnejaZvonkaBogdana.getName()))
                .andExpect(jsonPath("$.description").value(turnejaZvonkaBogdana.getDescription()))
                .andExpect(jsonPath("$.type").value(turnejaZvonkaBogdana.getType().toString()))
                .andExpect(jsonPath("$.childEvents.length()").value(((CompositeEvent)turnejaZvonkaBogdana).getChildEvents().size()));

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        koncertMayeBerovic.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        koncertMayeBerovic.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        koncertMayeBerovic.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        koncertMayeBerovic.getParentEvent())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_WithParent_RemoveParent_Success() throws Exception {
        exit.setParentEvent(null);



        String json = constructEventJson(exit);
        mvc.perform( MockMvcRequestBuilders
                .put(URL_PREFIX + "/custom")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX + "/" + exit.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(exit.getId()))
                .andExpect(jsonPath("$.name").value(exit.getName()))
                .andExpect(jsonPath("$.description").value(exit.getDescription()))
                .andExpect(jsonPath("$.type").value(exit.getType().toString()));

        mvc.perform( MockMvcRequestBuilders
                .get(URL_PREFIX)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$.[*].id").value(hasItems(
                        Integer.parseInt(exitAndSeaDance.getId().toString()),
                        Integer.parseInt(turnejaZvonkaBogdana.getId().toString()),
                        Integer.parseInt(exit.getId().toString()),
                        Integer.parseInt(koncertMayeBerovic.getId().toString()))))
                .andExpect(jsonPath("$.[*].name").value(hasItems(
                        exitAndSeaDance.getName(),
                        turnejaZvonkaBogdana.getName(),
                        exit.getName(),
                        koncertMayeBerovic.getName())))
                .andExpect(jsonPath("$.[*].description").value(hasItems(
                        exitAndSeaDance.getDescription(),
                        turnejaZvonkaBogdana.getDescription(),
                        exit.getDescription(),
                        koncertMayeBerovic.getDescription())))
                .andExpect(jsonPath("$.[*].type").value(hasItems(
                        exitAndSeaDance.getType().toString(),
                        turnejaZvonkaBogdana.getType().toString(),
                        exit.getType().toString(),
                        koncertMayeBerovic.getType().toString())))
                .andExpect(jsonPath("$.[*].parentEvent").value(hasItems(
                        exitAndSeaDance.getParentEvent(),
                        turnejaZvonkaBogdana.getParentEvent(),
                        exit.getParentEvent(),
                        koncertMayeBerovic.getParentEvent())));

    }

    @Test
    @Transactional
    @Rollback(true)
    public void search_Quick_0Match() throws Exception {
        String json =
                    "{" +
                        "\"name\":\"asdf\"," +
                        "\"description\":\"asdf\"," +
                        "\"space\":{" +
                            "\"name\":\"asdf\"," +
                            "\"location\":{" +
                                "\"name\":\"asdf\"," +
                                "\"address\":\"asdf\"," +
                                "\"city\":\"asdf\"," +
                                "\"country\":\"asdf\"" +
                            "}" +
                        "}," +
                        "\"spaceLayout\":{" +
                            "\"name\":\"asdf\"" +
                        "}" +
                    "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Quick")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Quick_1Match() throws Exception {
        String json =
                "{" +
                        "\"name\":\"eXiT\"," +
                        "\"description\":\"eXiT\"," +
                        "\"space\":{" +
                        "\"name\":\"eXiT\"," +
                        "\"location\":{" +
                        "\"name\":\"eXiT\"," +
                        "\"address\":\"eXiT\"," +
                        "\"city\":\"eXiT\"," +
                        "\"country\":\"eXiT\"" +
                        "}" +
                        "}," +
                        "\"spaceLayout\":{" +
                        "\"name\":\"eXiT\"" +
                        "}" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Quick")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Quick_2Match() throws Exception {
        String json =
                "{" +
                        "\"name\":\"goRi\"," +
                        "\"description\":\"goRi\"," +
                        "\"space\":{" +
                        "\"name\":\"goRi\"," +
                        "\"location\":{" +
                        "\"name\":\"goRi\"," +
                        "\"address\":\"goRi\"," +
                        "\"city\":\"goRi\"," +
                        "\"country\":\"goRi\"" +
                        "}" +
                        "}," +
                        "\"spaceLayout\":{" +
                        "\"name\":\"goRi\"" +
                        "}" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Quick")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Quick_3Match() throws Exception {
        String json =
                "{" +
                        "\"name\":\"a\"," +
                        "\"description\":\"a\"," +
                        "\"space\":{" +
                        "\"name\":\"a\"," +
                        "\"location\":{" +
                        "\"name\":\"a\"," +
                        "\"address\":\"a\"," +
                        "\"city\":\"a\"," +
                        "\"country\":\"a\"" +
                        "}" +
                        "}," +
                        "\"spaceLayout\":{" +
                        "\"name\":\"a\"" +
                        "}" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Quick")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Regular_1attribute_3match() throws Exception {
        String json =
                "{" +
                        "\"name\":\"a\"" +
                "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Regular")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Regular_1attribute_2match() throws Exception  {
        String json =
                "{" +
                        "\"description\":\"gori\"" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Regular")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)));
    }
    @Test
    @Transactional
    @Rollback(true)
    public void search_Regular_more_attributes_2match()  throws Exception {
        String json =
                "{" +
                        "\"description\":\"gori\"," +
                        "\"name\": \"a\"" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Regular")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void search_Regular_more_attributes_1match()  throws Exception {
        String json =
                "{" +
                        "\"description\":\"gori\"," +
                        "\"name\": \"exit\"" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Regular")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void search_Regular_more_attributes_no_match() throws Exception  {
        String json =
                "{" +
                        "\"description\":\"gori\"," +
                        "\"name\": \"turneja\"" +
                        "}";
        mvc.perform( MockMvcRequestBuilders
                .post(URL_PREFIX + "/search")
                .param("mode", "Regular")
                .contentType(contentType)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    public String getAPIPath() {
        return "/api/event";
    }

    @Test
    public void eventEarningsReport() throws Exception {
        long eventId = 9940L;
        String startDate = "31.12.2015";
        String endDate = "04.01.2025";
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/report/earnings/" + eventId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(".unit").value("earnings(dollars)"))
                .andExpect(jsonPath(".title").value("Exit festival"))
                .andExpect(jsonPath("$.data[\"Prvi dan Exit festivala\"]").value(8000.))
                .andExpect(jsonPath("$.data[\"Drugi dan Exit festivala\"]").value(8000.))
                .andExpect(jsonPath("$.data[\"Treci dan Exit festivala\"]").value(10000.))
                .andExpect(jsonPath("$.data[\"Cetvrti dan Exit festivala\"]").value(8000.));
    }

    @Test
    public void eventOccupancyReport() throws Exception {
        long eventId = 9940L;
        String startDate = "31.12.2015";
        String endDate = "04.01.2025";
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/report/occupancy/" + eventId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(".unit").value("tickets bought"))
                .andExpect(jsonPath(".title").value("Exit festival"))
                .andExpect(jsonPath("$.data[\"Prvi dan Exit festivala\"]").value(4))
                .andExpect(jsonPath("$.data[\"Drugi dan Exit festivala\"]").value(4))
                .andExpect(jsonPath("$.data[\"Treci dan Exit festivala\"]").value(5))
                .andExpect(jsonPath("$.data[\"Cetvrti dan Exit festivala\"]").value(4));
    }

    private String constructEventJson(Event e) throws JsonProcessingException {
        if(e.getClass().equals(CompositeEvent.class)){
            StringBuilder json = new StringBuilder("{\n");
            if(e.getId() != null)
                json.append("\t\"id\": \"")
                        .append(e.getId().toString())
                        .append("\",\n");
            json.append("\t\"name\": \"")
                    .append(e.getName().trim())
                    .append("\",\n")
                    .append("\t\"description\": \"")
                    .append(e.getDescription().trim())
                    .append("\",\n")
                    .append("\t\"type\": \"")
                    .append(e.getType().toString().trim())
                    .append("\",\n")
                    .append("\t\"parentEvent\": ")
                    .append(constructParentEventJson(e.getParentEvent()))
                    .append(",\n")
                    .append("\t\"classType\": \"CompositeEvent\",\n")
                    .append("\t\"childEvents\": [");

            ArrayList<Event> children = new ArrayList<>(((CompositeEvent)e).getChildEvents());
            for(int i = 0; i < children.size(); i++){
                if ( i == 0 ){
                    json.append(constructEventJson(children.get(i)));
                }else{
                    json.append(",")
                            .append(constructEventJson(children.get(i)));
                }
            }

            return json.toString() + "]\n" +
                    "}";
        } else {
            if(e.getId() != null){
                return "{\n" +
                        "\t\"id\": \"" + e.getId().toString() + "\",\n" +
                        "\t\"name\": \"" + e.getName().trim() + "\",\n" +
                        "\t\"description\": \"" + e.getDescription().trim() + "\",\n" +
                        "\t\"type\": \"" + e.getType().toString().trim() + "\",\n" +
                        "\t\"parentEvent\": " + constructParentEventJson(e.getParentEvent()) + ",\n" +
                        "\t\"classType\": \"single-event\",\n" +
                        "\t\"childEvents\": null,\n" +
                        "\t\"spaceId\": " + ((SingleEvent) e).getSpace().getId().toString().trim() + ",\n" +
                        "\t\"spaceLayoutId\": " + ((SingleEvent) e).getSpaceLayout().getId().toString().trim() + ",\n" +
                        "\t\"spaceLayout\": " + constructSpaceLayoutJson(((SingleEvent) e).getSpaceLayout()) + ",\n" +
                        "\t\"startDate\": \"" + ((SingleEvent) e).getStartDate().getTime() + "\",\n" +
                        "\t\"endDate\": \"" + ((SingleEvent) e).getEndDate().getTime() + "\",\n" +
                        "\t\"state\": \"" + ((SingleEvent) e).getState().toString().trim() + "\"\n" +
                        "}";
            }
            return "{\n" +
                    "\t\"name\": \"" + e.getName().trim() + "\",\n" +
                    "\t\"description\": \"" + e.getDescription().trim() + "\",\n" +
                    "\t\"type\": \"" + e.getType().toString().trim() + "\",\n" +
                    "\t\"parentEvent\": " + constructParentEventJson(e.getParentEvent()) + ",\n" +
                    "\t\"classType\": \"single-event\",\n" +
                    "\t\"childEvents\": null,\n" +
                    "\t\"spaceId\": " + ((SingleEvent) e).getSpace().getId().toString().trim() + ",\n" +
                    "\t\"spaceLayoutId\": " + ((SingleEvent) e).getSpaceLayout().getId().toString().trim() + ",\n" +
                    "\t\"startDate\": \"" + ((SingleEvent) e).getStartDate().getTime() + "\",\n" +
                    "\t\"endDate\": \"" + ((SingleEvent) e).getEndDate().getTime() + "\",\n" +
                    "\t\"state\": \"" + ((SingleEvent) e).getState().toString().trim() + "\"\n" +
                    "}";
        }
    }

    private String constructSpaceLayoutJson(SpaceLayout s) throws JsonProcessingException {
        if (s == null){
            return "null";
        }
        ObjectMapper objectMapper = new ObjectMapper();
        for(GroundFloor gf : s.getGroundFloors()) {
            gf.setSpaceLayout(null);
        }
        for(SeatSpace ss : s.getSeatSpaces()) {
            ss.setSpaceLayout(null);
        }

        s.setSingleEvent(null);


        return objectMapper.writeValueAsString(s);
    }

    private String constructParentEventJson(Event e){
        if(e == null)
            return "null";
        if(e.getId() == null){
            return"{\n" +
                    "\t\"name\": \"" + e.getName().trim() + "\",\n" +
                    "\t\"description\": \"" + e.getDescription().trim() + "\",\n" +
                    "\t\"type\": \"" + e.getType().toString().trim() + "\",\n" +
                    "\t\"classType\": \"CompositeEvent\",\n" +
                    "\t\"childEvents\": null\n" +
                    "}";
        }
        return"{\n" +
                "\t\"id\": \"" + e.getId().toString() + "\",\n" +
                "\t\"name\": \"" + e.getName().trim() + "\",\n" +
                "\t\"description\": \"" + e.getDescription().trim() + "\",\n" +
                "\t\"type\": \"" + e.getType().toString().trim() + "\",\n" +
                "\t\"classType\": \"CompositeEvent\",\n" +
                "\t\"childEvents\": null\n" +
                "}";
    }

    private void setUpAttributes() throws ParseException, FetchException, AddException, IDMappingException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Space tvrdjava = new Space();
        Space more = new Space();
        Location noviSad = new Location();
        Location crnaGora = new Location();
        SingleEvent exitDay1 = new SingleEvent();
        SingleEvent exitDay2 = new SingleEvent();
        SingleEvent exitDay3 = new SingleEvent();
        SingleEvent exitDay4 = new SingleEvent();
        SpaceLayout exitDay1SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay2SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay3SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay4SpaceLayout = new SpaceLayout();
        SingleEvent seaDanceDay1 = new SingleEvent();
        SingleEvent seaDanceDay2 = new SingleEvent();
        SingleEvent seaDanceDay3 = new SingleEvent();
        SpaceLayout seaDanceDay1SpaceLayout = new SpaceLayout();
        SpaceLayout seaDanceDay2SpaceLayout = new SpaceLayout();
        SpaceLayout seaDanceDay3SpaceLayout = new SpaceLayout();
        CompositeEvent exit = new CompositeEvent();
        CompositeEvent seaDance = new CompositeEvent();
        CompositeEvent exitAndSeadance = new CompositeEvent();


        HashSet<Space> nsSpaces = new HashSet<>();
        nsSpaces.add(tvrdjava);
        HashSet<Space> cgSpaces = new HashSet<>();
        cgSpaces.add(more);
        noviSad.setSpaces(nsSpaces);
        noviSad.setName("Novi Sad");
        noviSad.setCity("Novi Sad");
        noviSad.setAddress("Srbija");
        noviSad.setCountry("Srbija");
        crnaGora.setSpaces(cgSpaces);
        crnaGora.setName("Crna Gora");
        crnaGora.setCity("Crna Gora");
        crnaGora.setAddress("Balkan");
        crnaGora.setCountry("SCG");

        exitDay1.setParentEvent(exit);
        exitDay1.setTickets(new HashSet<>());
        exitDay1.setSpaceLayout(exitDay1SpaceLayout);
        exitDay1.setSpace(tvrdjava);
        exitDay1.setType(EventType.Music);
        exitDay1.setName("Prvi dan Exit festivala");
        exitDay1.setDescription("Prvi dan Exit festivala u Novom Sadu");
        exitDay1.setState(EventState.ForSale);
        exitDay1.setStartDate(sdf.parse("2020-12-31 18:00"));
        exitDay1.setEndDate(sdf.parse("2021-01-01 18:00"));
        exitDay2.setParentEvent(exit);
        exitDay2.setTickets(new HashSet<>());
        exitDay2.setSpaceLayout(exitDay2SpaceLayout);
        exitDay2.setSpace(tvrdjava);
        exitDay2.setType(EventType.Music);
        exitDay2.setName("Drugi dan Exit festivala");
        exitDay2.setDescription("Drugi dan Exit festivala u Novom Sadu");
        exitDay2.setState(EventState.ForSale);
        exitDay2.setStartDate(sdf.parse("2021-01-01 18:00"));
        exitDay2.setEndDate(sdf.parse("2021-01-02 18:00"));
        exitDay3.setParentEvent(exit);
        exitDay3.setTickets(new HashSet<>());
        exitDay3.setSpaceLayout(exitDay3SpaceLayout);
        exitDay3.setSpace(tvrdjava);
        exitDay3.setType(EventType.Music);
        exitDay3.setName("Treci dan Exit festivala");
        exitDay3.setDescription("Treci dan Exit festivala u Novom Sadu");
        exitDay3.setState(EventState.ForSale);
        exitDay3.setStartDate(sdf.parse("2021-01-02 18:00"));
        exitDay3.setEndDate(sdf.parse("2021-01-03 18:00"));
        exitDay4.setParentEvent(exit);
        exitDay4.setTickets(new HashSet<>());
        exitDay4.setSpaceLayout(exitDay4SpaceLayout);
        exitDay4.setSpace(tvrdjava);
        exitDay4.setType(EventType.Music);
        exitDay4.setName("Cetvrti dan Exit festivala");
        exitDay4.setDescription("Cetvrti dan Exit festivala u Novom Sadu");
        exitDay4.setState(EventState.ForSale);
        exitDay4.setStartDate(sdf.parse("2021-01-03 18:00"));
        exitDay4.setEndDate(sdf.parse("2021-01-04 18:00"));
        seaDanceDay1.setParentEvent(seaDance);
        seaDanceDay1.setTickets(new HashSet<>());
        seaDanceDay1.setSpaceLayout(seaDanceDay1SpaceLayout);
        seaDanceDay1.setSpace(more);
        seaDanceDay1.setType(EventType.Music);
        seaDanceDay1.setName("Prvi dan Sea Dance festivala");
        seaDanceDay1.setDescription("Prvi dan Sea Dance festivala");
        seaDanceDay1.setState(EventState.ForSale);
        seaDanceDay1.setStartDate(sdf.parse("2021-01-05 18:00"));
        seaDanceDay1.setEndDate(sdf.parse("2021-01-06 18:00"));
        seaDanceDay2.setParentEvent(seaDance);
        seaDanceDay2.setTickets(new HashSet<>());
        seaDanceDay2.setSpaceLayout(seaDanceDay2SpaceLayout);
        seaDanceDay2.setSpace(more);
        seaDanceDay2.setType(EventType.Music);
        seaDanceDay2.setName("Drugi dan Sea Dance festivala");
        seaDanceDay2.setDescription("Drugi dan Sea Dance festivala");
        seaDanceDay2.setState(EventState.ForSale);
        seaDanceDay2.setStartDate(sdf.parse("2021-01-06 18:00"));
        seaDanceDay2.setEndDate(sdf.parse("2021-01-07 18:00"));
        seaDanceDay3.setParentEvent(seaDance);
        seaDanceDay3.setTickets(new HashSet<>());
        seaDanceDay3.setSpaceLayout(seaDanceDay3SpaceLayout);
        seaDanceDay3.setSpace(more);
        seaDanceDay3.setType(EventType.Music);
        seaDanceDay3.setName("Treci dan Sea Dance festivala");
        seaDanceDay3.setDescription("Treci dan Sea Dance festivala");
        seaDanceDay3.setState(EventState.ForSale);
        seaDanceDay3.setStartDate(sdf.parse("2021-01-07 18:00"));
        seaDanceDay3.setEndDate(sdf.parse("2021-01-08 18:00"));
        HashSet<Event> exitDays = new HashSet<>();
        exitDays.add(exitDay1);
        exitDays.add(exitDay2);
        exitDays.add(exitDay3);
        exitDays.add(exitDay4);
        HashSet<Event> seaDanceDays = new HashSet<>();
        seaDanceDays.add(seaDanceDay1);
        seaDanceDays.add(seaDanceDay2);
        seaDanceDays.add(seaDanceDay3);
        HashSet<Event> exitSeadanceSet = new HashSet<>();
        exitSeadanceSet.add(exit);
        exitSeadanceSet.add(seaDance);
        exit.setChildEvents(exitDays);
        exit.setType(EventType.Music);
        exit.setName("Exit festival");
        exit.setDescription("Exit festival u Novom Sadu");
        exit.setParentEvent(exitAndSeadance);
        seaDance.setChildEvents(seaDanceDays);
        seaDance.setType(EventType.Music);
        seaDance.setName("Sea dance festival");
        seaDance.setDescription("Sea dance festival nedje u Crnoj Gori");
        seaDance.setParentEvent(exitAndSeadance);
        exitAndSeadance.setChildEvents(exitSeadanceSet);
        exitAndSeadance.setType(EventType.Music);
        exitAndSeadance.setName("Exit and seadance festivals");
        exitAndSeadance.setDescription("Exit festival u Novom Sadu i Seadance festival negde u Crnoj Gori");
        exitAndSeadance.setParentEvent(null);
        exitDay1SpaceLayout.setSpace(tvrdjava);
        exitDay1SpaceLayout.setSingleEvent(exitDay1);
        HashSet<GroundFloor> exitDay1SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay1SpaceLayoutGroundFloor = new GroundFloor();
        exitDay1SpaceLayoutGroundFloor.setSpaceLayout(exitDay1SpaceLayout);
        exitDay1SpaceLayoutGroundFloor.setCapacity(16);
        exitDay1SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay1SpaceLayoutGroundFloor.setHeight(4);
        exitDay1SpaceLayoutGroundFloor.setWidth(4);
        exitDay1SpaceLayoutGroundFloor.setPrice(2000);
        exitDay1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay1SpaceLayoutGroundFloorSet.add(exitDay1SpaceLayoutGroundFloor);
        exitDay1SpaceLayout.setGroundFloors(exitDay1SpaceLayoutGroundFloorSet);
        exitDay1SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay2SpaceLayout.setSpace(tvrdjava);
        exitDay2SpaceLayout.setSingleEvent(exitDay2);
        HashSet<GroundFloor> exitDay2SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay2SpaceLayoutGroundFloor = new GroundFloor();
        exitDay2SpaceLayoutGroundFloor.setSpaceLayout(exitDay2SpaceLayout);
        exitDay2SpaceLayoutGroundFloor.setCapacity(16);
        exitDay2SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay2SpaceLayoutGroundFloor.setHeight(4);
        exitDay2SpaceLayoutGroundFloor.setWidth(4);
        exitDay2SpaceLayoutGroundFloor.setPrice(2000);
        exitDay2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay2SpaceLayoutGroundFloorSet.add(exitDay2SpaceLayoutGroundFloor);
        exitDay2SpaceLayout.setGroundFloors(exitDay2SpaceLayoutGroundFloorSet);
        exitDay2SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay3SpaceLayout.setSpace(tvrdjava);
        exitDay3SpaceLayout.setSingleEvent(exitDay3);
        HashSet<GroundFloor> exitDay3SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay3SpaceLayoutGroundFloor = new GroundFloor();
        exitDay3SpaceLayoutGroundFloor.setSpaceLayout(exitDay3SpaceLayout);
        exitDay3SpaceLayoutGroundFloor.setCapacity(16);
        exitDay3SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay3SpaceLayoutGroundFloor.setHeight(4);
        exitDay3SpaceLayoutGroundFloor.setWidth(4);
        exitDay3SpaceLayoutGroundFloor.setPrice(2000);
        exitDay3SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay3SpaceLayoutGroundFloorSet.add(exitDay3SpaceLayoutGroundFloor);
        exitDay3SpaceLayout.setGroundFloors(exitDay3SpaceLayoutGroundFloorSet);
        exitDay3SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay4SpaceLayout.setSpace(tvrdjava);
        exitDay4SpaceLayout.setSingleEvent(exitDay4);
        HashSet<GroundFloor> exitDay4SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay4SpaceLayoutGroundFloor = new GroundFloor();
        exitDay4SpaceLayoutGroundFloor.setSpaceLayout(exitDay4SpaceLayout);
        exitDay4SpaceLayoutGroundFloor.setCapacity(16);
        exitDay4SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay4SpaceLayoutGroundFloor.setHeight(4);
        exitDay4SpaceLayoutGroundFloor.setWidth(4);
        exitDay4SpaceLayoutGroundFloor.setPrice(2000);
        exitDay4SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay4SpaceLayoutGroundFloorSet.add(exitDay4SpaceLayoutGroundFloor);
        exitDay4SpaceLayout.setGroundFloors(exitDay4SpaceLayoutGroundFloorSet);
        exitDay4SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay1SpaceLayout.setSpace(tvrdjava);
        seaDanceDay1SpaceLayout.setSingleEvent(seaDanceDay1);
        HashSet<GroundFloor> seaDanceDay1SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay1SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay1SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay1SpaceLayout);
        seaDanceDay1SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay1SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay1SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay1SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay1SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay1SpaceLayoutGroundFloorSet.add(seaDanceDay1SpaceLayoutGroundFloor);
        seaDanceDay1SpaceLayout.setGroundFloors(seaDanceDay1SpaceLayoutGroundFloorSet);
        seaDanceDay1SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay2SpaceLayout.setSpace(tvrdjava);
        seaDanceDay2SpaceLayout.setSingleEvent(seaDanceDay2);
        HashSet<GroundFloor> seaDanceDay2SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay2SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay2SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay2SpaceLayout);
        seaDanceDay2SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay2SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay2SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay2SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay2SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay2SpaceLayoutGroundFloorSet.add(seaDanceDay2SpaceLayoutGroundFloor);
        seaDanceDay2SpaceLayout.setGroundFloors(seaDanceDay2SpaceLayoutGroundFloorSet);
        seaDanceDay2SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay3SpaceLayout.setSpace(tvrdjava);
        seaDanceDay3SpaceLayout.setSingleEvent(seaDanceDay3);
        HashSet<GroundFloor> seaDanceDay3SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay3SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay3SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay3SpaceLayout);
        seaDanceDay3SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay3SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay3SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay3SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay3SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay3SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay3SpaceLayoutGroundFloorSet.add(seaDanceDay3SpaceLayoutGroundFloor);
        seaDanceDay3SpaceLayout.setGroundFloors(seaDanceDay3SpaceLayoutGroundFloorSet);
        seaDanceDay3SpaceLayout.setSeatSpaces(new HashSet<>());

        Location beograd = new Location();
        Space beogradskaArena = new Space();
        HashSet<Space> bgSpaces = new HashSet<>();
        bgSpaces.add(beogradskaArena);

        beograd.setSpaces(bgSpaces);
        beograd.setName("Beograd");
        beograd.setCity("Beograd");
        beograd.setAddress("Srbija");
        beograd.setCountry("Srbija");

        SingleEvent koncertMayeBerovic = new SingleEvent();
        SpaceLayout koncertMayeBerovicSpaceLayout = new SpaceLayout();
        koncertMayeBerovic.setParentEvent(null);
        koncertMayeBerovic.setTickets(new HashSet<>());
        koncertMayeBerovic.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovic.setSpace(beogradskaArena);
        koncertMayeBerovic.setType(EventType.Music);
        koncertMayeBerovic.setName("Novi koncert mayo berovica");
        koncertMayeBerovic.setDescription("Gori nego ikad");
        koncertMayeBerovic.setState(EventState.ForSale);
        koncertMayeBerovic.setStartDate(sdf.parse("2020-11-21 18:00"));
        koncertMayeBerovic.setEndDate(sdf.parse("2020-11-22 18:00"));
        koncertMayeBerovicSpaceLayout.setSpace(beogradskaArena);
        koncertMayeBerovicSpaceLayout.setSingleEvent(koncertMayeBerovic);
        HashSet<GroundFloor> koncertMayeBerovicSpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertMayeBerovicSpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertMayeBerovicSpaceLayoutGroundFloor = new GroundFloor();
        koncertMayeBerovicSpaceLayoutGroundFloor.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutGroundFloor.setCapacity(16);
        koncertMayeBerovicSpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertMayeBerovicSpaceLayoutGroundFloor.setHeight(4);
        koncertMayeBerovicSpaceLayoutGroundFloor.setWidth(4);
        koncertMayeBerovicSpaceLayoutGroundFloor.setPrice(2000);
        koncertMayeBerovicSpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertMayeBerovicSpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertMayeBerovicSpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setRows(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertMayeBerovicSpaceLayoutGroundFloorSet.add(koncertMayeBerovicSpaceLayoutGroundFloor);
        koncertMayeBerovicSpaceLayoutSeatSpaceSet.add(koncertMayeBerovicSpaceLayoutSeatSpaceJeftin);
        koncertMayeBerovicSpaceLayoutSeatSpaceSet.add(koncertMayeBerovicSpaceLayoutSeatSpaceSkup);
        koncertMayeBerovicSpaceLayout.setGroundFloors(koncertMayeBerovicSpaceLayoutGroundFloorSet);
        koncertMayeBerovicSpaceLayout.setSeatSpaces(koncertMayeBerovicSpaceLayoutSeatSpaceSet);

        SingleEvent koncertZvonkaBogdana1 = new SingleEvent();
        SpaceLayout koncertZvonkaBogdana1SpaceLayout = new SpaceLayout();
        SingleEvent koncertZvonkaBogdana2 = new SingleEvent();
        SpaceLayout koncertZvonkaBogdana2SpaceLayout = new SpaceLayout();
        CompositeEvent turnejaZvonkaBogdana = new CompositeEvent();
        koncertZvonkaBogdana1.setParentEvent(turnejaZvonkaBogdana);
        koncertZvonkaBogdana1.setTickets(new HashSet<>());
        koncertZvonkaBogdana1.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1.setSpace(beogradskaArena);
        koncertZvonkaBogdana1.setType(EventType.Music);
        koncertZvonkaBogdana1.setName("Koncert Zvonka Bogdana jedan");
        koncertZvonkaBogdana1.setDescription("Koncert Zvonka Bogdana jedan");
        koncertZvonkaBogdana1.setState(EventState.ForSale);
        koncertZvonkaBogdana1.setStartDate(sdf.parse("2020-11-23 18:00"));
        koncertZvonkaBogdana1.setEndDate(sdf.parse("2020-11-24 18:00"));
        koncertZvonkaBogdana2.setParentEvent(turnejaZvonkaBogdana);
        koncertZvonkaBogdana2.setTickets(new HashSet<>());
        koncertZvonkaBogdana2.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2.setSpace(beogradskaArena);
        koncertZvonkaBogdana2.setType(EventType.Music);
        koncertZvonkaBogdana2.setName("Koncert Zvonka Bogdana dva");
        koncertZvonkaBogdana2.setDescription("Koncert Zvonka Bogdana dva");
        koncertZvonkaBogdana2.setState(EventState.ForSale);
        koncertZvonkaBogdana2.setStartDate(sdf.parse("2020-12-03 18:00"));
        koncertZvonkaBogdana2.setEndDate(sdf.parse("2020-12-04 18:00"));
        HashSet<Event> koncertiZvonkaBogdana = new HashSet<>();
        koncertiZvonkaBogdana.add(koncertZvonkaBogdana1);
        koncertiZvonkaBogdana.add(koncertZvonkaBogdana2);
        turnejaZvonkaBogdana.setChildEvents(koncertiZvonkaBogdana);
        turnejaZvonkaBogdana.setType(EventType.Music);
        turnejaZvonkaBogdana.setName("Turneja Bonke Zvogdana");
        turnejaZvonkaBogdana.setDescription("Isti kao i uvek - jedan jedini - Bonko Zvogdan");
        turnejaZvonkaBogdana.setParentEvent(null);
        koncertZvonkaBogdana1SpaceLayout.setSpace(beogradskaArena);
        koncertZvonkaBogdana1SpaceLayout.setSingleEvent(koncertZvonkaBogdana1);
        HashSet<GroundFloor> koncertZvonkaBogdana1SpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertZvonkaBogdana1SpaceLayoutGroundFloor = new GroundFloor();
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setCapacity(16);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setHeight(4);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setWidth(4);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setPrice(2000);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setRows(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertZvonkaBogdana1SpaceLayoutGroundFloorSet.add(koncertZvonkaBogdana1SpaceLayoutGroundFloor);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup);
        koncertZvonkaBogdana1SpaceLayout.setGroundFloors(koncertZvonkaBogdana1SpaceLayoutGroundFloorSet);
        koncertZvonkaBogdana1SpaceLayout.setSeatSpaces(koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet);
        koncertZvonkaBogdana2SpaceLayout.setSpace(beogradskaArena);
        koncertZvonkaBogdana2SpaceLayout.setSingleEvent(koncertZvonkaBogdana2);
        HashSet<GroundFloor> koncertZvonkaBogdana2SpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertZvonkaBogdana2SpaceLayoutGroundFloor = new GroundFloor();
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setCapacity(16);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setHeight(4);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setWidth(4);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setPrice(2000);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setRows(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertZvonkaBogdana2SpaceLayoutGroundFloorSet.add(koncertZvonkaBogdana2SpaceLayoutGroundFloor);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup);
        koncertZvonkaBogdana2SpaceLayout.setGroundFloors(koncertZvonkaBogdana2SpaceLayoutGroundFloorSet);
        koncertZvonkaBogdana2SpaceLayout.setSeatSpaces(koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet);



        SingleEvent novi_koncertZvonkaBogdana1 = new SingleEvent();
        SingleEvent novi_koncertZvonkaBogdana2 = new SingleEvent();
        CompositeEvent nova_turnejaZvonkaBogdana = new CompositeEvent();
        novi_koncertZvonkaBogdana1.setParentEvent(nova_turnejaZvonkaBogdana);
        novi_koncertZvonkaBogdana1.setTickets(new HashSet<>());
        novi_koncertZvonkaBogdana1.setSpaceLayout(null);
        novi_koncertZvonkaBogdana1.setSpace(null);
        novi_koncertZvonkaBogdana1.setType(EventType.Music);
        novi_koncertZvonkaBogdana1.setName("Koncert Zvonka Bogdana u njegovom rodnom gradu");
        novi_koncertZvonkaBogdana1.setDescription("Koncert Zvonka Bogdana u Somboru");
        novi_koncertZvonkaBogdana1.setState(EventState.ForSale);
        novi_koncertZvonkaBogdana1.setStartDate(sdf.parse("2022-11-23 18:00"));
        novi_koncertZvonkaBogdana1.setEndDate(sdf.parse("2022-11-24 18:00"));
        novi_koncertZvonkaBogdana2.setParentEvent(nova_turnejaZvonkaBogdana);
        novi_koncertZvonkaBogdana2.setTickets(new HashSet<>());
        novi_koncertZvonkaBogdana2.setSpaceLayout(null);
        novi_koncertZvonkaBogdana2.setSpace(null);
        novi_koncertZvonkaBogdana2.setType(EventType.Music);
        novi_koncertZvonkaBogdana2.setName("Koncert Zvonka Bogdana u njegovom rodnom gradu");
        novi_koncertZvonkaBogdana2.setDescription("Koncert Zvonka Bogdana u Somboru");
        novi_koncertZvonkaBogdana2.setState(EventState.ForSale);
        novi_koncertZvonkaBogdana2.setStartDate(sdf.parse("2022-12-03 18:00"));
        novi_koncertZvonkaBogdana2.setEndDate(sdf.parse("2022-12-04 18:00"));
        HashSet<Event> novi_koncertiZvonkaBogdana = new HashSet<>();
        novi_koncertiZvonkaBogdana.add(novi_koncertZvonkaBogdana1);
        novi_koncertiZvonkaBogdana.add(novi_koncertZvonkaBogdana2);
        nova_turnejaZvonkaBogdana.setChildEvents(novi_koncertiZvonkaBogdana);
        nova_turnejaZvonkaBogdana.setType(EventType.Music);
        nova_turnejaZvonkaBogdana.setName("Turneja Bonke Zvogdana u Somboru");
        nova_turnejaZvonkaBogdana.setDescription("Isti kao i uvek - jedan jedini - Bonko Zvogdanovic");
        nova_turnejaZvonkaBogdana.setParentEvent(null);



        SingleEvent koncertLaneDelRey = new SingleEvent();
        koncertLaneDelRey.setParentEvent(null);
        koncertLaneDelRey.setTickets(new HashSet<>());
        koncertLaneDelRey.setSpaceLayout(null);
        koncertLaneDelRey.setSpace(null);
        koncertLaneDelRey.setType(EventType.Music);
        koncertLaneDelRey.setName("Novi koncert Lane del Rey");
        koncertLaneDelRey.setDescription("Not great, not terrible");
        koncertLaneDelRey.setState(EventState.ForSale);
        koncertLaneDelRey.setStartDate(sdf.parse("2020-11-26 18:00"));
        koncertLaneDelRey.setEndDate(sdf.parse("2020-11-27 18:00"));


        SingleEvent exitDay5 = new SingleEvent();
        exitDay5.setParentEvent(exit);
        exitDay5.setTickets(new HashSet<>());
        exitDay5.setSpaceLayout(null);
        exitDay5.setSpace(null);
        exitDay5.setType(EventType.Music);
        exitDay5.setName("Peti dan festivala exit");
        exitDay5.setDescription("Zbog velike potraznje otvoren je i peti dan exit festivala");
        exitDay5.setState(EventState.ForSale);
        exitDay5.setStartDate(sdf.parse("2020-01-05 18:00"));
        exitDay5.setEndDate(sdf.parse("2020-01-06 18:00"));


        beograd.setId(9940L);
        noviSad.setId(9941L);
        crnaGora.setId(9942L);

        beogradskaArena.setId(991L);
        tvrdjava.setId(992L);
        more.setId(993L);


        koncertMayeBerovicSpaceLayout.setId(995L);
        exitDay1SpaceLayout.setId(9910L);
        exitDay2SpaceLayout.setId(9913L);
        exitDay3SpaceLayout.setId(9916L);
        exitDay4SpaceLayout.setId(9919L);
        seaDanceDay1SpaceLayout.setId(9922L);
        seaDanceDay2SpaceLayout.setId(9925L);
        seaDanceDay3SpaceLayout.setId(9928L);
        koncertZvonkaBogdana1SpaceLayout.setId(9931L);
        koncertZvonkaBogdana2SpaceLayout.setId(9936L);


        koncertMayeBerovic.setId(994L);
        exitDay1.setId(999L);
        exitDay2.setId(9912L);
        exitDay3.setId(9915L);
        exitDay4.setId(9918L);
        seaDanceDay1.setId(9921L);
        seaDanceDay2.setId(9924L);
        seaDanceDay3.setId(9927L);
        koncertZvonkaBogdana1.setId(9930L);
        koncertZvonkaBogdana2.setId(9935L);
        exit.setId(9940L);
        seaDance.setId(9941L);
        exitAndSeadance.setId(9942L);
        turnejaZvonkaBogdana.setId(9943L);


        koncertMayeBerovicSpaceLayoutGroundFloor.setId(996L);
        exitDay1SpaceLayoutGroundFloor.setId(9911L);
        exitDay2SpaceLayoutGroundFloor.setId(9914L);
        exitDay3SpaceLayoutGroundFloor.setId(9917L);
        exitDay4SpaceLayoutGroundFloor.setId(9920L);
        seaDanceDay1SpaceLayoutGroundFloor.setId(9923L);
        seaDanceDay2SpaceLayoutGroundFloor.setId(9926L);
        seaDanceDay3SpaceLayoutGroundFloor.setId(9929L);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setId(9932L);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setId(9937L);


        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setId(998L);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setId(997L);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setId(9934L);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setId(9933L);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setId(9938L);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setId(9939L);

        this.exitDay1 = exitDay1;
        this.exitDay2 = exitDay2;
        this.exitDay3 = exitDay3;
        this.exitDay4 = exitDay4;
        this.seaDanceDay1 = seaDanceDay1;
        this.seaDanceDay2 = seaDanceDay2;
        this.seaDanceDay3 = seaDanceDay3;
        this.exit = exit;
        this.seaDance = seaDance;
        this.exitAndSeaDance = exitAndSeadance;
        this.koncertZvonkaBogdana1 = koncertZvonkaBogdana1;
        this.koncertZvonkaBogdana2 = koncertZvonkaBogdana2;
        this.turnejaZvonkaBogdana = turnejaZvonkaBogdana;
        this.koncertMayeBerovic = koncertMayeBerovic;

        this.tvrdjava = tvrdjava;
        this.exitDay4SpaceLayout = exitDay4SpaceLayout;

        novi_koncertZvonkaBogdana1.setId(99200L);
        novi_koncertZvonkaBogdana2.setId(99201L);
        nova_turnejaZvonkaBogdana.setId(99202L);
        koncertLaneDelRey.setId(99203L);
        exitDay5.setId(99204L);
        this.add_josJedanKoncertZvonkaBogdana1 = novi_koncertZvonkaBogdana1;
        this.add_josJedanKoncertZvonkaBogdana2 = novi_koncertZvonkaBogdana2;
        this.add_josJednaTurnejaZvonkaBogdana = nova_turnejaZvonkaBogdana;
        this.add_koncertLaneDelRey = koncertLaneDelRey;
        this.add_exitDay5 = exitDay5;


        User user = new User();
        user.setUsername("username");
        user.setSurname("Surname of password");
        user.setName("Name of user");
        user.setId(9944L);
        user.setPassword("password");
        user.setRole(UserRole.Regular);


        ArrayList<SingleEvent> singleEvents = new ArrayList<>();
        singleEvents.add(exitDay1);
        singleEvents.add(exitDay2);
        singleEvents.add(exitDay3);
        singleEvents.add(exitDay4);
        singleEvents.add(seaDanceDay1);
        singleEvents.add(seaDanceDay2);
        singleEvents.add(seaDanceDay3);
        singleEvents.add(koncertMayeBerovic);
        singleEvents.add(koncertZvonkaBogdana1);


//        ArrayList<Ticket> tickets = new ArrayList<>();
//
//        for( SingleEvent e : singleEvents ) {
//            for ( SeatSpace ss : e.getSpaceLayout().getSeatSpaces() ) {
//                for( int i = 0; i < ss.getRows(); i++ ){
//                    for (int j = 0; j < ss.getSeats(); j++ ) {
//                        if ( Math.random() < 0.5 ) {
//                            Ticket t1 = new Ticket();
//                            t1.setId(100000L + tickets.size());
//                            t1.setUser(user);
//                            t1.setSeat(i);
//                            t1.setRow(j);
//                            t1.setSingleEvent(e);
//                            t1.setSeatSpace(ss);
//                            t1.setPrice(ss.getPrice());
//                            if ( Math.random() < 0.5 ) {
//                                t1.setPaid(false);
//                            } else {
//                                t1.setPaid(true);
//                            }
//
//                            Calendar calendar = Calendar.getInstance();
//                            calendar.setTime(t1.getSingleEvent().getStartDate());
//                            calendar.add(Calendar.DAY_OF_MONTH, - 1);
//                            t1.setExpirationDate(calendar.getTime());
//                            tickets.add(t1);
//                        } else {
//                            Ticket t1 = new Ticket();
//                            t1.setId(100000L + tickets.size());
//                            t1.setUser(null);
//                            t1.setSeat(i);
//                            t1.setRow(j);
//                            t1.setSingleEvent(e);
//                            t1.setSeatSpace(ss);
//                            t1.setPrice(ss.getPrice());
//                            if ( Math.random() < 0.5 ) {
//                                t1.setPaid(false);
//                            } else {
//                                t1.setPaid(true);
//                            }
//
//                            Calendar calendar = Calendar.getInstance();
//                            calendar.setTime(t1.getSingleEvent().getStartDate());
//                            calendar.add(Calendar.DAY_OF_MONTH, - 1);
//                            t1.setExpirationDate(calendar.getTime());
//                            tickets.add(t1);
//                        }
//                    }
//                }
//            }
//
//            for (GroundFloor gf : e.getSpaceLayout().getGroundFloors() ) {
//                int counter = 0;
//                for( int i = 0; i < gf.getCapacity(); i++ ){
//                        if ( Math.random() < 0.5 ) {
//                            counter++;
//                            Ticket t1 = new Ticket();
//                            t1.setId(100000L + tickets.size());
//                            t1.setUser(user);
//                            t1.setSingleEvent(e);
//                            t1.setGroundFloor(gf);
//                            t1.setPrice(gf.getPrice());
//                            if ( Math.random() < 0.5 ) {
//                                t1.setPaid(false);
//                            } else {
//                                t1.setPaid(true);
//                            }
//
//                            Calendar calendar = Calendar.getInstance();
//                            calendar.setTime(t1.getSingleEvent().getStartDate());
//                            calendar.add(Calendar.DAY_OF_MONTH, - 1);
//                            t1.setExpirationDate(calendar.getTime());
//                            tickets.add(t1);
//                        }
//                }
//                gf.setSpotsReserved(counter);
//                System.out.println(gf.getId() + ": " + counter);
//            }
//        }
//
//        SingleEvent e = koncertZvonkaBogdana2;
//
//        for ( SeatSpace ss : e.getSpaceLayout().getSeatSpaces() ) {
//            for( int i = 0; i < ss.getRows(); i++ ){
//                for (int j = 0; j < ss.getSeats(); j++ ) {
//                        Ticket t1 = new Ticket();
//                        t1.setId(100000L + tickets.size());
//                        t1.setUser(user);
//                        t1.setSeat(i);
//                        t1.setRow(j);
//                        t1.setSingleEvent(e);
//                        t1.setSeatSpace(ss);
//                        t1.setPrice(ss.getPrice());
//                        if ( Math.random() < 0.5 ) {
//                            t1.setPaid(false);
//                        } else {
//                            t1.setPaid(true);
//                        }
//                        Calendar calendar = Calendar.getInstance();
//                        calendar.setTime(t1.getSingleEvent().getStartDate());
//                        calendar.add(Calendar.DAY_OF_MONTH, - 1);
//                        t1.setExpirationDate(calendar.getTime());
//                        tickets.add(t1);
//                }
//            }
//        }
//
//        for (GroundFloor gf : e.getSpaceLayout().getGroundFloors() ) {
//            int counter = 0;
//            for( int i = 0; i < gf.getCapacity(); i++ ){
//                    counter++;
//                    Ticket t1 = new Ticket();
//                    t1.setId(100000L + tickets.size());
//                    t1.setUser(user);
//                    t1.setSingleEvent(e);
//                    t1.setGroundFloor(gf);
//                    t1.setPrice(gf.getPrice());
//                    if ( Math.random() < 0.5 ) {
//                        t1.setPaid(false);
//                    } else {
//                        t1.setPaid(true);
//                    }
//                    Calendar calendar = Calendar.getInstance();
//                    calendar.setTime(t1.getSingleEvent().getStartDate());
//                    calendar.add(Calendar.DAY_OF_MONTH, - 1);
//                    t1.setExpirationDate(calendar.getTime());
//                    tickets.add(t1);
//            }
//            gf.setSpotsReserved(counter);
//            System.out.println(gf.getId() + ": " + counter);
//        }
//
//
//
//        for (Ticket t : tickets) {
//            this.ticketRepository.save(t);
//        }
    }

}
