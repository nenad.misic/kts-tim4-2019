package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.GroundFloorDto;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.GroundFloorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class GroundFloorControllerTest extends CRUDControllerGenericTest<GroundFloorController, GroundFloor, GroundFloorDto, GroundFloorRepository> {

    @Test
    @Override
    public void deleteById() throws Exception {
        GroundFloor groundFloor = createSampleGroundFloorNotTiedToAnEvent();
        GroundFloor groundFloorP = repository.save(groundFloor);
        Long id = getId(groundFloorP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));
        List<GroundFloor> allEntities = repository.findAll();
        for (GroundFloor e: allEntities) {
            assertFalse(getId(e).equals(id));
        }
    }

    @Test
    public void deleteFailTiedToAnEvent() throws Exception {
        GroundFloor groundFloor = createSampleGroundFloorTiedToAnEvent();
        GroundFloor groundFloorP = repository.save(groundFloor);
        Long id = getId(groundFloorP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Cannot delete GroundFloor with id " + id +
                        " GroundFloor is tied to an event"));
        repository.deleteById(id);
    }

    @Override
    protected GroundFloor createSampleEntity() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(99304L);
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setSpotsReserved(0);
        groundFloor.setTickets(new HashSet<Ticket>());
        groundFloor.setWidth(100);
        groundFloor.setHeight(50);
        groundFloor.setCapacity(1000);
        groundFloor.setSpaceLayout(spaceLayout);
        groundFloor.setPrice(1000);
        groundFloor.setDeleted(false);
        return groundFloor;
    }

    protected GroundFloor createSampleGroundFloorNotTiedToAnEvent() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(99304L);
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setSpotsReserved(0);
        groundFloor.setTickets(new HashSet<Ticket>());
        groundFloor.setWidth(100);
        groundFloor.setHeight(50);
        groundFloor.setCapacity(1000);
        groundFloor.setSpaceLayout(spaceLayout);
        groundFloor.setPrice(1000);
        groundFloor.setDeleted(false);
        return groundFloor;
    }

    protected GroundFloor createSampleGroundFloorTiedToAnEvent() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(995L);
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setSpotsReserved(0);
        groundFloor.setTickets(new HashSet<Ticket>());
        groundFloor.setWidth(100);
        groundFloor.setHeight(50);
        groundFloor.setCapacity(1000);
        groundFloor.setSpaceLayout(spaceLayout);
        groundFloor.setPrice(1000);
        groundFloor.setDeleted(false);
        return groundFloor;
    }

    @Override
    protected GroundFloorDto createSampleDto() {
        GroundFloorDto groundFloorDto = new GroundFloorDto();
        groundFloorDto.setTickets(new HashSet<>());
        groundFloorDto.setSpotsReserved(0);
        groundFloorDto.setCapacity(1000);
        groundFloorDto.setHeight(50);
        groundFloorDto.setWidth(100);
        groundFloorDto.setPrice(1000);
        groundFloorDto.setSpaceLayoutId(99304L);
        return groundFloorDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/groundFloor";
    }

    @Override
    protected Class getDTOType() {
        return GroundFloorDto.class;
    }

    @Override
    protected Long getId(GroundFloorDto groundFloorDto) {
        return groundFloorDto.getId();
    }

    @Override
    protected Long getId(GroundFloor entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(GroundFloorDto groundFloorDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(groundFloorDto);
    }

    @Override
    protected void setId(GroundFloorDto groundFloorDto, Long id) {
        groundFloorDto.setId(id);
    }
}