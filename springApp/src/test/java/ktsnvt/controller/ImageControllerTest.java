package ktsnvt.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.text.ParseException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class ImageControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mvc;

    @PostConstruct
    public void setup() throws ParseException {
        this.mvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void singleFileUpload() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Spring Framework".getBytes());
        this.mvc.perform(multipart(getAPIPath() + "/upload/?eventId=111111").file(multipartFile))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllImagesForEvent() throws Exception {
        Long eventId = 12L;
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/all/" + eventId)
            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("12/1.png,12/2.png,12/3.png,12/4.png,12/5.png"));
    }

    @Test
    public void getFirstImageForEvent() throws Exception {
        Long eventId = 12L;
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/" + eventId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("12/1.png"));
    }

    public String getAPIPath() {
        return "/api/image";
    }
}
