package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.LocationDto;
import ktsnvt.dto.SpaceDto;
import ktsnvt.entity.Location;
import ktsnvt.entity.Space;
import ktsnvt.repository.LocationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
@Transactional
public class LocationControllerTest extends CRUDControllerGenericTest<LocationController, Location, LocationDto, LocationRepository> {

    @Test
    @Override
    public void deleteById() throws Exception {
        Location location = createSampleLocationWithNoSpaces();
        Location locationP = repository.save(location);
        Long id = getId(locationP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));

        List<Location> allEntities = repository.findAll();
        for (Location e: allEntities) {
            assertFalse(getId(e).equals(id));
        }
    }

    @Test
    public void deleteByIdFailHasSpaces() throws Exception {
        Location location = createSampleLocationWithSpaces();
        Location locationP = repository.save(location);
        Long id = getId(locationP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Cannot delete Location with id " + id +
                        ": Location has relationships to one or more Spaces"));
        repository.deleteById(id);
    }

    @Test
    public void findByNamePage() throws Exception {
        List<Location> allLocations = repository.findAll();
        String searchParam = "o";
        List<Location> locationsContainingStr = allLocations.stream().filter((location) -> {
            return location.getName().toLowerCase().contains(searchParam.toLowerCase());
        }).collect(Collectors.toList());
        int length = locationsContainingStr.size();
        //get first page which contains all but 1 found element
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/findByNamePage?name=o&page=0&size=" + (length - 1))
            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(length - 1)));

        //get next page, and expect it to contain only 1 element
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/findByNamePage?name=o&page=1&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

        //get the third page and expect it to be empty
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/findByNamePage?name=o&page=2&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void locationEarningsDuringTimePeriod() throws Exception {
        long locationId = 9941L;
        String startDate = "31.12.2020";
        String endDate = "04.01.2021";
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/report/earnings/" + locationId +
                    "?startDate="+startDate+"&endDate="+endDate)
            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(".unit").value("earnings(dollars)"))
                .andExpect(jsonPath(".title").value("Petrovaradinska Tvrdjava earnings for time period: 31.12.2020-04.01.2021"))
                .andExpect(jsonPath("$.data[\"December/2020\"]").value(8000.))
                .andExpect(jsonPath("$.data[\"January/2021\"]").value(18000.));
    }

    @Test
    public void locationOccupancyDuringTimePeriod() throws Exception {
        long locationId = 9941L;
        String startDate = "31.12.2020";
        String endDate = "04.01.2021";
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/report/occupancy/" + locationId +
                        "?startDate="+startDate+"&endDate="+endDate)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(".unit").value("tickets bought"))
                .andExpect(jsonPath(".title").value("Petrovaradinska Tvrdjava visitors for time period: 31.12.2020-04.01.2021"))
                .andExpect(jsonPath("$.data[\"December/2020\"]").value(4))
                .andExpect(jsonPath("$.data[\"January/2021\"]").value(9));
    }

    @Override
    protected Location createSampleEntity() {
        Location location = new Location();
        location.setSpaces(new HashSet<Space>());
        location.setAddress("Sample address 1");
        location.setCity("Sample city 1");
        location.setCountry("Sample country 1");
        location.setName("Sample name 1");
        location.setDeleted(false);
        return location;
    }

    protected Location createSampleLocationWithNoSpaces() {
        Location location = new Location();
        location.setSpaces(new HashSet<Space>());
        location.setAddress("Sample address 1");
        location.setCity("Sample city 1");
        location.setCountry("Sample country 1");
        location.setName("Sample name 1");
        location.setDeleted(false);
        return location;
    }

    protected Location createSampleLocationWithSpaces() {
        Location location = new Location();
        location.setSpaces(new HashSet<Space>());
        Space space = new Space();
        location.getSpaces().add(space);
        location.setAddress("Sample address 1");
        location.setCity("Sample city 1");
        location.setCountry("Sample country 1");
        location.setName("Sample name 1");
        location.setDeleted(false);
        return location;
    }

    @Override
    protected LocationDto createSampleDto() {
        LocationDto locationDto = new LocationDto();
        locationDto.setSpaces(new HashSet<SpaceDto>());
        locationDto.setAddress("Sample address 1");
        locationDto.setCity("Sample city 1");
        locationDto.setCountry("Sample country 1");
        locationDto.setName("Sample name 1");
        return locationDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/location";
    }

    @Override
    protected Class getDTOType() {
        return LocationDto.class;
    }

    @Override
    protected Long getId(LocationDto locationDto) {
        return locationDto.getId();
    }

    @Override
    protected Long getId(Location entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(LocationDto locationDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(locationDto);
    }

    @Override
    protected void setId(LocationDto locationDto, Long id) {
        locationDto.setId(id);
    }
}
