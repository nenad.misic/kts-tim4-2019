package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.SeatSpaceDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.Location;
import ktsnvt.entity.SeatSpace;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.SeatSpaceRepository;
import ktsnvt.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SeatSpaceControllerTest extends CRUDControllerGenericTest<SeatSpaceController, SeatSpace, SeatSpaceDto, SeatSpaceRepository>{

    @Autowired
    TicketRepository ticketRepository;

    @Test
    @Override
    public void deleteById() throws Exception {
        SeatSpace seatSpace = createSampleSeatSpaceNotTiedToAnEvent();
        SeatSpace seatSpaceP = repository.save(seatSpace);
        Long id = getId(seatSpaceP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));
        List<SeatSpace> allEntities = repository.findAll();
        for (SeatSpace e: allEntities) {
            assertFalse(getId(e).equals(id));
        }
    }

    @Test
    public void deleteByIdFailTiedToAnEvent() throws Exception {
        SeatSpace seatSpace = createSampleSeatSpaceTiedToAnEvent();
        SeatSpace seatSpaceP = repository.save(seatSpace);
        Long id = getId(seatSpaceP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Cannot delete SeatSpace with id " + id +
                        " SeatSpace is tied to an event"));
        repository.deleteById(id);
    }

    @Test
    public void getSeatsTakenSuccess() throws Exception {
        long id = 9933L;
        SeatSpace seatSpace = repository.findById(id).get();
        List<Ticket> seatSpaceTickets = ticketRepository.findBySeatSpaceIdAndUserIsNotNull(9933L);
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/" + id + "/seatsTaken"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(seatSpaceTickets.size())));
    }

    @Test
    public void getSeatsTakenNonExistingId() throws Exception {
        long nonExistingId = -1L;
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/" + nonExistingId + "/seatsTaken"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void getSeatsTaken() throws Exception {
        List<SeatSpace> seatSpaces = repository.findAll();
        for (SeatSpace seatSpace: seatSpaces) {
            List<Ticket> tickets = ticketRepository.findAll();
            List<Ticket> seatSpaceTickets = tickets.stream().filter((ticket) -> {
                return ticket.getSeatSpace() != null &&
                        ticket.getSeatSpace().getId().equals(seatSpace.getId());
            }).collect(Collectors.toList());
            int seatsTaken = 0;
            for (Ticket ticket: seatSpaceTickets) {
                if (ticket.getUser() != null) {
                    seatsTaken++;
                }
            }
            mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/" + seatSpace.getId() + "/seatsTaken")
                .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$", hasSize(seatsTaken)));
        }
    }

    @Test
    public void getSeatsTakenNotFound() throws Exception {
        Long nonExistingId = -1L;
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/" + nonExistingId + "/seatsTaken")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Override
    protected SeatSpace createSampleEntity() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(995L);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setSeats(28);
        seatSpace.setRows(18);
        seatSpace.setPrice(400);
        seatSpace.setSpaceLayout(spaceLayout);
        seatSpace.setTickets(new HashSet<Ticket>());
        seatSpace.setDeleted(false);
        return seatSpace;
    }

    protected SeatSpace createSampleSeatSpaceNotTiedToAnEvent() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(99304L);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setSeats(28);
        seatSpace.setRows(18);
        seatSpace.setPrice(400);
        seatSpace.setSpaceLayout(spaceLayout);
        seatSpace.setTickets(new HashSet<Ticket>());
        seatSpace.setDeleted(false);
        return seatSpace;
    }

    protected SeatSpace createSampleSeatSpaceTiedToAnEvent() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setId(995L);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setSeats(28);
        seatSpace.setRows(18);
        seatSpace.setPrice(400);
        seatSpace.setSpaceLayout(spaceLayout);
        seatSpace.setTickets(new HashSet<Ticket>());
        seatSpace.setDeleted(false);
        return seatSpace;
    }

    @Override
    protected SeatSpaceDto createSampleDto() {
        SeatSpaceDto seatSpaceDto = new SeatSpaceDto();
        seatSpaceDto.setTickets(new HashSet<TicketDto>());
        seatSpaceDto.setPrice(400);
        seatSpaceDto.setRows(18);
        seatSpaceDto.setSeats(28);
        seatSpaceDto.setSpaceLayoutId(995L);
        return seatSpaceDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/seatSpace";
    }

    @Override
    protected Class getDTOType() {
        return SeatSpaceDto.class;
    }

    @Override
    protected Long getId(SeatSpaceDto seatSpaceDto) {
        return seatSpaceDto.getId();
    }

    @Override
    protected Long getId(SeatSpace entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(SeatSpaceDto seatSpaceDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(seatSpaceDto);
    }

    @Override
    protected void setId(SeatSpaceDto seatSpaceDto, Long id) {
        seatSpaceDto.setId(id);
    }
}