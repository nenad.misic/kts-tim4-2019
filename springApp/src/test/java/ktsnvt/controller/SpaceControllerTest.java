package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.LocationDto;
import ktsnvt.dto.SingleEventDto;
import ktsnvt.dto.SpaceDto;
import ktsnvt.dto.SpaceLayoutDto;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SpaceControllerTest extends CRUDControllerGenericTest<SpaceController, Space, SpaceDto, SpaceRepository> {

    @Test
    @Override
    public void deleteById() throws Exception {
        Space space = createSampleSpaceWithNoEvents();
        Space spaceP = repository.save(space);
        Long id = getId(spaceP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));

        List<Space> allEntities = repository.findAll();
        for (Space e: allEntities) {
            assertFalse(getId(e).equals(id));
        }
    }

    @Test
    public void deleteByIdFailHasEvents() throws Exception {
        Space spaceP = repository.findById(992L).get();
        Long id = getId(spaceP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Cannot delete Space with id " + id +
                        ": Space has relationships to one or more events"));
    }

    @Override
    protected Space createSampleEntity() {
        Space space = new Space();
        Location location = new Location();
        location.setId(99L);
        space.setDefaultSpaceLayouts(new HashSet<SpaceLayout>());
        space.setLocation(location);
        space.setName("Sample city 1");
        space.setEvents(new HashSet<SingleEvent>());
        space.setDeleted(false);
        return space;
    }

    protected Space createSampleSpaceWithNoEvents() {
        Space space = new Space();
        Location location = new Location();
        location.setId(99L);
        space.setDefaultSpaceLayouts(new HashSet<SpaceLayout>());
        space.setLocation(location);
        space.setName("Sample city 1");
        space.setEvents(new HashSet<SingleEvent>());
        space.setDeleted(false);
        return space;
    }

    protected Space createSampleSpaceWithEvents() {
        Space space = new Space();
        Location location = new Location();
        location.setId(99L);
        space.setDefaultSpaceLayouts(new HashSet<SpaceLayout>());
        space.setLocation(location);
        space.setName("Sample city 1");
        space.setEvents(new HashSet<SingleEvent>());
        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setSpace(space);
        space.getEvents().add(singleEvent);
        space.setDeleted(false);
        return space;
    }

    @Override
    protected SpaceDto createSampleDto() {
        SpaceDto spaceDto = new SpaceDto();
        LocationDto locationDto = new LocationDto();
        locationDto.setId(99L);
        spaceDto.setEvents(new HashSet<SingleEventDto>());
        spaceDto.setDefaultSpaceLayouts(new HashSet<SpaceLayoutDto>());
        spaceDto.setLocationId(99L);
        spaceDto.setName("Sample city 1");
        spaceDto.setLocation(locationDto);
        return spaceDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/space";
    }

    @Override
    protected Class getDTOType() {
        return SpaceDto.class;
    }

    @Override
    protected Long getId(SpaceDto spaceDto) {
        return spaceDto.getId();
    }

    @Override
    protected Long getId(Space entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(SpaceDto spaceDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(spaceDto);
    }

    @Override
    protected void setId(SpaceDto spaceDto, Long id) {
        spaceDto.setId(id);
    }
}