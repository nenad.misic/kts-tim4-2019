package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.*;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceLayoutRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SpaceLayoutControllerTest extends CRUDControllerGenericTest<SpaceLayoutController, SpaceLayout, SpaceLayoutDto, SpaceLayoutRepository> {

    @Test
    @Override
    public void deleteById() throws Exception {
        SpaceLayout spaceLayout = createSampleEntityWithNoEvents();
        SpaceLayout spaceLayoutP = repository.save(spaceLayout);
        Long id = getId(spaceLayoutP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Deleted"));
    }

    @Test
    public void deleteByIdFailTiedToAnEvent() throws Exception {
        SpaceLayout spaceLayout = createSampleEntityTiedToAnEvent();
        SpaceLayout spaceLayoutP = repository.save(spaceLayout);
        Long id = getId(spaceLayoutP);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Cannot delete spaceLayout with id: " + id + ", it is tied to an event"));
    }

    @Override
    protected SpaceLayout createSampleEntity() {
        SpaceLayout spaceLayout = new SpaceLayout();
        Space space = new Space();
        space.setId(991L);
        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setId(994L);
        spaceLayout.setSeatSpaces(new HashSet<SeatSpace>());
        spaceLayout.setGroundFloors(new HashSet<GroundFloor>());
        spaceLayout.setSpace(space);
        spaceLayout.setSingleEvent(singleEvent);
        spaceLayout.setDeleted(false);
        return spaceLayout;
    }

    protected SpaceLayout createSampleEntityWithNoEvents() {
        SpaceLayout spaceLayout = new SpaceLayout();
        Space space = new Space();
        space.setId(991L);
        spaceLayout.setSeatSpaces(new HashSet<SeatSpace>());
        spaceLayout.setGroundFloors(new HashSet<GroundFloor>());
        spaceLayout.setSpace(space);
        spaceLayout.setDeleted(false);
        return spaceLayout;
    }

    protected SpaceLayout createSampleEntityTiedToAnEvent() {
        SpaceLayout spaceLayout = new SpaceLayout();
        Space space = new Space();
        space.setId(991L);
        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setId(994L);
        spaceLayout.setSeatSpaces(new HashSet<SeatSpace>());
        spaceLayout.setGroundFloors(new HashSet<GroundFloor>());
        spaceLayout.setSpace(space);
        spaceLayout.setSingleEvent(singleEvent);
        spaceLayout.setDeleted(false);
        return spaceLayout;
    }

    @Override
    protected SpaceLayoutDto createSampleDto() {
        SpaceLayoutDto spaceLayoutDto = new SpaceLayoutDto();
        SpaceDto spaceDto = new SpaceDto();
        spaceDto.setId(991L);
        SingleEventDto singleEventDto = new SingleEventDto();
        singleEventDto.setId(994L);
        spaceLayoutDto.setSingleEventId(994L);
        spaceLayoutDto.setSingleEvent(singleEventDto);
        spaceLayoutDto.setSpaceId(991L);
        spaceLayoutDto.setSpace(spaceDto);
        spaceLayoutDto.setGroundFloors(new HashSet<GroundFloorDto>());
        spaceLayoutDto.setSeatSpaces(new HashSet<SeatSpaceDto>());
        return spaceLayoutDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/spaceLayout";
    }

    @Override
    protected Class getDTOType() {
        return SpaceLayoutDto.class;
    }

    @Override
    protected Long getId(SpaceLayoutDto spaceLayoutDto) {
        return spaceLayoutDto.getId();
    }

    @Override
    protected Long getId(SpaceLayout entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(SpaceLayoutDto spaceLayoutDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(spaceLayoutDto);
    }

    @Override
    protected void setId(SpaceLayoutDto spaceLayoutDto, Long id) {
        spaceLayoutDto.setId(id);
    }
}