package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.SeatSpaceDto;
import ktsnvt.dto.SingleEventDto;
import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.*;
import ktsnvt.repository.TicketRepository;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class TicketControllerTest extends CRUDControllerGenericTest<TicketController, Ticket, TicketDto, TicketRepository> {

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype());


    @Override
    @Test
    public void create() throws Exception {
        TicketDto dto = createSampleDto();
        String dtoJson = getJson(dto);
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath())
                .contentType(MediaType.APPLICATION_JSON)
                .content(dtoJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Invalid api endpoint."));
    }

//    @Test
//    public void createFailNoSeatSpaceAndGroundFloor() throws Exception {
//        TicketDto dto = createSampleDto();
//        dto.setSeatSpace(null);
//        dto.setSeatSpaceId(null);
//        dto.setGroundFloor(null);
//        dto.setGroundFloorId(null);
//        String dtoJson = getJson(dto);
//        mvc.perform(MockMvcRequestBuilders
//                .post(getAPIPath())
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(dtoJson)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.message").value("Unable to make reservation. Neither seatSpace nor GroundFloor selected"));
//    }

    @Override
    @Test
    public void deleteById() throws Exception {
        TestUtil.mockSecurityContext("username");
        Ticket object = createTicketDeletatble();
        Ticket o = repository.save(object);
        Long id = getId(o);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Deleted"));
        List<Ticket> allEntities = repository.findAll();
        for (Ticket e: allEntities) {
            if (e.getId().equals(id)) {
                assertNull(e.getUser());
            }
        }
    }

    @Test
    public void deleteFailTicketPurchased() throws Exception {
        TestUtil.mockSecurityContext();
        Ticket ticket = createSampleEntity();
        ticket.setExpirationDate(null);
        Ticket o = repository.save(ticket);
        Long id = getId(o);
        mvc.perform(MockMvcRequestBuilders
                .delete(getAPIPath() + "/" + id))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Ticket already purchased"));
        repository.deleteById(o.getId());

    }

    @Test
    public void getUserTickets() throws Exception {
        String username = "username";
        TestUtil.mockSecurityContext(username);
        List<Ticket> tickets = repository.findAll();
        List<Ticket> userTickets = tickets.stream().filter((ticket) -> {
            return ticket.getUser() != null && ticket.getUser().getUsername().equalsIgnoreCase("username");
        }).collect(Collectors.toList());
        int length = userTickets.size();
        //get first page which contains all but 1 found element
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/user?name=o&page=0&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(length - 1)));

        //get next page, and expect it to contain only 1 element
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/user?name=o&page=1&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

        //get the third page and expect it to be empty
        mvc.perform(MockMvcRequestBuilders
                .get(getAPIPath() + "/user?name=o&page=2&size=" + (length - 1))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Override
    protected Ticket createSampleEntity() {
        Ticket ticket = new Ticket();
        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setId(994L);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setId(997L);
        User user = new User();
        user.setId(9944L);
        ticket.setSeat(2);
        ticket.setRow(3);
        ticket.setExpirationDate(new GregorianCalendar(2021, Calendar.JANUARY, 1).getTime());
        ticket.setSingleEvent(singleEvent);
        ticket.setGroundFloor(null);
        ticket.setSeatSpace(seatSpace);
        ticket.setPrice(30000);
        ticket.setPaid(true);
        ticket.setUser(user);
        ticket.setDeleted(false);
        return ticket;
    }

    protected Ticket createTicketDeletatble() {
        Ticket ticket = new Ticket();
        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setId(994L);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setId(997L);
        User user = new User();
        user.setId(9944L);
        ticket.setSeat(2);
        ticket.setRow(3);
        ticket.setExpirationDate(new GregorianCalendar(2021, Calendar.JANUARY, 1).getTime());
        ticket.setSingleEvent(singleEvent);
        ticket.setGroundFloor(null);
        ticket.setSeatSpace(seatSpace);
        ticket.setPrice(30000);
        ticket.setPaid(false);
        ticket.setUser(user);
        ticket.setDeleted(false);
        return ticket;
    }


    @Override
    protected TicketDto createSampleDto() {
        TicketDto ticketDto = new TicketDto();
        SingleEventDto singleEventDto = new SingleEventDto();
        singleEventDto.setId(994L);
        SeatSpaceDto seatSpaceDto = new SeatSpaceDto();
        seatSpaceDto.setId(997L);
        UserDto userDto = new UserDto();
        userDto.setId(9944L);
        ticketDto.setSeat(4);
        ticketDto.setRow(4);
        ticketDto.setExpirationDate(new GregorianCalendar(2021, Calendar.JANUARY, 1).getTime());
        ticketDto.setSingleEvent(singleEventDto);
        ticketDto.setSingleEventId(994L);
        ticketDto.setGroundFloor(null);
        ticketDto.setGroundFloorId(null);
        ticketDto.setSeatSpace(seatSpaceDto);
        ticketDto.setSeatSpaceId(997L);
        ticketDto.setPrice(30000);
        ticketDto.setPaid(true);
        ticketDto.setUser(userDto);
        ticketDto.setUserId(9944L);
        return ticketDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/ticket";
    }

    @Override
    protected Class getDTOType() {
        return TicketDto.class;
    }

    @Override
    protected Long getId(TicketDto ticketDto) {
        return ticketDto.getId();
    }

    @Override
    protected Long getId(Ticket entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(TicketDto ticketDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(ticketDto);
    }

    @Override
    protected void setId(TicketDto ticketDto, Long id) {
        ticketDto.setId(id);
    }

    @Test
    @Transactional
    public void failedReserveTickets() throws Exception {
        TestUtil.mockSecurityContext();
        String json = "[{}]";
        mvc.perform(MockMvcRequestBuilders.post(getAPIPath() + "/custom")
            .contentType(contentType)
            .content(json))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void successfulReserveTickets() throws Exception {
        TestUtil.mockSecurityContext();
        String json = "[{\"groundFloorId\":\"9923\"},{\"seatSpaceId\":\"997\",\"seat\":\"1\",\"row\":\"0\"}]";
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/custom")
                .contentType(contentType)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Tickets reserved"));
    }

    @Test
    @Transactional
    public void failedPayForTicket() throws Exception {
        TestUtil.mockSecurityContext();
        long ticketId = 1L;
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/" + ticketId + "/pay")).
                andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void successfulPayForTicket() throws Exception {
        TestUtil.mockSecurityContext();
        long ticketId = 999992119L;
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/" + ticketId + "/pay")).
                andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void failedPayForTickets() throws Exception {
        TestUtil.mockSecurityContext();
        String json = "[{}]";
        mvc.perform(MockMvcRequestBuilders.post(getAPIPath() + "/pay")
                .contentType(contentType)
                .content(json))
                .andExpect(status().isBadRequest());

    }

    @Test
    @Transactional
    public void successfulPayForTickets() throws Exception {
        TestUtil.mockSecurityContext();
        String json = "[{\"groundFloorId\":\"9923\"},{\"seatSpaceId\":\"997\",\"seat\":\"1\",\"row\":\"0\"}]";
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/pay")
                .contentType(contentType)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void failedgetUserTickets() {
        TestUtil.mockSecurityContext();

    }

    @Test
    @Transactional
    public void successfulgetUserTickets() {
        TestUtil.mockSecurityContext();

    }
}