package ktsnvt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.dto.TicketDto;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.Ticket;
import ktsnvt.entity.User;
import ktsnvt.entity.UserRole;
import ktsnvt.entity.VerificationToken;
import ktsnvt.repository.UserRepository;
import ktsnvt.repository.VerificationTokenRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class UserControllerTest extends CRUDControllerGenericTest<UserController, User, UserDto, UserRepository> {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Test
    public void login() throws Exception {
        mvc.perform(MockMvcRequestBuilders
            .post(getAPIPath() + "/login")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"username\": \"username\", \"password\": \"password\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void loginFailed() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/login")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"nonExistingUsername\", \"password\": \"password\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Invalid username or password"));
    }

    @Test
    @Transactional
    public void register() throws Exception {
        UserDto userDto = createSampleDto();
        mvc.perform(MockMvcRequestBuilders
        .post(getAPIPath() + "/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(getJson(userDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("Sample username 1"))
                .andExpect(jsonPath("$.name").value("Sample name 1"))
                .andExpect(jsonPath("$.surname").value("Sample Surname 1"))
                .andExpect(jsonPath("$.email").value("boro@gmail.com"))
                .andExpect(jsonPath("$.username").value("Sample username 1"))
                .andExpect(jsonPath("$.username").value("Sample username 1"));
    }

    @Test
    public void registerFailedUsernameTaken() throws Exception {
        UserDto userDto = createSampleDto();
        userDto.setUsername("username");
        mvc.perform(MockMvcRequestBuilders
                .post(getAPIPath() + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getJson(userDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Username already taken!"));
    }

    @Test
    public void verifyUser() throws Exception {
        User user = createSampleEntity();
        User userP = repository.save(user);
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(userP);
        userP.setVerified(false);
        verificationToken.setId(1L);
        verificationToken.setToken("tokencic");
        verificationTokenRepository.save(verificationToken);
        mvc.perform(MockMvcRequestBuilders
            .get(getAPIPath() + "/verify/" + "tokencic"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("User verified"));
    }

    @Override
    protected User createSampleEntity() {
        User user = new User();
        user.setName("Sample name 1");
        user.setPassword("samplePw1");
        user.setRole(UserRole.Regular);
        user.setSurname("Sample Surname 1");
        user.setTickets(new HashSet<Ticket>());
        user.setUsername("Sample username 2");
        user.setDeleted(false);
        user.setEmail("boro@gmail.com");
        return user;
    }

    @Override
    protected UserDto createSampleDto() {
        UserDto userDto = new UserDto();
        userDto.setName("Sample name 1");
        userDto.setPassword("samplePw1");
        userDto.setRole(UserRole.Regular);
        userDto.setSurname("Sample Surname 1");
        userDto.setTickets(new HashSet<TicketDto>());
        userDto.setUsername("Sample username 1");
        userDto.setEmail("boro@gmail.com");
        return userDto;
    }

    @Override
    protected String getAPIPath() {
        return "/api/user";
    }

    @Override
    protected Class getDTOType() {
        return UserDto.class;
    }

    @Override
    protected Long getId(UserDto userDto) {
        return userDto.getId();
    }

    @Override
    protected Long getId(User entity) {
        return entity.getId();
    }

    @Override
    protected String getJson(UserDto userDto) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(userDto);
    }

    @Override
    protected void setId(UserDto userDto, Long id) {
        userDto.setId(id);
    }
}