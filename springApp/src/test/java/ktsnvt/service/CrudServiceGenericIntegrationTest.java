package ktsnvt.service;

import ktsnvt.common.IDeletable;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.utility.DataGraph;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public abstract class CrudServiceGenericIntegrationTest<Service extends CRUDService, Repository extends JpaRepository<Entity,
        Long>, Entity extends IDeletable> {


    @Autowired
    protected Repository repository;

    @Autowired
    protected Service crudService;

    @Test
    @Transactional
    public void getByIdNotExists() {
        Long id = getIdNotExists();
        assertThrows(FetchException.class, () -> {
            crudService.getById(id);
        });
    }

    @Test
    @Transactional
    public void getByIdExists() throws FetchException {
        Long id = getIdExists();
        Entity entity = (Entity) crudService.getById(id);
        assertNotNull(entity);
        Entity entityToCompare = getEntityFromIdExists();
        assertTrue(entityEquals(entityToCompare, entity));
    }

    @Test
    @Transactional
    public void getAll() {
        List<Entity> allEntities = crudService.getAll();
        for (Entity e: allEntities) {
            assertFalse(e.isDeleted());
        }
        assertEquals(getNumberOfEntitiesInDb(), allEntities.size());
        for (Entity entity: allEntities) {
            Entity toCompare = this.getEntityFromDataGraph(getIdFromEntity(entity));
            Assert.assertTrue(this.entityEquals(entity, toCompare));
        }
    }

    @Test
    @Transactional
    public void successfulAdd() throws AddException, FetchException {
        Entity entity = createSampleValue();
        crudService.add(entity);
        Long id = getIdFromEntity(entity);
        Entity retVal = (Entity) crudService.getById(id);

        assertNotNull(retVal);
        assertEquals(id, getIdFromEntity(retVal));

        Entity entityWithSameAttributes = createSampleValue();
        setEntityId(entityWithSameAttributes, id);

        assertTrue(entityEquals(entityWithSameAttributes, retVal));

        List<Entity> entities = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb() + 1, entities.size());
    }

    @Test
    @Transactional
    public void successfulUpdate() throws FetchException, UpdateException, AddException {
        Long id = getIdExists();
        Entity entity = (Entity) crudService.getById(id);
        changeEntitiesAttributes(entity);
        crudService.update(entity);
        Entity changedEntity = (Entity) crudService.getById(id);
        assertTrue(verifyChangedAttributes(changedEntity));
        List<Entity> entities = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb(), entities.size());
    }

    @Test
    @Transactional
    public void successfulDelete() throws FetchException, DeleteException {
        Long id = getIdDeletable();
        Entity entity = (Entity) crudService.getById(id);
        assertNotNull(entity);
        crudService.deleteById(id);
        assertThrows(FetchException.class, () -> {
           crudService.getById(id);
        });
        List<Entity> entities = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb() - 1, entities.size());
    }

    @Test
    @Transactional
    public void failedDeleteEntityNotExist() {
        Long id = getIdNotExists();
        assertThrows(FetchException.class, () -> {
           crudService.deleteById(id);
        });
        List<Entity> entities = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb(), entities.size());
    }

    @Test
    @Transactional
    public void getPage() {
        Pageable pageable =  PageRequest.of(0, getNumberOfEntitiesInDb() - 1);
        Page<Entity> page = this.crudService.getPage(pageable);
        assertEquals(pageable.getPageSize(), page.getNumberOfElements());

        Pageable pageable1 = PageRequest.of(1, getNumberOfEntitiesInDb() - 1);
        Page<Entity> page2 = this.crudService.getPage(pageable1);
        assertEquals(1, page2.getNumberOfElements());
    }


    public abstract Long getIdNotExists();

    public abstract Long getIdExists();

    public abstract Long getIdDeletable();

    public abstract Long getIdFromEntity(Entity entity);

    public abstract int getNumberOfEntitiesInDb();

    public abstract Entity createSampleValue();

    public abstract void changeEntitiesAttributes(Entity entity);

    public abstract boolean verifyChangedAttributes(Entity entity);

    public abstract boolean entityEquals(Entity e1, Entity e2);

    public abstract Entity getEntityFromIdExists();

    public abstract void setEntityId(Entity entity, Long id);

    public abstract  Entity getEntityFromDataGraph(Long id);
}
