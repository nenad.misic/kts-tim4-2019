package ktsnvt.service;

import ktsnvt.common.IDeletable;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.PageFormat;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


public abstract class CrudServiceGenericUnitTest<Service extends CRUDService, Repository extends JpaRepository<Entity,
        Long>, Entity extends IDeletable> {

    @Autowired
    protected   Service crudService;

    @Test
    public void getByIdNotExists() {
        Long id = 1l;
        Repository mockedRepo = getMockedRepo();
        Mockito.when(mockedRepo.findById(id)).thenReturn(Optional.empty());
        crudService.repository = mockedRepo;
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.getById(id);
        });
        assertEquals("Cannot find entity with id " + id, fetchException.getMessage());
    }

    @Test
    public void getByIdExists() throws FetchException {
        Long id = 1l;
        Entity entity = createSampleValue();
        Repository mockedRepo = getMockedRepo();
        Mockito.when(mockedRepo.findById(id)).thenReturn(Optional.of(entity));
        crudService.repository = mockedRepo;
        Entity retVal = (Entity) crudService.getById(id);
        assertEquals(entity, retVal);
    }

    @Test
    public void getAll() {
        List<Entity> allEntities = new ArrayList<Entity>()
        {
            {
                add(createSampleValue());
                add(createSampleValue());
            }
        };
        Repository mockedRepo = getMockedRepo();
        Mockito.when(mockedRepo.findAll()).thenReturn(allEntities);
        crudService.repository = mockedRepo;
        List<Entity> retVal = crudService.getAll();
        assertEquals(allEntities, retVal);
    }

    @Test
    public void successfulAdd() throws AddException {
        Entity entity = createSampleValue();
        Repository mockedRepo = getMockedRepo();
        crudService.repository = mockedRepo;
        crudService.add(entity);
        verify(mockedRepo, times(1)).save(entity);
    }

    @Test
    public void successfulUpdate() throws UpdateException, AddException {
        Entity entity = createSampleValue();
        Repository mockedRepo = getMockedRepo();
        crudService.repository = mockedRepo;
        crudService.update(entity);
        verify(mockedRepo, times(1)).save(entity);
    }

    @Test
    public void successfulDelete() throws FetchException, DeleteException {
        Long id = 1l;
        Entity entity = createSampleValue();
        Repository mockedRepo = getMockedRepo();
        when(mockedRepo.findById(id)).thenReturn(Optional.of(entity));
        crudService.repository = mockedRepo;
        crudService.deleteById(id);
        assertTrue(entity.isDeleted());
        verify(mockedRepo, times(1)).save(entity);
    }

    @Test
    public void failedDeleteEntityNotExist() {
        Long id = 1l;
        Repository mockedRepo = getMockedRepo();
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());
        crudService.repository = mockedRepo;
        assertThrows(FetchException.class, () -> {
            crudService.deleteById(id);
        });
    }

    @Test
    public void getPageTest() {
        Repository mockedRepo = getMockedRepo();
        Pageable pageable = PageRequest.of(2, 2);
        Page<Entity> page = Page.empty();
        when(mockedRepo.findAll(pageable)).thenReturn(page);
        crudService.repository = mockedRepo;
        Page retVal = crudService.getPage(pageable);
        assertEquals(page, retVal);
        verify(mockedRepo, times(1)).findAll(pageable);
    }



    public abstract Entity createSampleValue();

    public abstract Repository getMockedRepo();
}
