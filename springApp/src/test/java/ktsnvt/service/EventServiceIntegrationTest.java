package ktsnvt.service;

import ktsnvt.common.SearchMode;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.entity.*;
import ktsnvt.repository.CompositeEventRepository;
import ktsnvt.repository.SingleEventRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static junit.framework.TestCase.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class EventServiceIntegrationTest {
    @Autowired
    protected EventService eventService;


    @Autowired
    protected SpaceService spaceService;

    @Autowired
    protected  SpaceLayoutService spaceLayoutService;

    private Event exitDay1;
    private Event exitDay2;
    private Event exitDay3;
    private Event exitDay4;
    private Event seaDanceDay1;
    private Event seaDanceDay2;
    private Event seaDanceDay3;
    private Event exit;
    private Event seaDance;
    private Event exitAndSeaDance;
    private Event koncertZvonkaBogdana1;
    private Event koncertZvonkaBogdana2;
    private Event turnejaZvonkaBogdana;
    private Event koncertMayeBerovic;


    private Event add_josJednaTurnejaZvonkaBogdana;
    private Event add_josJedanKoncertZvonkaBogdana1;
    private Event add_josJedanKoncertZvonkaBogdana2;
    private Event add_koncertLaneDelRey;
    private Event add_exitDay5;

    @Before
    public void setup() throws ParseException {
        setUpAttributes();
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_SingleEvent_WithNoParent_Exists() throws FetchException {
        SingleEvent returnedSingleEvent = (SingleEvent) eventService.getById(koncertMayeBerovic.getId());
        Assert.assertTrue(eventsEqual(returnedSingleEvent, koncertMayeBerovic));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_SingleEvent_WithParent_Exists() throws FetchException {
        SingleEvent returnedSingleEvent = (SingleEvent) eventService.getById(koncertZvonkaBogdana2.getId());
        Assert.assertTrue(eventsEqual(returnedSingleEvent, koncertZvonkaBogdana2));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_WithNoParent_Exists() throws FetchException {
        CompositeEvent returnedSingleEvent = (CompositeEvent) eventService.getById(turnejaZvonkaBogdana.getId());
        Assert.assertTrue(eventsEqual(returnedSingleEvent, turnejaZvonkaBogdana));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_WithParent_Exists() throws FetchException {
        CompositeEvent returnedSingleEvent = (CompositeEvent) eventService.getById(exit.getId());
        Assert.assertTrue(eventsEqual(returnedSingleEvent, exit));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById_CompositeEvent_Complex_Exists() throws FetchException {
        CompositeEvent returnedSingleEvent = (CompositeEvent) eventService.getById(exitAndSeaDance.getId());
        Assert.assertTrue(eventsEqual(returnedSingleEvent, exitAndSeaDance));
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void getById_DoesNotExist() throws FetchException {
        Event returnedSingleEvent = eventService.getById(5000L);
    }


    @Test
    @Transactional
    @Rollback(true)
    public void getAll_Success() {
        List<Event> events = eventService.getAll();
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitAndSeaDance,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));
        assertEquals(events.size(), 3);
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_SingleEvent_WithNoParent() throws FetchException {
        eventService.deleteById(koncertMayeBerovic.getId());

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);

        //Expect this to throw exception
        eventService.getById(koncertMayeBerovic.getId());
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_SingleEvent_WithParent() throws FetchException {
        eventService.deleteById(koncertZvonkaBogdana2.getId());

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitAndSeaDance,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        //Remove the child from the detached parent object (to test if it is equal to the returned by getAll method)
        ((CompositeEvent)turnejaZvonkaBogdana).setChildEvents(new HashSet<>(Collections.singletonList(koncertZvonkaBogdana1)));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);

        //Expect this to throw exception
        eventService.getById(koncertZvonkaBogdana2.getId());
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_WithNoParent() throws FetchException {
        eventService.deleteById(turnejaZvonkaBogdana.getId());

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitAndSeaDance));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        turnejaZvonkaBogdana,
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);

        //Expect this to throw exception
        eventService.getById(turnejaZvonkaBogdana.getId());
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_WithParent() throws FetchException {
        eventService.deleteById(exit.getId());

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitAndSeaDance,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));


        //Remove the child from the detached parent object (to test if it is equal to the returned by getAll method)
        ((CompositeEvent)exitAndSeaDance).setChildEvents(new HashSet<>(Collections.singletonList(seaDance)));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);

        //Expect this to throw exception
        eventService.getById(exit.getId());
    }


    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_CompositeEvent_Complex() throws FetchException {
        eventService.deleteById(exitAndSeaDance.getId());

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);

        //Expect this to throw exception
        eventService.getById(exitAndSeaDance.getId());
    }

    @Test(expected = FetchException.class)
    @Transactional
    @Rollback(true)
    public void deleteById_IdDoesNotExist() throws FetchException {
        eventService.deleteById(5000L);

        //Test if there are no side effects
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);

    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_SingleEvent_WithNoParent_Success() throws FetchException, AddException {
        ((SingleEvent)add_koncertLaneDelRey).setSpace(spaceService.getById(99302L));
        ((SingleEvent)add_koncertLaneDelRey).setSpaceLayout(spaceLayoutService.getById(99304L));
        this.eventService.add(add_koncertLaneDelRey);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana,
                        add_koncertLaneDelRey));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 4);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_CompositeEvent_WithNoParent_Success() throws FetchException, AddException {

        ((SingleEvent)add_josJedanKoncertZvonkaBogdana1).setSpace(spaceService.getById(99303L));
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana2).setSpace(spaceService.getById(99303L));
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana1).setSpaceLayout(spaceLayoutService.getById(99305L));
        ((SingleEvent)add_josJedanKoncertZvonkaBogdana2).setSpaceLayout(spaceLayoutService.getById(99306L));
        ((CompositeEvent)add_josJednaTurnejaZvonkaBogdana).setChildEvents(new HashSet<>(Arrays.asList(add_josJedanKoncertZvonkaBogdana1, add_josJedanKoncertZvonkaBogdana2)));
        this.eventService.add(add_josJednaTurnejaZvonkaBogdana);


        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana,
                        add_josJednaTurnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        add_josJedanKoncertZvonkaBogdana1,
                        add_josJedanKoncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();


        assertEquals(events.size(), 4);
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

    }

    @Test
    @Transactional
    @Rollback(true)
    public void add_SingleEvent_WithParent_Success() throws FetchException, AddException {
        ((SingleEvent)add_exitDay5).setSpace(spaceService.getById(992L));
        ((SingleEvent)add_exitDay5).setSpaceLayout(spaceLayoutService.getById(99313L));
        this.eventService.add(add_exitDay5);

        ((CompositeEvent) exit).setChildEvents(new HashSet<>(Arrays.asList(exitDay1,exitDay2, exitDay3, exitDay4, add_exitDay5)));
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        add_exitDay5));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_SingleEvent_NoParent_Success() throws UpdateException, FetchException, AddException {
        koncertMayeBerovic.setDescription("Promenjen opis");
        koncertMayeBerovic.setName("Promenjeno ime");
        koncertMayeBerovic.setType(EventType.Movie);
        eventService.update(koncertMayeBerovic);
        assertTrue(eventsEqual(koncertMayeBerovic, eventService.getById(koncertMayeBerovic.getId())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_NoParent_Success() throws UpdateException, FetchException, AddException {
        turnejaZvonkaBogdana.setDescription("Promenjen opis");
        turnejaZvonkaBogdana.setName("Promenjeno ime");
        turnejaZvonkaBogdana.setType(EventType.Movie);
        eventService.update(turnejaZvonkaBogdana);
        assertTrue(eventsEqual(turnejaZvonkaBogdana, eventService.getById(turnejaZvonkaBogdana.getId())));
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_SingleEvent_NoParent_SetParent_Success() throws UpdateException, FetchException, AddException {
        koncertMayeBerovic.setDescription("Promenjen opis");
        koncertMayeBerovic.setName("Promenjeno ime");
        koncertMayeBerovic.setType(EventType.Movie);
        koncertMayeBerovic.setParentEvent((CompositeEvent) exit);
        eventService.update(koncertMayeBerovic);
        assertTrue(eventsEqual(koncertMayeBerovic, eventService.getById(koncertMayeBerovic.getId())));


        ((CompositeEvent) exit).setChildEvents(new HashSet<>(Arrays.asList(exitDay1,exitDay2, exitDay3, exitDay4, koncertMayeBerovic)));
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        koncertMayeBerovic));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);

    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_NoParent_SetParent_Success() throws UpdateException, FetchException, AddException {
        turnejaZvonkaBogdana.setDescription("Promenjen opis");
        turnejaZvonkaBogdana.setName("Promenjeno ime");
        turnejaZvonkaBogdana.setType(EventType.Movie);
        turnejaZvonkaBogdana.setParentEvent((CompositeEvent) exitAndSeaDance);
        eventService.update(turnejaZvonkaBogdana);
        assertTrue(eventsEqual(turnejaZvonkaBogdana, eventService.getById(turnejaZvonkaBogdana.getId())));


        ((CompositeEvent) exitAndSeaDance).setChildEvents(new HashSet<>(Arrays.asList(exit,seaDance)));
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        exit,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        turnejaZvonkaBogdana));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);

    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_WithParent_RemoveParent_Success() throws UpdateException, FetchException, AddException {
        exit.setDescription("Promenjen opis");
        exit.setName("Promenjeno ime");
        exit.setType(EventType.Movie);
        exit.setParentEvent(null);
        eventService.update(exit);
        assertTrue(eventsEqual(exit, eventService.getById(exit.getId())));


        ((CompositeEvent) exitAndSeaDance).setChildEvents(new HashSet<>(Arrays.asList(seaDance)));
        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        exit,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        List<Event> events = eventService.getAll();
        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 4);

    }

    @Test
    @Transactional
    @Rollback
    public void search_Quick_0Match() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        sl.setName("fghfghfghfhfghf");
        s.setName("fghfghfghfhfghf");
        l.setName("fghfghfghfhfghf");
        l.setAddress("fghfghfghfhfghf");
        l.setCity("fghfghfghfhfghf");
        l.setCountry("fghfghfghfhfghf");
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("fghfghfghfhfghf");
        se.setDescription("fghfghfghfhfghf");

        ce.setName("fghfghfghfhfghf");
        ce.setDescription("fghfghfghfhfghf");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Quick);

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 0);
    }

    @Test
    @Transactional
    @Rollback
    public void search_Quick_1Match() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        sl.setName("ExiT");
        s.setName("ExiT");
        l.setName("ExiT");
        l.setAddress("ExiT");
        l.setCity("ExiT");
        l.setCountry("ExiT");
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("ExiT");
        se.setDescription("ExiT");

        ce.setName("ExiT");
        ce.setDescription("ExiT");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Quick);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 1);
    }

    @Test
    @Transactional
    @Rollback
    public void search_Quick_2Match() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        sl.setName("gori");
        s.setName("gori");
        l.setName("gori");
        l.setAddress("gori");
        l.setCity("gori");
        l.setCountry("gori");
        s.setLocation(l);
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("gori");
        se.setDescription("gori");

        ce.setName("gori");
        ce.setDescription("gori");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Quick);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic
                ));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        turnejaZvonkaBogdana,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);
    }

    @Test
    @Transactional
    @Rollback
    public void search_Quick_3Match() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        sl.setName("a");
        s.setName("a");
        l.setName("a");
        l.setAddress("a");
        l.setCity("a");
        l.setCountry("a");
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("a");
        se.setDescription("a");

        ce.setName("a");
        ce.setDescription("a");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Quick);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana
                        ));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);
    }

    @Test
    @Rollback
    @Transactional
    public void search_Regular_1attribute_3match(){
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        se.setSpace(s);
        se.setSpaceLayout(sl);

        se.setName("a");
        ce.setName("a");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Regular);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic,
                        turnejaZvonkaBogdana
                ));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 3);
    }

    @Test
    @Rollback
    @Transactional
    public void search_Regular_1attribute_2match(){
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        se.setSpace(s);
        se.setSpaceLayout(sl);

        se.setDescription("gOrI");
        ce.setDescription("gOrI");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Regular);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        exitAndSeaDance,
                        koncertMayeBerovic
                ));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        turnejaZvonkaBogdana));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 2);
    }

    @Test
    @Rollback
    @Transactional
    public void search_Regular_more_attributes(){
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        se.setSpace(s);
        se.setSpaceLayout(sl);

        se.setName("berovic");
        se.setDescription("gori");
        ce.setName("berovic");
        se.setDescription("gori");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Regular);

        List<Event> shouldReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic
                ));

        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        turnejaZvonkaBogdana,
                        exitAndSeaDance));

        for (Event e1 : shouldReturnList) assertTrue(events.stream().anyMatch(e2 -> eventsEqual(e1, e2)));
        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 1);
    }

    @Test
    @Rollback
    @Transactional
    public void search_Regular_more_attributes_no_match(){
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        se.setSpace(s);
        se.setSpaceLayout(sl);

        se.setName("berovic");
        se.setDescription("gori");
        s.setName("nestostonepostoji");
        ce.setName("berovic");
        se.setDescription("gori");

        List<Event> events = this.eventService.search(se, ce, SearchMode.Regular);


        List<Event> shouldNotReturnList = new ArrayList<>(
                Arrays.asList(
                        koncertMayeBerovic,
                        exitDay1,
                        exitDay2,
                        exitDay3,
                        exitDay4,
                        seaDanceDay1,
                        seaDanceDay2,
                        seaDanceDay3,
                        seaDance,
                        exit,
                        koncertZvonkaBogdana1,
                        koncertZvonkaBogdana2,
                        turnejaZvonkaBogdana,
                        exitAndSeaDance));

        for (Event e1 : shouldNotReturnList) assertTrue(events.stream().noneMatch(e2 -> eventsEqual(e1, e2)));

        assertEquals(events.size(), 0);
    }

    @Transactional
    @Test
    public void calculateOccupancySingleEvent() throws FetchException {
        // prvi dan exita - single event
        Long id = 999l;
        SingleEvent exit1DB = (SingleEvent) eventService.getById(id);
        int retVal = this.eventService.calculateOccupancy(exit1DB);
        assertEquals(4, retVal);
    }

    @Transactional
    @Test
    public void calculateOccupancyCompositeEvent() throws FetchException {
        // ceo exit - composite event
        Long id = 9940l;
        CompositeEvent exitDB = (CompositeEvent) eventService.getById(id);
        int retVal = this.eventService.calculateOccupancy(exitDB);
        assertEquals(17, retVal);
    }

    @Transactional
    @Test
    public void calculateEarningsSingleEvent() throws FetchException {
        // prvi dan exita - single event
        Long id = 999l;
        SingleEvent exit1DB = (SingleEvent) eventService.getById(id);
        double retVal = this.eventService.calculateEarnings(exit1DB);
        assertEquals(8000.0, retVal, 0.001);
    }

    @Transactional
    @Test
    public void calculateEarningsCompositeEvent() throws FetchException {
        // ceo exit - composite event
        Long id = 9940l;
        CompositeEvent exitDB = (CompositeEvent) eventService.getById(id);
        double retVal = this.eventService.calculateEarnings(exitDB);
        assertEquals(34000, retVal, 0.001);
    }

    @Transactional
    @Test
    public void reportOccupancyOfChildrenFailedDoesNotExist() {
        Long id = 6969l;

        FetchException fetchException = assertThrows(FetchException.class, () -> {
           eventService.reportOccupancyOfChildren(id);
        });
        assertEquals("Unable to find event with ID: " + id, fetchException.getMessage());
    }

    @Transactional
    @Test
    public void reportOccupancyOfChildrenSingleEvent() throws FetchException {
        // prvi dan exita - single event
        Long id = 999l;
        String name = "Prvi dan Exit festivala";


        Map<String, Object> retVal = eventService.reportOccupancyOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("tickets bought", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(name));
        Assert.assertEquals(4, dataMap.get(name));
    }

    @Transactional
    @Test
    public void reportOccupancyOfChildrenCompositeEvent() throws FetchException {
        // ceo exit - composite event
        Long id = 9940l;
        String name = "Exit festival";

        String nameDay1 = "Prvi dan Exit festivala";
        String nameDay2 = "Drugi dan Exit festivala";
        String nameDay3 = "Treci dan Exit festivala";
        String nameDay4 = "Cetvrti dan Exit festivala";

        Map<String, Object> retVal = eventService.reportOccupancyOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("tickets bought", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(nameDay1));
        Assert.assertEquals(4, dataMap.get(nameDay1));

        Assert.assertTrue(dataMap.containsKey(nameDay2));
        Assert.assertEquals(4, dataMap.get(nameDay2));

        Assert.assertTrue(dataMap.containsKey(nameDay3));
        Assert.assertEquals(5, dataMap.get(nameDay3));

        Assert.assertTrue(dataMap.containsKey(nameDay4));
        Assert.assertEquals(4, dataMap.get(nameDay4));
    }

    @Transactional
    @Test
    public void reportEarningsOfChildrenFailedDoesNotExist() {
        Long id = 6969l;

        FetchException fetchException = assertThrows(FetchException.class, () -> {
            eventService.reportEarningsOfChildren(id);
        });
        Assert.assertEquals("Unable to find event with ID: " + id, fetchException.getMessage());
    }

    @Transactional
    @Test
    public void reportEarningsOfChildrenSingleEvent() throws FetchException {
        // prvi dan exita - single event
        Long id = 999l;
        String name = "Prvi dan Exit festivala";


        Map<String, Object> retVal = eventService.reportEarningsOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("earnings(dollars)", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(name));
        Assert.assertEquals(8000.0, (double)dataMap.get(name), 0.001);
    }

    @Transactional
    @Test
    public void reportEarningsOfChildrenCompositeEvent() throws FetchException {
        // ceo exit - composite event
        Long id = 9940l;
        String name = "Exit festival";

        String nameDay1 = "Prvi dan Exit festivala";
        String nameDay2 = "Drugi dan Exit festivala";
        String nameDay3 = "Treci dan Exit festivala";
        String nameDay4 = "Cetvrti dan Exit festivala";

        Map<String, Object> retVal = eventService.reportEarningsOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("earnings(dollars)", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(nameDay1));
        Assert.assertEquals(8000.0, (double)dataMap.get(nameDay1), 0.001);

        Assert.assertTrue(dataMap.containsKey(nameDay2));
        Assert.assertEquals(8000.0, (double)dataMap.get(nameDay2), 0.001);

        Assert.assertTrue(dataMap.containsKey(nameDay3));
        Assert.assertEquals(10000.0, (double)dataMap.get(nameDay3), 0.001);

        Assert.assertTrue(dataMap.containsKey(nameDay4));
        Assert.assertEquals(8000.0, (double)dataMap.get(nameDay4), 0.001);
    }

    private boolean eventsEqual(Event e1, Event e2){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if (e1 == e2) return true;
        if (!(e2.getClass().equals(e1.getClass()))) return false;

        boolean baseClassEqual = e1.isDeleted() == e2.isDeleted() &&
                //Objects.equals(e1.getId(), e2.getId()) &&
                Objects.equals(e1.getName(), e2.getName()) &&
                Objects.equals(e1.getDescription(), e2.getDescription()) &&
                e1.getType() == e2.getType() &&
                ((e1.getParentEvent() == null && e2.getParentEvent() == null) ||
                        eventsEqual(e1.getParentEvent(), e2.getParentEvent()));

        if(e2.getClass().equals(CompositeEvent.class)){
            CompositeEvent compositeEvent1 = (CompositeEvent) e1;
            CompositeEvent compositeEvent2 = (CompositeEvent) e2;
            return baseClassEqual &&
                    Objects.equals(compositeEvent1.getChildEvents().size(), compositeEvent2.getChildEvents().size());
        }else if(e2.getClass().equals(SingleEvent.class)){
            SingleEvent singleEvent1 = (SingleEvent) e1;
            SingleEvent singleEvent2 = (SingleEvent) e2;
            return baseClassEqual &&
                    singleEvent1.getState() == singleEvent2.getState() &&
                    Objects.equals(sdf.format(singleEvent1.getStartDate()), sdf.format(singleEvent2.getStartDate())) &&
                    Objects.equals(sdf.format(singleEvent1.getEndDate()), sdf.format(singleEvent2.getEndDate())) &&
                    ((singleEvent1.getSpace() == null && singleEvent2.getSpace() == null) ||
                            Objects.equals(singleEvent1.getSpace().getId(), singleEvent2.getSpace().getId())) &&
                    ((singleEvent1.getSpaceLayout() == null && singleEvent2.getSpaceLayout() == null) ||
                            Objects.equals(singleEvent1.getSpaceLayout().getId(), singleEvent2.getSpaceLayout().getId()));
        }else {
            return false;
        }
    }



    private void setUpAttributes() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Space tvrdjava = new Space();
        Space more = new Space();
        Location noviSad = new Location();
        Location crnaGora = new Location();
        SingleEvent exitDay1 = new SingleEvent();
        SingleEvent exitDay2 = new SingleEvent();
        SingleEvent exitDay3 = new SingleEvent();
        SingleEvent exitDay4 = new SingleEvent();
        SpaceLayout exitDay1SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay2SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay3SpaceLayout = new SpaceLayout();
        SpaceLayout exitDay4SpaceLayout = new SpaceLayout();
        SingleEvent seaDanceDay1 = new SingleEvent();
        SingleEvent seaDanceDay2 = new SingleEvent();
        SingleEvent seaDanceDay3 = new SingleEvent();
        SpaceLayout seaDanceDay1SpaceLayout = new SpaceLayout();
        SpaceLayout seaDanceDay2SpaceLayout = new SpaceLayout();
        SpaceLayout seaDanceDay3SpaceLayout = new SpaceLayout();
        CompositeEvent exit = new CompositeEvent();
        CompositeEvent seaDance = new CompositeEvent();
        CompositeEvent exitAndSeadance = new CompositeEvent();


        HashSet<Space> nsSpaces = new HashSet<>();
        nsSpaces.add(tvrdjava);
        HashSet<Space> cgSpaces = new HashSet<>();
        cgSpaces.add(more);
        noviSad.setSpaces(nsSpaces);
        noviSad.setName("Novi Sad");
        noviSad.setCity("Novi Sad");
        noviSad.setAddress("Srbija");
        noviSad.setCountry("Srbija");
        crnaGora.setSpaces(cgSpaces);
        crnaGora.setName("Crna Gora");
        crnaGora.setCity("Crna Gora");
        crnaGora.setAddress("Balkan");
        crnaGora.setCountry("SCG");

        exitDay1.setParentEvent(exit);
        exitDay1.setTickets(new HashSet<>());
        exitDay1.setSpaceLayout(exitDay1SpaceLayout);
        exitDay1.setSpace(tvrdjava);
        exitDay1.setType(EventType.Music);
        exitDay1.setName("Prvi dan Exit festivala");
        exitDay1.setDescription("Prvi dan Exit festivala u Novom Sadu");
        exitDay1.setState(EventState.ForSale);
        exitDay1.setStartDate(sdf.parse("2020-12-31 18:00"));
        exitDay1.setEndDate(sdf.parse("2021-01-01 18:00"));
        exitDay2.setParentEvent(exit);
        exitDay2.setTickets(new HashSet<>());
        exitDay2.setSpaceLayout(exitDay2SpaceLayout);
        exitDay2.setSpace(tvrdjava);
        exitDay2.setType(EventType.Music);
        exitDay2.setName("Drugi dan Exit festivala");
        exitDay2.setDescription("Drugi dan Exit festivala u Novom Sadu");
        exitDay2.setState(EventState.ForSale);
        exitDay2.setStartDate(sdf.parse("2021-01-01 18:00"));
        exitDay2.setEndDate(sdf.parse("2021-01-02 18:00"));
        exitDay3.setParentEvent(exit);
        exitDay3.setTickets(new HashSet<>());
        exitDay3.setSpaceLayout(exitDay3SpaceLayout);
        exitDay3.setSpace(tvrdjava);
        exitDay3.setType(EventType.Music);
        exitDay3.setName("Treci dan Exit festivala");
        exitDay3.setDescription("Treci dan Exit festivala u Novom Sadu");
        exitDay3.setState(EventState.ForSale);
        exitDay3.setStartDate(sdf.parse("2021-01-02 18:00"));
        exitDay3.setEndDate(sdf.parse("2021-01-03 18:00"));
        exitDay4.setParentEvent(exit);
        exitDay4.setTickets(new HashSet<>());
        exitDay4.setSpaceLayout(exitDay4SpaceLayout);
        exitDay4.setSpace(tvrdjava);
        exitDay4.setType(EventType.Music);
        exitDay4.setName("Cetvrti dan Exit festivala");
        exitDay4.setDescription("Cetvrti dan Exit festivala u Novom Sadu");
        exitDay4.setState(EventState.ForSale);
        exitDay4.setStartDate(sdf.parse("2021-01-03 18:00"));
        exitDay4.setEndDate(sdf.parse("2021-01-04 18:00"));
        seaDanceDay1.setParentEvent(seaDance);
        seaDanceDay1.setTickets(new HashSet<>());
        seaDanceDay1.setSpaceLayout(seaDanceDay1SpaceLayout);
        seaDanceDay1.setSpace(more);
        seaDanceDay1.setType(EventType.Music);
        seaDanceDay1.setName("Prvi dan Sea Dance festivala");
        seaDanceDay1.setDescription("Prvi dan Sea Dance festivala");
        seaDanceDay1.setState(EventState.ForSale);
        seaDanceDay1.setStartDate(sdf.parse("2021-01-05 18:00"));
        seaDanceDay1.setEndDate(sdf.parse("2021-01-06 18:00"));
        seaDanceDay2.setParentEvent(seaDance);
        seaDanceDay2.setTickets(new HashSet<>());
        seaDanceDay2.setSpaceLayout(seaDanceDay2SpaceLayout);
        seaDanceDay2.setSpace(more);
        seaDanceDay2.setType(EventType.Music);
        seaDanceDay2.setName("Drugi dan Sea Dance festivala");
        seaDanceDay2.setDescription("Drugi dan Sea Dance festivala");
        seaDanceDay2.setState(EventState.ForSale);
        seaDanceDay2.setStartDate(sdf.parse("2021-01-06 18:00"));
        seaDanceDay2.setEndDate(sdf.parse("2021-01-07 18:00"));
        seaDanceDay3.setParentEvent(seaDance);
        seaDanceDay3.setTickets(new HashSet<>());
        seaDanceDay3.setSpaceLayout(seaDanceDay3SpaceLayout);
        seaDanceDay3.setSpace(more);
        seaDanceDay3.setType(EventType.Music);
        seaDanceDay3.setName("Treci dan Sea Dance festivala");
        seaDanceDay3.setDescription("Treci dan Sea Dance festivala");
        seaDanceDay3.setState(EventState.ForSale);
        seaDanceDay3.setStartDate(sdf.parse("2021-01-07 18:00"));
        seaDanceDay3.setEndDate(sdf.parse("2021-01-08 18:00"));
        HashSet<Event> exitDays = new HashSet<>();
        exitDays.add(exitDay1);
        exitDays.add(exitDay2);
        exitDays.add(exitDay3);
        exitDays.add(exitDay4);
        HashSet<Event> seaDanceDays = new HashSet<>();
        seaDanceDays.add(seaDanceDay1);
        seaDanceDays.add(seaDanceDay2);
        seaDanceDays.add(seaDanceDay3);
        HashSet<Event> exitSeadanceSet = new HashSet<>();
        exitSeadanceSet.add(exit);
        exitSeadanceSet.add(seaDance);
        exit.setChildEvents(exitDays);
        exit.setType(EventType.Music);
        exit.setName("Exit festival");
        exit.setDescription("Exit festival u Novom Sadu");
        exit.setParentEvent(exitAndSeadance);
        seaDance.setChildEvents(seaDanceDays);
        seaDance.setType(EventType.Music);
        seaDance.setName("Sea dance festival");
        seaDance.setDescription("Sea dance festival nedje u Crnoj Gori");
        seaDance.setParentEvent(exitAndSeadance);
        exitAndSeadance.setChildEvents(exitSeadanceSet);
        exitAndSeadance.setType(EventType.Music);
        exitAndSeadance.setName("Exit and seadance festivals");
        exitAndSeadance.setDescription("Exit festival u Novom Sadu i Seadance festival negde u Crnoj Gori");
        exitAndSeadance.setParentEvent(null);
        exitDay1SpaceLayout.setSpace(tvrdjava);
        exitDay1SpaceLayout.setSingleEvent(exitDay1);
        HashSet<GroundFloor> exitDay1SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay1SpaceLayoutGroundFloor = new GroundFloor();
        exitDay1SpaceLayoutGroundFloor.setSpaceLayout(exitDay1SpaceLayout);
        exitDay1SpaceLayoutGroundFloor.setCapacity(16);
        exitDay1SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay1SpaceLayoutGroundFloor.setHeight(4);
        exitDay1SpaceLayoutGroundFloor.setWidth(4);
        exitDay1SpaceLayoutGroundFloor.setPrice(2000);
        exitDay1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay1SpaceLayoutGroundFloorSet.add(exitDay1SpaceLayoutGroundFloor);
        exitDay1SpaceLayout.setGroundFloors(exitDay1SpaceLayoutGroundFloorSet);
        exitDay1SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay2SpaceLayout.setSpace(tvrdjava);
        exitDay2SpaceLayout.setSingleEvent(exitDay2);
        HashSet<GroundFloor> exitDay2SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay2SpaceLayoutGroundFloor = new GroundFloor();
        exitDay2SpaceLayoutGroundFloor.setSpaceLayout(exitDay2SpaceLayout);
        exitDay2SpaceLayoutGroundFloor.setCapacity(16);
        exitDay2SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay2SpaceLayoutGroundFloor.setHeight(4);
        exitDay2SpaceLayoutGroundFloor.setWidth(4);
        exitDay2SpaceLayoutGroundFloor.setPrice(2000);
        exitDay2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay2SpaceLayoutGroundFloorSet.add(exitDay2SpaceLayoutGroundFloor);
        exitDay2SpaceLayout.setGroundFloors(exitDay2SpaceLayoutGroundFloorSet);
        exitDay2SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay3SpaceLayout.setSpace(tvrdjava);
        exitDay3SpaceLayout.setSingleEvent(exitDay3);
        HashSet<GroundFloor> exitDay3SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay3SpaceLayoutGroundFloor = new GroundFloor();
        exitDay3SpaceLayoutGroundFloor.setSpaceLayout(exitDay3SpaceLayout);
        exitDay3SpaceLayoutGroundFloor.setCapacity(16);
        exitDay3SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay3SpaceLayoutGroundFloor.setHeight(4);
        exitDay3SpaceLayoutGroundFloor.setWidth(4);
        exitDay3SpaceLayoutGroundFloor.setPrice(2000);
        exitDay3SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay3SpaceLayoutGroundFloorSet.add(exitDay3SpaceLayoutGroundFloor);
        exitDay3SpaceLayout.setGroundFloors(exitDay3SpaceLayoutGroundFloorSet);
        exitDay3SpaceLayout.setSeatSpaces(new HashSet<>());
        exitDay4SpaceLayout.setSpace(tvrdjava);
        exitDay4SpaceLayout.setSingleEvent(exitDay4);
        HashSet<GroundFloor> exitDay4SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor exitDay4SpaceLayoutGroundFloor = new GroundFloor();
        exitDay4SpaceLayoutGroundFloor.setSpaceLayout(exitDay4SpaceLayout);
        exitDay4SpaceLayoutGroundFloor.setCapacity(16);
        exitDay4SpaceLayoutGroundFloor.setSpotsReserved(0);
        exitDay4SpaceLayoutGroundFloor.setHeight(4);
        exitDay4SpaceLayoutGroundFloor.setWidth(4);
        exitDay4SpaceLayoutGroundFloor.setPrice(2000);
        exitDay4SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        exitDay4SpaceLayoutGroundFloorSet.add(exitDay4SpaceLayoutGroundFloor);
        exitDay4SpaceLayout.setGroundFloors(exitDay4SpaceLayoutGroundFloorSet);
        exitDay4SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay1SpaceLayout.setSpace(tvrdjava);
        seaDanceDay1SpaceLayout.setSingleEvent(seaDanceDay1);
        HashSet<GroundFloor> seaDanceDay1SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay1SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay1SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay1SpaceLayout);
        seaDanceDay1SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay1SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay1SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay1SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay1SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay1SpaceLayoutGroundFloorSet.add(seaDanceDay1SpaceLayoutGroundFloor);
        seaDanceDay1SpaceLayout.setGroundFloors(seaDanceDay1SpaceLayoutGroundFloorSet);
        seaDanceDay1SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay2SpaceLayout.setSpace(tvrdjava);
        seaDanceDay2SpaceLayout.setSingleEvent(seaDanceDay2);
        HashSet<GroundFloor> seaDanceDay2SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay2SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay2SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay2SpaceLayout);
        seaDanceDay2SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay2SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay2SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay2SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay2SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay2SpaceLayoutGroundFloorSet.add(seaDanceDay2SpaceLayoutGroundFloor);
        seaDanceDay2SpaceLayout.setGroundFloors(seaDanceDay2SpaceLayoutGroundFloorSet);
        seaDanceDay2SpaceLayout.setSeatSpaces(new HashSet<>());
        seaDanceDay3SpaceLayout.setSpace(tvrdjava);
        seaDanceDay3SpaceLayout.setSingleEvent(seaDanceDay3);
        HashSet<GroundFloor> seaDanceDay3SpaceLayoutGroundFloorSet = new HashSet<>();
        GroundFloor seaDanceDay3SpaceLayoutGroundFloor = new GroundFloor();
        seaDanceDay3SpaceLayoutGroundFloor.setSpaceLayout(seaDanceDay3SpaceLayout);
        seaDanceDay3SpaceLayoutGroundFloor.setCapacity(16);
        seaDanceDay3SpaceLayoutGroundFloor.setSpotsReserved(0);
        seaDanceDay3SpaceLayoutGroundFloor.setHeight(4);
        seaDanceDay3SpaceLayoutGroundFloor.setWidth(4);
        seaDanceDay3SpaceLayoutGroundFloor.setPrice(2000);
        seaDanceDay3SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        seaDanceDay3SpaceLayoutGroundFloorSet.add(seaDanceDay3SpaceLayoutGroundFloor);
        seaDanceDay3SpaceLayout.setGroundFloors(seaDanceDay3SpaceLayoutGroundFloorSet);
        seaDanceDay3SpaceLayout.setSeatSpaces(new HashSet<>());

        Location beograd = new Location();
        Space beogradskaArena = new Space();
        HashSet<Space> bgSpaces = new HashSet<>();
        bgSpaces.add(beogradskaArena);

        beograd.setSpaces(bgSpaces);
        beograd.setName("Beograd");
        beograd.setCity("Beograd");
        beograd.setAddress("Srbija");
        beograd.setCountry("Srbija");

        SingleEvent koncertMayeBerovic = new SingleEvent();
        SpaceLayout koncertMayeBerovicSpaceLayout = new SpaceLayout();
        koncertMayeBerovic.setParentEvent(null);
        koncertMayeBerovic.setTickets(new HashSet<>());
        koncertMayeBerovic.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovic.setSpace(beogradskaArena);
        koncertMayeBerovic.setType(EventType.Music);
        koncertMayeBerovic.setName("Novi koncert mayo berovica");
        koncertMayeBerovic.setDescription("Gori nego ikad");
        koncertMayeBerovic.setState(EventState.ForSale);
        koncertMayeBerovic.setStartDate(sdf.parse("2020-11-21 18:00"));
        koncertMayeBerovic.setEndDate(sdf.parse("2020-11-22 18:00"));
        koncertMayeBerovicSpaceLayout.setSpace(beogradskaArena);
        koncertMayeBerovicSpaceLayout.setSingleEvent(koncertMayeBerovic);
        HashSet<GroundFloor> koncertMayeBerovicSpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertMayeBerovicSpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertMayeBerovicSpaceLayoutGroundFloor = new GroundFloor();
        koncertMayeBerovicSpaceLayoutGroundFloor.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutGroundFloor.setCapacity(16);
        koncertMayeBerovicSpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertMayeBerovicSpaceLayoutGroundFloor.setHeight(4);
        koncertMayeBerovicSpaceLayoutGroundFloor.setWidth(4);
        koncertMayeBerovicSpaceLayoutGroundFloor.setPrice(2000);
        koncertMayeBerovicSpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertMayeBerovicSpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertMayeBerovicSpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertMayeBerovicSpaceLayout);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setRows(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertMayeBerovicSpaceLayoutGroundFloorSet.add(koncertMayeBerovicSpaceLayoutGroundFloor);
        koncertMayeBerovicSpaceLayoutSeatSpaceSet.add(koncertMayeBerovicSpaceLayoutSeatSpaceJeftin);
        koncertMayeBerovicSpaceLayoutSeatSpaceSet.add(koncertMayeBerovicSpaceLayoutSeatSpaceSkup);
        koncertMayeBerovicSpaceLayout.setGroundFloors(koncertMayeBerovicSpaceLayoutGroundFloorSet);
        koncertMayeBerovicSpaceLayout.setSeatSpaces(koncertMayeBerovicSpaceLayoutSeatSpaceSet);

        SingleEvent koncertZvonkaBogdana1 = new SingleEvent();
        SpaceLayout koncertZvonkaBogdana1SpaceLayout = new SpaceLayout();
        SingleEvent koncertZvonkaBogdana2 = new SingleEvent();
        SpaceLayout koncertZvonkaBogdana2SpaceLayout = new SpaceLayout();
        CompositeEvent turnejaZvonkaBogdana = new CompositeEvent();
        koncertZvonkaBogdana1.setParentEvent(turnejaZvonkaBogdana);
        koncertZvonkaBogdana1.setTickets(new HashSet<>());
        koncertZvonkaBogdana1.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1.setSpace(beogradskaArena);
        koncertZvonkaBogdana1.setType(EventType.Music);
        koncertZvonkaBogdana1.setName("Koncert Zvonka Bogdana jedan");
        koncertZvonkaBogdana1.setDescription("Koncert Zvonka Bogdana jedan");
        koncertZvonkaBogdana1.setState(EventState.ForSale);
        koncertZvonkaBogdana1.setStartDate(sdf.parse("2020-11-23 18:00"));
        koncertZvonkaBogdana1.setEndDate(sdf.parse("2020-11-24 18:00"));
        koncertZvonkaBogdana2.setParentEvent(turnejaZvonkaBogdana);
        koncertZvonkaBogdana2.setTickets(new HashSet<>());
        koncertZvonkaBogdana2.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2.setSpace(beogradskaArena);
        koncertZvonkaBogdana2.setType(EventType.Music);
        koncertZvonkaBogdana2.setName("Koncert Zvonka Bogdana dva");
        koncertZvonkaBogdana2.setDescription("Koncert Zvonka Bogdana dva");
        koncertZvonkaBogdana2.setState(EventState.ForSale);
        koncertZvonkaBogdana2.setStartDate(sdf.parse("2020-12-03 18:00"));
        koncertZvonkaBogdana2.setEndDate(sdf.parse("2020-12-04 18:00"));
        HashSet<Event> koncertiZvonkaBogdana = new HashSet<>();
        koncertiZvonkaBogdana.add(koncertZvonkaBogdana1);
        koncertiZvonkaBogdana.add(koncertZvonkaBogdana2);
        turnejaZvonkaBogdana.setChildEvents(koncertiZvonkaBogdana);
        turnejaZvonkaBogdana.setType(EventType.Music);
        turnejaZvonkaBogdana.setName("Turneja Bonke Zvogdana");
        turnejaZvonkaBogdana.setDescription("Isti kao i uvek - jedan jedini - Bonko Zvogdan");
        turnejaZvonkaBogdana.setParentEvent(null);
        koncertZvonkaBogdana1SpaceLayout.setSpace(beogradskaArena);
        koncertZvonkaBogdana1SpaceLayout.setSingleEvent(koncertZvonkaBogdana1);
        HashSet<GroundFloor> koncertZvonkaBogdana1SpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertZvonkaBogdana1SpaceLayoutGroundFloor = new GroundFloor();
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setCapacity(16);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setHeight(4);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setWidth(4);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setPrice(2000);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertZvonkaBogdana1SpaceLayout);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setRows(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertZvonkaBogdana1SpaceLayoutGroundFloorSet.add(koncertZvonkaBogdana1SpaceLayoutGroundFloor);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup);
        koncertZvonkaBogdana1SpaceLayout.setGroundFloors(koncertZvonkaBogdana1SpaceLayoutGroundFloorSet);
        koncertZvonkaBogdana1SpaceLayout.setSeatSpaces(koncertZvonkaBogdana1SpaceLayoutSeatSpaceSet);
        koncertZvonkaBogdana2SpaceLayout.setSpace(beogradskaArena);
        koncertZvonkaBogdana2SpaceLayout.setSingleEvent(koncertZvonkaBogdana2);
        HashSet<GroundFloor> koncertZvonkaBogdana2SpaceLayoutGroundFloorSet = new HashSet<>();
        HashSet<SeatSpace> koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet = new HashSet<>();
        GroundFloor koncertZvonkaBogdana2SpaceLayoutGroundFloor = new GroundFloor();
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setCapacity(16);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setSpotsReserved(0);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setHeight(4);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setWidth(4);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setPrice(2000);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin = new SeatSpace();
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setRows(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setSeats(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setPrice(3000);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setTickets(new HashSet<>());
        SeatSpace koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup = new SeatSpace();
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setSpaceLayout(koncertZvonkaBogdana2SpaceLayout);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setRows(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setSeats(3);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setPrice(30000);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setTickets(new HashSet<>());
        koncertZvonkaBogdana2SpaceLayoutGroundFloorSet.add(koncertZvonkaBogdana2SpaceLayoutGroundFloor);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet.add(koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup);
        koncertZvonkaBogdana2SpaceLayout.setGroundFloors(koncertZvonkaBogdana2SpaceLayoutGroundFloorSet);
        koncertZvonkaBogdana2SpaceLayout.setSeatSpaces(koncertZvonkaBogdana2SpaceLayoutSeatSpaceSet);



        SingleEvent novi_koncertZvonkaBogdana1 = new SingleEvent();
        SingleEvent novi_koncertZvonkaBogdana2 = new SingleEvent();
        CompositeEvent nova_turnejaZvonkaBogdana = new CompositeEvent();
        novi_koncertZvonkaBogdana1.setParentEvent(nova_turnejaZvonkaBogdana);
        novi_koncertZvonkaBogdana1.setTickets(new HashSet<>());
        novi_koncertZvonkaBogdana1.setSpaceLayout(null);
        novi_koncertZvonkaBogdana1.setSpace(null);
        novi_koncertZvonkaBogdana1.setType(EventType.Music);
        novi_koncertZvonkaBogdana1.setName("Koncert Zvonka Bogdana u njegovom rodnom gradu");
        novi_koncertZvonkaBogdana1.setDescription("Koncert Zvonka Bogdana u Somboru");
        novi_koncertZvonkaBogdana1.setState(EventState.ForSale);
        novi_koncertZvonkaBogdana1.setStartDate(sdf.parse("2022-11-23 18:00"));
        novi_koncertZvonkaBogdana1.setEndDate(sdf.parse("2022-11-24 18:00"));
        novi_koncertZvonkaBogdana2.setParentEvent(nova_turnejaZvonkaBogdana);
        novi_koncertZvonkaBogdana2.setTickets(new HashSet<>());
        novi_koncertZvonkaBogdana2.setSpaceLayout(null);
        novi_koncertZvonkaBogdana2.setSpace(null);
        novi_koncertZvonkaBogdana2.setType(EventType.Music);
        novi_koncertZvonkaBogdana2.setName("Koncert Zvonka Bogdana u njegovom rodnom gradu");
        novi_koncertZvonkaBogdana2.setDescription("Koncert Zvonka Bogdana u Somboru");
        novi_koncertZvonkaBogdana2.setState(EventState.ForSale);
        novi_koncertZvonkaBogdana2.setStartDate(sdf.parse("2022-12-03 18:00"));
        novi_koncertZvonkaBogdana2.setEndDate(sdf.parse("2022-12-04 18:00"));
        HashSet<Event> novi_koncertiZvonkaBogdana = new HashSet<>();
        novi_koncertiZvonkaBogdana.add(novi_koncertZvonkaBogdana1);
        novi_koncertiZvonkaBogdana.add(novi_koncertZvonkaBogdana2);
        nova_turnejaZvonkaBogdana.setChildEvents(novi_koncertiZvonkaBogdana);
        nova_turnejaZvonkaBogdana.setType(EventType.Music);
        nova_turnejaZvonkaBogdana.setName("Turneja Bonke Zvogdana u Somboru");
        nova_turnejaZvonkaBogdana.setDescription("Isti kao i uvek - jedan jedini - Bonko Zvogdanovic");
        nova_turnejaZvonkaBogdana.setParentEvent(null);



        SingleEvent koncertLaneDelRey = new SingleEvent();
        koncertLaneDelRey.setParentEvent(null);
        koncertLaneDelRey.setTickets(new HashSet<>());
        koncertLaneDelRey.setSpaceLayout(null);
        koncertLaneDelRey.setSpace(null);
        koncertLaneDelRey.setType(EventType.Music);
        koncertLaneDelRey.setName("Novi koncert Lane del Rey");
        koncertLaneDelRey.setDescription("Not great, not terrible");
        koncertLaneDelRey.setState(EventState.ForSale);
        koncertLaneDelRey.setStartDate(sdf.parse("2020-11-26 18:00"));
        koncertLaneDelRey.setEndDate(sdf.parse("2020-11-27 18:00"));


        SingleEvent exitDay5 = new SingleEvent();
        exitDay5.setParentEvent(exit);
        exitDay5.setTickets(new HashSet<>());
        exitDay5.setSpaceLayout(null);
        exitDay5.setSpace(null);
        exitDay5.setType(EventType.Music);
        exitDay5.setName("Peti dan festivala exit");
        exitDay5.setDescription("Zbog velike potraznje otvoren je i peti dan exit festivala");
        exitDay5.setState(EventState.ForSale);
        exitDay5.setStartDate(sdf.parse("2020-01-05 18:00"));
        exitDay5.setEndDate(sdf.parse("2020-01-06 18:00"));


        beograd.setId(9940L);
        noviSad.setId(9941L);
        crnaGora.setId(9942L);

        beogradskaArena.setId(991L);
        tvrdjava.setId(992L);
        more.setId(993L);


        koncertMayeBerovicSpaceLayout.setId(995L);
        exitDay1SpaceLayout.setId(9910L);
        exitDay2SpaceLayout.setId(9913L);
        exitDay3SpaceLayout.setId(9916L);
        exitDay4SpaceLayout.setId(9919L);
        seaDanceDay1SpaceLayout.setId(9922L);
        seaDanceDay2SpaceLayout.setId(9925L);
        seaDanceDay3SpaceLayout.setId(9928L);
        koncertZvonkaBogdana1SpaceLayout.setId(9931L);
        koncertZvonkaBogdana2SpaceLayout.setId(9936L);


        koncertMayeBerovic.setId(994L);
        exitDay1.setId(999L);
        exitDay2.setId(9912L);
        exitDay3.setId(9915L);
        exitDay4.setId(9918L);
        seaDanceDay1.setId(9921L);
        seaDanceDay2.setId(9924L);
        seaDanceDay3.setId(9927L);
        koncertZvonkaBogdana1.setId(9930L);
        koncertZvonkaBogdana2.setId(9935L);
        exit.setId(9940L);
        seaDance.setId(9941L);
        exitAndSeadance.setId(9942L);
        turnejaZvonkaBogdana.setId(9943L);


        koncertMayeBerovicSpaceLayoutGroundFloor.setId(996L);
        exitDay1SpaceLayoutGroundFloor.setId(9911L);
        exitDay2SpaceLayoutGroundFloor.setId(9914L);
        exitDay3SpaceLayoutGroundFloor.setId(9917L);
        exitDay4SpaceLayoutGroundFloor.setId(9920L);
        seaDanceDay1SpaceLayoutGroundFloor.setId(9923L);
        seaDanceDay2SpaceLayoutGroundFloor.setId(9926L);
        seaDanceDay3SpaceLayoutGroundFloor.setId(9929L);
        koncertZvonkaBogdana1SpaceLayoutGroundFloor.setId(9932L);
        koncertZvonkaBogdana2SpaceLayoutGroundFloor.setId(9937L);


        koncertMayeBerovicSpaceLayoutSeatSpaceJeftin.setId(998L);
        koncertMayeBerovicSpaceLayoutSeatSpaceSkup.setId(997L);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceJeftin.setId(9934L);
        koncertZvonkaBogdana1SpaceLayoutSeatSpaceSkup.setId(9933L);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceJeftin.setId(9938L);
        koncertZvonkaBogdana2SpaceLayoutSeatSpaceSkup.setId(9939L);

        this.exitDay1 = exitDay1;
        this.exitDay2 = exitDay2;
        this.exitDay3 = exitDay3;
        this.exitDay4 = exitDay4;
        this.seaDanceDay1 = seaDanceDay1;
        this.seaDanceDay2 = seaDanceDay2;
        this.seaDanceDay3 = seaDanceDay3;
        this.exit = exit;
        this.seaDance = seaDance;
        this.exitAndSeaDance = exitAndSeadance;
        this.koncertZvonkaBogdana1 = koncertZvonkaBogdana1;
        this.koncertZvonkaBogdana2 = koncertZvonkaBogdana2;
        this.turnejaZvonkaBogdana = turnejaZvonkaBogdana;
        this.koncertMayeBerovic = koncertMayeBerovic;

        novi_koncertZvonkaBogdana1.setId(99200L);
        novi_koncertZvonkaBogdana2.setId(99201L);
        nova_turnejaZvonkaBogdana.setId(99202L);
        koncertLaneDelRey.setId(99203L);
        exitDay5.setId(99204L);
        this.add_josJedanKoncertZvonkaBogdana1 = novi_koncertZvonkaBogdana1;
        this.add_josJedanKoncertZvonkaBogdana2 = novi_koncertZvonkaBogdana2;
        this.add_josJednaTurnejaZvonkaBogdana = nova_turnejaZvonkaBogdana;
        this.add_koncertLaneDelRey = koncertLaneDelRey;
        this.add_exitDay5 = exitDay5;
    }


}
