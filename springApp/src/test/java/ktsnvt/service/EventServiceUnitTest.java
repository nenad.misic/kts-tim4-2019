package ktsnvt.service;

import ktsnvt.common.SearchMode;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.common.exceptions.UpdateException;
import ktsnvt.entity.*;
import ktsnvt.repository.*;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import javax.validation.constraints.AssertTrue;
import java.util.*;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EventServiceUnitTest {

    @Autowired
    private EventService eventService;

    @MockBean
    private CompositeEventRepository compositeEventRepository;

    @MockBean
    private SingleEventRepository singleEventRepository;

    @MockBean
    private SeatSpaceRepository seatSpaceRepository;

    @MockBean
    private GroundFloorRepository groundFloorRepository;


    @Test
    public void getByIdSingleExists() throws FetchException {
        Long id = 1L;
        SingleEvent singleEventEntity = new SingleEvent();
        int fiveDayMillis = 432000000;
        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        singleEventEntity.setSpaceLayout(null);
        singleEventEntity.setTickets(new HashSet<>());
        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.of(singleEventEntity));


        SingleEvent returnedSingleEvent = (SingleEvent) eventService.getById(id);
        Assert.assertEquals(returnedSingleEvent, singleEventEntity);

    }

    @Test
    public void getById_SingleNotExists_CompositeExists() throws FetchException {
        Long id = 1L;
        SingleEvent singleEventEntity = new SingleEvent();
        CompositeEvent compositeEventEntity = new CompositeEvent();
        int fiveDayMillis = 432000000;

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        singleEventEntity.setSpaceLayout(null);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(compositeEventEntity);
        Set<Event> singleEventsSet = new HashSet<>();
        singleEventsSet.add(singleEventEntity);

        compositeEventEntity.setChildEvents(singleEventsSet);
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(compositeEventEntity));

        Event returned = eventService.getById(id);
        Assertions.assertEquals(returned, compositeEventEntity);
        Assertions.assertEquals(returned.getClass(), CompositeEvent.class);
        Assertions.assertEquals(((CompositeEvent)returned).getChildEvents().size(), 1);


    }

    @Test(expected = FetchException.class)
    public void getById_SingleNotExists_CompositeNotExists() throws FetchException{
        Long id = 1L;
        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());
        Event returned = eventService.getById(id);
    }

    @Test
    public void getAll_Success_TwoComposite_TwoSingle() {
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntity1 = new CompositeEvent();
        CompositeEvent compositeEventEntity2 = new CompositeEvent();
        int fiveDayMillis = 432000000;

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(null);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.Cancelled);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Movie);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(null);

        compositeEventEntity1.setChildEvents(new HashSet<>());
        compositeEventEntity1.setDescription("Description composite 1");
        compositeEventEntity1.setName("Name composite 1");
        compositeEventEntity1.setType(EventType.Music);

        compositeEventEntity2.setChildEvents(new HashSet<>());
        compositeEventEntity2.setDescription("Description composite 2");
        compositeEventEntity2.setName("Name composite 2");
        compositeEventEntity2.setType(EventType.Movie);

        List<SingleEvent> singleEventList = new ArrayList<>();
        List<CompositeEvent> compositeEventList = new ArrayList<>();
        singleEventList.add(singleEventEntity1);
        singleEventList.add(singleEventEntity2);
        compositeEventList.add(compositeEventEntity1);
        compositeEventList.add(compositeEventEntity2);
        Mockito.when(singleEventRepository.findByParentEvent(null)).thenReturn(singleEventList);
        Mockito.when(compositeEventRepository.findByParentEvent(null)).thenReturn(compositeEventList);


        List<Event> returnedList = eventService.getAll();

        Assertions.assertEquals(returnedList.size(), 4);
        Assert.assertThat( returnedList, CoreMatchers.hasItem(compositeEventEntity1));
        Assert.assertThat( returnedList, CoreMatchers.hasItem(compositeEventEntity2));
        Assert.assertThat( returnedList, CoreMatchers.hasItem(singleEventEntity1));
        Assert.assertThat( returnedList, CoreMatchers.hasItem(singleEventEntity2));
    }

    @Test
    public void getAll_Success_TwoComposite_ZeroSingle() {
        CompositeEvent compositeEventEntity1 = new CompositeEvent();
        CompositeEvent compositeEventEntity2 = new CompositeEvent();


        compositeEventEntity1.setChildEvents(new HashSet<>());
        compositeEventEntity1.setDescription("Description composite 1");
        compositeEventEntity1.setName("Name composite 1");
        compositeEventEntity1.setType(EventType.Music);

        compositeEventEntity2.setChildEvents(new HashSet<>());
        compositeEventEntity2.setDescription("Description composite 2");
        compositeEventEntity2.setName("Name composite 2");
        compositeEventEntity2.setType(EventType.Movie);

        List<CompositeEvent> compositeEventList = new ArrayList<>();

        compositeEventList.add(compositeEventEntity1);
        compositeEventList.add(compositeEventEntity2);
        Mockito.when(singleEventRepository.findByParentEvent(null)).thenReturn(new ArrayList<>());
        Mockito.when(compositeEventRepository.findByParentEvent(null)).thenReturn(compositeEventList);

        List<Event> returnedList = eventService.getAll();

        Assertions.assertEquals(returnedList.size(), 2);
        Assert.assertThat( returnedList, CoreMatchers.hasItem(compositeEventEntity1));
        Assert.assertThat( returnedList, CoreMatchers.hasItem(compositeEventEntity2));
    }

    @Test
    public void getAll_Success_ZeroComposite_TwoSingle() {
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        int fiveDayMillis = 432000000;

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(null);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.Cancelled);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Movie);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(null);



        List<SingleEvent> singleEventList = new ArrayList<>();
        singleEventList.add(singleEventEntity1);
        singleEventList.add(singleEventEntity2);
        Mockito.when(singleEventRepository.findByParentEvent(null)).thenReturn(singleEventList);
        Mockito.when(compositeEventRepository.findByParentEvent(null)).thenReturn(new ArrayList<>());


        List<Event> returnedList = eventService.getAll();

        Assertions.assertEquals(returnedList.size(), 2);
        Assert.assertThat( returnedList, CoreMatchers.hasItem(singleEventEntity1));
        Assert.assertThat( returnedList, CoreMatchers.hasItem(singleEventEntity2));
    }

    @Test
    public void getAll_Success_ZeroComposite_ZeroSingle() {
        Mockito.when(singleEventRepository.findByParentEvent(null)).thenReturn(new ArrayList<>());
        Mockito.when(compositeEventRepository.findByParentEvent(null)).thenReturn(new ArrayList<>());
        List<Event> returnedList = eventService.getAll();
        Assertions.assertEquals(returnedList.size(), 0);

    }

    @Captor
    ArgumentCaptor<CompositeEvent> compositeEventArgumentCaptor;

    @Captor
    ArgumentCaptor<SingleEvent> singleEventArgumentCaptor;

    @Test
    @Transactional
    @Rollback(true)
    public void addSingleEvent() throws AddException {
        SingleEvent singleEventEntity = new SingleEvent();

        int fiveDayMillis = 432000000;

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        SpaceLayout sl = new SpaceLayout();
        sl.setSeatSpaces(new HashSet<>());
        singleEventEntity.setSpaceLayout(sl);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(null);

        eventService.add(singleEventEntity);

        Mockito.verify(singleEventRepository, Mockito.times(1)).save(singleEventEntity);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());

    }

    @Test
    @Transactional
    @Rollback(true)
    public void addCompositeEvent() throws AddException {
        CompositeEvent compositeEventEntity = new CompositeEvent();


        compositeEventEntity.setChildEvents(new HashSet<>());
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);

        Mockito.when(compositeEventRepository.save(compositeEventEntity)).thenReturn(compositeEventEntity);

        eventService.add(compositeEventEntity);


        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).save(compositeEventEntity);

    }

    @Test
    @Transactional
    @Rollback(true)
    public void addCompositeEventWithSingleEvent() throws AddException {
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntity = new CompositeEvent();
        Space space = new Space();
        space.setLocation(null);
        space.setName("test");
        space.setEvents(new HashSet<>());

        SpaceLayout spaceLayout = new SpaceLayout();

        SeatSpace seatspace = new SeatSpace();
        seatspace.setId(33L);
        seatspace.setPrice(200);
        seatspace.setSeats(3);
        seatspace.setRows(3);
        seatspace.setLeft(1);
        seatspace.setTop(1);
        seatspace.setTickets(new HashSet<>());
        seatspace.setSpaceLayout(spaceLayout);
        Set<SeatSpace> seatspaces = new HashSet<>();
        seatspaces.add(seatspace);

        GroundFloor groundfloor = new GroundFloor();
        groundfloor.setSpotsReserved(0);
        groundfloor.setId(34L);
        groundfloor.setPrice(100);
        groundfloor.setWidth(10);
        groundfloor.setHeight(10);
        groundfloor.setSpaceLayout(spaceLayout);
        groundfloor.setTop(5);
        groundfloor.setTop(5);
        groundfloor.setTickets(new HashSet<>());

        Set<GroundFloor> groundfloors = new HashSet<>();
        groundfloors.add(groundfloor);

        spaceLayout.setName("test2");
        spaceLayout.setSpace(space);
        spaceLayout.setSeatSpaces(seatspaces);
        spaceLayout.setGroundFloors(groundfloors);
        int fiveDayMillis = 432000000;

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(space);
        singleEventEntity1.setSpaceLayout(spaceLayout);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(compositeEventEntity);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.ForSale);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Music);
        singleEventEntity2.setSpace(space);
        singleEventEntity2.setSpaceLayout(spaceLayout);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(compositeEventEntity);

        Set<Event> eventSet = new HashSet<>();
        eventSet.add(singleEventEntity1);
        eventSet.add(singleEventEntity2);

        compositeEventEntity.setChildEvents(new HashSet<>());
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);
        compositeEventEntity.setChildEvents(eventSet);

        Mockito.when(compositeEventRepository.save(compositeEventEntity)).thenReturn(compositeEventEntity);
        Mockito.when(singleEventRepository.save(singleEventEntity1)).thenReturn(singleEventEntity1);
        Mockito.when(singleEventRepository.save(singleEventEntity2)).thenReturn(singleEventEntity2);
        Mockito.when(groundFloorRepository.findById(groundfloor.getId())).thenReturn(Optional.of(groundfloor));
        Mockito.when(seatSpaceRepository.findById(seatspace.getId())).thenReturn(Optional.of(seatspace));
        eventService.add(compositeEventEntity);

        // This was earlier behaviour
        // Instead of calling each repository in our code, we now rely on jpa/hibernate cascadeType.MERGE, so single repo is not called
        //Mockito.verify(singleEventRepository, Mockito.times(2)).save(singleEventArgumentCaptor.capture());

        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventArgumentCaptor.capture());

        Mockito.verify(compositeEventRepository, Mockito.times(1)).save(compositeEventEntity);
    }


    @Test
    public void deleteById_singleEvent() throws FetchException {
        Long id = 1L;

        SingleEvent singleEventEntity = new SingleEvent();

        int fiveDayMillis = 432000000;

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        singleEventEntity.setSpaceLayout(null);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(null);

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.of(singleEventEntity));

        eventService.deleteById(id);
        Assert.assertTrue(singleEventEntity.isDeleted());
        Mockito.verify(singleEventRepository, Mockito.times(1)).save(singleEventEntity);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());
    }

    @Test
    public void deleteById_compositeEvent() throws FetchException {
        Long id = 1L;
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntity = new CompositeEvent();
        int fiveDayMillis = 432000000;

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(compositeEventEntity);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.ForSale);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Music);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(compositeEventEntity);

        Set<Event> eventSet = new HashSet<>();
        eventSet.add(singleEventEntity1);
        eventSet.add(singleEventEntity2);

        compositeEventEntity.setChildEvents(new HashSet<>());
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);
        compositeEventEntity.setChildEvents(eventSet);


        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(compositeEventEntity));

        eventService.deleteById(id);


        Assert.assertTrue(compositeEventEntity.isDeleted());
        for (Event childEvent : compositeEventEntity.getChildEvents()){
            Assert.assertTrue(childEvent.isDeleted());
        }
        Mockito.verify(singleEventRepository, Mockito.times(2)).save(singleEventArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).save(compositeEventEntity);
    }

    @Test(expected = FetchException.class)
    public void deleteById_IdNotExist() throws FetchException {
        Long id = 1L;
        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        eventService.deleteById(id);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_IdNull_TestIfAddsNew() throws UpdateException, AddException {

        SingleEvent singleEventEntity = new SingleEvent();

        int fiveDayMillis = 432000000;

        singleEventEntity.setId(null);

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        SpaceLayout sl = new SpaceLayout();
        sl.setSeatSpaces(new HashSet<>());
        singleEventEntity.setSpaceLayout(sl);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(null);

        eventService.update(singleEventEntity);

        //Mockito.verify(eventService, Mockito.times(1)).add(singleEventEntity);
        Mockito.verify(singleEventRepository, Mockito.times(1)).save(singleEventEntity);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());


    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent() throws UpdateException, AddException {
        Long id = 1L;
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntity = new CompositeEvent();
        int fiveDayMillis = 432000000;

        compositeEventEntity.setId(id);

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(compositeEventEntity);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.ForSale);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Music);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(compositeEventEntity);

        Set<Event> eventSet = new HashSet<>();
        eventSet.add(singleEventEntity1);
        eventSet.add(singleEventEntity2);

        compositeEventEntity.setChildEvents(new HashSet<>());
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);
        compositeEventEntity.setChildEvents(eventSet);

        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(compositeEventEntity));

        eventService.update(compositeEventEntity);

        //Mockito.verify(eventService, Mockito.times(0)).add(compositeEventEntity);
        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).save(compositeEventArgumentCaptor.capture());


    }


    @Test
    @Transactional
    @Rollback(true)
    public void update_CompositeEvent_addedChildren() throws UpdateException, AddException {
        Long id = 1L;
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntityNoChildren = new CompositeEvent();
        CompositeEvent compositeEventEntityTwoChildren = new CompositeEvent();
        int fiveDayMillis = 432000000;

        compositeEventEntityNoChildren.setId(id);
        compositeEventEntityTwoChildren.setId(id);

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(compositeEventEntityTwoChildren);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.ForSale);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Music);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(compositeEventEntityTwoChildren);

        Set<Event> eventSet = new HashSet<>();
        eventSet.add(singleEventEntity1);
        eventSet.add(singleEventEntity2);

        compositeEventEntityTwoChildren.setChildEvents(new HashSet<>());
        compositeEventEntityTwoChildren.setDescription("Description composite");
        compositeEventEntityTwoChildren.setName("Name composite");
        compositeEventEntityTwoChildren.setType(EventType.Music);
        compositeEventEntityTwoChildren.setChildEvents(eventSet);

        compositeEventEntityNoChildren.setChildEvents(new HashSet<>());
        compositeEventEntityNoChildren.setDescription("Description composite");
        compositeEventEntityNoChildren.setName("Name composite");
        compositeEventEntityNoChildren.setType(EventType.Music);

        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(compositeEventEntityNoChildren));

        eventService.update(compositeEventEntityTwoChildren);

        //Mockito.verify(eventService, Mockito.times(0)).add(compositeEventEntityTwoChildren);
        //Mockito.verify(eventService, Mockito.times(0)).add(compositeEventEntityNoChildren);

        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).save(compositeEventEntityTwoChildren);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventEntityNoChildren);


    }

    @Test
    @Transactional
    @Rollback(true)
    public void update_SingleEvent() throws UpdateException, AddException {
        Long id = 1L;

        SingleEvent singleEventEntity = new SingleEvent();

        int fiveDayMillis = 432000000;

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        singleEventEntity.setSpaceLayout(null);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(null);
        singleEventEntity.setId(id);


        Mockito.when(singleEventRepository.existsById(id)).thenReturn(true);
        eventService.update(singleEventEntity);

        //Mockito.verify(eventService, Mockito.times(0)).add(singleEventEntity);
        Mockito.verify(singleEventRepository, Mockito.times(1)).save(singleEventEntity);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());

    }

    @Test(expected = UpdateException.class)
    @Transactional
    @Rollback(true)
    public void update_UpdateException_onSingleEvent() throws UpdateException, AddException {
        Long id = 1L;

        SingleEvent singleEventEntity = new SingleEvent();

        int fiveDayMillis = 432000000;

        singleEventEntity.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity.setState(EventState.ForSale);
        singleEventEntity.setDescription("Description");
        singleEventEntity.setName("Name");
        singleEventEntity.setType(EventType.Music);
        singleEventEntity.setSpace(null);
        singleEventEntity.setSpaceLayout(null);
        singleEventEntity.setTickets(new HashSet<>());
        singleEventEntity.setParentEvent(null);
        singleEventEntity.setId(id);


        Mockito.when(singleEventRepository.existsById(id)).thenReturn(false);
        eventService.update(singleEventEntity);

        Mockito.verify(eventService, Mockito.times(0)).add(singleEventEntity);
        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventEntity);
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());
    }

    @Test(expected = UpdateException.class)
    @Transactional
    @Rollback(true)
    public void update_UpdateException_onCompositeEvent() throws UpdateException, AddException {
        Long id = 1L;
        SingleEvent singleEventEntity1 = new SingleEvent();
        SingleEvent singleEventEntity2 = new SingleEvent();
        CompositeEvent compositeEventEntity = new CompositeEvent();
        int fiveDayMillis = 432000000;

        compositeEventEntity.setId(id);

        singleEventEntity1.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity1.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity1.setState(EventState.ForSale);
        singleEventEntity1.setDescription("Description1");
        singleEventEntity1.setName("Name1");
        singleEventEntity1.setType(EventType.Music);
        singleEventEntity1.setSpace(null);
        singleEventEntity1.setSpaceLayout(null);
        singleEventEntity1.setTickets(new HashSet<>());
        singleEventEntity1.setParentEvent(compositeEventEntity);

        singleEventEntity2.setEndDate(new Date(System.currentTimeMillis() + 2 * fiveDayMillis));
        singleEventEntity2.setStartDate(new Date(System.currentTimeMillis() + fiveDayMillis));
        singleEventEntity2.setState(EventState.ForSale);
        singleEventEntity2.setDescription("Description2");
        singleEventEntity2.setName("Name2");
        singleEventEntity2.setType(EventType.Music);
        singleEventEntity2.setSpace(null);
        singleEventEntity2.setSpaceLayout(null);
        singleEventEntity2.setTickets(new HashSet<>());
        singleEventEntity2.setParentEvent(compositeEventEntity);

        Set<Event> eventSet = new HashSet<>();
        eventSet.add(singleEventEntity1);
        eventSet.add(singleEventEntity2);

        compositeEventEntity.setChildEvents(new HashSet<>());
        compositeEventEntity.setDescription("Description composite");
        compositeEventEntity.setName("Name composite");
        compositeEventEntity.setType(EventType.Music);
        compositeEventEntity.setChildEvents(eventSet);

        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        eventService.update(compositeEventEntity);

        Mockito.verify(eventService, Mockito.times(0)).add(compositeEventEntity);
        Mockito.verify(singleEventRepository, Mockito.times(0)).save(singleEventArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(0)).save(compositeEventArgumentCaptor.capture());
    }



    @Test
    public void calculateOccupancySingleEvent() {
        int paidTickets = 15;
        int unpaidTickets = 10;

        SingleEvent se = new SingleEvent();
        se.setTickets(new HashSet<Ticket>());

        for (int i = 0; i < paidTickets; i++) {
            se.getTickets().add(createTicket(true, 3));
        }
        for (int i = 0; i < unpaidTickets; i++) {
            se.getTickets().add(createTicket(false, 3));
        }

        int retVal = this.eventService.calculateOccupancy(se);
        Assert.assertEquals(paidTickets, retVal);
    }

    @Test
    public void calculateOccupancyCompositeEvent() {
        CompositeEvent root = new CompositeEvent();
        CompositeEvent rootChild1 = new CompositeEvent();
        SingleEvent rootChild1Child1 = new SingleEvent();
        SingleEvent rootChild1Child2 = new SingleEvent();
        SingleEvent rootChild2 = new SingleEvent();

        root.setChildEvents(new HashSet<Event>());
        root.getChildEvents().add(rootChild1);
        root.getChildEvents().add(rootChild2);

        rootChild1.setChildEvents(new HashSet<Event>());
        rootChild1.getChildEvents().add(rootChild1Child1);
        rootChild1.getChildEvents().add(rootChild1Child2);

        SingleEvent[] singleEvents = {rootChild2, rootChild1Child1, rootChild1Child2};
        int[] paidTickets = {15, 20, 30};
        int[] unpaidTickets = {10, 30, 0};

        for (int i = 0; i < singleEvents.length; i++) {
            singleEvents[i].setTickets(new HashSet<Ticket>());
            for (int j = 0; j < paidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(true, 15));
            }
            for (int j = 0; j < unpaidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(false, 23));
            }
        }

        int retVal = eventService.calculateOccupancy(root);
        Assert.assertEquals(IntStream.of(paidTickets).sum(), retVal);
    }

    @Test
    public void calculateEarningsSingleEvent() {
        int paidTickets = 15;
        double price = 13.5;
        int unpaidTickets = 10;

        SingleEvent se = new SingleEvent();
        se.setTickets(new HashSet<Ticket>());

        for (int i = 0; i < paidTickets; i++) {
            se.getTickets().add(createTicket(true, price));
        }
        for (int i = 0; i < unpaidTickets; i++) {
            se.getTickets().add(createTicket(false, price));
        }

        double retVal = this.eventService.calculateEarnings(se);
        Assert.assertEquals(paidTickets * price, retVal, 0.001);
    }

    @Test
    public void calculateEarningsCompositeEvent() {
        CompositeEvent root = new CompositeEvent();
        CompositeEvent rootChild1 = new CompositeEvent();
        SingleEvent rootChild1Child1 = new SingleEvent();
        SingleEvent rootChild1Child2 = new SingleEvent();
        SingleEvent rootChild2 = new SingleEvent();

        root.setChildEvents(new HashSet<Event>());
        root.getChildEvents().add(rootChild1);
        root.getChildEvents().add(rootChild2);

        rootChild1.setChildEvents(new HashSet<Event>());
        rootChild1.getChildEvents().add(rootChild1Child1);
        rootChild1.getChildEvents().add(rootChild1Child2);

        SingleEvent[] singleEvents = {rootChild2, rootChild1Child1, rootChild1Child2};
        int[] paidTickets = {15, 20, 30};
        int[] unpaidTickets = {10, 30, 0};
        double price = 13.5;


        for (int i = 0; i < singleEvents.length; i++) {
            singleEvents[i].setTickets(new HashSet<Ticket>());
            for (int j = 0; j < paidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(true, price));
            }
            for (int j = 0; j < unpaidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(false, price));
            }
        }

        double retVal = eventService.calculateEarnings(root);
        Assert.assertEquals(IntStream.of(paidTickets).sum() * price, retVal, 0.001);
    }

    @Test
    public void reportOccupancyOfChildrenFailedDoesNotExist() {
        Long id = 1l;

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        FetchException fetchException = assertThrows(FetchException.class, () -> {
           eventService.reportOccupancyOfChildren(id);
        });
        Assert.assertEquals("Unable to find event with ID: 1", fetchException.getMessage());
    }

    @Test
    public void reportOccupancyOfChildrenSingleEvent() throws FetchException {
        int paidTickets = 15;
        int unpaidTickets = 10;
        Long id = 1l;
        String name = "SeTestName";

        SingleEvent se = new SingleEvent();
        se.setName(name);
        se.setId(id);
        se.setTickets(new HashSet<Ticket>());

        for (int i = 0; i < paidTickets; i++) {
            se.getTickets().add(createTicket(true, 3));
        }
        for (int i = 0; i < unpaidTickets; i++) {
            se.getTickets().add(createTicket(false, 3));
        }

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.of(se));
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        Map<String, Object> retVal = eventService.reportOccupancyOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("tickets bought", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(name));
        Assert.assertEquals(paidTickets, dataMap.get(name));
    }


    @Test
    public void reportOccupancyOfChildrenCompositeEvent() throws FetchException {
        Long id = 1l;
        String rootName = "root";
        String child1Name = "rootChild1";
        String child2Name = "rootChild2";

        CompositeEvent root = new CompositeEvent();
        CompositeEvent rootChild1 = new CompositeEvent();
        SingleEvent rootChild1Child1 = new SingleEvent();
        SingleEvent rootChild1Child2 = new SingleEvent();
        SingleEvent rootChild2 = new SingleEvent();

        root.setId(id);
        root.setName(rootName);
        root.setChildEvents(new HashSet<Event>());
        root.getChildEvents().add(rootChild1);
        root.getChildEvents().add(rootChild2);

        rootChild1.setChildEvents(new HashSet<Event>());
        rootChild1.getChildEvents().add(rootChild1Child1);
        rootChild1.getChildEvents().add(rootChild1Child2);

        rootChild1.setName(child1Name);
        rootChild2.setName(child2Name);

        SingleEvent[] singleEvents = {rootChild2, rootChild1Child1, rootChild1Child2};
        int[] paidTickets = {15, 20, 30};
        int[] unpaidTickets = {10, 30, 0};

        for (int i = 0; i < singleEvents.length; i++) {
            singleEvents[i].setTickets(new HashSet<Ticket>());
            for (int j = 0; j < paidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(true, 15));
            }
            for (int j = 0; j < unpaidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(false, 23));
            }
        }

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(root));

        Map<String, Object> retVal = eventService.reportOccupancyOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("tickets bought", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals(rootName, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(child1Name));
        Assert.assertEquals(paidTickets[1] + paidTickets[2], dataMap.get(child1Name));

        Assert.assertTrue(dataMap.containsKey(child2Name));
        Assert.assertEquals(paidTickets[0], dataMap.get(child2Name));
    }


    @Test
    public void reportEarningsOfChildrenFailedDoesNotExist() {
        Long id = 1l;

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        FetchException fetchException = assertThrows(FetchException.class, () -> {
            eventService.reportEarningsOfChildren(id);
        });
        Assert.assertEquals("Unable to find event with ID: 1", fetchException.getMessage());
    }

    @Test
    public void reportEarningsOfChildrenSingleEvent() throws FetchException {
        int paidTickets = 15;
        int unpaidTickets = 10;
        double price = 13.5;
        Long id = 1l;
        String name = "SeTestName";

        SingleEvent se = new SingleEvent();
        se.setName(name);
        se.setId(id);
        se.setTickets(new HashSet<Ticket>());

        for (int i = 0; i < paidTickets; i++) {
            se.getTickets().add(createTicket(true, price));
        }
        for (int i = 0; i < unpaidTickets; i++) {
            se.getTickets().add(createTicket(false, price));
        }

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.of(se));
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.empty());

        Map<String, Object> retVal = eventService.reportEarningsOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("earnings(dollars)", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals( name, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(name));
        Assert.assertEquals(paidTickets * price, dataMap.get(name));
    }

    @Test
    public void reportEarningsOfChildrenCompositeEvent() throws FetchException {
        Long id = 1l;
        String rootName = "root";
        String child1Name = "rootChild1";
        String child2Name = "rootChild2";

        CompositeEvent root = new CompositeEvent();
        CompositeEvent rootChild1 = new CompositeEvent();
        SingleEvent rootChild1Child1 = new SingleEvent();
        SingleEvent rootChild1Child2 = new SingleEvent();
        SingleEvent rootChild2 = new SingleEvent();

        root.setId(id);
        root.setName(rootName);
        root.setChildEvents(new HashSet<Event>());
        root.getChildEvents().add(rootChild1);
        root.getChildEvents().add(rootChild2);

        rootChild1.setChildEvents(new HashSet<Event>());
        rootChild1.getChildEvents().add(rootChild1Child1);
        rootChild1.getChildEvents().add(rootChild1Child2);

        rootChild1.setName(child1Name);
        rootChild2.setName(child2Name);

        SingleEvent[] singleEvents = {rootChild2, rootChild1Child1, rootChild1Child2};
        int[] paidTickets = {15, 20, 30};
        int[] unpaidTickets = {10, 30, 0};
        double price = 13.5;

        for (int i = 0; i < singleEvents.length; i++) {
            singleEvents[i].setTickets(new HashSet<Ticket>());
            for (int j = 0; j < paidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(true, price));
            }
            for (int j = 0; j < unpaidTickets[i]; j++) {
                singleEvents[i].getTickets().add(createTicket(false, price));
            }
        }

        Mockito.when(singleEventRepository.findById(id)).thenReturn(Optional.empty());
        Mockito.when(compositeEventRepository.findById(id)).thenReturn(Optional.of(root));

        Map<String, Object> retVal = eventService.reportEarningsOfChildren(id);

        Assert.assertTrue(retVal.containsKey("unit"));
        Assert.assertEquals("earnings(dollars)", retVal.get("unit"));

        Assert.assertTrue(retVal.containsKey("title"));
        Assert.assertEquals(rootName, retVal.get("title"));

        Assert.assertTrue(retVal.containsKey("data"));
        Map<String, Object> dataMap = (Map<String, Object>) retVal.get("data");

        Assert.assertTrue(dataMap.containsKey(child1Name));
        Assert.assertEquals((double)(paidTickets[1] + paidTickets[2]) * price, (double)dataMap.get(child1Name), 0.001);

        Assert.assertTrue(dataMap.containsKey(child2Name));
        Assert.assertEquals((double)(paidTickets[0] * price), (double)dataMap.get(child2Name), 0.001);
    }


    @Captor
    ArgumentCaptor<Example<CompositeEvent>> compositeEventExampleArgumentCaptor;

    @Captor
    ArgumentCaptor<Example<SingleEvent>> singleEventExamplArgumentCaptor;

    @Transactional
    @Test
    @Rollback(true)
    public void search_Quick() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        sl.setName("ExiT");
        s.setName("ExiT");
        l.setName("ExiT");
        l.setAddress("ExiT");
        l.setCity("ExiT");
        l.setCountry("ExiT");
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("ExiT");
        se.setDescription("ExiT");

        ce.setName("ExiT");
        ce.setDescription("ExiT");


        Mockito.when(compositeEventRepository.findAll(any(Example.class))).thenReturn(new ArrayList());
        Mockito.when(singleEventRepository.findAll(any(Example.class))).thenReturn(new ArrayList());


        this.eventService.search(se, ce, SearchMode.Quick);
        Mockito.verify(singleEventRepository, Mockito.times(1)).findAll(singleEventExamplArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).findAll(compositeEventExampleArgumentCaptor.capture());
    }

    @Transactional
    @Test
    @Rollback(true)
    public void search_Regular() {
        SingleEvent se = new SingleEvent();
        CompositeEvent ce = new CompositeEvent();
        SpaceLayout sl = new SpaceLayout();
        Space s = new Space();
        Location l = new Location();
        s.setLocation(l);
        sl.setName("SeADanCe");
        s.setName("SeADanCe");
        l.setName("SeADanCe");
        l.setAddress("SeADanCe");
        l.setCity("SeADanCe");
        l.setCountry("SeADanCe");
        se.setSpace(s);
        se.setSpaceLayout(sl);
        se.setName("SeADanCe");
        se.setDescription("SeADanCe");

        ce.setName("SeADanCe");
        ce.setDescription("SeADanCe");


        Mockito.when(compositeEventRepository.findAll(any(Example.class))).thenReturn(new ArrayList());
        Mockito.when(singleEventRepository.findAll(any(Example.class))).thenReturn(new ArrayList());


        this.eventService.search(se, ce, SearchMode.Regular);
        Mockito.verify(singleEventRepository, Mockito.times(1)).findAll(singleEventExamplArgumentCaptor.capture());
        Mockito.verify(compositeEventRepository, Mockito.times(1)).findAll(compositeEventExampleArgumentCaptor.capture());
    }

    private Ticket createTicket(boolean paid, double price) {
        Ticket retVal = new Ticket();
        retVal.setPaid(paid);
        retVal.setPrice(price);
        return retVal;
    }
}
