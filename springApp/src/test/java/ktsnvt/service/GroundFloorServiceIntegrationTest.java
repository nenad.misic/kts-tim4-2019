package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class GroundFloorServiceIntegrationTest extends CrudServiceGenericIntegrationTest<GroundFloorService, GroundFloorRepository, GroundFloor>  {

    private DataGraph dataGraph;

    public GroundFloorServiceIntegrationTest() {this.dataGraph = TestUtil.extractDataFromDataSql(); }

    @Test
    @Transactional
    public void failedDeleteTiedEvent() throws FetchException {
        Long id = getIdHasEvent();
        GroundFloor groundFloor = crudService.getById(id);
        assertNotNull(groundFloor);
        assertNotNull(groundFloor.getSpaceLayout().getSingleEvent());
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
           crudService.deleteById(id);
        });
        assertEquals("Cannot delete GroundFloor with id 9929 GroundFloor is tied to an event", deleteException.getMessage());
        GroundFloor afterTriedDelete = crudService.getById(id);
        assertNotNull(afterTriedDelete);
        List<GroundFloor> groundFloors = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb(), groundFloors.size());
    }

    public Long getIdHasEvent() {
        return 9929l;
    }

    public Long getIdNoEvent() {
        return 99315l;
    }


    @Override
    public Long getIdNotExists() {
        return 6969l;
    }

    @Override
    public Long getIdExists() {
        return this.dataGraph.getGroundFloors().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        for(GroundFloor groundFloor: dataGraph.getGroundFloors()) {
            if (!groundFloor.isDeleted() && groundFloor.getSpaceLayout().getSingleEvent() == null) {
                return groundFloor.getId();
            }
        }
        return null;
    }


    @Override
    public Long getIdFromEntity(GroundFloor groundFloor) {
        return groundFloor.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getGroundFloors().size();
    }

    @Override
    public GroundFloor createSampleValue() {
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setTickets(new HashSet<Ticket>());
        groundFloor.setName("TestName");
        groundFloor.setCapacity(20);
        groundFloor.setPrice(20.0);
        groundFloor.setSpotsReserved(0);
        return groundFloor;
    }

    @Override
    public void changeEntitiesAttributes(GroundFloor groundFloor) {
        groundFloor.setSpotsReserved(99);
    }

    @Override
    public boolean verifyChangedAttributes(GroundFloor groundFloor) {
        return 99 == groundFloor.getSpotsReserved();
    }

    @Override
    public boolean entityEquals(GroundFloor e1, GroundFloor e2) {
        if (e1 == e2) return true;
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (!e1.getCapacity().equals(e2.getCapacity())) {
            return false;
        }
        if (e1.getHeight() != e2.getHeight()) {
            return false;
        }
        if (e1.getWidth() != e2.getWidth()) {
            return false;
        }
        if (e1.getPrice() != e2.getPrice()) {
            return false;
        }
        if (!e1.getSpotsReserved().equals(e2.getSpotsReserved())) {
            return false;
        }
        ArrayList<Ticket> tickets1 = new ArrayList<Ticket>(e1.getTickets());
        ArrayList<Ticket> tickets2 = new ArrayList<Ticket>(e2.getTickets());
        tickets1.sort(ticketSorter);
        tickets2.sort(ticketSorter);
        if (tickets1.size() != tickets2.size()) {
            return false;
        }
        for (int i = 0; i < tickets1.size(); i++) {
            if (!tickets1.get(i).getId().equals(tickets2.get(i).getId())) {
                return false;
            }
        }
        if (e1.getSpaceLayout() == null && e2.getSpaceLayout() == null) {
            return true;
        }
        if (!e1.getSpaceLayout().getId().equals(e2.getSpaceLayout().getId())) {
            return false;
        }
        return true;
    }


    private Comparator<Ticket> ticketSorter = Comparator.comparing(Ticket::getId);

    @Override
    public GroundFloor getEntityFromIdExists() {
        Long id = getIdExists();
        return this.dataGraph.findGroundFloorById(id);
    }

    @Override
    public void setEntityId(GroundFloor groundFloor, Long id) {
        groundFloor.setId(id);
    }

    @Override
    public GroundFloor getEntityFromDataGraph(Long id) {
        return this.dataGraph.findGroundFloorById(id);
    }
}
