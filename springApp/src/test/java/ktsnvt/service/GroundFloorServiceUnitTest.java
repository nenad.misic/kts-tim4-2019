package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.EventState;
import ktsnvt.entity.GroundFloor;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GroundFloorServiceUnitTest extends CrudServiceGenericUnitTest<GroundFloorService,
        GroundFloorRepository, GroundFloor> {

    @MockBean
    private TicketRepository ticketRepositoryMocked;

    @MockBean
    private GroundFloorRepository mockedRepo;

    @Override
    public void successfulDelete() throws DeleteException, FetchException {
        Long id = 1l;
        GroundFloor groundFloor = createSampleValue();
        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.of(groundFloor));
        when(ticketRepositoryMocked.existsBySingleEvent_StateAndGroundFloorId(EventState.ForSale, id)).thenReturn(false);
        crudService.deleteById(id);
        verify(mockedRepo, times(1)).deleteById(id);
    }

    @Test
    public void failedDeleteTiedEvent() {
        Long id = 1l;
        GroundFloor groundFloor = createSampleValue();
        groundFloor.getSpaceLayout().setSingleEvent(new SingleEvent());
        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.of(groundFloor));
        when(ticketRepositoryMocked.existsBySingleEvent_StateAndGroundFloorId(EventState.ForSale, id)).thenReturn(true);
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete GroundFloor with id 1 GroundFloor is tied to an event", deleteException.getMessage());
    }



    @Override
    public GroundFloor createSampleValue() {
        GroundFloor retVal = new GroundFloor();
        retVal.setSpaceLayout(new SpaceLayout());
        return retVal;
    }

    @Override
    public GroundFloorRepository getMockedRepo() {
        return mockedRepo;
    }

}
