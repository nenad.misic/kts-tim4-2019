package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.*;
import ktsnvt.repository.LocationRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class LocationServiceIntegrationTest extends CrudServiceGenericIntegrationTest<LocationService, LocationRepository, Location> {

    private DataGraph dataGraph;

    public LocationServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }

    @Test
    @Transactional
    public void failedDeleteLocationHasSpaces() throws FetchException {
        Long id = getIdLocationWithSpaces();
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete Location with id 99: Location has relationships to one or more Spaces", deleteException.getMessage());
        Location location = crudService.getById(id);
        assertNotNull(location);
    }

    @Test
    @Transactional
    public void failedOccupancyReportLocationNotExist() {
        Long id = getIdNotExists();
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.reportOccupancyDuringTimePeriod(id, null, null);
        });
        assertEquals("Unable to find location with ID: " + id, fetchException.getMessage());
    }

    @Test
    @Transactional
    public void successfulOccupancyReportNoVisitorsInPeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = getIdLocationWithEvents();
        Location location = dataGraph.findLocationById(id);

        // No events in this time period
        Date startDate = sdf.parse("01/01/2015");
        Date endDate = sdf.parse("02/01/2015");

        Map<String, Object> retVal = crudService.reportOccupancyDuringTimePeriod(id, startDate, endDate);

        assertEquals(String.format("%s visitors for time period: %s-%s",
                location.getName(), "01.01.2015", "02.01.2015"), retVal.get("title"));
        assertEquals("tickets bought", retVal.get("unit"));
        Map<String, Object> data = (Map<String, Object>) retVal.get("data");
        assertEquals(0, data.size());
    }

    @Test
    @Transactional
    public void successfulOccupancyReportSomeVisitorsInPeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = getIdLocationWithEvents();
        Location location = dataGraph.findLocationById(id);

        // Exit festival in this time period
        Date startDate = sdf.parse("31/12/2020");
        Date endDate = sdf.parse("04/01/2021");

        Map<String, Object> retVal = crudService.reportOccupancyDuringTimePeriod(id, startDate, endDate);

        assertEquals(String.format("%s visitors for time period: %s-%s",
                location.getName(), "31.12.2020", "04.01.2021"), retVal.get("title"));
        assertEquals("tickets bought", retVal.get("unit"));
        Map<String, Object> data = (Map<String, Object>) retVal.get("data");
        assertEquals(2, data.size());
        assertEquals(9, data.get("January/2021"));
        assertEquals(4, data.get("December/2020"));
    }

    @Test
    @Transactional
    public void failedEarningsReportLocationNotExist() {
        Long id = getIdNotExists();
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.reportEarningsDuringTimePeriod(id, null, null);
        });
        assertEquals("Unable to find location with ID: " + id, fetchException.getMessage());
    }

    @Test
    @Transactional
    public void successfulEarningsReportNoVisitorsInTimePeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = getIdLocationWithEvents();
        Location location = dataGraph.findLocationById(id);

        // No events in this time period
        Date startDate = sdf.parse("01/01/2015");
        Date endDate = sdf.parse("02/01/2015");

        Map<String, Object> retVal = crudService.reportEarningsDuringTimePeriod(id, startDate, endDate);

        assertEquals(String.format("%s earnings for time period: %s-%s",
                location.getName(), "01.01.2015", "02.01.2015"), retVal.get("title"));
        assertEquals("earnings(dollars)", retVal.get("unit"));
        Map<String, Object> data = (Map<String, Object>) retVal.get("data");
        assertEquals(0, data.size());
    }

    @Test
    @Transactional
    public void successfulEarningsReportSomeVisitorsInTimePeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = getIdLocationWithEvents();
        Location location = dataGraph.findLocationById(id);

        // Exit festival in this time period
        Date startDate = sdf.parse("31/12/2020");
        Date endDate = sdf.parse("04/01/2021");

        Map<String, Object> retVal = crudService.reportEarningsDuringTimePeriod(id, startDate, endDate);

        assertEquals(String.format("%s earnings for time period: %s-%s",
                location.getName(), "31.12.2020", "04.01.2021"), retVal.get("title"));
        assertEquals("earnings(dollars)", retVal.get("unit"));
        Map<String, Object> data = (Map<String, Object>) retVal.get("data");
        assertEquals(2, data.size());
        assertEquals(18000.0, data.get("January/2021"));
        assertEquals(8000.0, data.get("December/2020"));
    }

    @Test
    @Transactional
    public void findPageByName() {
        String searchString = "ad";
        List<Location> allMatching = dataGraph.getLocations()
                .stream().filter((value) -> value.getName().toLowerCase().contains(searchString))
                .collect(Collectors.toList());
        Page<Location> firstPage = crudService.findPageByName(searchString, PageRequest.of(0, allMatching.size()));
        Page<Location> secondPage = crudService.findPageByName(searchString, PageRequest.of(1, allMatching.size()));
        assertEquals(allMatching.size(), firstPage.getNumberOfElements());
        assertEquals(0, secondPage.getNumberOfElements());
    }

    public Long getIdLocationWithEvents() {
        return 9941l;
    }

    @Override
    public Long getIdNotExists() {
        return 6969l;
    }

    @Override
    public Long getIdExists() {
        return this.dataGraph.getLocations().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        return dataGraph.getLocations().stream()
                .filter((location) -> location.getSpaces().size() == 0).findFirst().orElse(null).getId();
    }

    public Long getIdLocationWithSpaces() {
        return dataGraph.getLocations().stream()
                .filter((location) -> location.getSpaces().size() != 0).findFirst().orElse(null).getId();
    }

    @Override
    public Long getIdFromEntity(Location location) {
        return location.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return 8;
    }

    @Override
    public Location createSampleValue() {
        Location retVal = new Location();
        retVal.setCity("SampleCity");
        retVal.setSpaces(new HashSet<Space>());
        retVal.setAddress("SampleAddress");
        retVal.setCountry("SampleCountry");
        retVal.setName("SampleName");
        retVal.setDeleted(false);
        retVal.setSpaces(new HashSet<Space>());
        Space space = new Space();
        space.setId(6969l);
        retVal.getSpaces().add(space);
        return  retVal;
    }

    @Override
    public void changeEntitiesAttributes(Location location) {
        location.setCity("IzmenjeniGrad");
        location.setAddress("IzmenjenaAdresa");
        location.setName("IzmenjeniNaziv");
        location.setCountry("IzmenjenaDrzava");
    }

    @Override
    public boolean verifyChangedAttributes(Location location) {
        if (!"IzmenjeniGrad".equals(location.getCity())) {
            return false;
        }
        if (!"IzmenjenaAdresa".equals(location.getAddress())) {
            return false;
        }
        if (!"IzmenjeniNaziv".equals(location.getName())) {
            return false;
        }
        if (!"IzmenjenaDrzava".equals(location.getCountry())) {
            return false;
        }
        return true;
    }

    @Override
    public boolean entityEquals(Location e1, Location e2) {
        if (e1 == e2) return true;
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (!e1.getCity().equals(e2.getCity())) {
            return false;
        }
        if (!e1.getAddress().equals(e2.getAddress())) {
            return false;
        }
        if (!e1.getCountry().equals(e2.getCountry())) {
            return false;
        }
        if (!e1.getName().equals(e2.getName())) {
            return false;
        }
        if (e1.getSpaces().size() != e2.getSpaces().size()) {
            return false;
        }
        List<Space> e1List = new ArrayList<Space>(e1.getSpaces());
        List<Space> e2List = new ArrayList<Space>(e2.getSpaces());
        e1List.sort(spaceSorter);
        e2List.sort(spaceSorter);
        for (int i = 0; i < e1.getSpaces().size(); i++) {
            if (!e1List.get(i).getId().equals(e2List.get(i).getId())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Location getEntityFromIdExists() {
        return this.dataGraph.findLocationById(getIdExists());
    }

    @Override
    public void setEntityId(Location location, Long id) {
        location.setId(id);
    }

    @Override
    public Location getEntityFromDataGraph(Long id) {
        return this.dataGraph.findLocationById(id);
    }

    private Comparator<Space> spaceSorter = Comparator.comparing(Space::getId);

}
