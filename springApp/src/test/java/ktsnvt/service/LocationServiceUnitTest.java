package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.Location;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Space;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.LocationRepository;
import ktsnvt.repository.SingleEventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.data.domain.Pageable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LocationServiceUnitTest extends CrudServiceGenericUnitTest<LocationService, LocationRepository, Location> {


    @MockBean
    private LocationRepository mockedRepo;

    @MockBean
    private SingleEventRepository mockedSingleEventRepo;

    @Override
    public Location createSampleValue() {
        Location retVal = new Location();
        retVal.setSpaces(new HashSet<Space>());
        return retVal;
    }

    private Location createSampleValueWithSpaces() {
        Location retVal = createSampleValue();
        retVal.getSpaces().add(new Space());
        return retVal;
    }


    @Override
    public LocationRepository getMockedRepo() {
        return mockedRepo;
    }

    @Override
    public void successfulDelete() throws DeleteException, FetchException {
        Long id = 1l;
        Location location = createSampleValue();
        when(mockedRepo.findById(id)).thenReturn(Optional.of(location));
        crudService.repository = mockedRepo;
        crudService.deleteById(id);
        verify(mockedRepo, times(1)).deleteById(id);
    }

    @Test
    public void failedDeleteLocationHasSpaces() {
        Long id = 1l;
        Location location = createSampleValueWithSpaces();
        when(mockedRepo.findById(id)).thenReturn(Optional.of(location));
        crudService.repository = mockedRepo;
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete Location with id 1: Location has relationships to one or more Spaces", deleteException.getMessage());
    }

    @Test
    public void failedEarningsReportLocationNotExist() {
        Long id = 1l;
        Location location = createSampleValue();
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());
        crudService.repository = mockedRepo;
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.reportEarningsDuringTimePeriod(id, null, null);
        });
        assertEquals("Unable to find location with ID: 1", fetchException.getMessage());
    }

    @Test
    public void successfulEarningsReportNoEarningsInPeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = 1l;
        Date startDate = sdf.parse("01/02/2015");
        Date endDate = sdf.parse("30/4/2015");

        Location location = createSampleValue();
        location.setName("TestLocationName");

        Space space1 = new Space();
        space1.setId(2l);
        space1.setName("TestSpace1");
        location.getSpaces().add(space1);

        Space space2= new Space();
        space2.setId(3l);
        space2.setName("TestSpace2");
        location.getSpaces().add(space2);

        when(this.mockedRepo.findById(id)).thenReturn(Optional.of(location));
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space1.getId(), startDate, endDate))
                .thenReturn(new ArrayList<SingleEvent>());
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space2.getId(), startDate, endDate))
                .thenReturn(new ArrayList<SingleEvent>());

        Map<String, Object> result = this.crudService.reportEarningsDuringTimePeriod(id, startDate, endDate);

        assertEquals("TestLocationName earnings for time period: 01.02.2015-30.04.2015", result.get("title"));
        assertEquals("earnings(dollars)", result.get("unit"));
        Map<String, Object> data = (Map<String, Object>) result.get("data");
        assertEquals(0, data.size());
    }


    @Test
    public void successfulEarningsReportSomeEarningsInPeriod() throws FetchException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = 1l;
        Date startDate = sdf.parse("01/02/2015");
        Date endDate = sdf.parse("31/5/2015");
        Ticket ticket;

        Location location = createSampleValue();
        location.setName("TestLocationName");


        Space space1 = new Space();
        space1.setId(2l);
        space1.setName("TestSpace1");
        location.getSpaces().add(space1);

        SingleEvent space1Event1 = new SingleEvent(); // total earnings: 45
        space1Event1.setStartDate(sdf.parse("27/02/2015"));
        space1Event1.setTickets(new HashSet<Ticket>());

        space1Event1.getTickets().add( createPaidTicket(5l, 10));
        space1Event1.getTickets().add(createPaidTicket(6l, 15));
        space1Event1.getTickets().add(createPaidTicket(7l, 20));
        space1Event1.getTickets().add(createUnpaidTicket(8l)); // should not factor into price

        SingleEvent space1Event2 = new SingleEvent(); // total earnings 37
        space1Event2.setStartDate(sdf.parse("01/03/2015"));
        space1Event2.setTickets(new HashSet<Ticket>());

        space1Event2.getTickets().add( createPaidTicket(9l, 37));
        space1Event2.getTickets().add(createUnpaidTicket(10l)); // should not factor into price

        SingleEvent space1Event3 = new SingleEvent(); //total earnings 0
        space1Event3.setStartDate(sdf.parse("01/03/2015"));
        space1Event3.setTickets(new HashSet<Ticket>());

        space1Event3.getTickets().add(createUnpaidTicket(11l)); // should not factor into price

        List<SingleEvent> space1Events = new ArrayList<SingleEvent>() {
            {
                add(space1Event1);
                add(space1Event2);
                add(space1Event3);
            }
        };

        Space space2= new Space();
        space2.setId(3l);
        space2.setName("TestSpace2");
        location.getSpaces().add(space2);

        SingleEvent space2Event1 = new SingleEvent(); // total earnings 0
        space2Event1.setStartDate(sdf.parse("30/02/2015"));
        space2Event1.setTickets(new HashSet<Ticket>());

        SingleEvent space2Event2 = new SingleEvent(); // total earnings 3
        space2Event2.setStartDate(sdf.parse("25/03/2015"));
        space2Event2.setTickets(new HashSet<Ticket>());

        space2Event2.getTickets().add(createPaidTicket(12l, 3));

        SingleEvent space2Event3 = new SingleEvent(); // total earnings 69
        space2Event3.setStartDate(sdf.parse("02/04/2015"));
        space2Event3.setTickets(new HashSet<Ticket>());

        space2Event3.getTickets().add(createPaidTicket(13l, 69));
        space2Event3.getTickets().add(createUnpaidTicket(14l)); // should not factor into price

        List<SingleEvent> space2Events = new ArrayList<SingleEvent>() {
            {
                add(space2Event1);
                add(space2Event2);
                add(space2Event3);
            }
        };

        when(this.mockedRepo.findById(id)).thenReturn(Optional.of(location));
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space1.getId(), startDate, endDate))
                .thenReturn(space1Events);
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space2.getId(), startDate, endDate))
                .thenReturn(space2Events);

        Map<String, Object> result = this.crudService.reportEarningsDuringTimePeriod(id, startDate, endDate);
        Map<String, Object> data = (Map<String, Object>) result.get("data");

        assertEquals("TestLocationName earnings for time period: 01.02.2015-31.05.2015", result.get("title"));
        assertEquals("earnings(dollars)", result.get("unit"));
        assertEquals(3, data.size());

        Map<String, Object> expected = new HashMap<String, Object>() {
            {
                put("March/2015", new Double(40));
                put("April/2015", new Double(69));
                put("February/2015", new Double(45));
            }
        };

        for(String key: data.keySet()) {
            assertEquals(expected.get(key), data.get(key));
        }
    }


    @Test
    public void failedOccupancyReportLocationNotExist() {
        Long id = 1l;
        Location location = createSampleValue();
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());
        crudService.repository = mockedRepo;
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.reportOccupancyDuringTimePeriod(id, null, null);
        });
        assertEquals("Unable to find location with ID: 1", fetchException.getMessage());
    }

    @Test
    public void successfulOccupancyReportNoVisitorsInPeriod() throws ParseException, FetchException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = 1l;
        Date startDate = sdf.parse("01/02/2015");
        Date endDate = sdf.parse("30/4/2015");

        Location location = createSampleValue();
        location.setName("TestLocationName");

        Space space1 = new Space();
        space1.setId(2l);
        space1.setName("TestSpace1");
        location.getSpaces().add(space1);

        Space space2= new Space();
        space2.setId(3l);
        space2.setName("TestSpace2");
        location.getSpaces().add(space2);

        when(this.mockedRepo.findById(id)).thenReturn(Optional.of(location));
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space1.getId(), startDate, endDate))
                .thenReturn(new ArrayList<SingleEvent>());
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space2.getId(), startDate, endDate))
                .thenReturn(new ArrayList<SingleEvent>());

        Map<String, Object> result = this.crudService.reportOccupancyDuringTimePeriod(id, startDate, endDate);

        assertEquals("TestLocationName visitors for time period: 01.02.2015-30.04.2015", result.get("title"));
        assertEquals("tickets bought", result.get("unit"));
        Map<String, Object> data = (Map<String, Object>) result.get("data");
        assertEquals(0, data.size());
    }

    @Test
    public void successfulOccupancyReportSomeVisitorsInPeriod() throws FetchException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Long id = 1l;
        Date startDate = sdf.parse("01/02/2015");
        Date endDate = sdf.parse("31/5/2015");
        Ticket ticket;

        Location location = createSampleValue();
        location.setName("TestLocationName");


        Space space1 = new Space();
        space1.setId(2l);
        space1.setName("TestSpace1");
        location.getSpaces().add(space1);

        SingleEvent space1Event1 = new SingleEvent(); // total occupancy: 5
        space1Event1.setStartDate(sdf.parse("27/02/2015"));
        space1Event1.setTickets(new HashSet<Ticket>());

        space1Event1.getTickets().add(createPaidTicket(5l, 10));
        space1Event1.getTickets().add(createPaidTicket(6l, 15));
        space1Event1.getTickets().add(createPaidTicket(7l, 20));
        space1Event1.getTickets().add(createPaidTicket(8l, 10));
        space1Event1.getTickets().add(createPaidTicket(9l, 10));
        space1Event1.getTickets().add(createUnpaidTicket(11l));
        space1Event1.getTickets().add(createUnpaidTicket(12l));
        space1Event1.getTickets().add(createUnpaidTicket(13l));

        SingleEvent space1Event2 = new SingleEvent(); // total occupancy 4
        space1Event2.setStartDate(sdf.parse("01/03/2015"));
        space1Event2.setTickets(new HashSet<Ticket>());

        space1Event2.getTickets().add(createPaidTicket(14l, 1));
        space1Event2.getTickets().add(createPaidTicket(15l, 2));
        space1Event2.getTickets().add(createPaidTicket(16l, 3));
        space1Event2.getTickets().add(createPaidTicket(17l, 5));
        space1Event2.getTickets().add(createUnpaidTicket(18l));
        space1Event2.getTickets().add(createUnpaidTicket(19l));
        space1Event2.getTickets().add(createUnpaidTicket(20l));

        SingleEvent space1Event3 = new SingleEvent(); //total occupancy 0
        space1Event3.setStartDate(sdf.parse("01/03/2015"));
        space1Event3.setTickets(new HashSet<Ticket>());

        space1Event2.getTickets().add(createUnpaidTicket(21l));

        List<SingleEvent> space1Events = new ArrayList<SingleEvent>() {
            {
                add(space1Event1);
                add(space1Event2);
                add(space1Event3);
            }
        };

        Space space2= new Space();
        space2.setId(3l);
        space2.setName("TestSpace2");
        location.getSpaces().add(space2);

        SingleEvent space2Event1 = new SingleEvent(); // total occupancy 0
        space2Event1.setStartDate(sdf.parse("30/02/2015"));
        space2Event1.setTickets(new HashSet<Ticket>());

        SingleEvent space2Event2 = new SingleEvent(); // total occupancy 1
        space2Event2.setStartDate(sdf.parse("25/03/2015"));
        space2Event2.setTickets(new HashSet<Ticket>());

        space2Event2.getTickets().add(createPaidTicket(22l, 3));

        SingleEvent space2Event3 = new SingleEvent(); // total occupancy 1
        space2Event3.setStartDate(sdf.parse("02/04/2015"));
        space2Event3.setTickets(new HashSet<Ticket>());

        space2Event3.getTickets().add(createPaidTicket(23l, 69));
        space2Event3.getTickets().add(createUnpaidTicket(24l));

        List<SingleEvent> space2Events = new ArrayList<SingleEvent>() {
            {
                add(space2Event1);
                add(space2Event2);
                add(space2Event3);
            }
        };

        when(this.mockedRepo.findById(id)).thenReturn(Optional.of(location));
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space1.getId(), startDate, endDate))
                .thenReturn(space1Events);
        when(mockedSingleEventRepo.findAllBySpaceIdAndStartDateGreaterThanEqualAndEndDateLessThanEqual(space2.getId(), startDate, endDate))
                .thenReturn(space2Events);

        Map<String, Object> result = this.crudService.reportOccupancyDuringTimePeriod(id, startDate, endDate);
        Map<String, Object> data = (Map<String, Object>) result.get("data");

        assertEquals("TestLocationName visitors for time period: 01.02.2015-31.05.2015", result.get("title"));
        assertEquals("tickets bought", result.get("unit"));
        assertEquals(3, data.size());

        Map<String, Object> expected = new HashMap<String, Object>() {
            {
                put("March/2015", new Double(5));
                put("April/2015", new Double(1));
                put("February/2015", new Double(5));
            }
        };

        for(String key: data.keySet()) {
            assertEquals(expected.get("key"), data.get("key"));
        }
    }

    @Test
    public void findByPageName() {
        String name = "ad";
        Pageable pageable = (Pageable) PageRequest.of(0, 5);
        Page<Location> page = Page.empty();
        when(mockedRepo.findByNameContainingIgnoreCase(name, pageable)).thenReturn(page);
        Page retVal = crudService.findPageByName(name, pageable);
        assertEquals(page, retVal);
        verify(mockedRepo, times(1)).findByNameContainingIgnoreCase(name, pageable);
    }


    private Ticket createPaidTicket(Long id, double price) {
        Ticket retVal = new Ticket();
        retVal.setId(id);
        retVal.setPaid(true);
        retVal.setPrice(price);
        return retVal;
    }

    private Ticket createUnpaidTicket(Long id) {
        Ticket retVal = new Ticket();
        retVal.setPaid(false);
        return retVal;
    }



}
