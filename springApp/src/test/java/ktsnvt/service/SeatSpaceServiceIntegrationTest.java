package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.dto.SeatDto;
import ktsnvt.entity.SeatSpace;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.SpaceLayout;
import ktsnvt.entity.Ticket;
import ktsnvt.repository.SeatSpaceRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SeatSpaceServiceIntegrationTest extends CrudServiceGenericIntegrationTest<SeatSpaceService, SeatSpaceRepository, SeatSpace> {


    private DataGraph dataGraph;

    public SeatSpaceServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }

    @Override
    public void successfulDelete() throws FetchException, DeleteException {
        Long id = getIdNoEvent();
        SeatSpace seatSpace = crudService.getById(id);
        assertNotNull(seatSpace);
        assertNull(seatSpace.getSpaceLayout().getSingleEvent());
        crudService.deleteById(id);
        assertThrows(FetchException.class, () -> {
            crudService.getById(id);
        });
        List<SeatSpace> seatSpaces = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb() - 1, seatSpaces.size());
    }

    @Transactional
    @Test
    public void failedDeleteTiedEvent() throws FetchException {
        Long id = getIdHasEvent();
        SeatSpace seatSpace = crudService.getById(id);
        assertNotNull(seatSpace);
        assertNotNull(seatSpace.getSpaceLayout().getSingleEvent());
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
           crudService.deleteById(id);
        });
        assertEquals("Cannot delete SeatSpace with id " + id + " SeatSpace is tied to an event", deleteException.getMessage());
        SeatSpace afterTriedDelete = crudService.getById(id);
        assertNotNull(afterTriedDelete);
        List<SeatSpace> seatSpaces = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb(), seatSpaces.size());
    }

    @Transactional
    @Test
    public void failedGetSeatsTakenNoSeatSpace() {
        Long id = getIdNotExists();
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.getSeatsTaken(id);
        });
        assertEquals("Seat space with id " + id + " was not found.", fetchException.getMessage());
    }

    @Transactional
    @Test
    public void successfulSeatsTaken() throws FetchException {
        Long id = getIdHasSeatsTaken();
        List<SeatDto> seatDtoList = crudService.getSeatsTaken(id);
        // 4 users bough tickets, assert list size 4
        assertEquals(4, seatDtoList.size());
    }

    @Transactional
    @Test
    public void failedAddTicketsNoSeatSpace() {
        long id = getIdNotExists();
        AddException addException = assertThrows(AddException.class, () -> {
           crudService.addTickets(id, null);
        });
        assertEquals("Seat space with id " + id + " does not exist.", addException.getMessage());
    }


    @Transactional
    @Test
    public void successfulAddTickets() throws AddException, FetchException {
        SingleEvent singleEvent = dataGraph.getSingleEvents().get(0);
        Long seatSpaceId = getIdNoTickets();

       crudService.addTickets(seatSpaceId, singleEvent);

       SeatSpace seatSpace = crudService.getById(seatSpaceId);
       assertEquals(seatSpace.getSeats() * seatSpace.getRows(), seatSpace.getTickets().size());
        for (int i = 0; i < seatSpace.getRows(); i++) {
            for (int j = 0; j < seatSpace.getSeats(); j++) {
                int finalI = i;
                int finalJ = j;
                Ticket ticket = seatSpace.getTickets().stream()
                        .filter(value -> value.getRow() == finalI && value.getSeat() == finalJ)
                        .findFirst().orElse(null);
                assertNotNull(ticket);
            }
        }
    }

    public long getIdNoTickets() {
        return 99310l;
    }

    public Long getIdHasSeatsTaken() {
        return 997l;
    }

    @Override
    public Long getIdNotExists() {
        return 696969l;
    }

    @Override
    public Long getIdExists() {
        return dataGraph.getSeatSpaces().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        return null;
    }

    public Long getIdHasEvent() {
        for (SeatSpace seatSpace: dataGraph.getSeatSpaces()) {
            if (seatSpace.getSpaceLayout().getSingleEvent() != null) {
                return seatSpace.getId();
            }
        }
        return null;
    }

    public Long getIdNoEvent() {
        for (SeatSpace seatSpace: dataGraph.getSeatSpaces()) {
            if (seatSpace.getSpaceLayout().getSingleEvent() == null) {
                return seatSpace.getId();
            }
        }
        return null;
    }


    @Override
    public Long getIdFromEntity(SeatSpace seatSpace) {
        return seatSpace.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getSeatSpaces().size();
    }

    @Override
    public SeatSpace createSampleValue() {
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setRows(13);
        seatSpace.setSeats(14);
        seatSpace.setTickets(new HashSet<Ticket>());
        return seatSpace;
    }

    @Override
    public void changeEntitiesAttributes(SeatSpace seatSpace) {
        seatSpace.setRows(20);
        seatSpace.setSeats(30);
        seatSpace.setPrice(420);
    }

    @Override
    public boolean verifyChangedAttributes(SeatSpace seatSpace) {

        if (!seatSpace.getRows().equals(20)) {
            return false;
        }
        if (!seatSpace.getSeats().equals(30)) {
            return false;
        }
        if (seatSpace.getPrice() != 420) {
            return false;
        }
        return true;
    }

    @Override
    public boolean entityEquals(SeatSpace e1, SeatSpace e2) {
        if (e1 == e2) return true;
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (e1.getPrice() != e2.getPrice()) {
            return false;
        }
        if (!e1.getRows().equals(e2.getRows())) {
            return false;
        }
        if (!e1.getSeats().equals(e2.getSeats())) {
            return false;
        }
        ArrayList<Ticket> tickets1 = new ArrayList<Ticket>(e1.getTickets());
        ArrayList<Ticket> tickets2 = new ArrayList<Ticket>(e2.getTickets());
        tickets1.sort(ticketSorter);
        tickets2.sort(ticketSorter);
        if (tickets1.size() != tickets2.size()) {
            return false;
        }
        for (int i = 0; i < tickets1.size(); i++) {
            if (!tickets1.get(i).getId().equals(tickets2.get(i).getId())) {
                return false;
            }
        }
        if (e1.getSpaceLayout() == null && e2.getSpaceLayout() == null) {
            return true;
        }
        if (!e1.getSpaceLayout().getId().equals(e2.getSpaceLayout().getId())) {
            return false;
        }
        return true;
    }

    private Comparator<Ticket> ticketSorter = Comparator.comparing(Ticket::getId);

    @Override
    public SeatSpace getEntityFromIdExists() {
        return dataGraph.findSeatSpaceById(getIdExists());
    }

    @Override
    public void setEntityId(SeatSpace seatSpace, Long id) {
        seatSpace.setId(id);
    }

    @Override
    public SeatSpace getEntityFromDataGraph(Long id) {
        return this.dataGraph.findSeatSpaceById(id);
    }


}
