package ktsnvt.service;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.dto.SeatDto;
import ktsnvt.entity.*;
import ktsnvt.repository.SeatSpaceRepository;
import ktsnvt.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SeatSpaceServiceUnitTest extends CrudServiceGenericUnitTest<SeatSpaceService,
        SeatSpaceRepository, SeatSpace>  {

    @Override
    public SeatSpace createSampleValue() {
        SeatSpace retVal = new SeatSpace();
        retVal.setSpaceLayout(new SpaceLayout());
        return retVal;
    }


    @Override
    public SeatSpaceRepository getMockedRepo() {
        return mockedRepo;
    }

    @MockBean
    private TicketRepository ticketRepositoryMocked;

    @MockBean
    private SeatSpaceRepository mockedRepo;

    @Override
    public void successfulDelete() throws DeleteException, FetchException {
        Long id = 1l;
        SeatSpace seatSpace = createSampleValue();
        crudService.repository = mockedRepo;
        when(ticketRepositoryMocked.existsBySingleEvent_StateAndSeatSpaceId(EventState.ForSale, id)).thenReturn(false);
        when(mockedRepo.findById(id)).thenReturn(Optional.of(seatSpace));
        crudService.deleteById(id);
        verify(mockedRepo, times(1)).deleteById(id);
    }

    @Test
    public void failedDeleteTiedEvent() {
        Long id = 1l;
        SeatSpace seatSpace = createSampleValue();
        seatSpace.getSpaceLayout().setSingleEvent(new SingleEvent());
        crudService.repository = mockedRepo;
        when(ticketRepositoryMocked.existsBySingleEvent_StateAndSeatSpaceId(EventState.ForSale, id)).thenReturn(true);
        when(mockedRepo.findById(id)).thenReturn(Optional.of(seatSpace));
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete SeatSpace with id 1 SeatSpace is tied to an event", deleteException.getMessage());
    }

    @Test
    public void failedGetSeatsTakenNoSeatSpace() {
        Long id = 1l;
        crudService.repository = mockedRepo;
        when(mockedRepo.existsById(id)).thenReturn(false);
        FetchException fetchException = assertThrows(FetchException.class, () -> {
            crudService.getSeatsTaken(id);
        });
        assertEquals("Seat space with id 1 was not found.", fetchException.getMessage());
    }


    @Test
    public void successfulSeatsTaken() throws FetchException {
        Long id =  1l;
        crudService.repository = mockedRepo;
        when(mockedRepo.existsById(id)).thenReturn(true);
        List<Ticket> tickets = new ArrayList<Ticket>() {
            {
                add(ticketWithSeat(1, 1));
                add(ticketWithSeat(2, 2));
                add(ticketWithSeat(3,3));
            }
        };
        when(ticketRepositoryMocked.findBySeatSpaceIdAndUserIsNotNull(id)).thenReturn(tickets);
        List<SeatDto> retVal = crudService.getSeatsTaken(id);
        assertEquals(tickets.size(), retVal.size());
        for (Ticket ticket: tickets) {
            SeatDto matching = retVal.stream()
                    .filter(seatDto -> seatDto.getRow() == ticket.getRow() && seatDto.getSeat() == ticket.getSeat())
                    .findFirst().orElse(null);
            assertNotNull(matching);
        }
    }

    @Test
    public void failedAddTicketsNoSeatSpace() {
        Long id = 1l;
        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());
        AddException addException = assertThrows(AddException.class, () -> {
           crudService.addTickets(id, null);
        });
        assertEquals("Seat space with id 1 does not exist.", addException.getMessage());
    }

    @Test
    public void successfulAddTickets() throws AddException {
        Long id = 1l;
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setTickets(new HashSet<Ticket>());
        seatSpace.setSeats(5);
        seatSpace.setRows(6);
        seatSpace.setId(id);
        SingleEvent singleEvent = new SingleEvent();

        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.of(seatSpace));

        crudService.addTickets(id, singleEvent);
        assertEquals(seatSpace.getSeats() * seatSpace.getRows(), seatSpace.getTickets().size());
        for (int i = 0; i < seatSpace.getRows(); i++) {
            for (int j = 0; j < seatSpace.getSeats(); j++) {
                int finalI = i;
                int finalJ = j;
                Ticket ticket = seatSpace.getTickets().stream()
                        .filter(value -> value.getRow() == finalI && value.getSeat() == finalJ)
                        .findFirst().orElse(null);
                assertNotNull(ticket);

            }
        }
        verify(mockedRepo, times(1)).save(seatSpace);
    }




    private Ticket ticketWithSeat(int row, int seat) {
        Ticket retVal = new Ticket();
        retVal.setRow(row);
        retVal.setSeat(seat);
        return retVal;
    }




}
