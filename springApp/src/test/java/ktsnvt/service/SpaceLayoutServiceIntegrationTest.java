package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceLayoutRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SpaceLayoutServiceIntegrationTest extends CrudServiceGenericIntegrationTest<SpaceLayoutService, SpaceLayoutRepository, SpaceLayout> {

    private DataGraph dataGraph;

    public SpaceLayoutServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }

    @Test
    public void failedDeleteTiedToEvent() throws FetchException {
        Long id = getIdHasEvent();
        SpaceLayout spaceLayout = crudService.getById(id);
        assertNotNull(spaceLayout.getSingleEvent());
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        SpaceLayout afterTriedDelete = crudService.getById(id);
        assertNotNull(afterTriedDelete);
        List<SpaceLayout> spaceLayouts = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb(), spaceLayouts.size());
    }


    @Override
    public Long getIdNotExists() {
        return 6969l;
    }

    @Override
    public Long getIdExists() {
        return dataGraph.getSpaceLayouts().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        for (SpaceLayout spaceLayout: dataGraph.getSpaceLayouts()) {
            if (spaceLayout.getSingleEvent() == null) {
                return spaceLayout.getId();
            }
        }
        return null;
    }

    public Long getIdHasEvent() {
        return 9928l;
    }

    public Long getIdNoEvent() {
        return 99314l;
    }

    @Override
    public Long getIdFromEntity(SpaceLayout spaceLayout) {
        return spaceLayout.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getSpaceLayouts().size();
    }

    @Override
    public SpaceLayout createSampleValue() {
        SpaceLayout spaceLayout = new SpaceLayout();
        spaceLayout.setSeatSpaces(new HashSet<SeatSpace>());
        spaceLayout.setGroundFloors(new HashSet<GroundFloor>());
        return spaceLayout;
    }

    @Override
    public boolean entityEquals(SpaceLayout e1, SpaceLayout e2) {
        if (e1 == e2) {
            return true;
        }
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (e1.getSingleEvent() != null || e2.getSingleEvent() != null) {
            if (!e1.getSingleEvent().getId().equals(e2.getSingleEvent().getId())) {
                return false;
            }
        }
        if (e1.getSpace() == null && e2.getSpace() == null) {
            return true;
        }
        if (!e1.getSpace().getId().equals(e2.getSpace().getId())) {
            return false;
        }
        List<GroundFloor> groundFloors1 = new ArrayList<GroundFloor>(e1.getGroundFloors());
        List<GroundFloor> groundFloors2 = new ArrayList<GroundFloor>(e2.getGroundFloors());
        groundFloors1.sort(groundFloorSorter);
        groundFloors2.sort(groundFloorSorter);
        if (groundFloors1.size() != groundFloors2.size()) {
            return false;
        }
        for(int i = 0; i < groundFloors1.size(); i++) {
            if (!groundFloors1.get(i).getId().equals(groundFloors2.get(i).getId())) {
                return false;
            }
        }
        List<SeatSpace> seatSpaces1 = new ArrayList<SeatSpace>(e1.getSeatSpaces());
        List<SeatSpace> seatSpaces2 = new ArrayList<SeatSpace>(e2.getSeatSpaces());
        seatSpaces1.sort(seatSpaceSorter);
        seatSpaces2.sort(seatSpaceSorter);
        if (seatSpaces1.size() != seatSpaces2.size()) {
            return false;
        }
        for (int i = 0; i < seatSpaces1.size(); i++) {
            if (!seatSpaces1.get(i).getId().equals(seatSpaces2.get(i).getId())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public SpaceLayout getEntityFromIdExists() {
        return dataGraph.findSpaceLayoutById(getIdExists());
    }

    @Override
    public void setEntityId(SpaceLayout spaceLayout, Long id) {
        spaceLayout.setId(id);
    }

    @Override
    public SpaceLayout getEntityFromDataGraph(Long id) {
        return this.dataGraph.findSpaceLayoutById(id);
    }


    private Comparator<GroundFloor> groundFloorSorter = Comparator.comparing(GroundFloor::getId);

    private Comparator<SeatSpace> seatSpaceSorter = Comparator.comparing(SeatSpace::getId);


    //currently overrided to be empty because they do not make sense for this class, it has no primitive attributes
    @Override
    public void successfulUpdate() {}

    //these two methods also are not used because successfulUpdateTest is overriden to be empty
    @Override
    public void changeEntitiesAttributes(SpaceLayout spaceLayout) { }

    @Override
    public boolean verifyChangedAttributes(SpaceLayout spaceLayout) {
        return false;
    }



}
