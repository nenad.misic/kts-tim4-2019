package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.*;
import ktsnvt.repository.SpaceLayoutRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpaceLayoutServiceUnitTest extends CrudServiceGenericUnitTest<SpaceLayoutService, SpaceLayoutRepository, SpaceLayout> {

    @Override
    public SpaceLayout createSampleValue() {
        SpaceLayout retVal = new SpaceLayout();
        retVal.setGroundFloors(new HashSet<GroundFloor>());
        retVal.setSeatSpaces(new HashSet<SeatSpace>());
        return retVal;
    }

    @MockBean
    private SpaceLayoutRepository mockedRepo;

    @MockBean
    private GroundFloorService groundFloorServiceMocked;

    @MockBean
    private SeatSpaceService seatSpaceServiceMocked;

    @Override
    public SpaceLayoutRepository getMockedRepo() {
        return mockedRepo;
    }

    @Override
    public void successfulDelete() throws FetchException, DeleteException {
        Long id = 1l;
        Long groundFloorId = 2l;
        Long seatSpaceId = 3l;

        SpaceLayout spaceLayout = createSampleValue();
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setId(groundFloorId);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setId(seatSpaceId);
        spaceLayout.getGroundFloors().add(groundFloor);
        spaceLayout.getSeatSpaces().add(seatSpace);

        when(mockedRepo.findById(id)).thenReturn(Optional.of(spaceLayout));

        crudService.deleteById(id);

        verify(groundFloorServiceMocked, times(1)).deleteById(groundFloorId);
        verify(seatSpaceServiceMocked, times(1)).deleteById(seatSpaceId);
        verify(mockedRepo, times(1)).deleteById(id);
    }


    @Test
    public void failedDeleteTiedToEvent() throws FetchException, DeleteException {
        Long id = 1l;
        Long groundFloorId = 2l;
        Long seatSpaceId = 3l;

        SpaceLayout spaceLayout = createSampleValue();
        spaceLayout.setId(1l);
        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setId(groundFloorId);
        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setId(seatSpaceId);
        spaceLayout.getGroundFloors().add(groundFloor);
        spaceLayout.getSeatSpaces().add(seatSpace);

        SingleEvent singleEvent = new SingleEvent();
        singleEvent.setId(4l);
        spaceLayout.setSingleEvent(singleEvent);

        when(mockedRepo.findById(id)).thenReturn(Optional.of(spaceLayout));

        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete spaceLayout with id: 1, it is tied to an event", deleteException.getMessage());

        verify(groundFloorServiceMocked, times(0)).deleteById(groundFloorId);
        verify(seatSpaceServiceMocked, times(0)).deleteById(seatSpaceId);
        verify(mockedRepo, times(0)).deleteById(id);
    }

}
