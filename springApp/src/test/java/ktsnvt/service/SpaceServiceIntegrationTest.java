package ktsnvt.service;

import ktsnvt.common.exceptions.AddException;
import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.Location;
import ktsnvt.entity.Space;
import ktsnvt.repository.SpaceRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class SpaceServiceIntegrationTest extends CrudServiceGenericIntegrationTest<SpaceService, SpaceRepository, Space> {


    private DataGraph dataGraph;

    public SpaceServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }

    @Autowired
    private LocationService locationService;

    @Transactional
    @Test
    public void failedDeleteSpaceHasEvents() throws FetchException {
        Long id = getIdSpaceWithEvents();
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
           crudService.deleteById(id);
        });
        assertEquals("Cannot delete Space with id 991: Space has relationships to one or more events", deleteException.getMessage());
        Space space = crudService.getById(id);
        assertNotNull(space);
    }

    @Override
    public void successfulAdd() throws AddException, FetchException {
        Space space = createSampleValue();
        Location location = locationService.getAll().get(0);
        space.setLocation(location);
        crudService.add(space);
        Long id = space.getId();
        Space retVal = crudService.getById(id);
        assertNotNull(retVal);
        assertEquals(id, retVal.getId());
        Space spaceWithSameAttributes = createSampleValue();
        spaceWithSameAttributes.setId(id);
        spaceWithSameAttributes.setLocation(location);
        assertTrue(entityEquals(spaceWithSameAttributes, retVal));
        List<Space> spaces = crudService.getAll();
        assertEquals(getNumberOfEntitiesInDb() + 1, spaces.size());
    }

    @Override
    public Long getIdNotExists() {
        return 6969l;
    }

    @Override
    public Long getIdExists() {
        return dataGraph.getSpaces().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        for (Space space: dataGraph.getSpaces()) {
            if (space.getEvents().size() == 0) {
                return space.getId();
            }
        }
        return null;
    }


    @Override
    public Long getIdFromEntity(Space space) {
        return space.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getSpaces().size();
    }

    @Override
    public Space createSampleValue() {
        Space space = new Space();
        space.setName("SpaceSampleName");
        return space;
    }

    @Override
    public void changeEntitiesAttributes(Space space) {
        space.setName("IzmenjenoIme");
    }

    @Override
    public boolean verifyChangedAttributes(Space space) {
        return "IzmenjenoIme".equals(space.getName());
    }

    @Override
    public boolean entityEquals(Space e1, Space e2) {
        if (e1 == e2) return true;
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (!e1.getName().equals(e2.getName())) {
            return false;
        }
        if (!e1.getLocation().getId().equals(e2.getLocation().getId())) {
            return false;
        }
        return true;
    }

    @Override
    public Space getEntityFromIdExists() {
        return dataGraph.findSpaceById(getIdExists());
    }

    @Override
    public void setEntityId(Space space, Long id) {
        space.setId(id);
    }

    @Override
    public Space getEntityFromDataGraph(Long id) {
        return dataGraph.findSpaceById(id);
    }

    public Long getIdSpaceWithEvents() {
        return 991l;
    }
}
