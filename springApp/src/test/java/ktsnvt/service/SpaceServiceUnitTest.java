package ktsnvt.service;

import ktsnvt.common.exceptions.DeleteException;
import ktsnvt.common.exceptions.FetchException;
import ktsnvt.entity.EventState;
import ktsnvt.entity.SingleEvent;
import ktsnvt.entity.Space;
import ktsnvt.repository.SpaceRepository;
import org.hibernate.sql.Delete;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpaceServiceUnitTest extends CrudServiceGenericUnitTest<SpaceService, SpaceRepository, Space> {

    @MockBean
    private SpaceRepository mockedRepo;

    @Override
    public Space createSampleValue() {
        Space retVal = new Space();
        retVal.setEvents(new HashSet<SingleEvent>());
        return retVal;
    }


    @Override
    public SpaceRepository getMockedRepo() {
        return mockedRepo;
    }


    @Test
    public void failedDeleteSpaceSpaceHasEvents() {
        Long id = 1l;
        Space space = createSampleValue();
        space.getEvents().add(new SingleEvent());
        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.of(space));
        DeleteException deleteException = assertThrows(DeleteException.class, () -> {
            crudService.deleteById(id);
        });
        assertEquals("Cannot delete Space with id 1: Space has relationships to one or more events", deleteException.getMessage());
    }

    @Override
    public void successfulDelete() throws FetchException, DeleteException {
        Long id = 1l;
        Space space = createSampleValue();
        crudService.repository = mockedRepo;
        when(mockedRepo.findById(id)).thenReturn(Optional.of(space));
        crudService.deleteById(id);
        verify(mockedRepo, times(1)).deleteById(id);
    }

}