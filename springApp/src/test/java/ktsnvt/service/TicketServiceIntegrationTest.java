package ktsnvt.service;

import ktsnvt.common.exceptions.*;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.*;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class TicketServiceIntegrationTest extends CrudServiceGenericIntegrationTest<TicketService, TicketRepository, Ticket> {

    private DataGraph dataGraph;

    public TicketServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }

    @Autowired
    private GroundFloorRepository groundFloorRepository;


    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Override
    public Long getIdNotExists() {
        return 6969L;
    }

    @Override
    public Long getIdExists() {
        return dataGraph.getTickets().get(0).getId();
    }

    @Override
    public Long getIdDeletable() {
        return null;
    }

    @Override
    public Long getIdFromEntity(Ticket ticket) {
        return ticket.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getTickets().size();
    }

    @Override
    public Ticket createSampleValue() {
        return new Ticket();
    }

    @Override
    public void changeEntitiesAttributes(Ticket ticket) {
        ticket.setSeat(420);
    }

    @Override
    public boolean verifyChangedAttributes(Ticket ticket) {
        return 420 == ticket.getSeat();
    }

    @Override
    public boolean entityEquals(Ticket e1, Ticket e2) {
        if (e1 == e2) {
            return true;
        }
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (e1.getSeat() != e2.getSeat()) {
            return false;
        }
        if (e1.getRow() != e2.getRow()) {
            return false;
        }
        if (e1.isPaid() != e2.isPaid()) {
            return false;
        }
        if (e1.getUser() != null && e2.getUser() != null) {
            if (!e1.getUser().getId().equals(e2.getUser().getId())) {
                return false;
            }
        } else if (!(e1.getUser() == null && e2.getUser() == null)) {
            return false;
        }
        if (e1.getGroundFloor() != null && e2.getGroundFloor() != null) {
            if (!e1.getGroundFloor().getId().equals(e2.getGroundFloor().getId())) {
                return false;
            }
        } else if (!(e1.getGroundFloor() == null && e2.getGroundFloor() == null)) {
            return false;
        }
        if (e1.getSeatSpace() != null && e2.getSeatSpace() != null) {
            if (!e1.getSeatSpace().getId().equals(e2.getSeatSpace().getId())) {
                return false;
            }
        } else if (!(e1.getSeatSpace() == null && e2.getSeatSpace() == null)) {
            return false;
        }


        if (!e1.getSingleEvent().getId().equals(e2.getSingleEvent().getId())) {
            return false;
        }
        if (e1.getPrice() != e2.getPrice()) {
            return false;
        }
        return true;
    }

    @Override
    public Ticket getEntityFromIdExists() {
        return dataGraph.findTicketById(getIdExists());
    }

    @Override
    public void setEntityId(Ticket ticket, Long id) {

    }

    @Override
    public Ticket getEntityFromDataGraph(Long id) {
        return dataGraph.findTicketById(id);
    }

    @Override
    public void successfulAdd() {}


    private void verifyDataUnchanged() {
        List<Ticket> tickets = repository.findAll();
        assertEquals(tickets.size(), dataGraph.getTickets().size());
        for (Ticket t : tickets) {
            Ticket dataGraphTicket = dataGraph.findTicketById(t.getId());
            assertNotNull(dataGraphTicket.getId());
            if (t.getUser() == null) {
                assertNull(dataGraphTicket.getUser());
            } else {
                assertEquals(dataGraphTicket.getUser().getUsername(), t.getUser().getUsername());
            }
            assertEquals(t.isPaid(), dataGraphTicket.isPaid());
        }

        List<GroundFloor> groundFloors = groundFloorRepository.findAll();
        for (GroundFloor g : groundFloors) {
            GroundFloor dataGraphGF = dataGraph.findGroundFloorById(g.getId());
            assertEquals(g.getSpotsReserved(), dataGraphGF.getSpotsReserved());
        }
    }

    @Test
    @Transactional
    public void failedAdd_NoUser() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext("wrongUsername");
        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Unable to find logged in user");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_InvalidSeatAndRow() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(997L);
        ticketDto.setSeat(4);
        ticketDto.setRow(4);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ticket");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_TicketHasUser() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(997L);
        ticketDto.setSeat(1);
        ticketDto.setRow(2);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ticket is already reserved");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_InvalidGroundFloorId() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ground floor id in ticket");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_GroundFloorHasNoSingleEvent() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(99307L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ground floor is part of default space layout.");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_GroundFloorFull() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(9937L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedAdd_NotEnoughSpaceInGroundFloor() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        GroundFloor gf = dataGraph.findGroundFloorById(9917L);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        for (int i = 0; i < gf.getCapacity() - gf.getSpotsReserved() + 1; i++) {
            TicketDto ticketDto = new TicketDto();
            ticketDto.setGroundFloorId(9917L);
            ticketDtos.add(ticketDto);
        }

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");

        crudService.add(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void successfulAdd_GroundFloor() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();
        int numberOfAddedTickets = 5;
        Long groundFloorId = 9923L;

        GroundFloor gf = dataGraph.findGroundFloorById(groundFloorId);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        for (int i = 0; i < numberOfAddedTickets; i++) {
            TicketDto ticketDto = new TicketDto();
            ticketDto.setGroundFloorId(groundFloorId);
            ticketDtos.add(ticketDto);
        }
        crudService.add(ticketDtos);

        List<Ticket> tickets = repository.findAll();
        assertEquals(dataGraph.getTickets().size() + numberOfAddedTickets, tickets.size());

        GroundFloor groundFloor = groundFloorRepository.getOne(groundFloorId);
        long spotsReserved = groundFloor.getSpotsReserved();
        assertEquals(spotsReserved, gf.getSpotsReserved() + numberOfAddedTickets);

        ArrayList<Ticket> addedTickets = new ArrayList<>();
        for(Ticket t : tickets) {
            if (t.getGroundFloor() == null) continue;
            if (t.getGroundFloor().getId().equals(gf.getId())) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals(t.getGroundFloor().getId(), groundFloorId);
                assertEquals(t.getSingleEvent().getId(), gf.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), gf.getPrice(), 1d);
                assertFalse(t.isPaid());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(gf.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            }
        }
        assertEquals(addedTickets.size(), numberOfAddedTickets);
    }

    @Test
    @Transactional
    public void successfulAdd_SeatSpace() throws AddException, InterruptedException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();
        Long seatSpaceId = 997L;
        SeatSpace seatSpace = dataGraph.findSeatSpaceById(seatSpaceId);
        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setSeatSpaceId(seatSpaceId);
        ticketDto1.setRow(1);
        ticketDto1.setSeat(0);
        ticketDtos.add(ticketDto1);

        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setSeatSpaceId(seatSpaceId);
        ticketDto2.setRow(0);
        ticketDto2.setSeat(1);
        ticketDtos.add(ticketDto2);

        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(seatSpaceId);
        ticketDto3.setRow(1);
        ticketDto3.setSeat(1);
        ticketDtos.add(ticketDto3);

        TicketDto ticketDto4 = new TicketDto();
        ticketDto4.setSeatSpaceId(seatSpaceId);
        ticketDto4.setRow(1);
        ticketDto4.setSeat(2);
        ticketDtos.add(ticketDto4);

        TicketDto ticketDto5 = new TicketDto();
        ticketDto5.setSeatSpaceId(seatSpaceId);
        ticketDto5.setRow(2);
        ticketDto5.setSeat(2);
        ticketDtos.add(ticketDto5);

        crudService.add(ticketDtos);

        ArrayList<Ticket> tickets = new ArrayList<>();
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 0));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 0, 1));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 1));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 2));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 2, 2));

        for (Ticket t : tickets) {
            assertNotNull(t);
            assertNotNull(t.getUser());
            assertEquals(t.getUser().getUsername(), "username");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(seatSpace.getSpaceLayout().getSingleEvent().getStartDate());
            calendar.add(Calendar.DAY_OF_MONTH, - 1);
            assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
            assertFalse(t.isPaid());
        }
        assertEquals(repository.findAll().size(), dataGraph.getTickets().size());
    }

    @Test
    @Transactional
    public void successfulAdd_SeatSpaceAndGroundFloor() throws AddException, FetchException, IDMappingException, InterruptedException {
        TestUtil.mockSecurityContext();
        long groundFloorId1 = 9923L;
        long groundFloorId2 = 996L;
        Long seatSpaceId1 = 997L;
        Long seatSpaceId2 = 998L;

        GroundFloor groundFloor1 = dataGraph.findGroundFloorById(groundFloorId1);
        GroundFloor groundFloor2 = dataGraph.findGroundFloorById(groundFloorId2);
        SeatSpace seatSpace1 = dataGraph.findSeatSpaceById(seatSpaceId1);
        SeatSpace seatSpace2 = dataGraph.findSeatSpaceById(seatSpaceId2);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(groundFloorId1);
        ticketDtos.add(ticketDto1);

        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(groundFloorId2);
        ticketDtos.add(ticketDto2);

        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(seatSpaceId1);
        ticketDto3.setRow(1);
        ticketDto3.setSeat(0);
        ticketDtos.add(ticketDto3);

        TicketDto ticketDto4 = new TicketDto();
        ticketDto4.setSeatSpaceId(seatSpaceId2);
        ticketDto4.setRow(2);
        ticketDto4.setSeat(0);
        ticketDtos.add(ticketDto4);

        TicketDto ticketDto5 = new TicketDto();
        ticketDto5.setGroundFloorId(groundFloorId2);
        ticketDtos.add(ticketDto5);

        crudService.add(ticketDtos);

        List<Ticket> tickets = repository.findAll();
        assertEquals(dataGraph.getTickets().size() + 3, tickets.size());

        Ticket ticket3 = repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId1, 1, 0);
        assertNotNull(ticket3);
        assertNotNull(ticket3.getUser());
        assertEquals(ticket3.getUser().getUsername(), "username");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(seatSpace1.getSpaceLayout().getSingleEvent().getStartDate());
        calendar.add(Calendar.DAY_OF_MONTH, - 1);
        assertEquals(ticket3.getExpirationDate().compareTo(calendar.getTime()), 0);
        assertFalse(ticket3.isPaid());

        Ticket ticket4 = repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId2, 2, 0);
        assertNotNull(ticket4);
        assertNotNull(ticket4.getUser());
        assertEquals(ticket4.getUser().getUsername(), "username");
        calendar.setTime(seatSpace2.getSpaceLayout().getSingleEvent().getStartDate());
        calendar.add(Calendar.DAY_OF_MONTH, - 1);
        assertEquals(ticket4.getExpirationDate().compareTo(calendar.getTime()), 0);
        assertFalse(ticket4.isPaid());

        GroundFloor gf1 = groundFloorRepository.getOne(groundFloorId1);
        assertEquals((long)groundFloor1.getSpotsReserved() + 1, (long)gf1.getSpotsReserved());

        GroundFloor gf2 = groundFloorRepository.getOne(groundFloorId2);
        assertEquals((long)groundFloor2.getSpotsReserved() + 2, (long)gf2.getSpotsReserved());


        ArrayList<Ticket> addedTickets = new ArrayList<>();
        for(Ticket t : tickets) {
            if (t.getGroundFloor() == null) continue;
            if (t.getGroundFloor().getId().equals(groundFloor1.getId())) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals((long)t.getGroundFloor().getId(), groundFloorId1);
                assertEquals(t.getSingleEvent().getId(), groundFloor1.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), groundFloor1.getPrice(), 1d);
                assertFalse(t.isPaid());
                calendar.setTime(groundFloor1.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            } else if (t.getGroundFloor().getId().equals(groundFloorId2)) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals((long)t.getGroundFloor().getId(), groundFloorId2);
                assertEquals(t.getSingleEvent().getId(), groundFloor2.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), groundFloor2.getPrice(), 1d);
                assertFalse(t.isPaid());
                calendar.setTime(groundFloor2.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            }
        }
        assertEquals(addedTickets.size(), 3);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_NoUser() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext("wrongUsername");
        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Unable to find logged in user");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_InvalidSeatAndRow() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(997L);
        ticketDto.setSeat(4);
        ticketDto.setRow(4);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ticket");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_TicketHasUser() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(997L);
        ticketDto.setSeat(1);
        ticketDto.setRow(2);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ticket is already reserved");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_InvalidGroundFloorId() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ground floor id in ticket");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_GroundFloorHasNoSingleEvent() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(99307L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ground floor is part of default space layout.");

        crudService.createPayedTickets(ticketDtos);


        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_GroundFloorFull() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(9937L);
        ticketDtos.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_NotEnoughSpaceInGroundFloor() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();

        GroundFloor gf = dataGraph.findGroundFloorById(9917L);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        for (int i = 0; i < gf.getCapacity() - gf.getSpotsReserved() + 1; i++) {
            TicketDto ticketDto = new TicketDto();
            ticketDto.setGroundFloorId(9917L);
            ticketDtos.add(ticketDto);
        }

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");

        crudService.createPayedTickets(ticketDtos);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void successfulCreatePayedTickets_GroundFloor() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();
        int numberOfAddedTickets = 5;
        Long groundFloorId = 9923L;

        GroundFloor gf = dataGraph.findGroundFloorById(groundFloorId);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();
        for (int i = 0; i < numberOfAddedTickets; i++) {
            TicketDto ticketDto = new TicketDto();
            ticketDto.setGroundFloorId(groundFloorId);
            ticketDtos.add(ticketDto);
        }
        crudService.createPayedTickets(ticketDtos);

        List<Ticket> tickets = repository.findAll();
        assertEquals(dataGraph.getTickets().size() + numberOfAddedTickets, tickets.size());

        GroundFloor groundFloor = groundFloorRepository.getOne(groundFloorId);
        long spotsReserved = groundFloor.getSpotsReserved();
        assertEquals(spotsReserved, gf.getSpotsReserved() + numberOfAddedTickets);

        ArrayList<Ticket> addedTickets = new ArrayList<>();
        for(Ticket t : tickets) {
            if (t.getGroundFloor() == null) continue;
            if (t.getGroundFloor().getId().equals(gf.getId())) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals(t.getGroundFloor().getId(), groundFloorId);
                assertEquals(t.getSingleEvent().getId(), gf.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), gf.getPrice(), 1d);
                assertTrue(t.isPaid());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(gf.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            }
        }
        assertEquals(addedTickets.size(), numberOfAddedTickets);
    }

    @Test
    @Transactional
    public void successfulCreatePayedTickets_SeatSpace() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();
        Long seatSpaceId = 997L;
        SeatSpace seatSpace = dataGraph.findSeatSpaceById(seatSpaceId);
        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setSeatSpaceId(seatSpaceId);
        ticketDto1.setRow(1);
        ticketDto1.setSeat(0);
        ticketDtos.add(ticketDto1);

        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setSeatSpaceId(seatSpaceId);
        ticketDto2.setRow(0);
        ticketDto2.setSeat(1);
        ticketDtos.add(ticketDto2);

        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(seatSpaceId);
        ticketDto3.setRow(1);
        ticketDto3.setSeat(1);
        ticketDtos.add(ticketDto3);

        TicketDto ticketDto4 = new TicketDto();
        ticketDto4.setSeatSpaceId(seatSpaceId);
        ticketDto4.setRow(1);
        ticketDto4.setSeat(2);
        ticketDtos.add(ticketDto4);

        TicketDto ticketDto5 = new TicketDto();
        ticketDto5.setSeatSpaceId(seatSpaceId);
        ticketDto5.setRow(2);
        ticketDto5.setSeat(2);
        ticketDtos.add(ticketDto5);

        crudService.createPayedTickets(ticketDtos);

        ArrayList<Ticket> tickets = new ArrayList<>();
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 0));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 0, 1));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 1));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 1, 2));
        tickets.add(repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId, 2, 2));

        for (Ticket t : tickets) {
            assertNotNull(t);
            assertNotNull(t.getUser());
            assertEquals(t.getUser().getUsername(), "username");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(seatSpace.getSpaceLayout().getSingleEvent().getStartDate());
            calendar.add(Calendar.DAY_OF_MONTH, - 1);
            assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
            assertTrue(t.isPaid());
        }
        assertEquals(repository.findAll().size(), dataGraph.getTickets().size());
    }

    @Test
    @Transactional
    public void successfulCreatePayedTickets_SeatSpaceAndGroundFloor() throws AddException, FetchException, IDMappingException {
        TestUtil.mockSecurityContext();
        long groundFloorId1 = 9923L;
        long groundFloorId2 = 996L;
        Long seatSpaceId1 = 997L;
        Long seatSpaceId2 = 998L;

        GroundFloor groundFloor1 = dataGraph.findGroundFloorById(groundFloorId1);
        GroundFloor groundFloor2 = dataGraph.findGroundFloorById(groundFloorId2);
        SeatSpace seatSpace1 = dataGraph.findSeatSpaceById(seatSpaceId1);
        SeatSpace seatSpace2 = dataGraph.findSeatSpaceById(seatSpaceId2);

        ArrayList<TicketDto> ticketDtos = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(groundFloorId1);
        ticketDtos.add(ticketDto1);

        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(groundFloorId2);
        ticketDtos.add(ticketDto2);

        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(seatSpaceId1);
        ticketDto3.setRow(1);
        ticketDto3.setSeat(0);
        ticketDtos.add(ticketDto3);

        TicketDto ticketDto4 = new TicketDto();
        ticketDto4.setSeatSpaceId(seatSpaceId2);
        ticketDto4.setRow(2);
        ticketDto4.setSeat(0);
        ticketDtos.add(ticketDto4);

        TicketDto ticketDto5 = new TicketDto();
        ticketDto5.setGroundFloorId(groundFloorId2);
        ticketDtos.add(ticketDto5);

        crudService.createPayedTickets(ticketDtos);

        List<Ticket> tickets = repository.findAll();
        assertEquals(dataGraph.getTickets().size() + 3, tickets.size());

        Ticket ticket3 = repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId1, 1, 0);
        assertNotNull(ticket3);
        assertNotNull(ticket3.getUser());
        assertEquals(ticket3.getUser().getUsername(), "username");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(seatSpace1.getSpaceLayout().getSingleEvent().getStartDate());
        calendar.add(Calendar.DAY_OF_MONTH, - 1);
        assertEquals(ticket3.getExpirationDate().compareTo(calendar.getTime()), 0);
        assertTrue(ticket3.isPaid());

        Ticket ticket4 = repository.findBySeatSpaceIdAndRowAndSeat(seatSpaceId2, 2, 0);
        assertNotNull(ticket4);
        assertNotNull(ticket4.getUser());
        assertEquals(ticket4.getUser().getUsername(), "username");
        calendar.setTime(seatSpace2.getSpaceLayout().getSingleEvent().getStartDate());
        calendar.add(Calendar.DAY_OF_MONTH, - 1);
        assertEquals(ticket4.getExpirationDate().compareTo(calendar.getTime()), 0);
        assertTrue(ticket4.isPaid());

        GroundFloor gf1 = groundFloorRepository.getOne(groundFloorId1);
        assertEquals((long)groundFloor1.getSpotsReserved() + 1, (long)gf1.getSpotsReserved());

        GroundFloor gf2 = groundFloorRepository.getOne(groundFloorId2);
        assertEquals((long)groundFloor2.getSpotsReserved() + 2, (long)gf2.getSpotsReserved());


        ArrayList<Ticket> addedTickets = new ArrayList<>();
        for(Ticket t : tickets) {
            if (t.getGroundFloor() == null) continue;
            if (t.getGroundFloor().getId().equals(groundFloor1.getId())) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals((long)t.getGroundFloor().getId(), groundFloorId1);
                assertEquals(t.getSingleEvent().getId(), groundFloor1.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), groundFloor1.getPrice(), 1d);
                assertTrue(t.isPaid());
                calendar.setTime(groundFloor1.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            } else if (t.getGroundFloor().getId().equals(groundFloorId2)) {
                if (dataGraph.findTicketById(t.getId()).getId() != null) {
                    continue;
                }
                assertEquals((long)t.getGroundFloor().getId(), groundFloorId2);
                assertEquals(t.getSingleEvent().getId(), groundFloor2.getSpaceLayout().getSingleEvent().getId());
                assertEquals(t.getPrice(), groundFloor2.getPrice(), 1d);
                assertTrue(t.isPaid());
                calendar.setTime(groundFloor2.getSpaceLayout().getSingleEvent().getStartDate());
                calendar.add(Calendar.DAY_OF_MONTH, - 1);
                assertEquals(t.getExpirationDate().compareTo(calendar.getTime()), 0);
                assertEquals(t.getUser().getUsername(), "username");
                addedTickets.add(t);
            }
        }
        assertEquals(addedTickets.size(), 3);
    }

    @Test
    @Transactional
    public void failedPayForTicket_WrongTicketUser() throws FetchException, UpdateException {
        TestUtil.mockSecurityContext("username2");
        long ticketId = 999992119L;

        exceptionRule.expect(UpdateException.class);
        exceptionRule.expectMessage("Ticket does not belong to this user.");

        crudService.payForTicket(ticketId);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedPayForTicket_TicketIsPaid() throws FetchException, UpdateException {
        TestUtil.mockSecurityContext();
        long ticketId = 999992118L;

        exceptionRule.expect(UpdateException.class);
        exceptionRule.expectMessage("Ticket already paid for.");

        crudService.payForTicket(ticketId);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void successPayForTicket() throws FetchException, UpdateException {
        TestUtil.mockSecurityContext();
        long ticketId = 999992119L;

        crudService.payForTicket(ticketId);
        Ticket ticket = repository.getOne(ticketId);
        assertTrue(ticket.isPaid());
    }

    @Test
    @Transactional
    public void failedDeleteById_WrongTicketUser() throws FetchException, DeleteException {
        TestUtil.mockSecurityContext("username2");
        long ticketId = 999992119L;

        exceptionRule.expect(DeleteException.class);
        exceptionRule.expectMessage("Ticket does not belong to this user.");

        crudService.deleteById(ticketId);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void failedDeleteById_TicketIsPaid() throws FetchException, DeleteException {
        TestUtil.mockSecurityContext();
        long ticketId = 999992118L;

        exceptionRule.expect(DeleteException.class);
        exceptionRule.expectMessage("Ticket already purchased");

        crudService.deleteById(ticketId);

        verifyDataUnchanged();
    }

    @Test
    @Transactional
    public void successDeleteById_SeatSpace() throws FetchException, DeleteException {
        TestUtil.mockSecurityContext();
        long ticketId = 999992119L;

        crudService.deleteById(ticketId);

        Ticket ticket = repository.getOne(ticketId);
        assertNull(ticket.getUser());
    }

    @Test
    @Transactional
    public void successDeleteById_GroundFloor() throws FetchException, DeleteException {
        TestUtil.mockSecurityContext();
        long ticketId = 999992110L;

        crudService.deleteById(ticketId);

        Ticket ticket = repository.getOne(ticketId);
        assertTrue(ticket.isDeleted());
        List<Ticket> tickets = repository.findAll();
        assertEquals(tickets.size() + 1, dataGraph.getTickets().size());
    }

    @Override
    public void successfulDelete() {

    }


}
