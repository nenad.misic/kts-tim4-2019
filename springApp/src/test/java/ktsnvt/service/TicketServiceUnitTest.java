package ktsnvt.service;

import ktsnvt.common.exceptions.*;
import ktsnvt.dto.TicketDto;
import ktsnvt.entity.*;
import ktsnvt.repository.GroundFloorRepository;
import ktsnvt.repository.TicketRepository;
import ktsnvt.repository.UserRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TicketServiceUnitTest extends CrudServiceGenericUnitTest<TicketService, TicketRepository, Ticket> {


    @MockBean
    private TicketRepository mockedRepo;

    @MockBean
    private UserRepository mockedUserRepo;

    @MockBean
    private GroundFloorRepository mockedGroundFloorRepo;

    @Autowired
    private TicketService ticketService;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Override
    public Ticket createSampleValue() {
        return new Ticket();
    }


    @Override
    public TicketRepository getMockedRepo() {
        return mockedRepo;
    }

    private Ticket createSampleValueSuccessfulAddGroundFloor() {
        Ticket retVal = new Ticket();

        SingleEvent event = new SingleEvent();
        retVal.setSingleEvent(event);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 5);
        event.setStartDate(calendar.getTime());

        GroundFloor groundFloor = new GroundFloor();
        groundFloor.setCapacity(10);
        groundFloor.setSpotsReserved(9);
        retVal.setGroundFloor(groundFloor);

        return retVal;
    }

    private Ticket createSampleValueSuccessfulAddSeatSpace() {
        Ticket retVal = new Ticket();

        SingleEvent event = new SingleEvent();
        retVal.setSingleEvent(event);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 5);
        event.setStartDate(calendar.getTime());

        SeatSpace seatSpace = new SeatSpace();
        seatSpace.setId(2L);
        seatSpace.setRows(5);
        seatSpace.setSeats(5);
        retVal.setSeatSpace(seatSpace);

        retVal.setSeat(3);
        retVal.setRow(3);

        return retVal;
    }

    //for now left empty because name is not adequate
    @Override
    public void successfulAdd() {}



    @Test
    public void successfulAddSeatSpaceRowSeatMin() throws AddException {
        Ticket ticket = createSampleValueSuccessfulAddSeatSpace();
        ticket.setSeat(1);
        ticket.setRow(1);
        SeatSpace seatSpace = ticket.getSeatSpace();
        crudService.repository = mockedRepo;
        when(mockedRepo.existsBySeatSpaceIdAndRowAndSeat(seatSpace.getId(), ticket.getRow(), ticket.getSeat())).thenReturn(false);
        crudService.add(ticket);

        verify(mockedRepo, times(1)).save(ticket);
    }

    @Test
    public void successfulAddSeatSpaceRowSeatMax() throws AddException {
        Ticket ticket = createSampleValueSuccessfulAddSeatSpace();
        ticket.setSeat(ticket.getSeatSpace().getSeats());
        ticket.setRow(ticket.getSeatSpace().getRows());
        SeatSpace seatSpace = ticket.getSeatSpace();
        crudService.repository = mockedRepo;
        when(mockedRepo.existsBySeatSpaceIdAndRowAndSeat(seatSpace.getId(), ticket.getRow(), ticket.getSeat())).thenReturn(false);
        crudService.add(ticket);

        verify(mockedRepo, times(1)).save(ticket);
    }


    private void mockSecurityContext() {
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        when(mockedUserRepo.findByUsername("username")).thenReturn(user);
        Authentication authentication = mock(Authentication.class);
        org.springframework.security.core.userdetails.User userAuth =
                new org.springframework.security.core.userdetails.User(
                        "username", "password", new ArrayList<>());
        when(authentication.getPrincipal()).thenReturn(userAuth);
        SecurityContext sc = mock(SecurityContext.class);
        when(sc.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(sc);
    }

    @Test
    public void failedDelete_WrongUser() throws FetchException, DeleteException {
        mockSecurityContext();
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        User user = new User();
        user.setUsername("wrongUsername");
        ticket.setUser(user);
        when(mockedRepo.findById(1L)).thenReturn(Optional.of(ticket));

        exceptionRule.expect(DeleteException.class);
        exceptionRule.expectMessage("Ticket does not belong to this user.");
        this.ticketService.deleteById(1L);
    }

    @Test
    public void failedDelete_TicketIsPaid() throws FetchException, DeleteException {
        mockSecurityContext();
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        User user = new User();
        user.setUsername("username");
        ticket.setUser(user);
        ticket.setPaid(true);
        when(mockedRepo.findById(1L)).thenReturn(Optional.of(ticket));

        exceptionRule.expect(DeleteException.class);
        exceptionRule.expectMessage("Ticket already purchased");
        this.ticketService.deleteById(1L);
    }

    @Test
    public void failedDelete_TicketDoesNotExist() throws FetchException, DeleteException {
        mockSecurityContext();
        Long id = 1L;
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Ticket with id: " + id + "does not exist.");
        this.ticketService.deleteById(id);
    }

    @Override
    public void successfulDelete() {

    }

    @Test
    public void successfulDelete_TicketHasGroundFloor() throws FetchException, DeleteException {
        mockSecurityContext();
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        User user = new User();
        user.setUsername("username");
        ticket.setUser(user);
        ticket.setPaid(false);
        GroundFloor gf = new GroundFloor();
        gf.setSpotsReserved(10);
        gf.setCapacity(20);
        ticket.setGroundFloor(gf);
        when(mockedRepo.findById(1L)).thenReturn(Optional.of(ticket));
        this.ticketService.deleteById(1L);
        verify(mockedRepo, times(1)).save(ticket);
        verify(mockedGroundFloorRepo, times(1)).save(gf);
    }

    @Test
    public void successfulDelete_TicketHasSeatSpace() throws FetchException, DeleteException {
        mockSecurityContext();
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        User user = new User();
        user.setUsername("username");
        ticket.setUser(user);
        ticket.setPaid(false);
        ticket.setSeatSpace(new SeatSpace());
        when(mockedRepo.findById(1L)).thenReturn(Optional.of(ticket));
        this.ticketService.deleteById(1L);
        verify(mockedRepo, times(1)).save(ticket);
    }

    @Test
    public void failedAdd_NoUser() throws AddException, InterruptedException, FetchException, IDMappingException {
        when(mockedUserRepo.findByUsername("username")).thenReturn(null);
        Authentication authentication = mock(Authentication.class);
        org.springframework.security.core.userdetails.User userAuth =
                new org.springframework.security.core.userdetails.User(
                        "username", "password", new ArrayList<>());
        when(authentication.getPrincipal()).thenReturn(userAuth);
        SecurityContext sc = mock(SecurityContext.class);
        when(sc.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(sc);
        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Unable to find logged in user");
        this.ticketService.add(new ArrayList<>());
    }

    @Test
    public void failedAdd_BothSeatSpaceAndGroundFloor() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        ticketDto.setSeatSpaceId(1L);
        tickets.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation on both seat space and ground floor");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_SeatSpaceTicketNotExists() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(null);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(null);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ticket");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_SeatSpaceTicketHasUser() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(new User());

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ticket is already reserved");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_SeatSpaceEventDateBeforeDateNow() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);

        SingleEvent event = new SingleEvent();
        event.setStartDate(new Date(0));
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for event that has already happened.");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_SeatSpaceEventDateLessThan24h() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);

        SingleEvent event = new SingleEvent();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 12);
        event.setStartDate(calendar.getTime());
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation 24h before the event.");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_SeatSpaceSameTicketMultipleTimes() throws AddException, InterruptedException, FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setSeatSpaceId(1L);
        ticketDto1.setRow(1);
        ticketDto1.setSeat(1);
        tickets.add(ticketDto1);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);

        SingleEvent event = new SingleEvent();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 1000);
        event.setStartDate(calendar.getTime());
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);
        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for the same seat twice.");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_InvalidGroundFloorId() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.empty());

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ground floor id in ticket");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_GroundFloorNoEvent() throws AddException, InterruptedException, FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(null);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ground floor is part of default space layout.");
        this.ticketService.add(tickets);

    }


    @Test
    public void failedAdd_GroundFloorEventDateBeforeDateNow() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        SingleEvent event = new SingleEvent();
        event.setStartDate(new Date(0));

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for event that has already happened.");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_NoSeatSpaceNorGroundFloor() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        tickets.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation. Neither seatSpace nor GroundFloor selected");
        this.ticketService.add(tickets);
    }


    @Test
    public void failedAdd_NotEnoughSpaceInGroundFloor() throws AddException, InterruptedException, FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(1L);
        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(1L);
        tickets.add(ticketDto1);
        tickets.add(ticketDto2);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 2);
        date = c.getTime();

        SingleEvent event = new SingleEvent();
        event.setStartDate(date);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        gf.setCapacity(2);
        gf.setSpotsReserved(1);
        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));


        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");
        this.ticketService.add(tickets);
    }

    @Test
    public void failedAdd_GroundFloorEventDateLessThan24h() throws AddException, InterruptedException,
            FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        SingleEvent event = new SingleEvent();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 12);
        event.setStartDate(calendar.getTime());

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation 24h before the event.");
        this.ticketService.add(tickets);
    }


    @Test
    public void successAdd() throws AddException, InterruptedException, FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(1L);
        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(1L);
        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(1L);
        ticketDto3.setSeat(1);
        ticketDto3.setRow(1);
        tickets.add(ticketDto1);
        tickets.add(ticketDto2);
        tickets.add(ticketDto3);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 2);
        date = c.getTime();

        SingleEvent event = new SingleEvent();
        event.setStartDate(date);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        gf.setCapacity(5);
        gf.setSpotsReserved(1);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);
        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        this.ticketService.add(tickets);

        verify(mockedRepo, times(3)).save(any());
        verify(mockedGroundFloorRepo, times(1)).save(any());
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_NoUser() throws AddException, FetchException, IDMappingException {
        when(mockedUserRepo.findByUsername("username")).thenReturn(null);
        Authentication authentication = mock(Authentication.class);
        org.springframework.security.core.userdetails.User userAuth =
                new org.springframework.security.core.userdetails.User(
                        "username", "password", new ArrayList<>());
        when(authentication.getPrincipal()).thenReturn(userAuth);
        SecurityContext sc = mock(SecurityContext.class);
        when(sc.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(sc);
        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Unable to find logged in user");
        this.ticketService.createPayedTickets(new ArrayList<>());
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_BothSeatSpaceAndGroundFloor() throws AddException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        ticketDto.setSeatSpaceId(1L);
        tickets.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation on both seat space and ground floor");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_SeatSpaceTicketNotExists() throws AddException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(null);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(null);

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ticket");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_SeatSpaceTicketHasUser() throws AddException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(new User());

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ticket is already reserved");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_SeatSpaceEventDateBeforeDateNow() throws AddException,
            FetchException, IDMappingException {
        this.mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);

        SingleEvent event = new SingleEvent();
        event.setStartDate(new Date(0));
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for event that has already happened.");
        this.ticketService.createPayedTickets(tickets);

    }


    @Test
    @Transactional
    public void failedCreatePayedTickets_SeatSpaceSameTicketMultipleTimes() throws AddException, FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setSeatSpaceId(1L);
        ticketDto.setRow(1);
        ticketDto.setSeat(1);
        tickets.add(ticketDto);

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setSeatSpaceId(1L);
        ticketDto1.setRow(1);
        ticketDto1.setSeat(1);
        tickets.add(ticketDto1);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);

        SingleEvent event = new SingleEvent();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 1000);
        event.setStartDate(calendar.getTime());
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);
        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for the same seat twice.");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_InvalidGroundFloorId() throws AddException,
            FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.empty());

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Invalid ground floor id in ticket");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_GroundFloorNoEvent() throws AddException, FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(null);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Ground floor is part of default space layout.");
        this.ticketService.createPayedTickets(tickets);

    }


    @Test
    @Transactional
    public void failedCreatePayedTickets_GroundFloorEventDateBeforeDateNow() throws AddException,
            FetchException, IDMappingException {
        mockSecurityContext();

        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        ticketDto.setGroundFloorId(1L);
        tickets.add(ticketDto);

        SingleEvent event = new SingleEvent();
        event.setStartDate(new Date(0));

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation for event that has already happened.");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void failedCreatePayedTickets_NoSeatSpaceNorGroundFloor() throws AddException,
            FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();
        TicketDto ticketDto = new TicketDto();
        tickets.add(ticketDto);

        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Unable to make reservation. Neither seatSpace nor GroundFloor selected");
        this.ticketService.createPayedTickets(tickets);
    }


    @Test
    @Transactional
    public void failedCreatePayedTickets_NotEnoughSpaceInGroundFloor() throws AddException, FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(1L);
        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(1L);
        tickets.add(ticketDto1);
        tickets.add(ticketDto2);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 2);
        date = c.getTime();

        SingleEvent event = new SingleEvent();
        event.setStartDate(date);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        gf.setCapacity(2);
        gf.setSpotsReserved(1);
        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));


        exceptionRule.expect(AddException.class);
        exceptionRule.expectMessage("Not enough space in ground fljoor");
        this.ticketService.createPayedTickets(tickets);
    }

    @Test
    @Transactional
    public void successCreatePayedTickets() throws AddException, FetchException, IDMappingException {
        mockSecurityContext();
        ArrayList<TicketDto> tickets = new ArrayList<>();

        TicketDto ticketDto1 = new TicketDto();
        ticketDto1.setGroundFloorId(1L);
        TicketDto ticketDto2 = new TicketDto();
        ticketDto2.setGroundFloorId(1L);
        TicketDto ticketDto3 = new TicketDto();
        ticketDto3.setSeatSpaceId(1L);
        ticketDto3.setSeat(1);
        ticketDto3.setRow(1);
        tickets.add(ticketDto1);
        tickets.add(ticketDto2);
        tickets.add(ticketDto3);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 2);
        date = c.getTime();

        SingleEvent event = new SingleEvent();
        event.setStartDate(date);

        SpaceLayout sl = new SpaceLayout();
        sl.setSingleEvent(event);

        GroundFloor gf = new GroundFloor();
        gf.setSpaceLayout(sl);

        gf.setCapacity(5);
        gf.setSpotsReserved(1);

        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setUser(null);
        ticket.setSingleEvent(event);

        when(mockedRepo.findBySeatSpaceIdAndRowAndSeat(1L, 1, 1)).thenReturn(ticket);
        when(this.mockedGroundFloorRepo.fetchByIdAndLock(1L)).thenReturn(Optional.of(gf));

        this.ticketService.createPayedTickets(tickets);

        verify(mockedRepo, times(3)).save(any());
        verify(mockedGroundFloorRepo, times(1)).save(any());
    }

    @Test
    public void failedPayForTicket_TicketPaidTrue() throws FetchException, UpdateException {
        mockSecurityContext();

        Ticket ticket = new Ticket();
        ticket.setPaid(true);
        when(mockedRepo.findById(1L)).thenReturn(Optional.of(ticket));

        exceptionRule.expect(UpdateException.class);
        exceptionRule.expectMessage("Ticket already paid for.");
        this.ticketService.payForTicket(1L);
    }

    @Test
    public void failedPayForTicket_TicketDoesNotExist() throws FetchException, UpdateException {
        mockSecurityContext();

        long id = 1L;
        when(mockedRepo.findById(id)).thenReturn(Optional.empty());

        exceptionRule.expect(FetchException.class);
        exceptionRule.expectMessage("Ticket with id: " + id + " does not exist.");
        this.ticketService.payForTicket(id);
    }

    @Test
    public void failedPayForTicket_UserIdDoesNotMatch() throws FetchException, UpdateException {
        mockSecurityContext();

        long id = 1L;
        Ticket ticket = new Ticket();
        ticket.setPaid(false);

        User user = new User();
        user.setId(2L);
        ticket.setUser(user);

        when(mockedRepo.findById(id)).thenReturn(Optional.of(ticket));

        exceptionRule.expect(UpdateException.class);
        exceptionRule.expectMessage("Ticket does not belong to this user.");
        this.ticketService.payForTicket(id);
    }
}
