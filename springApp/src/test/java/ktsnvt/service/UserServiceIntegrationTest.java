package ktsnvt.service;

import com.sun.mail.iap.Argument;
import ktsnvt.dto.UserDto;
import ktsnvt.entity.*;
import ktsnvt.repository.UserRepository;
import ktsnvt.repository.VerificationTokenRepository;
import ktsnvt.utility.DataGraph;
import ktsnvt.utility.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

import org.springframework.security.core.userdetails.UserDetails;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class UserServiceIntegrationTest extends CrudServiceGenericIntegrationTest<UserService, UserRepository, User> {

    private DataGraph dataGraph;

    @MockBean
    private EmailService emailService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    public UserServiceIntegrationTest() {
        this.dataGraph = TestUtil.extractDataFromDataSql();
    }


    @Transactional
    @Test
    public void loadByUsernameDoesNotExist() {
        String username = "izmisljenUsernamenjohnjoh";

        UsernameNotFoundException usernameNotFoundException = assertThrows(UsernameNotFoundException.class, () -> {
            crudService.loadUserByUsername(username);
        });
        assertEquals(String.format("No user found with username '%s'.", username), usernameNotFoundException.getMessage());
    }

    @Transactional
    @Test
    public void loadByUsernameExists() {
        User user = dataGraph.findUserById(getIdUserRoleNormal());

        UserDetails retVal = crudService.loadUserByUsername(user.getUsername());

        assertEquals(user.getUsername(), retVal.getUsername());
        assertEquals(user.getPassword(), retVal.getPassword());

        assertEquals(UserRole.Regular.toString(), retVal.getAuthorities().iterator().next().getAuthority());
    }

    @Transactional
    @Test
    public void registerFailedUsernameTaken() {
        User user = dataGraph.findUserById(getIdExists());
        String username = user.getUsername();

        UserDto userDto = new UserDto();
        userDto.setUsername(username);

        Exception exception = assertThrows(Exception.class, () -> {
            crudService.register(userDto);
        });
        assertEquals("Username already taken!", exception.getMessage());
    }

    @Transactional
    @Test
    public void registerSuccessful() throws Exception {
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);
        String username = "TestUsername";
        String password = "password";
        String email = "testEmail@email.com";
        String name = "TestName";
        String surname = "TestSurname";
        String encodedPassword = "encodedPassword";

        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        userDto.setPassword(password);
        userDto.setEmail(email);
        userDto.setName(name);
        userDto.setSurname(surname);

        Mockito.when(passwordEncoder.encode(password)).thenReturn(encodedPassword);

        User retVal = crudService.register(userDto);

        Mockito.verify(emailService, times(1))
                .sendVerificationMessage(userArgumentCaptor.capture(), verificationTokenArgumentCaptor.capture());
        assertEquals(userArgumentCaptor.getValue(), retVal);

        assertEquals(username, retVal.getUsername());
        assertEquals(encodedPassword, retVal.getPassword());
        assertEquals(email, retVal.getEmail());
        assertEquals(name, retVal.getName());
        assertEquals(surname, retVal.getSurname());
    }

    @Transactional
    @Test
    public void verificationFailedBadLink() {
        String token = "badToken";

        Exception exception = assertThrows(Exception.class, () -> {
           crudService.verifyUser(token);
        });
    }

    @Transactional
    @Test
    public void verificationSuccessful() throws Exception {
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);
        String username = "TestUsername";
        String password = "password";
        String email = "testEmail@email.com";
        String name = "TestName";
        String surname = "TestSurname";

        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        userDto.setPassword(password);
        userDto.setEmail(email);
        userDto.setName(name);
        userDto.setSurname(surname);

        crudService.register(userDto);

        Mockito.verify(emailService, times(1))
                .sendVerificationMessage(userArgumentCaptor.capture(), verificationTokenArgumentCaptor.capture());

        User preVerification = this.repository.findByUsername(username);
        assertFalse(preVerification.isVerified());

        VerificationToken verificationToken = verificationTokenArgumentCaptor.getValue();
        crudService.verifyUser(verificationToken.getToken());

        User postVerification = this.repository.findByUsername(username);
        assertTrue(postVerification.isVerified());
    }


    public Long getIdUserRoleNormal() {
        for (User user: dataGraph.getUsers()) {
            if (user.getRole().equals(UserRole.Regular)) {
                return user.getId();
            }
        }
        return null;
    }

    @Override
    public Long getIdNotExists() {
        return 6969l;
    }

    @Override
    public Long getIdExists() {
        return dataGraph.getUsers().get(0).getId();
    }


    @Override
    public Long getIdFromEntity(User user) {
        return user.getId();
    }

    @Override
    public int getNumberOfEntitiesInDb() {
        return dataGraph.getUsers().size();
    }

    @Override
    public User createSampleValue() {
        User user = new User();
        user.setTickets(new HashSet<Ticket>());
        user.setUsername("username");
        user.setPassword("password");
        user.setName("name");
        user.setSurname("surname");
        user.setEmail("email");
        user.setVerified(false);
        user.setRole(UserRole.Regular);
        user.setUserAuthorities(new HashSet<UserAuthority>());
        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setRole(UserRole.Regular);
        user.getUserAuthorities().add(userAuthority);
        return user;
    }

    @Override
    public void changeEntitiesAttributes(User user) {
        user.setName("IzmenjenoIme");
    }

    @Override
    public boolean verifyChangedAttributes(User user) {
        return "IzmenjenoIme".equals(user.getName());
    }

    @Override
    public boolean entityEquals(User e1, User e2) {
        if (e1 == e2) {
            return true;
        }
        if (e1 == null || e2 == null) {
            return false;
        }
        if (!e1.getId().equals(e2.getId())) {
            return false;
        }
        if (!e1.getUsername().equals(e2.getUsername())) {
            return  false;
        }
        if (!e1.getPassword().equals(e2.getPassword())) {
            return false;
        }
        if (!e1.getName().equals(e2.getName())) {
            return false;
        }
        if (!e1.getSurname().equals(e2.getSurname())) {
            return false;
        }
        if (!e1.getRole().equals(e2.getRole())) {
            return false;
        }

        return true;

    }

    @Override
    public User getEntityFromIdExists() {
        return dataGraph.findUserById(getIdExists());
    }

    @Override
    public void setEntityId(User user, Long id) {
        user.setId(id);
    }

    @Override
    public User getEntityFromDataGraph(Long id) {
        return dataGraph.findUserById(id);
    }

    // users never get deleted
    @Override
    public void successfulDelete() { }

    @Override
    public Long getIdDeletable() {
        return null;
    }


}
