package ktsnvt.service;

import ktsnvt.dto.UserDto;
import ktsnvt.entity.User;
import ktsnvt.entity.UserAuthority;
import ktsnvt.entity.UserRole;
import ktsnvt.entity.VerificationToken;
import ktsnvt.repository.UserAuthorityRepository;
import ktsnvt.repository.UserRepository;
import ktsnvt.repository.VerificationTokenRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceUnitTest extends CrudServiceGenericUnitTest<UserService, UserRepository, User> {

    @MockBean
    private UserRepository mockedRepo;

    @MockBean
    private EmailService emailServiceMocked;

    @MockBean
    private VerificationTokenRepository verificationTokenRepositoryMocked;

    @MockBean
    private UserAuthorityRepository userAuthorityRepositoryMocked;

    @MockBean
    private PasswordEncoder passwordEncoderMocked;


    @Override
    public User createSampleValue() {
        return new User();
    }

    @Override
    public UserRepository getMockedRepo() {
        return mockedRepo;
    }

    @Test
    public void loadByUsernameDoesNotExist() {
        String username = "TestUsername";
        crudService.repository = mockedRepo;
        when(mockedRepo.findByUsername(username)).thenReturn(null);
        UsernameNotFoundException usernameNotFoundException = assertThrows(UsernameNotFoundException.class, () -> {
           crudService.loadUserByUsername(username);
        });
        assertEquals(String.format("No user found with username '%s'.", username), usernameNotFoundException.getMessage());
    }

    @Test
    public void loadByUsernameExists() {
        String username = "TestUsername";
        String password = "password";
        UserRole role = UserRole.Admin;

        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setRole(role);

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        user.setUserAuthorities(new HashSet<>());
        user.getUserAuthorities().add(userAuthority);

        crudService.repository = mockedRepo;
        when(mockedRepo.findByUsername(username)).thenReturn(user);

        UserDetails springUser = crudService.loadUserByUsername(username);

        assertEquals(username, springUser.getUsername());
        assertEquals(password, springUser.getPassword());
        assertEquals(1, springUser.getAuthorities().size());
        assertTrue(springUser.getAuthorities().contains(userAuthority));
    }

    @Test
    public void registerFailedUsernameTaken() {
        String username = "TestUsername";

        UserDto userDto = new UserDto();
        userDto.setUsername(username);

        crudService.repository = mockedRepo;
        when(mockedRepo.findByUsername(username)).thenReturn(new User());

        Exception exception = assertThrows(Exception.class, () -> {
            crudService.register(userDto);
        });
        assertEquals("Username already taken!", exception.getMessage());
    }

    @Test
    public void registerSuccessful() throws Exception {
        String username = "TestUsername";
        String password = "password";
        String email = "testEmail@email.com";
        String name = "TestName";
        String surname = "TestSurname";

        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        userDto.setPassword(password);
        userDto.setEmail(email);
        userDto.setName(name);
        userDto.setSurname(surname);

        UserAuthority userAuthority = new UserAuthority();

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);

        when(passwordEncoderMocked.encode(password)).thenReturn("encodedPassword");
        when(userAuthorityRepositoryMocked.findByRole(UserRole.Regular)).thenReturn(userAuthority);

        User retVal = crudService.register(userDto);

        assertEquals(username, retVal.getUsername());
        assertEquals("encodedPassword", retVal.getPassword());
        assertEquals(email, retVal.getEmail());
        assertEquals(name, retVal.getName());
        assertEquals(surname, retVal.getSurname());
        assertFalse(retVal.isVerified());
        assertEquals(UserRole.Regular, retVal.getRole()); // Only regular users can be created through registration
        assertEquals(1, retVal.getUserAuthorities().size());
        assertTrue(retVal.getUserAuthorities().contains(userAuthority));

        verify(mockedRepo).save(retVal);
        verify(emailServiceMocked).sendVerificationMessage(userArgumentCaptor.capture(), verificationTokenArgumentCaptor.capture());

        assertEquals(userArgumentCaptor.getValue(), retVal);
        assertEquals(retVal, verificationTokenArgumentCaptor.getValue().getUser());

        verify(verificationTokenRepositoryMocked).save(verificationTokenArgumentCaptor.getValue());
    }

    @Test
    public void verifyFailedBadLink() {
        String token = "badToken";

        when(verificationTokenRepositoryMocked.findByToken(token)).thenReturn(null);

        Exception exception = assertThrows(Exception.class, () -> {
            crudService.verifyUser(token);
        });
        assertEquals("The link is invalid or broken!", exception.getMessage());
    }

    @Test
    public void verifySuccessful() throws Exception {
        String token = "goodToken";
        String username = "Username";

        User user = new User();
        user.setUsername(username);
        user.setVerified(false);

        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        when(verificationTokenRepositoryMocked.findByToken(token)).thenReturn(verificationToken);
        when(mockedRepo.findByUsername(username)).thenReturn(user);

        crudService.verifyUser(token);

        assertTrue(user.isVerified());
        verify(mockedRepo, times(1)).save(user);
    }


}
