package ktsnvt.utility;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import ktsnvt.entity.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DataGraph {

    private ArrayList<User> users;
    private ArrayList<Location> locations;
    private ArrayList<Space> spaces;
    private ArrayList<SpaceLayout> spaceLayouts;
    private ArrayList<SeatSpace> seatSpaces;
    private ArrayList<GroundFloor> groundFloors;
    private ArrayList<Event> events;
    private ArrayList<SingleEvent> singleEvents;
    private ArrayList<CompositeEvent> compositeEvents;
    private ArrayList<Ticket> tickets;

    public DataGraph() {
        users = new ArrayList<>();
        locations = new ArrayList<>();
        spaces = new ArrayList<>();
        spaceLayouts = new ArrayList<>();
        seatSpaces = new ArrayList<>();
        groundFloors = new ArrayList<>();
        events = new ArrayList<>();
        singleEvents = new ArrayList<>();
        compositeEvents = new ArrayList<>();
        tickets = new ArrayList<>();
    }

    public DataGraph(ArrayList<User> users,
                     ArrayList<Location> locations,
                     ArrayList<Space> spaces,
                     ArrayList<SpaceLayout> spaceLayouts,
                     ArrayList<SeatSpace> seatSpaces,
                     ArrayList<GroundFloor> groundFloors,
                     ArrayList<Event> events,
                     ArrayList<SingleEvent> singleEvents,
                     ArrayList<CompositeEvent> compositeEvents,
                     ArrayList<Ticket> tickets) {
        this.users = users;
        this.locations = locations;
        this.spaces = spaces;
        this.spaceLayouts = spaceLayouts;
        this.seatSpaces = seatSpaces;
        this.groundFloors = groundFloors;
        this.events = events;
        this.singleEvents = singleEvents;
        this.compositeEvents = compositeEvents;
        this.tickets = tickets;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    public ArrayList<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(ArrayList<Space> spaces) {
        this.spaces = spaces;
    }

    public ArrayList<SpaceLayout> getSpaceLayouts() {
        return spaceLayouts;
    }

    public void setSpaceLayouts(ArrayList<SpaceLayout> spaceLayouts) {
        this.spaceLayouts = spaceLayouts;
    }

    public ArrayList<SeatSpace> getSeatSpaces() {
        return seatSpaces;
    }

    public void setSeatSpaces(ArrayList<SeatSpace> seatSpaces) {
        this.seatSpaces = seatSpaces;
    }

    public ArrayList<GroundFloor> getGroundFloors() {
        return groundFloors;
    }

    public void setGroundFloors(ArrayList<GroundFloor> groundFloors) {
        this.groundFloors = groundFloors;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public ArrayList<SingleEvent> getSingleEvents() {
        return singleEvents;
    }

    public void setSingleEvents(ArrayList<SingleEvent> singleEvents) {
        this.singleEvents = singleEvents;
    }

    public ArrayList<CompositeEvent> getCompositeEvents() {
        return compositeEvents;
    }

    public void setCompositeEvents(ArrayList<CompositeEvent> compositeEvents) {
        this.compositeEvents = compositeEvents;
    }

    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Location findLocationById(Long id) {
        for (Location l : locations) {
            if (l.getId().equals(id)) return l;
        }
        return new Location();
    }

    public Space findSpaceById(Long id) {
        for (Space s: spaces) {
            if (s.getId().equals(id)) return s;
        }
        return new Space();
    }

    public SpaceLayout findSpaceLayoutById(Long id) {
        for (SpaceLayout sl : spaceLayouts) {
            if (sl.getId().equals(id)) return sl;
        }
        return new SpaceLayout();
    }

    public Event findEventById(Long id) {
        for (Event e : events) {
            if (e.getId().equals(id)) return e;
        }
        return new SingleEvent();
    }

    public CompositeEvent findCompositeEventById(Long id) {
        for (CompositeEvent ce : compositeEvents) {
            if (ce.getId().equals(id)) return ce;
        }
        return new CompositeEvent();
    }

    public SingleEvent findSingleEventById(Long id) {
        for (SingleEvent se : singleEvents) {
            if (se.getId().equals(id)) return  se;
        }
        return new SingleEvent();
    }

    public GroundFloor findGroundFloorById(Long id) {
        for (GroundFloor gf : groundFloors) {
            if (gf.getId().equals(id)) return gf;
        }
        return new GroundFloor();
    }

    public SeatSpace findSeatSpaceById(Long id) {
        for (SeatSpace ss : seatSpaces) {
            if (ss.getId().equals(id)) return ss;
        }
        return new SeatSpace();
    }

    public User findUserById(Long id) {
        for (User u : users) {
            if (u.getId().equals(id)) return u;
        }
        return new User();
    }

    public Ticket findTicketById(Long id) {
        for (Ticket t: tickets) {
            if (t.getId().equals(id)) return t;
        }
        return new Ticket();
    }
}
