package ktsnvt.utility;


import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import ktsnvt.entity.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUtil {

    public static String json(Object object)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper.writeValueAsString(object);
    }

    public static void mockSecurityContext() {
        Authentication authentication = mock(Authentication.class);
        org.springframework.security.core.userdetails.User userAuth =
                new org.springframework.security.core.userdetails.User(
                        "username", "password", new ArrayList<>());
        when(authentication.getPrincipal()).thenReturn(userAuth);
        SecurityContext sc = mock(SecurityContext.class);
        when(sc.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(sc);
    }


    public static void mockSecurityContext(String username) {
        Authentication authentication = mock(Authentication.class);
        org.springframework.security.core.userdetails.User userAuth =
                new org.springframework.security.core.userdetails.User(
                        username, "password", new ArrayList<>());
        when(authentication.getPrincipal()).thenReturn(userAuth);
        SecurityContext sc = mock(SecurityContext.class);
        when(sc.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(sc);
    }

    public static DataGraph extractDataFromDataSql() {
        DataGraph dg = new DataGraph();
        URL url = TestUtil.class.getClassLoader().getResource("data.sql");
        File file = new File(url.getPath().replaceAll("%20", " "));
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            while(line != null) {
                if (line.contains("INSERT INTO ktsnvttest.user ")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, deleted, name, password, role, surname, username, email, verified)
                    User user = new User();
                    user.setId(Long.parseLong(values[0].trim()));
                    user.setDeleted(Boolean.parseBoolean(values[1]));
                    user.setName(values[2]);
                    user.setPassword(values[3]);
                    user.setRole(UserRole.values()[Integer.parseInt(values[4].trim())]);
                    user.setSurname(values[5]);
                    user.setUsername(values[6]);
                    user.setEmail(values[7]);
                    user.setVerified(Boolean.parseBoolean(values[8]));
                    user.setTickets(new HashSet<>());
                    dg.getUsers().add(user);

                } else if (line.contains("INSERT INTO ktsnvttest.location ")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, name, address, city, deleted,country)
                    Location location = new Location();
                    location.setId(Long.parseLong(values[0].trim()));
                    location.setName(values[1]);
                    location.setAddress(values[2]);
                    location.setCity(values[3]);
                    location.setDeleted(Boolean.parseBoolean(values[4]));
                    location.setCountry(values[5]);
                    location.setSpaces(new HashSet<>());
                    location.setLongitude(Double.parseDouble(values[6].trim()));
                    location.setLatitude(Double.parseDouble(values[7].trim()));

                    dg.getLocations().add(location);
                } else if (line.contains("INSERT INTO ktsnvttest.space ")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, name, location, deleted)
                    Space space = new Space();
                    space.setId(Long.parseLong(values[0].trim()));
                    space.setName(values[1]);
                    Location location = dg.findLocationById(Long.parseLong(values[2].trim()));
                    location.getSpaces().add(space);
                    space.setLocation(location);
                    space.setDeleted(Boolean.parseBoolean(values[3]));
                    space.setEvents(new HashSet<>());
                    space.setDefaultSpaceLayouts(new HashSet<>());

                    dg.getSpaces().add(space);
                } else if (line.contains("INSERT INTO ktsnvttest.space_layout")){
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, name, deleted, single_event, space)
                    SpaceLayout spaceLayout = new SpaceLayout();
                    spaceLayout.setId(Long.parseLong(values[0].trim()));
                    spaceLayout.setName(values[1]);
                    spaceLayout.setDeleted(Boolean.parseBoolean(values[2]));
                    spaceLayout.setSingleEvent(null);
                    if (values[4].equals("NULL")) {
                        spaceLayout.setSpace(null);
                    } else {
                        Space space = dg.findSpaceById(Long.parseLong(values[4].trim()));
                        space.getDefaultSpaceLayouts().add(spaceLayout);
                        spaceLayout.setSpace(space);
                    }
                    spaceLayout.setSeatSpaces(new HashSet<>());
                    spaceLayout.setGroundFloors(new HashSet<>());

                    dg.getSpaceLayouts().add(spaceLayout);
                } else if (line.contains("INSERT INTO ktsnvttest.ground_floor")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout)
                    GroundFloor groundFloor = new GroundFloor();
                    groundFloor.setId(Long.parseLong(values[0].trim()));
                    groundFloor.setName(values[1]);
                    groundFloor.setTop(Integer.parseInt(values[2].trim()));
                    groundFloor.setLeft(Integer.parseInt(values[3].trim()));
                    groundFloor.setCapacity(Integer.parseInt(values[4].trim()));
                    groundFloor.setDeleted(Boolean.parseBoolean(values[5]));
                    groundFloor.setHeight(Double.parseDouble(values[6].trim()));
                    groundFloor.setPrice(Double.parseDouble(values[7].trim()));
                    groundFloor.setSpotsReserved(Integer.parseInt(values[8].trim()));
                    groundFloor.setWidth(Double.parseDouble(values[9].trim()));
                    SpaceLayout spaceLayout = dg.findSpaceLayoutById(Long.parseLong(values[10].trim()));
                    spaceLayout.getGroundFloors().add(groundFloor);
                    groundFloor.setSpaceLayout(spaceLayout);
                    groundFloor.setTickets(new HashSet<>());

                    dg.getGroundFloors().add(groundFloor);
                } else if (line.contains("INSERT INTO ktsnvttest.seat_space")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout)
                    SeatSpace seatSpace = new SeatSpace();
                    seatSpace.setId(Long.parseLong(values[0].trim()));
                    seatSpace.setName(values[1]);
                    seatSpace.setTop(Integer.parseInt(values[2].trim()));
                    seatSpace.setLeft(Integer.parseInt(values[3].trim()));
                    seatSpace.setDeleted(Boolean.parseBoolean(values[4]));
                    seatSpace.setPrice(Double.parseDouble(values[5].trim()));
                    seatSpace.setRows(Integer.parseInt(values[6].trim()));
                    seatSpace.setSeats(Integer.parseInt(values[7].trim()));
                    SpaceLayout spaceLayout = dg.findSpaceLayoutById(Long.parseLong(values[8].trim()));
                    spaceLayout.getSeatSpaces().add(seatSpace);
                    seatSpace.setSpaceLayout(spaceLayout);
                    seatSpace.setTickets(new HashSet<>());

                    dg.getSeatSpaces().add(seatSpace);
                } else if (line.contains("INSERT INTO ktsnvttest.event ")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout)
                    if (values[0].equals("single_event")) {
                        SingleEvent singleEvent = new SingleEvent();
                        singleEvent.setClassType("single_event");
                        singleEvent.setId(Long.parseLong(values[1].trim()));
                        singleEvent.setDeleted(Boolean.parseBoolean(values[2]));
                        singleEvent.setDescription(values[3]);
                        singleEvent.setName(values[4]);
                        singleEvent.setType(EventType.values()[Integer.parseInt(values[5].trim())]);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        singleEvent.setEndDate(sdf.parse(values[6]));
                        singleEvent.setStartDate(sdf.parse(values[7]));
                        singleEvent.setType(EventType.values()[Integer.parseInt(values[8].trim())]);
                        Space space = dg.findSpaceById(Long.parseLong(values[9].trim()));
                        singleEvent.setSpace(space);
                        SpaceLayout spaceLayout = dg.findSpaceLayoutById(Long.parseLong(values[10].trim()));
                        singleEvent.setSpaceLayout(spaceLayout);
                        spaceLayout.setSingleEvent(singleEvent);
                        spaceLayout.setSpace(null);
                        singleEvent.setTickets(new HashSet<>());

                        dg.getSingleEvents().add(singleEvent);
                        dg.getEvents().add(singleEvent);
                    } else {
                        CompositeEvent compositeEvent = new CompositeEvent();
                        compositeEvent.setClassType("CompositeEvent");
                        compositeEvent.setId(Long.parseLong(values[1].trim()));
                        compositeEvent.setDeleted(Boolean.parseBoolean(values[2]));
                        compositeEvent.setDescription(values[3]);
                        compositeEvent.setName(values[4]);
                        compositeEvent.setType(EventType.values()[Integer.parseInt(values[5].trim())]);
                        compositeEvent.setChildEvents(new HashSet<>());

                        dg.getCompositeEvents().add(compositeEvent);
                        dg.getEvents().add(compositeEvent);
                    }
                } else if (line.contains("INSERT INTO ktsnvttest.event_child_parent")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    CompositeEvent parentEvent = dg.findCompositeEventById(Long.parseLong(values[0].trim()));
                    Event childEvent = dg.findEventById(Long.parseLong(values[1].trim()));
                    parentEvent.getChildEvents().add(childEvent);
                    childEvent.setParentEvent(parentEvent);
                } else if (line.contains("INSERT INTO ktsnvttest.ticket ")) {
                    String[] values = line.split("VALUES ")[1]
                            .replaceAll("\\(", "")
                            .replaceAll("\\);","")
                            .replaceAll("'", "")
                            .split(", ");
                    // (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer)
                    Ticket ticket = new Ticket();
                    ticket.setId(Long.parseLong(values[0].trim()));
                    ticket.setDeleted(Boolean.parseBoolean(values[1]));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    ticket.setExpirationDate(sdf.parse(values[2]));
                    ticket.setPaid(Boolean.parseBoolean(values[3]));
                    ticket.setPrice(Double.parseDouble(values[4].trim()));
                    ticket.setRow(Integer.parseInt(values[5].trim()));
                    ticket.setSeat(Integer.parseInt(values[6].trim()));
                    if (values[7].equals("NULL")) {
                        ticket.setGroundFloor(null);
                    } else {
                        GroundFloor groundFloor = dg.findGroundFloorById(Long.parseLong(values[7].trim()));
                        ticket.setGroundFloor(groundFloor);
                        groundFloor.getTickets().add(ticket);
                    }
                    if (values[8].equals("NULL")) {
                        ticket.setSeatSpace(null);
                    } else {
                        SeatSpace seatSpace = dg.findSeatSpaceById(Long.parseLong(values[8].trim()));
                        ticket.setSeatSpace(seatSpace);
                        seatSpace.getTickets().add(ticket);
                    }
                    SingleEvent singleEvent = dg.findSingleEventById(Long.parseLong(values[9].trim()));
                    ticket.setSingleEvent(singleEvent);
                    singleEvent.getTickets().add(ticket);
                    if (values[10].equals("NULL")) {
                        ticket.setUser(null);
                    } else {
                        User user = dg.findUserById(Long.parseLong(values[10].trim()));
                        ticket.setUser(user);
                        user.getTickets().add(ticket);
                    }

                    dg.getTickets().add(ticket);
                }

                line = br.readLine();
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return dg;
    }

}
