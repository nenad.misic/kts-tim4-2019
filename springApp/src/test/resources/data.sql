
INSERT INTO ktsnvttest.user_authority (id, role) VALUES (654, 'Admin');
INSERT INTO ktsnvttest.user_authority (id, role) VALUES (655, 'Regular');

INSERT INTO ktsnvttest.user (id, deleted, name, password, role, surname, username, email, verified) VALUES (30226, false, 'Admin', '$2a$10$Hl0zLeCt01mzDQsVbgW.auOx0uqYRpYxx0ugtony1/iLFKukxFc/u', 0, 'Admin', 'admin', 'admin@ktsnvttest.com', true);
INSERT INTO ktsnvttest.user_user_authority (user_id, authority_id) VALUES (30226, 654);


INSERT INTO ktsnvttest.user (id, deleted, name, password, role, surname, username, email, verified) VALUES (9944, false, 'Name of user', '$2a$10$Hl0zLeCt01mzDQsVbgW.auOx0uqYRpYxx0ugtony1/iLFKukxFc/u', 1, 'Surname of password', 'username', 'email@mail.com', true);
INSERT INTO ktsnvttest.user_user_authority (user_id, authority_id) VALUES (9944, 655);


INSERT INTO ktsnvttest.user (id, deleted, name, password, role, surname, username, email, verified) VALUES (9945, false, 'Name of user 2', '$2a$10$Hl0zLeCt01mzDQsVbgW.auOx0uqYRpYxx0ugtony1/iLFKukxFc/u', 1, 'Surname of password 2', 'username2', 'email@mail.com', true);
INSERT INTO ktsnvttest.user_user_authority (user_id, authority_id) VALUES (9945, 655);

--Locations: two active with spaces, one active without spaces and one deleted
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (99, 'Spens', 'Maksima Gorkog 22', 'Novi Sad', false, 'Srbija', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (100, 'Marakana', 'Vojvode Stepe 69', 'Beograd', false, 'Srbija', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (101, 'Muzej Crnog Djordja', 'Karadjordjeva 22', 'Ruma', false, 'Srbija', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (102, 'Oronuo obrisani muzej', 'Propala ulica 420', 'Yemen', true, 'Kazahstan', 45.246908, 19.845978);

--Spaces: two for  each location that should have them
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (103, 'Spens sala 1', 99, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (104, 'Spens sala 2', 99, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (107, 'Spens sala 3', 99, false);

INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (105, 'Veliki stadion', 100, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (106, 'Mali stadion', 100, false);

--
-- Data for Name: location; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (9940, 'Beogradska Arena', 'Bulevar Arsenija Carnojevica 58', 'Beograd', false, 'Srbija', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (9941, 'Petrovaradinska Tvrdjava', 'Petrovaradinska Tvrdjava', 'Novi Sad', false, 'Srbija', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (9942, 'Plaza Buljarica', 'Plaza Buljarica BB', 'Budva', false, 'Crna Gora', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (99300, 'New York Arena', 'Near the Empire State Building', 'New York', false, 'USA', 45.246908, 19.845978);
INSERT INTO ktsnvttest.location (id, name, address, city, deleted, country, longitude, latitude) VALUES (99301, 'Somborska Arena', 'Somborska Ulica 3', 'Sombor', false, 'Srbija', 45.246908, 19.845978);


--
-- Data for Name: space; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (991, 'Beogradska arena', 9940, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (992, 'Petrovaradinska tvrdjava', 9941, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (993, 'Plaza Buljarica', 9942, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (99302, 'New York arena', 99300, false);
INSERT INTO ktsnvttest.space (id, name, location, deleted) VALUES (99303, 'Somborska arena', 99301, false);

--
-- Data for Name: space_layout; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (995  , 'space layout 1', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9910 , 'space layout 2', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9913 , 'space layout 3', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9916 , 'space layout 4', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9919 , 'space layout 5', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9922 , 'space layout 6', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9925 , 'space layout 7', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9928 , 'space layout 8', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9931 , 'space layout 9', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (9936 , 'space layout 10', false, NULL, NULL);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (99304, 'space layout 11', false, NULL, 99302);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (99305, 'space layout 12', false, NULL, 99303);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (99306, 'space layout 13', false, NULL, 99303);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (99313, 'space layout 14', false, NULL, 992);
INSERT INTO ktsnvttest.space_layout (id, name, deleted, single_event, space) VALUES (99314, 'space layout 15', false, NULL, 99303);
 --purposefully wont be tied to event


--
-- Data for Name: ground_floor; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (996  , 'a', 2, 2, 16, false, 4, 2000, 8, 4, 995);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9911 , 'a', 2, 2, 16, false, 4, 2000, 8, 4, 9910);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9914 , 'a', 2, 2, 16, false, 4, 2000, 8, 4, 9913);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9917 , 'a', 2, 2, 16, false, 4, 2000, 12, 4, 9916);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9920 , 'a', 2, 2, 16, false, 4, 2000, 8, 4, 9919);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9923 , 'a', 2, 2, 16, false, 4, 2000, 6, 4, 9922);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9926 , 'a', 2, 2, 16, false, 4, 2000, 10, 4, 9925);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9929 , 'a', 2, 2, 16, false, 4, 2000, 7, 4, 9928);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9932 , 'a', 2, 2, 16, false, 4, 2000, 8, 4, 9931);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (9937 , 'a', 2, 2, 16, false, 4, 2000, 16, 4, 9936);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (99307, 'a', 2, 2, 16, false, 4, 2000, 0, 4, 99304);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (99308, 'a', 2, 2, 16, false, 4, 3000, 0, 4, 99305);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (99309, 'a', 2, 2, 16, false, 4, 3000, 0, 4, 99306);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (99314, 'a', 2, 2, 16, false, 4, 2000, 0, 4, 99313);
INSERT INTO ktsnvttest.ground_floor (id, name, top_coordinate, left_coordinate, capacity, deleted, height, price, spots_reserved, width, space_layout) VALUES (99315, 'a', 2, 2, 16, false, 4, 3000, 0, 4, 99314);

--
-- Data for Name: seat_space; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (997, 'a', 10, 10, false, 30000, 3, 3, 995);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (998, 'a', 15, 15, false, 3000, 3, 3, 995);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (9933, 'a', 10, 10, false, 30000, 3, 3, 9931);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (9934, 'a', 15, 15, false, 3000, 3, 3, 9931);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (9938, 'a', 10, 10, false, 3000, 3, 3, 9936);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (9939, 'a', 15, 15, false, 30000, 3, 3, 9936);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (99310, 'a', 10, 10, false, 5000, 3, 3, 99304);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (99311, 'a', 10, 10, false, 3000, 3, 3, 99305);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (99312, 'a', 10, 10, false, 3000, 3, 3, 99306);
INSERT INTO ktsnvttest.seat_space (id, name, top_coordinate, left_coordinate, deleted, price, rows, seats, space_layout) VALUES (99316, 'a', 10, 10, false, 3000, 3, 3, 99314);

--
-- Data for Name: event; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 994, false, 'Gori nego ikad', 'Novi koncert mayo berovica', 2, '2020-11-22 18:00:00', '2020-11-21 18:00:00', 0, 991, 995);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 999, false, 'Prvi dan Exit festivala u Novom Sadu', 'Prvi dan Exit festivala', 2, '2021-01-01 18:00:00', '2020-12-31 18:00:00', 0, 992, 9910);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9912, false, 'Drugi dan Exit festivala u Novom Sadu', 'Drugi dan Exit festivala', 2, '2021-01-02 18:00:00', '2021-01-01 18:00:00', 0, 992, 9913);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9915, false, 'Treci dan Exit festivala u Novom Sadu', 'Treci dan Exit festivala', 2, '2021-01-03 18:00:00', '2021-01-02 18:00:00', 0, 992, 9916);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9918, false, 'Cetvrti dan Exit festivala u Novom Sadu', 'Cetvrti dan Exit festivala', 2, '2021-01-04 18:00:00', '2021-01-03 18:00:00', 0, 992, 9919);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9921, false, 'Prvi dan Sea Dance festivala', 'Prvi dan Sea Dance festivala', 2, '2021-01-06 18:00:00', '2021-01-05 18:00:00', 0, 993, 9922);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9924, false, 'Drugi dan Sea Dance festivala', 'Drugi dan Sea Dance festivala', 2, '2021-01-07 18:00:00', '2021-01-06 18:00:00', 0, 993, 9925);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9927, false, 'Treci dan Sea Dance festivala', 'Treci dan Sea Dance festivala', 2, '2021-01-08 18:00:00', '2021-01-07 18:00:00', 0, 993, 9928);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9930, false, 'Koncert Zvonka Bogdana jedan', 'Koncert Zvonka Bogdana jedan', 2, '2020-11-24 18:00:00', '2020-11-23 18:00:00', 0, 991, 9931);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('single_event', 9935, false, 'Koncert Zvonka Bogdana dva', 'Koncert Zvonka Bogdana dva', 2, '2020-12-04 18:00:00', '2020-12-03 18:00:00', 0, 991, 9936);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('CompositeEvent', 9940, false, 'Exit festival u Novom Sadu', 'Exit festival', 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('CompositeEvent', 9941, false, 'Sea dance festival nedje u Crnoj Gori', 'Sea dance festival', 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('CompositeEvent', 9942, false, 'Exit festival u Novom Sadu i Seadance festival negde u Crnoj Gori', 'Exit and seadance festivals', 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO ktsnvttest.event (event_type, id, deleted, description, name, type, end_date, start_date, state, space, space_layout) VALUES ('CompositeEvent', 9943, false, 'Isti kao i uvek - jedan jedini - Bonko Zvogdan', 'Turneja Bonke Zvogdana', 2, NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: event_child_parent; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9940, 9915);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9940, 9918);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9940, 9912);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9940, 999);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9941, 9921);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9941, 9924);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9941, 9927);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9942, 9941);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9942, 9940);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9943, 9935);
INSERT INTO ktsnvttest.event_child_parent (parent, child) VALUES (9943, 9930);


UPDATE ktsnvttest.space_layout SET single_event = 994  WHERE id=995;
UPDATE ktsnvttest.space_layout SET single_event = 999  WHERE id=9910;
UPDATE ktsnvttest.space_layout SET single_event = 9912 WHERE id=9913;
UPDATE ktsnvttest.space_layout SET single_event = 9915 WHERE id=9916;
UPDATE ktsnvttest.space_layout SET single_event = 9918 WHERE id=9919;
UPDATE ktsnvttest.space_layout SET single_event = 9921 WHERE id=9922;
UPDATE ktsnvttest.space_layout SET single_event = 9924 WHERE id=9925;
UPDATE ktsnvttest.space_layout SET single_event = 9927 WHERE id=9928;
UPDATE ktsnvttest.space_layout SET single_event = 9930 WHERE id=9931;
UPDATE ktsnvttest.space_layout SET single_event = 9935 WHERE id=9936;


--
-- Data for Name: ticket; Type: TABLE DATA; Schema: ktsnvttest; Owner: postgres
--

INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999921, false, '2020-12-30 18:00:00', false, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999922, false, '2020-12-30 18:00:00', true, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999923, false, '2020-12-30 18:00:00', true, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999924, false, '2020-12-30 18:00:00', false, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999925, false, '2020-12-30 18:00:00', true, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999926, false, '2020-12-30 18:00:00', false, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999927, false, '2020-12-30 18:00:00', true, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999928, false, '2020-12-30 18:00:00', false, 2000, 0, 0, 9911, NULL, 999, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (9999929, false, '2020-12-31 18:00:00', true, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999210, false, '2020-12-31 18:00:00', false, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999211, false, '2020-12-31 18:00:00', false, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999212, false, '2020-12-31 18:00:00', true, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999213, false, '2020-12-31 18:00:00', true, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999214, false, '2020-12-31 18:00:00', false, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999215, false, '2020-12-31 18:00:00', false, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999216, false, '2020-12-31 18:00:00', true, 2000, 0, 0, 9914, NULL, 9912, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999217, false, '2021-01-01 18:00:00', true, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999218, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999219, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999220, false, '2021-01-01 18:00:00', true, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999221, false, '2021-01-01 18:00:00', true, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999222, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999223, false, '2021-01-01 18:00:00', true, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999224, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999225, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999226, false, '2021-01-01 18:00:00', true, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999227, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999228, false, '2021-01-01 18:00:00', false, 2000, 0, 0, 9917, NULL, 9915, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999229, false, '2021-01-02 18:00:00', true, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999230, false, '2021-01-02 18:00:00', true, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999231, false, '2021-01-02 18:00:00', true, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999232, false, '2021-01-02 18:00:00', true, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999233, false, '2021-01-02 18:00:00', false, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999234, false, '2021-01-02 18:00:00', false, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999235, false, '2021-01-02 18:00:00', false, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999236, false, '2021-01-02 18:00:00', false, 2000, 0, 0, 9920, NULL, 9918, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999237, false, '2021-01-04 18:00:00', false, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999238, false, '2021-01-04 18:00:00', false, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999239, false, '2021-01-04 18:00:00', false, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999240, false, '2021-01-04 18:00:00', false, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999241, false, '2021-01-04 18:00:00', true, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999242, false, '2021-01-04 18:00:00', false, 2000, 0, 0, 9923, NULL, 9921, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999243, false, '2021-01-05 18:00:00', true, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999244, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999245, false, '2021-01-05 18:00:00', true, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999246, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999247, false, '2021-01-05 18:00:00', true, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999248, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999249, false, '2021-01-05 18:00:00', true, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999250, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999251, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999252, false, '2021-01-05 18:00:00', false, 2000, 0, 0, 9926, NULL, 9924, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999253, false, '2021-01-06 18:00:00', true, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999254, false, '2021-01-06 18:00:00', false, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999255, false, '2021-01-06 18:00:00', true, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999256, false, '2021-01-06 18:00:00', false, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999257, false, '2021-01-06 18:00:00', false, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999258, false, '2021-01-06 18:00:00', false, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999259, false, '2021-01-06 18:00:00', true, 2000, 0, 0, 9929, NULL, 9927, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999260, false, '2020-11-20 18:00:00', true, 3000, 0, 0, NULL, 998, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999261, false, '2020-11-20 18:00:00', false, 3000, 1, 0, NULL, 998, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999262, false, '2020-11-20 18:00:00', false, 3000, 2, 0, NULL, 998, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999263, false, '2020-11-20 18:00:00', true, 3000, 0, 1, NULL, 998, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999264, false, '2020-11-20 18:00:00', false, 3000, 1, 1, NULL, 998, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999265, false, '2020-11-20 18:00:00', true, 3000, 2, 1, NULL, 998, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999266, false, '2020-11-20 18:00:00', false, 3000, 0, 2, NULL, 998, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999267, false, '2020-11-20 18:00:00', true, 3000, 1, 2, NULL, 998, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999268, false, '2020-11-20 18:00:00', false, 3000, 2, 2, NULL, 998, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999269, false, '2020-11-20 18:00:00', true, 30000, 0, 0, NULL, 997, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999270, false, '2020-11-20 18:00:00', true, 30000, 1, 0, NULL, 997, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999271, false, '2020-11-20 18:00:00', true, 30000, 2, 0, NULL, 997, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999272, false, '2020-11-20 18:00:00', false, 30000, 0, 1, NULL, 997, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999273, false, '2020-11-20 18:00:00', false, 30000, 1, 1, NULL, 997, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999274, false, '2020-11-20 18:00:00', true, 30000, 2, 1, NULL, 997, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999275, false, '2020-11-20 18:00:00', false, 30000, 0, 2, NULL, 997, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999276, false, '2020-11-20 18:00:00', true, 30000, 1, 2, NULL, 997, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999277, false, '2020-11-20 18:00:00', true, 30000, 2, 2, NULL, 997, 994, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999278, false, '2020-11-20 18:00:00', true, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999279, false, '2020-11-20 18:00:00', true, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999280, false, '2020-11-20 18:00:00', false, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999281, false, '2020-11-20 18:00:00', false, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999282, false, '2020-11-20 18:00:00', true, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999283, false, '2020-11-20 18:00:00', true, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999284, false, '2020-11-20 18:00:00', true, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999285, false, '2020-11-20 18:00:00', false, 2000, 0, 0, 996, NULL, 994, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999286, false, '2020-11-22 18:00:00', true, 30000, 0, 0, NULL, 9933, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999287, false, '2020-11-22 18:00:00', true, 30000, 1, 0, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999288, false, '2020-11-22 18:00:00', true, 30000, 2, 0, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999289, false, '2020-11-22 18:00:00', false, 30000, 0, 1, NULL, 9933, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999290, false, '2020-11-22 18:00:00', false, 30000, 1, 1, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999291, false, '2020-11-22 18:00:00', false, 30000, 2, 1, NULL, 9933, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999292, false, '2020-11-22 18:00:00', false, 30000, 0, 2, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999293, false, '2020-11-22 18:00:00', true, 30000, 1, 2, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999294, false, '2020-11-22 18:00:00', true, 30000, 2, 2, NULL, 9933, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999295, false, '2020-11-22 18:00:00', false, 3000, 0, 0, NULL, 9934, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999296, false, '2020-11-22 18:00:00', false, 3000, 1, 0, NULL, 9934, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999297, false, '2020-11-22 18:00:00', false, 3000, 2, 0, NULL, 9934, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999298, false, '2020-11-22 18:00:00', true, 3000, 0, 1, NULL, 9934, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (99999299, false, '2020-11-22 18:00:00', false, 3000, 1, 1, NULL, 9934, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992100, false, '2020-11-22 18:00:00', true, 3000, 2, 1, NULL, 9934, 9930, NULL);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992101, false, '2020-11-22 18:00:00', true, 3000, 0, 2, NULL, 9934, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992102, false, '2020-11-22 18:00:00', true, 3000, 1, 2, NULL, 9934, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992103, false, '2020-11-22 18:00:00', true, 3000, 2, 2, NULL, 9934, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992104, false, '2020-11-22 18:00:00', true, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992105, false, '2020-11-22 18:00:00', true, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992106, false, '2020-11-22 18:00:00', false, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992107, false, '2020-11-22 18:00:00', true, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992108, false, '2020-11-22 18:00:00', false, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992109, false, '2020-11-22 18:00:00', false, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992110, false, '2020-11-22 18:00:00', false, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992111, false, '2020-11-22 18:00:00', false, 2000, 0, 0, 9932, NULL, 9930, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992112, false, '2020-12-02 18:00:00', true, 30000, 0, 0, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992113, false, '2020-12-02 18:00:00', true, 30000, 1, 0, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992114, false, '2020-12-02 18:00:00', true, 30000, 2, 0, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992115, false, '2020-12-02 18:00:00', true, 30000, 0, 1, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992116, false, '2020-12-02 18:00:00', false, 30000, 1, 1, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992117, false, '2020-12-02 18:00:00', true, 30000, 2, 1, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992118, false, '2020-12-02 18:00:00', true, 30000, 0, 2, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992119, false, '2020-12-02 18:00:00', false, 30000, 1, 2, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992120, false, '2020-12-02 18:00:00', true, 30000, 2, 2, NULL, 9939, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992121, false, '2020-12-02 18:00:00', true, 3000, 0, 0, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992122, false, '2020-12-02 18:00:00', true, 3000, 1, 0, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992123, false, '2020-12-02 18:00:00', true, 3000, 2, 0, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992124, false, '2020-12-02 18:00:00', true, 3000, 0, 1, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992125, false, '2020-12-02 18:00:00', true, 3000, 1, 1, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992126, false, '2020-12-02 18:00:00', true, 3000, 2, 1, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992127, false, '2020-12-02 18:00:00', true, 3000, 0, 2, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992128, false, '2020-12-02 18:00:00', true, 3000, 1, 2, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992129, false, '2020-12-02 18:00:00', true, 3000, 2, 2, NULL, 9938, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992130, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992131, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992132, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992133, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992134, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992135, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992136, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992137, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992138, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992139, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992140, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992141, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992142, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992143, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992144, false, '2020-12-02 18:00:00', true, 2000, 0, 0, 9937, NULL, 9935, 9944);
INSERT INTO ktsnvttest.ticket (id, deleted, expiration_date, paid, price, seat_row, seat, ground_floor, seat_space, single_event, customer) VALUES (999992145, false, '2020-12-02 18:00:00', false, 2000, 0, 0, 9937, NULL, 9935, 9944);
